package ngapConvert

import (
	"encoding/hex"

	"ngap/ngapCodec"
	"ngap/ngapType"
)

func AmfIdToNgap(amfId string) (regionId, setId, ptrId ngapType.BitString) {
	regionId = HexToBitString(amfId[:2], 8)
	setId = HexToBitString(amfId[2:5], 10)
	tmpByte, err := hex.DecodeString(amfId[4:])
	if err != nil {
		return
	}
	shiftByte, err := ngapCodec.GetBitString(tmpByte, 2, 6)
	if err != nil {
		return
	}
	ptrId.BitLength = 6
	ptrId.Bytes = shiftByte
	return
}

func AmfIdToModels(regionId, setId, ptrId ngapType.BitString) (amfId string) {
	regionHex := BitStringToHex(&regionId)
	tmpByte := []byte{setId.Bytes[0], (setId.Bytes[1] & 0xc0) | (ptrId.Bytes[0] >> 2)}
	restHex := hex.EncodeToString(tmpByte)
	amfId = regionHex + restHex
	return
}

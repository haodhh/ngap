package ngapBuild

import (
	"ngap/ngapType"
)

func BuildPDUSessionResourceSetupRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePDUSessionResourceSetup
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentPDUSessionResourceSetupRequest
	pduMsg.Value.PDUSessionResourceSetupRequest = new(ngapType.PDUSessionResourceSetupRequest)
	pduContent := pduMsg.Value.PDUSessionResourceSetupRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PDUSessionResourceSetupRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.PDUSessionResourceSetupRequestIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PDUSessionResourceSetupRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.PDUSessionResourceSetupRequestIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PDUSessionResourceSetupRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRANPagingPriority
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PDUSessionResourceSetupRequestIEsPresentRANPagingPriority
	ie3.Value.RANPagingPriority = new(ngapType.RANPagingPriority)
	*ie3.Value.RANPagingPriority = BuildRANPagingPriority()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PDUSessionResourceSetupRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDNASPDU
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.PDUSessionResourceSetupRequestIEsPresentNASPDU
	ie4.Value.NASPDU = new(ngapType.NASPDU)
	*ie4.Value.NASPDU = BuildNASPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.PDUSessionResourceSetupRequestIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceSetupListSUReq
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.PDUSessionResourceSetupRequestIEsPresentPDUSessionResourceSetupListSUReq
	ie5.Value.PDUSessionResourceSetupListSUReq = new(ngapType.PDUSessionResourceSetupListSUReq)
	*ie5.Value.PDUSessionResourceSetupListSUReq = BuildPDUSessionResourceSetupListSUReq()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.PDUSessionResourceSetupRequestIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDUEAggregateMaximumBitRate
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.PDUSessionResourceSetupRequestIEsPresentUEAggregateMaximumBitRate
	ie6.Value.UEAggregateMaximumBitRate = new(ngapType.UEAggregateMaximumBitRate)
	*ie6.Value.UEAggregateMaximumBitRate = BuildUEAggregateMaximumBitRate()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	return
}

func BuildPDUSessionResourceReleaseCommand() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePDUSessionResourceRelease
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentPDUSessionResourceReleaseCommand
	pduMsg.Value.PDUSessionResourceReleaseCommand = new(ngapType.PDUSessionResourceReleaseCommand)
	pduContent := pduMsg.Value.PDUSessionResourceReleaseCommand
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PDUSessionResourceReleaseCommandIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.PDUSessionResourceReleaseCommandIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PDUSessionResourceReleaseCommandIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.PDUSessionResourceReleaseCommandIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PDUSessionResourceReleaseCommandIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRANPagingPriority
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PDUSessionResourceReleaseCommandIEsPresentRANPagingPriority
	ie3.Value.RANPagingPriority = new(ngapType.RANPagingPriority)
	*ie3.Value.RANPagingPriority = BuildRANPagingPriority()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PDUSessionResourceReleaseCommandIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDNASPDU
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.PDUSessionResourceReleaseCommandIEsPresentNASPDU
	ie4.Value.NASPDU = new(ngapType.NASPDU)
	*ie4.Value.NASPDU = BuildNASPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.PDUSessionResourceReleaseCommandIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceToReleaseListRelCmd
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.PDUSessionResourceReleaseCommandIEsPresentPDUSessionResourceToReleaseListRelCmd
	ie5.Value.PDUSessionResourceToReleaseListRelCmd = new(ngapType.PDUSessionResourceToReleaseListRelCmd)
	*ie5.Value.PDUSessionResourceToReleaseListRelCmd = BuildPDUSessionResourceToReleaseListRelCmd()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildPDUSessionResourceModifyRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePDUSessionResourceModify
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentPDUSessionResourceModifyRequest
	pduMsg.Value.PDUSessionResourceModifyRequest = new(ngapType.PDUSessionResourceModifyRequest)
	pduContent := pduMsg.Value.PDUSessionResourceModifyRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PDUSessionResourceModifyRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.PDUSessionResourceModifyRequestIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PDUSessionResourceModifyRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.PDUSessionResourceModifyRequestIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PDUSessionResourceModifyRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRANPagingPriority
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PDUSessionResourceModifyRequestIEsPresentRANPagingPriority
	ie3.Value.RANPagingPriority = new(ngapType.RANPagingPriority)
	*ie3.Value.RANPagingPriority = BuildRANPagingPriority()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PDUSessionResourceModifyRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceModifyListModReq
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.PDUSessionResourceModifyRequestIEsPresentPDUSessionResourceModifyListModReq
	ie4.Value.PDUSessionResourceModifyListModReq = new(ngapType.PDUSessionResourceModifyListModReq)
	*ie4.Value.PDUSessionResourceModifyListModReq = BuildPDUSessionResourceModifyListModReq()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildPDUSessionResourceNotify() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePDUSessionResourceNotify
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentPDUSessionResourceNotify
	pduMsg.Value.PDUSessionResourceNotify = new(ngapType.PDUSessionResourceNotify)
	pduContent := pduMsg.Value.PDUSessionResourceNotify
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PDUSessionResourceNotifyIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.PDUSessionResourceNotifyIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PDUSessionResourceNotifyIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.PDUSessionResourceNotifyIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PDUSessionResourceNotifyIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceNotifyList
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.PDUSessionResourceNotifyIEsPresentPDUSessionResourceNotifyList
	ie3.Value.PDUSessionResourceNotifyList = new(ngapType.PDUSessionResourceNotifyList)
	*ie3.Value.PDUSessionResourceNotifyList = BuildPDUSessionResourceNotifyList()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PDUSessionResourceNotifyIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceReleasedListNot
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.PDUSessionResourceNotifyIEsPresentPDUSessionResourceReleasedListNot
	ie4.Value.PDUSessionResourceReleasedListNot = new(ngapType.PDUSessionResourceReleasedListNot)
	*ie4.Value.PDUSessionResourceReleasedListNot = BuildPDUSessionResourceReleasedListNot()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.PDUSessionResourceNotifyIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.PDUSessionResourceNotifyIEsPresentUserLocationInformation
	ie5.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie5.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildPDUSessionResourceModifyIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePDUSessionResourceModifyIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentPDUSessionResourceModifyIndication
	pduMsg.Value.PDUSessionResourceModifyIndication = new(ngapType.PDUSessionResourceModifyIndication)
	pduContent := pduMsg.Value.PDUSessionResourceModifyIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PDUSessionResourceModifyIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.PDUSessionResourceModifyIndicationIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PDUSessionResourceModifyIndicationIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.PDUSessionResourceModifyIndicationIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PDUSessionResourceModifyIndicationIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceModifyListModInd
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.PDUSessionResourceModifyIndicationIEsPresentPDUSessionResourceModifyListModInd
	ie3.Value.PDUSessionResourceModifyListModInd = new(ngapType.PDUSessionResourceModifyListModInd)
	*ie3.Value.PDUSessionResourceModifyListModInd = BuildPDUSessionResourceModifyListModInd()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PDUSessionResourceModifyIndicationIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.PDUSessionResourceModifyIndicationIEsPresentUserLocationInformation
	ie4.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie4.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildInitialContextSetupRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeInitialContextSetup
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentInitialContextSetupRequest
	pduMsg.Value.InitialContextSetupRequest = new(ngapType.InitialContextSetupRequest)
	pduContent := pduMsg.Value.InitialContextSetupRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.InitialContextSetupRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.InitialContextSetupRequestIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.InitialContextSetupRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.InitialContextSetupRequestIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.InitialContextSetupRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDOldAMF
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.InitialContextSetupRequestIEsPresentOldAMF
	ie3.Value.OldAMF = new(ngapType.AMFName)
	*ie3.Value.OldAMF = BuildAMFName()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.InitialContextSetupRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDUEAggregateMaximumBitRate
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.InitialContextSetupRequestIEsPresentUEAggregateMaximumBitRate
	ie4.Value.UEAggregateMaximumBitRate = new(ngapType.UEAggregateMaximumBitRate)
	*ie4.Value.UEAggregateMaximumBitRate = BuildUEAggregateMaximumBitRate()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.InitialContextSetupRequestIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDCoreNetworkAssistanceInformationForInactive
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.InitialContextSetupRequestIEsPresentCoreNetworkAssistanceInformationForInactive
	ie5.Value.CoreNetworkAssistanceInformationForInactive = new(ngapType.CoreNetworkAssistanceInformationForInactive)
	*ie5.Value.CoreNetworkAssistanceInformationForInactive = BuildCoreNetworkAssistanceInformationForInactive()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.InitialContextSetupRequestIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDGUAMI
	ie6.Criticality.Value = ngapType.CriticalityPresentReject
	ie6.Value.Present = ngapType.InitialContextSetupRequestIEsPresentGUAMI
	ie6.Value.GUAMI = new(ngapType.GUAMI)
	*ie6.Value.GUAMI = BuildGUAMI()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.InitialContextSetupRequestIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceSetupListCxtReq
	ie7.Criticality.Value = ngapType.CriticalityPresentReject
	ie7.Value.Present = ngapType.InitialContextSetupRequestIEsPresentPDUSessionResourceSetupListCxtReq
	ie7.Value.PDUSessionResourceSetupListCxtReq = new(ngapType.PDUSessionResourceSetupListCxtReq)
	*ie7.Value.PDUSessionResourceSetupListCxtReq = BuildPDUSessionResourceSetupListCxtReq()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.InitialContextSetupRequestIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDAllowedNSSAI
	ie8.Criticality.Value = ngapType.CriticalityPresentReject
	ie8.Value.Present = ngapType.InitialContextSetupRequestIEsPresentAllowedNSSAI
	ie8.Value.AllowedNSSAI = new(ngapType.AllowedNSSAI)
	*ie8.Value.AllowedNSSAI = BuildAllowedNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	ie9 := ngapType.InitialContextSetupRequestIEs{}
	ie9.Id.Value = ngapType.ProtocolIEIDUESecurityCapabilities
	ie9.Criticality.Value = ngapType.CriticalityPresentReject
	ie9.Value.Present = ngapType.InitialContextSetupRequestIEsPresentUESecurityCapabilities
	ie9.Value.UESecurityCapabilities = new(ngapType.UESecurityCapabilities)
	*ie9.Value.UESecurityCapabilities = BuildUESecurityCapabilities()

	pduContentIEs.List = append(pduContentIEs.List, ie9)

	ie10 := ngapType.InitialContextSetupRequestIEs{}
	ie10.Id.Value = ngapType.ProtocolIEIDSecurityKey
	ie10.Criticality.Value = ngapType.CriticalityPresentReject
	ie10.Value.Present = ngapType.InitialContextSetupRequestIEsPresentSecurityKey
	ie10.Value.SecurityKey = new(ngapType.SecurityKey)
	*ie10.Value.SecurityKey = BuildSecurityKey()

	pduContentIEs.List = append(pduContentIEs.List, ie10)

	ie11 := ngapType.InitialContextSetupRequestIEs{}
	ie11.Id.Value = ngapType.ProtocolIEIDTraceActivation
	ie11.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie11.Value.Present = ngapType.InitialContextSetupRequestIEsPresentTraceActivation
	ie11.Value.TraceActivation = new(ngapType.TraceActivation)
	*ie11.Value.TraceActivation = BuildTraceActivation()

	pduContentIEs.List = append(pduContentIEs.List, ie11)

	ie12 := ngapType.InitialContextSetupRequestIEs{}
	ie12.Id.Value = ngapType.ProtocolIEIDMobilityRestrictionList
	ie12.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie12.Value.Present = ngapType.InitialContextSetupRequestIEsPresentMobilityRestrictionList
	ie12.Value.MobilityRestrictionList = new(ngapType.MobilityRestrictionList)
	*ie12.Value.MobilityRestrictionList = BuildMobilityRestrictionList()

	pduContentIEs.List = append(pduContentIEs.List, ie12)

	ie13 := ngapType.InitialContextSetupRequestIEs{}
	ie13.Id.Value = ngapType.ProtocolIEIDUERadioCapability
	ie13.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie13.Value.Present = ngapType.InitialContextSetupRequestIEsPresentUERadioCapability
	ie13.Value.UERadioCapability = new(ngapType.UERadioCapability)
	*ie13.Value.UERadioCapability = BuildUERadioCapability()

	pduContentIEs.List = append(pduContentIEs.List, ie13)

	ie14 := ngapType.InitialContextSetupRequestIEs{}
	ie14.Id.Value = ngapType.ProtocolIEIDIndexToRFSP
	ie14.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie14.Value.Present = ngapType.InitialContextSetupRequestIEsPresentIndexToRFSP
	ie14.Value.IndexToRFSP = new(ngapType.IndexToRFSP)
	*ie14.Value.IndexToRFSP = BuildIndexToRFSP()

	pduContentIEs.List = append(pduContentIEs.List, ie14)

	ie15 := ngapType.InitialContextSetupRequestIEs{}
	ie15.Id.Value = ngapType.ProtocolIEIDMaskedIMEISV
	ie15.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie15.Value.Present = ngapType.InitialContextSetupRequestIEsPresentMaskedIMEISV
	ie15.Value.MaskedIMEISV = new(ngapType.MaskedIMEISV)
	*ie15.Value.MaskedIMEISV = BuildMaskedIMEISV()

	pduContentIEs.List = append(pduContentIEs.List, ie15)

	ie16 := ngapType.InitialContextSetupRequestIEs{}
	ie16.Id.Value = ngapType.ProtocolIEIDNASPDU
	ie16.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie16.Value.Present = ngapType.InitialContextSetupRequestIEsPresentNASPDU
	ie16.Value.NASPDU = new(ngapType.NASPDU)
	*ie16.Value.NASPDU = BuildNASPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie16)

	ie17 := ngapType.InitialContextSetupRequestIEs{}
	ie17.Id.Value = ngapType.ProtocolIEIDEmergencyFallbackIndicator
	ie17.Criticality.Value = ngapType.CriticalityPresentReject
	ie17.Value.Present = ngapType.InitialContextSetupRequestIEsPresentEmergencyFallbackIndicator
	ie17.Value.EmergencyFallbackIndicator = new(ngapType.EmergencyFallbackIndicator)
	*ie17.Value.EmergencyFallbackIndicator = BuildEmergencyFallbackIndicator()

	pduContentIEs.List = append(pduContentIEs.List, ie17)

	ie18 := ngapType.InitialContextSetupRequestIEs{}
	ie18.Id.Value = ngapType.ProtocolIEIDRRCInactiveTransitionReportRequest
	ie18.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie18.Value.Present = ngapType.InitialContextSetupRequestIEsPresentRRCInactiveTransitionReportRequest
	ie18.Value.RRCInactiveTransitionReportRequest = new(ngapType.RRCInactiveTransitionReportRequest)
	*ie18.Value.RRCInactiveTransitionReportRequest = BuildRRCInactiveTransitionReportRequest()

	pduContentIEs.List = append(pduContentIEs.List, ie18)

	ie19 := ngapType.InitialContextSetupRequestIEs{}
	ie19.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityForPaging
	ie19.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie19.Value.Present = ngapType.InitialContextSetupRequestIEsPresentUERadioCapabilityForPaging
	ie19.Value.UERadioCapabilityForPaging = new(ngapType.UERadioCapabilityForPaging)
	*ie19.Value.UERadioCapabilityForPaging = BuildUERadioCapabilityForPaging()

	pduContentIEs.List = append(pduContentIEs.List, ie19)

	ie20 := ngapType.InitialContextSetupRequestIEs{}
	ie20.Id.Value = ngapType.ProtocolIEIDRedirectionVoiceFallback
	ie20.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie20.Value.Present = ngapType.InitialContextSetupRequestIEsPresentRedirectionVoiceFallback
	ie20.Value.RedirectionVoiceFallback = new(ngapType.RedirectionVoiceFallback)
	*ie20.Value.RedirectionVoiceFallback = BuildRedirectionVoiceFallback()

	pduContentIEs.List = append(pduContentIEs.List, ie20)

	ie21 := ngapType.InitialContextSetupRequestIEs{}
	ie21.Id.Value = ngapType.ProtocolIEIDLocationReportingRequestType
	ie21.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie21.Value.Present = ngapType.InitialContextSetupRequestIEsPresentLocationReportingRequestType
	ie21.Value.LocationReportingRequestType = new(ngapType.LocationReportingRequestType)
	*ie21.Value.LocationReportingRequestType = BuildLocationReportingRequestType()

	pduContentIEs.List = append(pduContentIEs.List, ie21)

	ie22 := ngapType.InitialContextSetupRequestIEs{}
	ie22.Id.Value = ngapType.ProtocolIEIDCNAssistedRANTuning
	ie22.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie22.Value.Present = ngapType.InitialContextSetupRequestIEsPresentCNAssistedRANTuning
	ie22.Value.CNAssistedRANTuning = new(ngapType.CNAssistedRANTuning)
	*ie22.Value.CNAssistedRANTuning = BuildCNAssistedRANTuning()

	pduContentIEs.List = append(pduContentIEs.List, ie22)

	ie23 := ngapType.InitialContextSetupRequestIEs{}
	ie23.Id.Value = ngapType.ProtocolIEIDSRVCCOperationPossible
	ie23.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie23.Value.Present = ngapType.InitialContextSetupRequestIEsPresentSRVCCOperationPossible
	ie23.Value.SRVCCOperationPossible = new(ngapType.SRVCCOperationPossible)
	*ie23.Value.SRVCCOperationPossible = BuildSRVCCOperationPossible()

	pduContentIEs.List = append(pduContentIEs.List, ie23)

	ie24 := ngapType.InitialContextSetupRequestIEs{}
	ie24.Id.Value = ngapType.ProtocolIEIDIABAuthorized
	ie24.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie24.Value.Present = ngapType.InitialContextSetupRequestIEsPresentIABAuthorized
	ie24.Value.IABAuthorized = new(ngapType.IABAuthorized)
	*ie24.Value.IABAuthorized = BuildIABAuthorized()

	pduContentIEs.List = append(pduContentIEs.List, ie24)

	ie25 := ngapType.InitialContextSetupRequestIEs{}
	ie25.Id.Value = ngapType.ProtocolIEIDEnhancedCoverageRestriction
	ie25.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie25.Value.Present = ngapType.InitialContextSetupRequestIEsPresentEnhancedCoverageRestriction
	ie25.Value.EnhancedCoverageRestriction = new(ngapType.EnhancedCoverageRestriction)
	*ie25.Value.EnhancedCoverageRestriction = BuildEnhancedCoverageRestriction()

	pduContentIEs.List = append(pduContentIEs.List, ie25)

	ie26 := ngapType.InitialContextSetupRequestIEs{}
	ie26.Id.Value = ngapType.ProtocolIEIDExtendedConnectedTime
	ie26.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie26.Value.Present = ngapType.InitialContextSetupRequestIEsPresentExtendedConnectedTime
	ie26.Value.ExtendedConnectedTime = new(ngapType.ExtendedConnectedTime)
	*ie26.Value.ExtendedConnectedTime = BuildExtendedConnectedTime()

	pduContentIEs.List = append(pduContentIEs.List, ie26)

	ie27 := ngapType.InitialContextSetupRequestIEs{}
	ie27.Id.Value = ngapType.ProtocolIEIDUEDifferentiationInfo
	ie27.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie27.Value.Present = ngapType.InitialContextSetupRequestIEsPresentUEDifferentiationInfo
	ie27.Value.UEDifferentiationInfo = new(ngapType.UEDifferentiationInfo)
	*ie27.Value.UEDifferentiationInfo = BuildUEDifferentiationInfo()

	pduContentIEs.List = append(pduContentIEs.List, ie27)

	ie28 := ngapType.InitialContextSetupRequestIEs{}
	ie28.Id.Value = ngapType.ProtocolIEIDNRV2XServicesAuthorized
	ie28.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie28.Value.Present = ngapType.InitialContextSetupRequestIEsPresentNRV2XServicesAuthorized
	ie28.Value.NRV2XServicesAuthorized = new(ngapType.NRV2XServicesAuthorized)
	*ie28.Value.NRV2XServicesAuthorized = BuildNRV2XServicesAuthorized()

	pduContentIEs.List = append(pduContentIEs.List, ie28)

	ie29 := ngapType.InitialContextSetupRequestIEs{}
	ie29.Id.Value = ngapType.ProtocolIEIDLTEV2XServicesAuthorized
	ie29.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie29.Value.Present = ngapType.InitialContextSetupRequestIEsPresentLTEV2XServicesAuthorized
	ie29.Value.LTEV2XServicesAuthorized = new(ngapType.LTEV2XServicesAuthorized)
	*ie29.Value.LTEV2XServicesAuthorized = BuildLTEV2XServicesAuthorized()

	pduContentIEs.List = append(pduContentIEs.List, ie29)

	ie30 := ngapType.InitialContextSetupRequestIEs{}
	ie30.Id.Value = ngapType.ProtocolIEIDNRUESidelinkAggregateMaximumBitrate
	ie30.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie30.Value.Present = ngapType.InitialContextSetupRequestIEsPresentNRUESidelinkAggregateMaximumBitrate
	ie30.Value.NRUESidelinkAggregateMaximumBitrate = new(ngapType.NRUESidelinkAggregateMaximumBitrate)
	*ie30.Value.NRUESidelinkAggregateMaximumBitrate = BuildNRUESidelinkAggregateMaximumBitrate()

	pduContentIEs.List = append(pduContentIEs.List, ie30)

	ie31 := ngapType.InitialContextSetupRequestIEs{}
	ie31.Id.Value = ngapType.ProtocolIEIDLTEUESidelinkAggregateMaximumBitrate
	ie31.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie31.Value.Present = ngapType.InitialContextSetupRequestIEsPresentLTEUESidelinkAggregateMaximumBitrate
	ie31.Value.LTEUESidelinkAggregateMaximumBitrate = new(ngapType.LTEUESidelinkAggregateMaximumBitrate)
	*ie31.Value.LTEUESidelinkAggregateMaximumBitrate = BuildLTEUESidelinkAggregateMaximumBitrate()

	pduContentIEs.List = append(pduContentIEs.List, ie31)

	ie32 := ngapType.InitialContextSetupRequestIEs{}
	ie32.Id.Value = ngapType.ProtocolIEIDPC5QoSParameters
	ie32.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie32.Value.Present = ngapType.InitialContextSetupRequestIEsPresentPC5QoSParameters
	ie32.Value.PC5QoSParameters = new(ngapType.PC5QoSParameters)
	*ie32.Value.PC5QoSParameters = BuildPC5QoSParameters()

	pduContentIEs.List = append(pduContentIEs.List, ie32)

	ie33 := ngapType.InitialContextSetupRequestIEs{}
	ie33.Id.Value = ngapType.ProtocolIEIDCEmodeBrestricted
	ie33.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie33.Value.Present = ngapType.InitialContextSetupRequestIEsPresentCEmodeBrestricted
	ie33.Value.CEmodeBrestricted = new(ngapType.CEmodeBrestricted)
	*ie33.Value.CEmodeBrestricted = BuildCEmodeBrestricted()

	pduContentIEs.List = append(pduContentIEs.List, ie33)

	ie34 := ngapType.InitialContextSetupRequestIEs{}
	ie34.Id.Value = ngapType.ProtocolIEIDUEUPCIoTSupport
	ie34.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie34.Value.Present = ngapType.InitialContextSetupRequestIEsPresentUEUPCIoTSupport
	ie34.Value.UEUPCIoTSupport = new(ngapType.UEUPCIoTSupport)
	*ie34.Value.UEUPCIoTSupport = BuildUEUPCIoTSupport()

	pduContentIEs.List = append(pduContentIEs.List, ie34)

	ie35 := ngapType.InitialContextSetupRequestIEs{}
	ie35.Id.Value = ngapType.ProtocolIEIDRGLevelWirelineAccessCharacteristics
	ie35.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie35.Value.Present = ngapType.InitialContextSetupRequestIEsPresentRGLevelWirelineAccessCharacteristics
	ie35.Value.RGLevelWirelineAccessCharacteristics = new(ngapType.RGLevelWirelineAccessCharacteristics)
	*ie35.Value.RGLevelWirelineAccessCharacteristics = BuildRGLevelWirelineAccessCharacteristics()

	pduContentIEs.List = append(pduContentIEs.List, ie35)

	ie36 := ngapType.InitialContextSetupRequestIEs{}
	ie36.Id.Value = ngapType.ProtocolIEIDManagementBasedMDTPLMNList
	ie36.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie36.Value.Present = ngapType.InitialContextSetupRequestIEsPresentManagementBasedMDTPLMNList
	ie36.Value.ManagementBasedMDTPLMNList = new(ngapType.MDTPLMNList)
	*ie36.Value.ManagementBasedMDTPLMNList = BuildMDTPLMNList()

	pduContentIEs.List = append(pduContentIEs.List, ie36)

	ie37 := ngapType.InitialContextSetupRequestIEs{}
	ie37.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityID
	ie37.Criticality.Value = ngapType.CriticalityPresentReject
	ie37.Value.Present = ngapType.InitialContextSetupRequestIEsPresentUERadioCapabilityID
	ie37.Value.UERadioCapabilityID = new(ngapType.UERadioCapabilityID)
	*ie37.Value.UERadioCapabilityID = BuildUERadioCapabilityID()

	pduContentIEs.List = append(pduContentIEs.List, ie37)

	return
}

func BuildUEContextReleaseRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextReleaseRequest
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUEContextReleaseRequest
	pduMsg.Value.UEContextReleaseRequest = new(ngapType.UEContextReleaseRequest)
	pduContent := pduMsg.Value.UEContextReleaseRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextReleaseRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UEContextReleaseRequestIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextReleaseRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UEContextReleaseRequestIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEContextReleaseRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceListCxtRelReq
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.UEContextReleaseRequestIEsPresentPDUSessionResourceListCxtRelReq
	ie3.Value.PDUSessionResourceListCxtRelReq = new(ngapType.PDUSessionResourceListCxtRelReq)
	*ie3.Value.PDUSessionResourceListCxtRelReq = BuildPDUSessionResourceListCxtRelReq()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEContextReleaseRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCause
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UEContextReleaseRequestIEsPresentCause
	ie4.Value.Cause = new(ngapType.Cause)
	*ie4.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildUEContextReleaseCommand() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextRelease
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUEContextReleaseCommand
	pduMsg.Value.UEContextReleaseCommand = new(ngapType.UEContextReleaseCommand)
	pduContent := pduMsg.Value.UEContextReleaseCommand
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextReleaseCommandIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDUENGAPIDs
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UEContextReleaseCommandIEsPresentUENGAPIDs
	ie1.Value.UENGAPIDs = new(ngapType.UENGAPIDs)
	*ie1.Value.UENGAPIDs = BuildUENGAPIDs()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextReleaseCommandIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDCause
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UEContextReleaseCommandIEsPresentCause
	ie2.Value.Cause = new(ngapType.Cause)
	*ie2.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	return
}

func BuildUEContextResumeRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextResume
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUEContextResumeRequest
	pduMsg.Value.UEContextResumeRequest = new(ngapType.UEContextResumeRequest)
	pduContent := pduMsg.Value.UEContextResumeRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextResumeRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UEContextResumeRequestIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextResumeRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UEContextResumeRequestIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEContextResumeRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRRCResumeCause
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UEContextResumeRequestIEsPresentRRCResumeCause
	ie3.Value.RRCResumeCause = new(ngapType.RRCEstablishmentCause)
	*ie3.Value.RRCResumeCause = BuildRRCEstablishmentCause()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEContextResumeRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceResumeListRESReq
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.UEContextResumeRequestIEsPresentPDUSessionResourceResumeListRESReq
	ie4.Value.PDUSessionResourceResumeListRESReq = new(ngapType.PDUSessionResourceResumeListRESReq)
	*ie4.Value.PDUSessionResourceResumeListRESReq = BuildPDUSessionResourceResumeListRESReq()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.UEContextResumeRequestIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceFailedToResumeListRESReq
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.UEContextResumeRequestIEsPresentPDUSessionResourceFailedToResumeListRESReq
	ie5.Value.PDUSessionResourceFailedToResumeListRESReq = new(ngapType.PDUSessionResourceFailedToResumeListRESReq)
	*ie5.Value.PDUSessionResourceFailedToResumeListRESReq = BuildPDUSessionResourceFailedToResumeListRESReq()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.UEContextResumeRequestIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDSuspendRequestIndication
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.UEContextResumeRequestIEsPresentSuspendRequestIndication
	ie6.Value.SuspendRequestIndication = new(ngapType.SuspendRequestIndication)
	*ie6.Value.SuspendRequestIndication = BuildSuspendRequestIndication()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	return
}

func BuildUEContextSuspendRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextSuspend
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUEContextSuspendRequest
	pduMsg.Value.UEContextSuspendRequest = new(ngapType.UEContextSuspendRequest)
	pduContent := pduMsg.Value.UEContextSuspendRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextSuspendRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UEContextSuspendRequestIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextSuspendRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UEContextSuspendRequestIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEContextSuspendRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDInfoOnRecommendedCellsAndRANNodesForPaging
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UEContextSuspendRequestIEsPresentInfoOnRecommendedCellsAndRANNodesForPaging
	ie3.Value.InfoOnRecommendedCellsAndRANNodesForPaging = new(ngapType.InfoOnRecommendedCellsAndRANNodesForPaging)
	*ie3.Value.InfoOnRecommendedCellsAndRANNodesForPaging = BuildInfoOnRecommendedCellsAndRANNodesForPaging()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEContextSuspendRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPagingAssisDataforCEcapabUE
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UEContextSuspendRequestIEsPresentPagingAssisDataforCEcapabUE
	ie4.Value.PagingAssisDataforCEcapabUE = new(ngapType.PagingAssisDataforCEcapabUE)
	*ie4.Value.PagingAssisDataforCEcapabUE = BuildPagingAssisDataforCEcapabUE()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.UEContextSuspendRequestIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceSuspendListSUSReq
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.UEContextSuspendRequestIEsPresentPDUSessionResourceSuspendListSUSReq
	ie5.Value.PDUSessionResourceSuspendListSUSReq = new(ngapType.PDUSessionResourceSuspendListSUSReq)
	*ie5.Value.PDUSessionResourceSuspendListSUSReq = BuildPDUSessionResourceSuspendListSUSReq()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildUEContextModificationRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextModification
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUEContextModificationRequest
	pduMsg.Value.UEContextModificationRequest = new(ngapType.UEContextModificationRequest)
	pduContent := pduMsg.Value.UEContextModificationRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextModificationRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UEContextModificationRequestIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextModificationRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UEContextModificationRequestIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEContextModificationRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRANPagingPriority
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UEContextModificationRequestIEsPresentRANPagingPriority
	ie3.Value.RANPagingPriority = new(ngapType.RANPagingPriority)
	*ie3.Value.RANPagingPriority = BuildRANPagingPriority()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEContextModificationRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDSecurityKey
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.UEContextModificationRequestIEsPresentSecurityKey
	ie4.Value.SecurityKey = new(ngapType.SecurityKey)
	*ie4.Value.SecurityKey = BuildSecurityKey()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.UEContextModificationRequestIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDIndexToRFSP
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.UEContextModificationRequestIEsPresentIndexToRFSP
	ie5.Value.IndexToRFSP = new(ngapType.IndexToRFSP)
	*ie5.Value.IndexToRFSP = BuildIndexToRFSP()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.UEContextModificationRequestIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDUEAggregateMaximumBitRate
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.UEContextModificationRequestIEsPresentUEAggregateMaximumBitRate
	ie6.Value.UEAggregateMaximumBitRate = new(ngapType.UEAggregateMaximumBitRate)
	*ie6.Value.UEAggregateMaximumBitRate = BuildUEAggregateMaximumBitRate()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.UEContextModificationRequestIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDUESecurityCapabilities
	ie7.Criticality.Value = ngapType.CriticalityPresentReject
	ie7.Value.Present = ngapType.UEContextModificationRequestIEsPresentUESecurityCapabilities
	ie7.Value.UESecurityCapabilities = new(ngapType.UESecurityCapabilities)
	*ie7.Value.UESecurityCapabilities = BuildUESecurityCapabilities()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.UEContextModificationRequestIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDCoreNetworkAssistanceInformationForInactive
	ie8.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie8.Value.Present = ngapType.UEContextModificationRequestIEsPresentCoreNetworkAssistanceInformationForInactive
	ie8.Value.CoreNetworkAssistanceInformationForInactive = new(ngapType.CoreNetworkAssistanceInformationForInactive)
	*ie8.Value.CoreNetworkAssistanceInformationForInactive = BuildCoreNetworkAssistanceInformationForInactive()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	ie9 := ngapType.UEContextModificationRequestIEs{}
	ie9.Id.Value = ngapType.ProtocolIEIDEmergencyFallbackIndicator
	ie9.Criticality.Value = ngapType.CriticalityPresentReject
	ie9.Value.Present = ngapType.UEContextModificationRequestIEsPresentEmergencyFallbackIndicator
	ie9.Value.EmergencyFallbackIndicator = new(ngapType.EmergencyFallbackIndicator)
	*ie9.Value.EmergencyFallbackIndicator = BuildEmergencyFallbackIndicator()

	pduContentIEs.List = append(pduContentIEs.List, ie9)

	ie10 := ngapType.UEContextModificationRequestIEs{}
	ie10.Id.Value = ngapType.ProtocolIEIDNewAMFUENGAPID
	ie10.Criticality.Value = ngapType.CriticalityPresentReject
	ie10.Value.Present = ngapType.UEContextModificationRequestIEsPresentNewAMFUENGAPID
	ie10.Value.NewAMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie10.Value.NewAMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie10)

	ie11 := ngapType.UEContextModificationRequestIEs{}
	ie11.Id.Value = ngapType.ProtocolIEIDRRCInactiveTransitionReportRequest
	ie11.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie11.Value.Present = ngapType.UEContextModificationRequestIEsPresentRRCInactiveTransitionReportRequest
	ie11.Value.RRCInactiveTransitionReportRequest = new(ngapType.RRCInactiveTransitionReportRequest)
	*ie11.Value.RRCInactiveTransitionReportRequest = BuildRRCInactiveTransitionReportRequest()

	pduContentIEs.List = append(pduContentIEs.List, ie11)

	ie12 := ngapType.UEContextModificationRequestIEs{}
	ie12.Id.Value = ngapType.ProtocolIEIDNewGUAMI
	ie12.Criticality.Value = ngapType.CriticalityPresentReject
	ie12.Value.Present = ngapType.UEContextModificationRequestIEsPresentNewGUAMI
	ie12.Value.NewGUAMI = new(ngapType.GUAMI)
	*ie12.Value.NewGUAMI = BuildGUAMI()

	pduContentIEs.List = append(pduContentIEs.List, ie12)

	ie13 := ngapType.UEContextModificationRequestIEs{}
	ie13.Id.Value = ngapType.ProtocolIEIDCNAssistedRANTuning
	ie13.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie13.Value.Present = ngapType.UEContextModificationRequestIEsPresentCNAssistedRANTuning
	ie13.Value.CNAssistedRANTuning = new(ngapType.CNAssistedRANTuning)
	*ie13.Value.CNAssistedRANTuning = BuildCNAssistedRANTuning()

	pduContentIEs.List = append(pduContentIEs.List, ie13)

	ie14 := ngapType.UEContextModificationRequestIEs{}
	ie14.Id.Value = ngapType.ProtocolIEIDSRVCCOperationPossible
	ie14.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie14.Value.Present = ngapType.UEContextModificationRequestIEsPresentSRVCCOperationPossible
	ie14.Value.SRVCCOperationPossible = new(ngapType.SRVCCOperationPossible)
	*ie14.Value.SRVCCOperationPossible = BuildSRVCCOperationPossible()

	pduContentIEs.List = append(pduContentIEs.List, ie14)

	ie15 := ngapType.UEContextModificationRequestIEs{}
	ie15.Id.Value = ngapType.ProtocolIEIDIABAuthorized
	ie15.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie15.Value.Present = ngapType.UEContextModificationRequestIEsPresentIABAuthorized
	ie15.Value.IABAuthorized = new(ngapType.IABAuthorized)
	*ie15.Value.IABAuthorized = BuildIABAuthorized()

	pduContentIEs.List = append(pduContentIEs.List, ie15)

	ie16 := ngapType.UEContextModificationRequestIEs{}
	ie16.Id.Value = ngapType.ProtocolIEIDNRV2XServicesAuthorized
	ie16.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie16.Value.Present = ngapType.UEContextModificationRequestIEsPresentNRV2XServicesAuthorized
	ie16.Value.NRV2XServicesAuthorized = new(ngapType.NRV2XServicesAuthorized)
	*ie16.Value.NRV2XServicesAuthorized = BuildNRV2XServicesAuthorized()

	pduContentIEs.List = append(pduContentIEs.List, ie16)

	ie17 := ngapType.UEContextModificationRequestIEs{}
	ie17.Id.Value = ngapType.ProtocolIEIDLTEV2XServicesAuthorized
	ie17.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie17.Value.Present = ngapType.UEContextModificationRequestIEsPresentLTEV2XServicesAuthorized
	ie17.Value.LTEV2XServicesAuthorized = new(ngapType.LTEV2XServicesAuthorized)
	*ie17.Value.LTEV2XServicesAuthorized = BuildLTEV2XServicesAuthorized()

	pduContentIEs.List = append(pduContentIEs.List, ie17)

	ie18 := ngapType.UEContextModificationRequestIEs{}
	ie18.Id.Value = ngapType.ProtocolIEIDNRUESidelinkAggregateMaximumBitrate
	ie18.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie18.Value.Present = ngapType.UEContextModificationRequestIEsPresentNRUESidelinkAggregateMaximumBitrate
	ie18.Value.NRUESidelinkAggregateMaximumBitrate = new(ngapType.NRUESidelinkAggregateMaximumBitrate)
	*ie18.Value.NRUESidelinkAggregateMaximumBitrate = BuildNRUESidelinkAggregateMaximumBitrate()

	pduContentIEs.List = append(pduContentIEs.List, ie18)

	ie19 := ngapType.UEContextModificationRequestIEs{}
	ie19.Id.Value = ngapType.ProtocolIEIDLTEUESidelinkAggregateMaximumBitrate
	ie19.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie19.Value.Present = ngapType.UEContextModificationRequestIEsPresentLTEUESidelinkAggregateMaximumBitrate
	ie19.Value.LTEUESidelinkAggregateMaximumBitrate = new(ngapType.LTEUESidelinkAggregateMaximumBitrate)
	*ie19.Value.LTEUESidelinkAggregateMaximumBitrate = BuildLTEUESidelinkAggregateMaximumBitrate()

	pduContentIEs.List = append(pduContentIEs.List, ie19)

	ie20 := ngapType.UEContextModificationRequestIEs{}
	ie20.Id.Value = ngapType.ProtocolIEIDPC5QoSParameters
	ie20.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie20.Value.Present = ngapType.UEContextModificationRequestIEsPresentPC5QoSParameters
	ie20.Value.PC5QoSParameters = new(ngapType.PC5QoSParameters)
	*ie20.Value.PC5QoSParameters = BuildPC5QoSParameters()

	pduContentIEs.List = append(pduContentIEs.List, ie20)

	ie21 := ngapType.UEContextModificationRequestIEs{}
	ie21.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityID
	ie21.Criticality.Value = ngapType.CriticalityPresentReject
	ie21.Value.Present = ngapType.UEContextModificationRequestIEsPresentUERadioCapabilityID
	ie21.Value.UERadioCapabilityID = new(ngapType.UERadioCapabilityID)
	*ie21.Value.UERadioCapabilityID = BuildUERadioCapabilityID()

	pduContentIEs.List = append(pduContentIEs.List, ie21)

	return
}

func BuildRRCInactiveTransitionReport() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeRRCInactiveTransitionReport
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentRRCInactiveTransitionReport
	pduMsg.Value.RRCInactiveTransitionReport = new(ngapType.RRCInactiveTransitionReport)
	pduContent := pduMsg.Value.RRCInactiveTransitionReport
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.RRCInactiveTransitionReportIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.RRCInactiveTransitionReportIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.RRCInactiveTransitionReportIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.RRCInactiveTransitionReportIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.RRCInactiveTransitionReportIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRRCState
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.RRCInactiveTransitionReportIEsPresentRRCState
	ie3.Value.RRCState = new(ngapType.RRCState)
	*ie3.Value.RRCState = BuildRRCState()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.RRCInactiveTransitionReportIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.RRCInactiveTransitionReportIEsPresentUserLocationInformation
	ie4.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie4.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildRetrieveUEInformation() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeRetrieveUEInformation
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentRetrieveUEInformation
	pduMsg.Value.RetrieveUEInformation = new(ngapType.RetrieveUEInformation)
	pduContent := pduMsg.Value.RetrieveUEInformation
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.RetrieveUEInformationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDFiveGSTMSI
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.RetrieveUEInformationIEsPresentFiveGSTMSI
	ie1.Value.FiveGSTMSI = new(ngapType.FiveGSTMSI)
	*ie1.Value.FiveGSTMSI = BuildFiveGSTMSI()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	return
}

func BuildUEInformationTransfer() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEInformationTransfer
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUEInformationTransfer
	pduMsg.Value.UEInformationTransfer = new(ngapType.UEInformationTransfer)
	pduContent := pduMsg.Value.UEInformationTransfer
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEInformationTransferIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDFiveGSTMSI
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UEInformationTransferIEsPresentFiveGSTMSI
	ie1.Value.FiveGSTMSI = new(ngapType.FiveGSTMSI)
	*ie1.Value.FiveGSTMSI = BuildFiveGSTMSI()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEInformationTransferIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDNBIoTUEPriority
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UEInformationTransferIEsPresentNBIoTUEPriority
	ie2.Value.NBIoTUEPriority = new(ngapType.NBIoTUEPriority)
	*ie2.Value.NBIoTUEPriority = BuildNBIoTUEPriority()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEInformationTransferIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDUERadioCapability
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UEInformationTransferIEsPresentUERadioCapability
	ie3.Value.UERadioCapability = new(ngapType.UERadioCapability)
	*ie3.Value.UERadioCapability = BuildUERadioCapability()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEInformationTransferIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDSNSSAI
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UEInformationTransferIEsPresentSNSSAI
	ie4.Value.SNSSAI = new(ngapType.SNSSAI)
	*ie4.Value.SNSSAI = BuildSNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.UEInformationTransferIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDAllowedNSSAI
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.UEInformationTransferIEsPresentAllowedNSSAI
	ie5.Value.AllowedNSSAI = new(ngapType.AllowedNSSAI)
	*ie5.Value.AllowedNSSAI = BuildAllowedNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.UEInformationTransferIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDUEDifferentiationInfo
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.UEInformationTransferIEsPresentUEDifferentiationInfo
	ie6.Value.UEDifferentiationInfo = new(ngapType.UEDifferentiationInfo)
	*ie6.Value.UEDifferentiationInfo = BuildUEDifferentiationInfo()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	return
}

func BuildRANCPRelocationIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeRANCPRelocationIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentRANCPRelocationIndication
	pduMsg.Value.RANCPRelocationIndication = new(ngapType.RANCPRelocationIndication)
	pduContent := pduMsg.Value.RANCPRelocationIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.RANCPRelocationIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.RANCPRelocationIndicationIEsPresentRANUENGAPID
	ie1.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie1.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.RANCPRelocationIndicationIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDFiveGSTMSI
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.RANCPRelocationIndicationIEsPresentFiveGSTMSI
	ie2.Value.FiveGSTMSI = new(ngapType.FiveGSTMSI)
	*ie2.Value.FiveGSTMSI = BuildFiveGSTMSI()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.RANCPRelocationIndicationIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDEUTRACGI
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.RANCPRelocationIndicationIEsPresentEUTRACGI
	ie3.Value.EUTRACGI = new(ngapType.EUTRACGI)
	*ie3.Value.EUTRACGI = BuildEUTRACGI()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.RANCPRelocationIndicationIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDTAI
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.RANCPRelocationIndicationIEsPresentTAI
	ie4.Value.TAI = new(ngapType.TAI)
	*ie4.Value.TAI = BuildTAI()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.RANCPRelocationIndicationIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDULCPSecurityInformation
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.RANCPRelocationIndicationIEsPresentULCPSecurityInformation
	ie5.Value.ULCPSecurityInformation = new(ngapType.ULCPSecurityInformation)
	*ie5.Value.ULCPSecurityInformation = BuildULCPSecurityInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildHandoverRequired() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeHandoverPreparation
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentHandoverRequired
	pduMsg.Value.HandoverRequired = new(ngapType.HandoverRequired)
	pduContent := pduMsg.Value.HandoverRequired
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.HandoverRequiredIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.HandoverRequiredIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.HandoverRequiredIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.HandoverRequiredIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.HandoverRequiredIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDHandoverType
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.HandoverRequiredIEsPresentHandoverType
	ie3.Value.HandoverType = new(ngapType.HandoverType)
	*ie3.Value.HandoverType = BuildHandoverType()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.HandoverRequiredIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCause
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.HandoverRequiredIEsPresentCause
	ie4.Value.Cause = new(ngapType.Cause)
	*ie4.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.HandoverRequiredIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDTargetID
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.HandoverRequiredIEsPresentTargetID
	ie5.Value.TargetID = new(ngapType.TargetID)
	*ie5.Value.TargetID = BuildTargetID()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.HandoverRequiredIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDDirectForwardingPathAvailability
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.HandoverRequiredIEsPresentDirectForwardingPathAvailability
	ie6.Value.DirectForwardingPathAvailability = new(ngapType.DirectForwardingPathAvailability)
	*ie6.Value.DirectForwardingPathAvailability = BuildDirectForwardingPathAvailability()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.HandoverRequiredIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceListHORqd
	ie7.Criticality.Value = ngapType.CriticalityPresentReject
	ie7.Value.Present = ngapType.HandoverRequiredIEsPresentPDUSessionResourceListHORqd
	ie7.Value.PDUSessionResourceListHORqd = new(ngapType.PDUSessionResourceListHORqd)
	*ie7.Value.PDUSessionResourceListHORqd = BuildPDUSessionResourceListHORqd()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.HandoverRequiredIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDSourceToTargetTransparentContainer
	ie8.Criticality.Value = ngapType.CriticalityPresentReject
	ie8.Value.Present = ngapType.HandoverRequiredIEsPresentSourceToTargetTransparentContainer
	ie8.Value.SourceToTargetTransparentContainer = new(ngapType.SourceToTargetTransparentContainer)
	*ie8.Value.SourceToTargetTransparentContainer = BuildSourceToTargetTransparentContainer()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	return
}

func BuildHandoverRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeHandoverResourceAllocation
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentHandoverRequest
	pduMsg.Value.HandoverRequest = new(ngapType.HandoverRequest)
	pduContent := pduMsg.Value.HandoverRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.HandoverRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.HandoverRequestIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.HandoverRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDHandoverType
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.HandoverRequestIEsPresentHandoverType
	ie2.Value.HandoverType = new(ngapType.HandoverType)
	*ie2.Value.HandoverType = BuildHandoverType()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.HandoverRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCause
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.HandoverRequestIEsPresentCause
	ie3.Value.Cause = new(ngapType.Cause)
	*ie3.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.HandoverRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDUEAggregateMaximumBitRate
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.HandoverRequestIEsPresentUEAggregateMaximumBitRate
	ie4.Value.UEAggregateMaximumBitRate = new(ngapType.UEAggregateMaximumBitRate)
	*ie4.Value.UEAggregateMaximumBitRate = BuildUEAggregateMaximumBitRate()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.HandoverRequestIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDCoreNetworkAssistanceInformationForInactive
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.HandoverRequestIEsPresentCoreNetworkAssistanceInformationForInactive
	ie5.Value.CoreNetworkAssistanceInformationForInactive = new(ngapType.CoreNetworkAssistanceInformationForInactive)
	*ie5.Value.CoreNetworkAssistanceInformationForInactive = BuildCoreNetworkAssistanceInformationForInactive()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.HandoverRequestIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDUESecurityCapabilities
	ie6.Criticality.Value = ngapType.CriticalityPresentReject
	ie6.Value.Present = ngapType.HandoverRequestIEsPresentUESecurityCapabilities
	ie6.Value.UESecurityCapabilities = new(ngapType.UESecurityCapabilities)
	*ie6.Value.UESecurityCapabilities = BuildUESecurityCapabilities()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.HandoverRequestIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDSecurityContext
	ie7.Criticality.Value = ngapType.CriticalityPresentReject
	ie7.Value.Present = ngapType.HandoverRequestIEsPresentSecurityContext
	ie7.Value.SecurityContext = new(ngapType.SecurityContext)
	*ie7.Value.SecurityContext = BuildSecurityContext()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.HandoverRequestIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDNewSecurityContextInd
	ie8.Criticality.Value = ngapType.CriticalityPresentReject
	ie8.Value.Present = ngapType.HandoverRequestIEsPresentNewSecurityContextInd
	ie8.Value.NewSecurityContextInd = new(ngapType.NewSecurityContextInd)
	*ie8.Value.NewSecurityContextInd = BuildNewSecurityContextInd()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	ie9 := ngapType.HandoverRequestIEs{}
	ie9.Id.Value = ngapType.ProtocolIEIDNASC
	ie9.Criticality.Value = ngapType.CriticalityPresentReject
	ie9.Value.Present = ngapType.HandoverRequestIEsPresentNASC
	ie9.Value.NASC = new(ngapType.NASPDU)
	*ie9.Value.NASC = BuildNASPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie9)

	ie10 := ngapType.HandoverRequestIEs{}
	ie10.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceSetupListHOReq
	ie10.Criticality.Value = ngapType.CriticalityPresentReject
	ie10.Value.Present = ngapType.HandoverRequestIEsPresentPDUSessionResourceSetupListHOReq
	ie10.Value.PDUSessionResourceSetupListHOReq = new(ngapType.PDUSessionResourceSetupListHOReq)
	*ie10.Value.PDUSessionResourceSetupListHOReq = BuildPDUSessionResourceSetupListHOReq()

	pduContentIEs.List = append(pduContentIEs.List, ie10)

	ie11 := ngapType.HandoverRequestIEs{}
	ie11.Id.Value = ngapType.ProtocolIEIDAllowedNSSAI
	ie11.Criticality.Value = ngapType.CriticalityPresentReject
	ie11.Value.Present = ngapType.HandoverRequestIEsPresentAllowedNSSAI
	ie11.Value.AllowedNSSAI = new(ngapType.AllowedNSSAI)
	*ie11.Value.AllowedNSSAI = BuildAllowedNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie11)

	ie12 := ngapType.HandoverRequestIEs{}
	ie12.Id.Value = ngapType.ProtocolIEIDTraceActivation
	ie12.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie12.Value.Present = ngapType.HandoverRequestIEsPresentTraceActivation
	ie12.Value.TraceActivation = new(ngapType.TraceActivation)
	*ie12.Value.TraceActivation = BuildTraceActivation()

	pduContentIEs.List = append(pduContentIEs.List, ie12)

	ie13 := ngapType.HandoverRequestIEs{}
	ie13.Id.Value = ngapType.ProtocolIEIDMaskedIMEISV
	ie13.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie13.Value.Present = ngapType.HandoverRequestIEsPresentMaskedIMEISV
	ie13.Value.MaskedIMEISV = new(ngapType.MaskedIMEISV)
	*ie13.Value.MaskedIMEISV = BuildMaskedIMEISV()

	pduContentIEs.List = append(pduContentIEs.List, ie13)

	ie14 := ngapType.HandoverRequestIEs{}
	ie14.Id.Value = ngapType.ProtocolIEIDSourceToTargetTransparentContainer
	ie14.Criticality.Value = ngapType.CriticalityPresentReject
	ie14.Value.Present = ngapType.HandoverRequestIEsPresentSourceToTargetTransparentContainer
	ie14.Value.SourceToTargetTransparentContainer = new(ngapType.SourceToTargetTransparentContainer)
	*ie14.Value.SourceToTargetTransparentContainer = BuildSourceToTargetTransparentContainer()

	pduContentIEs.List = append(pduContentIEs.List, ie14)

	ie15 := ngapType.HandoverRequestIEs{}
	ie15.Id.Value = ngapType.ProtocolIEIDMobilityRestrictionList
	ie15.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie15.Value.Present = ngapType.HandoverRequestIEsPresentMobilityRestrictionList
	ie15.Value.MobilityRestrictionList = new(ngapType.MobilityRestrictionList)
	*ie15.Value.MobilityRestrictionList = BuildMobilityRestrictionList()

	pduContentIEs.List = append(pduContentIEs.List, ie15)

	ie16 := ngapType.HandoverRequestIEs{}
	ie16.Id.Value = ngapType.ProtocolIEIDLocationReportingRequestType
	ie16.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie16.Value.Present = ngapType.HandoverRequestIEsPresentLocationReportingRequestType
	ie16.Value.LocationReportingRequestType = new(ngapType.LocationReportingRequestType)
	*ie16.Value.LocationReportingRequestType = BuildLocationReportingRequestType()

	pduContentIEs.List = append(pduContentIEs.List, ie16)

	ie17 := ngapType.HandoverRequestIEs{}
	ie17.Id.Value = ngapType.ProtocolIEIDRRCInactiveTransitionReportRequest
	ie17.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie17.Value.Present = ngapType.HandoverRequestIEsPresentRRCInactiveTransitionReportRequest
	ie17.Value.RRCInactiveTransitionReportRequest = new(ngapType.RRCInactiveTransitionReportRequest)
	*ie17.Value.RRCInactiveTransitionReportRequest = BuildRRCInactiveTransitionReportRequest()

	pduContentIEs.List = append(pduContentIEs.List, ie17)

	ie18 := ngapType.HandoverRequestIEs{}
	ie18.Id.Value = ngapType.ProtocolIEIDGUAMI
	ie18.Criticality.Value = ngapType.CriticalityPresentReject
	ie18.Value.Present = ngapType.HandoverRequestIEsPresentGUAMI
	ie18.Value.GUAMI = new(ngapType.GUAMI)
	*ie18.Value.GUAMI = BuildGUAMI()

	pduContentIEs.List = append(pduContentIEs.List, ie18)

	ie19 := ngapType.HandoverRequestIEs{}
	ie19.Id.Value = ngapType.ProtocolIEIDRedirectionVoiceFallback
	ie19.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie19.Value.Present = ngapType.HandoverRequestIEsPresentRedirectionVoiceFallback
	ie19.Value.RedirectionVoiceFallback = new(ngapType.RedirectionVoiceFallback)
	*ie19.Value.RedirectionVoiceFallback = BuildRedirectionVoiceFallback()

	pduContentIEs.List = append(pduContentIEs.List, ie19)

	ie20 := ngapType.HandoverRequestIEs{}
	ie20.Id.Value = ngapType.ProtocolIEIDCNAssistedRANTuning
	ie20.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie20.Value.Present = ngapType.HandoverRequestIEsPresentCNAssistedRANTuning
	ie20.Value.CNAssistedRANTuning = new(ngapType.CNAssistedRANTuning)
	*ie20.Value.CNAssistedRANTuning = BuildCNAssistedRANTuning()

	pduContentIEs.List = append(pduContentIEs.List, ie20)

	ie21 := ngapType.HandoverRequestIEs{}
	ie21.Id.Value = ngapType.ProtocolIEIDSRVCCOperationPossible
	ie21.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie21.Value.Present = ngapType.HandoverRequestIEsPresentSRVCCOperationPossible
	ie21.Value.SRVCCOperationPossible = new(ngapType.SRVCCOperationPossible)
	*ie21.Value.SRVCCOperationPossible = BuildSRVCCOperationPossible()

	pduContentIEs.List = append(pduContentIEs.List, ie21)

	ie22 := ngapType.HandoverRequestIEs{}
	ie22.Id.Value = ngapType.ProtocolIEIDIABAuthorized
	ie22.Criticality.Value = ngapType.CriticalityPresentReject
	ie22.Value.Present = ngapType.HandoverRequestIEsPresentIABAuthorized
	ie22.Value.IABAuthorized = new(ngapType.IABAuthorized)
	*ie22.Value.IABAuthorized = BuildIABAuthorized()

	pduContentIEs.List = append(pduContentIEs.List, ie22)

	ie23 := ngapType.HandoverRequestIEs{}
	ie23.Id.Value = ngapType.ProtocolIEIDEnhancedCoverageRestriction
	ie23.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie23.Value.Present = ngapType.HandoverRequestIEsPresentEnhancedCoverageRestriction
	ie23.Value.EnhancedCoverageRestriction = new(ngapType.EnhancedCoverageRestriction)
	*ie23.Value.EnhancedCoverageRestriction = BuildEnhancedCoverageRestriction()

	pduContentIEs.List = append(pduContentIEs.List, ie23)

	ie24 := ngapType.HandoverRequestIEs{}
	ie24.Id.Value = ngapType.ProtocolIEIDUEDifferentiationInfo
	ie24.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie24.Value.Present = ngapType.HandoverRequestIEsPresentUEDifferentiationInfo
	ie24.Value.UEDifferentiationInfo = new(ngapType.UEDifferentiationInfo)
	*ie24.Value.UEDifferentiationInfo = BuildUEDifferentiationInfo()

	pduContentIEs.List = append(pduContentIEs.List, ie24)

	ie25 := ngapType.HandoverRequestIEs{}
	ie25.Id.Value = ngapType.ProtocolIEIDNRV2XServicesAuthorized
	ie25.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie25.Value.Present = ngapType.HandoverRequestIEsPresentNRV2XServicesAuthorized
	ie25.Value.NRV2XServicesAuthorized = new(ngapType.NRV2XServicesAuthorized)
	*ie25.Value.NRV2XServicesAuthorized = BuildNRV2XServicesAuthorized()

	pduContentIEs.List = append(pduContentIEs.List, ie25)

	ie26 := ngapType.HandoverRequestIEs{}
	ie26.Id.Value = ngapType.ProtocolIEIDLTEV2XServicesAuthorized
	ie26.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie26.Value.Present = ngapType.HandoverRequestIEsPresentLTEV2XServicesAuthorized
	ie26.Value.LTEV2XServicesAuthorized = new(ngapType.LTEV2XServicesAuthorized)
	*ie26.Value.LTEV2XServicesAuthorized = BuildLTEV2XServicesAuthorized()

	pduContentIEs.List = append(pduContentIEs.List, ie26)

	ie27 := ngapType.HandoverRequestIEs{}
	ie27.Id.Value = ngapType.ProtocolIEIDNRUESidelinkAggregateMaximumBitrate
	ie27.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie27.Value.Present = ngapType.HandoverRequestIEsPresentNRUESidelinkAggregateMaximumBitrate
	ie27.Value.NRUESidelinkAggregateMaximumBitrate = new(ngapType.NRUESidelinkAggregateMaximumBitrate)
	*ie27.Value.NRUESidelinkAggregateMaximumBitrate = BuildNRUESidelinkAggregateMaximumBitrate()

	pduContentIEs.List = append(pduContentIEs.List, ie27)

	ie28 := ngapType.HandoverRequestIEs{}
	ie28.Id.Value = ngapType.ProtocolIEIDLTEUESidelinkAggregateMaximumBitrate
	ie28.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie28.Value.Present = ngapType.HandoverRequestIEsPresentLTEUESidelinkAggregateMaximumBitrate
	ie28.Value.LTEUESidelinkAggregateMaximumBitrate = new(ngapType.LTEUESidelinkAggregateMaximumBitrate)
	*ie28.Value.LTEUESidelinkAggregateMaximumBitrate = BuildLTEUESidelinkAggregateMaximumBitrate()

	pduContentIEs.List = append(pduContentIEs.List, ie28)

	ie29 := ngapType.HandoverRequestIEs{}
	ie29.Id.Value = ngapType.ProtocolIEIDPC5QoSParameters
	ie29.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie29.Value.Present = ngapType.HandoverRequestIEsPresentPC5QoSParameters
	ie29.Value.PC5QoSParameters = new(ngapType.PC5QoSParameters)
	*ie29.Value.PC5QoSParameters = BuildPC5QoSParameters()

	pduContentIEs.List = append(pduContentIEs.List, ie29)

	ie30 := ngapType.HandoverRequestIEs{}
	ie30.Id.Value = ngapType.ProtocolIEIDCEmodeBrestricted
	ie30.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie30.Value.Present = ngapType.HandoverRequestIEsPresentCEmodeBrestricted
	ie30.Value.CEmodeBrestricted = new(ngapType.CEmodeBrestricted)
	*ie30.Value.CEmodeBrestricted = BuildCEmodeBrestricted()

	pduContentIEs.List = append(pduContentIEs.List, ie30)

	ie31 := ngapType.HandoverRequestIEs{}
	ie31.Id.Value = ngapType.ProtocolIEIDUEUPCIoTSupport
	ie31.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie31.Value.Present = ngapType.HandoverRequestIEsPresentUEUPCIoTSupport
	ie31.Value.UEUPCIoTSupport = new(ngapType.UEUPCIoTSupport)
	*ie31.Value.UEUPCIoTSupport = BuildUEUPCIoTSupport()

	pduContentIEs.List = append(pduContentIEs.List, ie31)

	ie32 := ngapType.HandoverRequestIEs{}
	ie32.Id.Value = ngapType.ProtocolIEIDManagementBasedMDTPLMNList
	ie32.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie32.Value.Present = ngapType.HandoverRequestIEsPresentManagementBasedMDTPLMNList
	ie32.Value.ManagementBasedMDTPLMNList = new(ngapType.MDTPLMNList)
	*ie32.Value.ManagementBasedMDTPLMNList = BuildMDTPLMNList()

	pduContentIEs.List = append(pduContentIEs.List, ie32)

	ie33 := ngapType.HandoverRequestIEs{}
	ie33.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityID
	ie33.Criticality.Value = ngapType.CriticalityPresentReject
	ie33.Value.Present = ngapType.HandoverRequestIEsPresentUERadioCapabilityID
	ie33.Value.UERadioCapabilityID = new(ngapType.UERadioCapabilityID)
	*ie33.Value.UERadioCapabilityID = BuildUERadioCapabilityID()

	pduContentIEs.List = append(pduContentIEs.List, ie33)

	return
}

func BuildHandoverNotify() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeHandoverNotification
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentHandoverNotify
	pduMsg.Value.HandoverNotify = new(ngapType.HandoverNotify)
	pduContent := pduMsg.Value.HandoverNotify
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.HandoverNotifyIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.HandoverNotifyIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.HandoverNotifyIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.HandoverNotifyIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.HandoverNotifyIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.HandoverNotifyIEsPresentUserLocationInformation
	ie3.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie3.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.HandoverNotifyIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDNotifySourceNGRANNode
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.HandoverNotifyIEsPresentNotifySourceNGRANNode
	ie4.Value.NotifySourceNGRANNode = new(ngapType.NotifySourceNGRANNode)
	*ie4.Value.NotifySourceNGRANNode = BuildNotifySourceNGRANNode()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildPathSwitchRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePathSwitchRequest
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentPathSwitchRequest
	pduMsg.Value.PathSwitchRequest = new(ngapType.PathSwitchRequest)
	pduContent := pduMsg.Value.PathSwitchRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PathSwitchRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.PathSwitchRequestIEsPresentRANUENGAPID
	ie1.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie1.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PathSwitchRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDSourceAMFUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.PathSwitchRequestIEsPresentSourceAMFUENGAPID
	ie2.Value.SourceAMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie2.Value.SourceAMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PathSwitchRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PathSwitchRequestIEsPresentUserLocationInformation
	ie3.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie3.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PathSwitchRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDUESecurityCapabilities
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.PathSwitchRequestIEsPresentUESecurityCapabilities
	ie4.Value.UESecurityCapabilities = new(ngapType.UESecurityCapabilities)
	*ie4.Value.UESecurityCapabilities = BuildUESecurityCapabilities()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.PathSwitchRequestIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceToBeSwitchedDLList
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.PathSwitchRequestIEsPresentPDUSessionResourceToBeSwitchedDLList
	ie5.Value.PDUSessionResourceToBeSwitchedDLList = new(ngapType.PDUSessionResourceToBeSwitchedDLList)
	*ie5.Value.PDUSessionResourceToBeSwitchedDLList = BuildPDUSessionResourceToBeSwitchedDLList()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.PathSwitchRequestIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceFailedToSetupListPSReq
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.PathSwitchRequestIEsPresentPDUSessionResourceFailedToSetupListPSReq
	ie6.Value.PDUSessionResourceFailedToSetupListPSReq = new(ngapType.PDUSessionResourceFailedToSetupListPSReq)
	*ie6.Value.PDUSessionResourceFailedToSetupListPSReq = BuildPDUSessionResourceFailedToSetupListPSReq()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.PathSwitchRequestIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDRRCResumeCause
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.PathSwitchRequestIEsPresentRRCResumeCause
	ie7.Value.RRCResumeCause = new(ngapType.RRCEstablishmentCause)
	*ie7.Value.RRCResumeCause = BuildRRCEstablishmentCause()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	return
}

func BuildHandoverCancel() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeHandoverCancel
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentHandoverCancel
	pduMsg.Value.HandoverCancel = new(ngapType.HandoverCancel)
	pduContent := pduMsg.Value.HandoverCancel
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.HandoverCancelIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.HandoverCancelIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.HandoverCancelIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.HandoverCancelIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.HandoverCancelIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCause
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.HandoverCancelIEsPresentCause
	ie3.Value.Cause = new(ngapType.Cause)
	*ie3.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildHandoverSuccess() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeHandoverSuccess
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentHandoverSuccess
	pduMsg.Value.HandoverSuccess = new(ngapType.HandoverSuccess)
	pduContent := pduMsg.Value.HandoverSuccess
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.HandoverSuccessIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.HandoverSuccessIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.HandoverSuccessIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.HandoverSuccessIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	return
}

func BuildUplinkRANEarlyStatusTransfer() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUplinkRANEarlyStatusTransfer
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUplinkRANEarlyStatusTransfer
	pduMsg.Value.UplinkRANEarlyStatusTransfer = new(ngapType.UplinkRANEarlyStatusTransfer)
	pduContent := pduMsg.Value.UplinkRANEarlyStatusTransfer
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UplinkRANEarlyStatusTransferIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UplinkRANEarlyStatusTransferIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UplinkRANEarlyStatusTransferIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UplinkRANEarlyStatusTransferIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UplinkRANEarlyStatusTransferIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDEarlyStatusTransferTransparentContainer
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.UplinkRANEarlyStatusTransferIEsPresentEarlyStatusTransferTransparentContainer
	ie3.Value.EarlyStatusTransferTransparentContainer = new(ngapType.EarlyStatusTransferTransparentContainer)
	*ie3.Value.EarlyStatusTransferTransparentContainer = BuildEarlyStatusTransferTransparentContainer()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildDownlinkRANEarlyStatusTransfer() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeDownlinkRANEarlyStatusTransfer
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentDownlinkRANEarlyStatusTransfer
	pduMsg.Value.DownlinkRANEarlyStatusTransfer = new(ngapType.DownlinkRANEarlyStatusTransfer)
	pduContent := pduMsg.Value.DownlinkRANEarlyStatusTransfer
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.DownlinkRANEarlyStatusTransferIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.DownlinkRANEarlyStatusTransferIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.DownlinkRANEarlyStatusTransferIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.DownlinkRANEarlyStatusTransferIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.DownlinkRANEarlyStatusTransferIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDEarlyStatusTransferTransparentContainer
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.DownlinkRANEarlyStatusTransferIEsPresentEarlyStatusTransferTransparentContainer
	ie3.Value.EarlyStatusTransferTransparentContainer = new(ngapType.EarlyStatusTransferTransparentContainer)
	*ie3.Value.EarlyStatusTransferTransparentContainer = BuildEarlyStatusTransferTransparentContainer()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildUplinkRANStatusTransfer() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUplinkRANStatusTransfer
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUplinkRANStatusTransfer
	pduMsg.Value.UplinkRANStatusTransfer = new(ngapType.UplinkRANStatusTransfer)
	pduContent := pduMsg.Value.UplinkRANStatusTransfer
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UplinkRANStatusTransferIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UplinkRANStatusTransferIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UplinkRANStatusTransferIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UplinkRANStatusTransferIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UplinkRANStatusTransferIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRANStatusTransferTransparentContainer
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.UplinkRANStatusTransferIEsPresentRANStatusTransferTransparentContainer
	ie3.Value.RANStatusTransferTransparentContainer = new(ngapType.RANStatusTransferTransparentContainer)
	*ie3.Value.RANStatusTransferTransparentContainer = BuildRANStatusTransferTransparentContainer()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildDownlinkRANStatusTransfer() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeDownlinkRANStatusTransfer
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentDownlinkRANStatusTransfer
	pduMsg.Value.DownlinkRANStatusTransfer = new(ngapType.DownlinkRANStatusTransfer)
	pduContent := pduMsg.Value.DownlinkRANStatusTransfer
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.DownlinkRANStatusTransferIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.DownlinkRANStatusTransferIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.DownlinkRANStatusTransferIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.DownlinkRANStatusTransferIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.DownlinkRANStatusTransferIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRANStatusTransferTransparentContainer
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.DownlinkRANStatusTransferIEsPresentRANStatusTransferTransparentContainer
	ie3.Value.RANStatusTransferTransparentContainer = new(ngapType.RANStatusTransferTransparentContainer)
	*ie3.Value.RANStatusTransferTransparentContainer = BuildRANStatusTransferTransparentContainer()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildPaging() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePaging
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentPaging
	pduMsg.Value.Paging = new(ngapType.Paging)
	pduContent := pduMsg.Value.Paging
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PagingIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDUEPagingIdentity
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.PagingIEsPresentUEPagingIdentity
	ie1.Value.UEPagingIdentity = new(ngapType.UEPagingIdentity)
	*ie1.Value.UEPagingIdentity = BuildUEPagingIdentity()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PagingIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDPagingDRX
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.PagingIEsPresentPagingDRX
	ie2.Value.PagingDRX = new(ngapType.PagingDRX)
	*ie2.Value.PagingDRX = BuildPagingDRX()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PagingIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDTAIListForPaging
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PagingIEsPresentTAIListForPaging
	ie3.Value.TAIListForPaging = new(ngapType.TAIListForPaging)
	*ie3.Value.TAIListForPaging = BuildTAIListForPaging()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PagingIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPagingPriority
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.PagingIEsPresentPagingPriority
	ie4.Value.PagingPriority = new(ngapType.PagingPriority)
	*ie4.Value.PagingPriority = BuildPagingPriority()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.PagingIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityForPaging
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.PagingIEsPresentUERadioCapabilityForPaging
	ie5.Value.UERadioCapabilityForPaging = new(ngapType.UERadioCapabilityForPaging)
	*ie5.Value.UERadioCapabilityForPaging = BuildUERadioCapabilityForPaging()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.PagingIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDPagingOrigin
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.PagingIEsPresentPagingOrigin
	ie6.Value.PagingOrigin = new(ngapType.PagingOrigin)
	*ie6.Value.PagingOrigin = BuildPagingOrigin()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.PagingIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDAssistanceDataForPaging
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.PagingIEsPresentAssistanceDataForPaging
	ie7.Value.AssistanceDataForPaging = new(ngapType.AssistanceDataForPaging)
	*ie7.Value.AssistanceDataForPaging = BuildAssistanceDataForPaging()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.PagingIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDNBIoTPagingEDRXInfo
	ie8.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie8.Value.Present = ngapType.PagingIEsPresentNBIoTPagingEDRXInfo
	ie8.Value.NBIoTPagingEDRXInfo = new(ngapType.NBIoTPagingEDRXInfo)
	*ie8.Value.NBIoTPagingEDRXInfo = BuildNBIoTPagingEDRXInfo()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	ie9 := ngapType.PagingIEs{}
	ie9.Id.Value = ngapType.ProtocolIEIDNBIoTPagingDRX
	ie9.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie9.Value.Present = ngapType.PagingIEsPresentNBIoTPagingDRX
	ie9.Value.NBIoTPagingDRX = new(ngapType.NBIoTPagingDRX)
	*ie9.Value.NBIoTPagingDRX = BuildNBIoTPagingDRX()

	pduContentIEs.List = append(pduContentIEs.List, ie9)

	ie10 := ngapType.PagingIEs{}
	ie10.Id.Value = ngapType.ProtocolIEIDEnhancedCoverageRestriction
	ie10.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie10.Value.Present = ngapType.PagingIEsPresentEnhancedCoverageRestriction
	ie10.Value.EnhancedCoverageRestriction = new(ngapType.EnhancedCoverageRestriction)
	*ie10.Value.EnhancedCoverageRestriction = BuildEnhancedCoverageRestriction()

	pduContentIEs.List = append(pduContentIEs.List, ie10)

	ie11 := ngapType.PagingIEs{}
	ie11.Id.Value = ngapType.ProtocolIEIDWUSAssistanceInformation
	ie11.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie11.Value.Present = ngapType.PagingIEsPresentWUSAssistanceInformation
	ie11.Value.WUSAssistanceInformation = new(ngapType.WUSAssistanceInformation)
	*ie11.Value.WUSAssistanceInformation = BuildWUSAssistanceInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie11)

	ie12 := ngapType.PagingIEs{}
	ie12.Id.Value = ngapType.ProtocolIEIDPagingeDRXInformation
	ie12.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie12.Value.Present = ngapType.PagingIEsPresentPagingeDRXInformation
	ie12.Value.PagingeDRXInformation = new(ngapType.PagingeDRXInformation)
	*ie12.Value.PagingeDRXInformation = BuildPagingeDRXInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie12)

	ie13 := ngapType.PagingIEs{}
	ie13.Id.Value = ngapType.ProtocolIEIDCEmodeBrestricted
	ie13.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie13.Value.Present = ngapType.PagingIEsPresentCEmodeBrestricted
	ie13.Value.CEmodeBrestricted = new(ngapType.CEmodeBrestricted)
	*ie13.Value.CEmodeBrestricted = BuildCEmodeBrestricted()

	pduContentIEs.List = append(pduContentIEs.List, ie13)

	return
}

func BuildInitialUEMessage() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeInitialUEMessage
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentInitialUEMessage
	pduMsg.Value.InitialUEMessage = new(ngapType.InitialUEMessage)
	pduContent := pduMsg.Value.InitialUEMessage
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.InitialUEMessageIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.InitialUEMessageIEsPresentRANUENGAPID
	ie1.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie1.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.InitialUEMessageIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDNASPDU
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.InitialUEMessageIEsPresentNASPDU
	ie2.Value.NASPDU = new(ngapType.NASPDU)
	*ie2.Value.NASPDU = BuildNASPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.InitialUEMessageIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.InitialUEMessageIEsPresentUserLocationInformation
	ie3.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie3.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.InitialUEMessageIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDRRCEstablishmentCause
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.InitialUEMessageIEsPresentRRCEstablishmentCause
	ie4.Value.RRCEstablishmentCause = new(ngapType.RRCEstablishmentCause)
	*ie4.Value.RRCEstablishmentCause = BuildRRCEstablishmentCause()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.InitialUEMessageIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDFiveGSTMSI
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.InitialUEMessageIEsPresentFiveGSTMSI
	ie5.Value.FiveGSTMSI = new(ngapType.FiveGSTMSI)
	*ie5.Value.FiveGSTMSI = BuildFiveGSTMSI()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.InitialUEMessageIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDAMFSetID
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.InitialUEMessageIEsPresentAMFSetID
	ie6.Value.AMFSetID = new(ngapType.AMFSetID)
	*ie6.Value.AMFSetID = BuildAMFSetID()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.InitialUEMessageIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDUEContextRequest
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.InitialUEMessageIEsPresentUEContextRequest
	ie7.Value.UEContextRequest = new(ngapType.UEContextRequest)
	*ie7.Value.UEContextRequest = BuildUEContextRequest()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.InitialUEMessageIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDAllowedNSSAI
	ie8.Criticality.Value = ngapType.CriticalityPresentReject
	ie8.Value.Present = ngapType.InitialUEMessageIEsPresentAllowedNSSAI
	ie8.Value.AllowedNSSAI = new(ngapType.AllowedNSSAI)
	*ie8.Value.AllowedNSSAI = BuildAllowedNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	ie9 := ngapType.InitialUEMessageIEs{}
	ie9.Id.Value = ngapType.ProtocolIEIDSourceToTargetAMFInformationReroute
	ie9.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie9.Value.Present = ngapType.InitialUEMessageIEsPresentSourceToTargetAMFInformationReroute
	ie9.Value.SourceToTargetAMFInformationReroute = new(ngapType.SourceToTargetAMFInformationReroute)
	*ie9.Value.SourceToTargetAMFInformationReroute = BuildSourceToTargetAMFInformationReroute()

	pduContentIEs.List = append(pduContentIEs.List, ie9)

	ie10 := ngapType.InitialUEMessageIEs{}
	ie10.Id.Value = ngapType.ProtocolIEIDSelectedPLMNIdentity
	ie10.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie10.Value.Present = ngapType.InitialUEMessageIEsPresentSelectedPLMNIdentity
	ie10.Value.SelectedPLMNIdentity = new(ngapType.PLMNIdentity)
	*ie10.Value.SelectedPLMNIdentity = BuildPLMNIdentity()

	pduContentIEs.List = append(pduContentIEs.List, ie10)

	ie11 := ngapType.InitialUEMessageIEs{}
	ie11.Id.Value = ngapType.ProtocolIEIDIABNodeIndication
	ie11.Criticality.Value = ngapType.CriticalityPresentReject
	ie11.Value.Present = ngapType.InitialUEMessageIEsPresentIABNodeIndication
	ie11.Value.IABNodeIndication = new(ngapType.IABNodeIndication)
	*ie11.Value.IABNodeIndication = BuildIABNodeIndication()

	pduContentIEs.List = append(pduContentIEs.List, ie11)

	ie12 := ngapType.InitialUEMessageIEs{}
	ie12.Id.Value = ngapType.ProtocolIEIDCEmodeBSupportIndicator
	ie12.Criticality.Value = ngapType.CriticalityPresentReject
	ie12.Value.Present = ngapType.InitialUEMessageIEsPresentCEmodeBSupportIndicator
	ie12.Value.CEmodeBSupportIndicator = new(ngapType.CEmodeBSupportIndicator)
	*ie12.Value.CEmodeBSupportIndicator = BuildCEmodeBSupportIndicator()

	pduContentIEs.List = append(pduContentIEs.List, ie12)

	ie13 := ngapType.InitialUEMessageIEs{}
	ie13.Id.Value = ngapType.ProtocolIEIDLTEMIndication
	ie13.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie13.Value.Present = ngapType.InitialUEMessageIEsPresentLTEMIndication
	ie13.Value.LTEMIndication = new(ngapType.LTEMIndication)
	*ie13.Value.LTEMIndication = BuildLTEMIndication()

	pduContentIEs.List = append(pduContentIEs.List, ie13)

	ie14 := ngapType.InitialUEMessageIEs{}
	ie14.Id.Value = ngapType.ProtocolIEIDEDTSession
	ie14.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie14.Value.Present = ngapType.InitialUEMessageIEsPresentEDTSession
	ie14.Value.EDTSession = new(ngapType.EDTSession)
	*ie14.Value.EDTSession = BuildEDTSession()

	pduContentIEs.List = append(pduContentIEs.List, ie14)

	ie15 := ngapType.InitialUEMessageIEs{}
	ie15.Id.Value = ngapType.ProtocolIEIDAuthenticatedIndication
	ie15.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie15.Value.Present = ngapType.InitialUEMessageIEsPresentAuthenticatedIndication
	ie15.Value.AuthenticatedIndication = new(ngapType.AuthenticatedIndication)
	*ie15.Value.AuthenticatedIndication = BuildAuthenticatedIndication()

	pduContentIEs.List = append(pduContentIEs.List, ie15)

	ie16 := ngapType.InitialUEMessageIEs{}
	ie16.Id.Value = ngapType.ProtocolIEIDNPNAccessInformation
	ie16.Criticality.Value = ngapType.CriticalityPresentReject
	ie16.Value.Present = ngapType.InitialUEMessageIEsPresentNPNAccessInformation
	ie16.Value.NPNAccessInformation = new(ngapType.NPNAccessInformation)
	*ie16.Value.NPNAccessInformation = BuildNPNAccessInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie16)

	return
}

func BuildDownlinkNASTransport() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeDownlinkNASTransport
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentDownlinkNASTransport
	pduMsg.Value.DownlinkNASTransport = new(ngapType.DownlinkNASTransport)
	pduContent := pduMsg.Value.DownlinkNASTransport
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.DownlinkNASTransportIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.DownlinkNASTransportIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.DownlinkNASTransportIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.DownlinkNASTransportIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.DownlinkNASTransportIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDOldAMF
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.DownlinkNASTransportIEsPresentOldAMF
	ie3.Value.OldAMF = new(ngapType.AMFName)
	*ie3.Value.OldAMF = BuildAMFName()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.DownlinkNASTransportIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDRANPagingPriority
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.DownlinkNASTransportIEsPresentRANPagingPriority
	ie4.Value.RANPagingPriority = new(ngapType.RANPagingPriority)
	*ie4.Value.RANPagingPriority = BuildRANPagingPriority()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.DownlinkNASTransportIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDNASPDU
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.DownlinkNASTransportIEsPresentNASPDU
	ie5.Value.NASPDU = new(ngapType.NASPDU)
	*ie5.Value.NASPDU = BuildNASPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.DownlinkNASTransportIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDMobilityRestrictionList
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.DownlinkNASTransportIEsPresentMobilityRestrictionList
	ie6.Value.MobilityRestrictionList = new(ngapType.MobilityRestrictionList)
	*ie6.Value.MobilityRestrictionList = BuildMobilityRestrictionList()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.DownlinkNASTransportIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDIndexToRFSP
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.DownlinkNASTransportIEsPresentIndexToRFSP
	ie7.Value.IndexToRFSP = new(ngapType.IndexToRFSP)
	*ie7.Value.IndexToRFSP = BuildIndexToRFSP()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.DownlinkNASTransportIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDUEAggregateMaximumBitRate
	ie8.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie8.Value.Present = ngapType.DownlinkNASTransportIEsPresentUEAggregateMaximumBitRate
	ie8.Value.UEAggregateMaximumBitRate = new(ngapType.UEAggregateMaximumBitRate)
	*ie8.Value.UEAggregateMaximumBitRate = BuildUEAggregateMaximumBitRate()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	ie9 := ngapType.DownlinkNASTransportIEs{}
	ie9.Id.Value = ngapType.ProtocolIEIDAllowedNSSAI
	ie9.Criticality.Value = ngapType.CriticalityPresentReject
	ie9.Value.Present = ngapType.DownlinkNASTransportIEsPresentAllowedNSSAI
	ie9.Value.AllowedNSSAI = new(ngapType.AllowedNSSAI)
	*ie9.Value.AllowedNSSAI = BuildAllowedNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie9)

	ie10 := ngapType.DownlinkNASTransportIEs{}
	ie10.Id.Value = ngapType.ProtocolIEIDSRVCCOperationPossible
	ie10.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie10.Value.Present = ngapType.DownlinkNASTransportIEsPresentSRVCCOperationPossible
	ie10.Value.SRVCCOperationPossible = new(ngapType.SRVCCOperationPossible)
	*ie10.Value.SRVCCOperationPossible = BuildSRVCCOperationPossible()

	pduContentIEs.List = append(pduContentIEs.List, ie10)

	ie11 := ngapType.DownlinkNASTransportIEs{}
	ie11.Id.Value = ngapType.ProtocolIEIDEnhancedCoverageRestriction
	ie11.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie11.Value.Present = ngapType.DownlinkNASTransportIEsPresentEnhancedCoverageRestriction
	ie11.Value.EnhancedCoverageRestriction = new(ngapType.EnhancedCoverageRestriction)
	*ie11.Value.EnhancedCoverageRestriction = BuildEnhancedCoverageRestriction()

	pduContentIEs.List = append(pduContentIEs.List, ie11)

	ie12 := ngapType.DownlinkNASTransportIEs{}
	ie12.Id.Value = ngapType.ProtocolIEIDExtendedConnectedTime
	ie12.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie12.Value.Present = ngapType.DownlinkNASTransportIEsPresentExtendedConnectedTime
	ie12.Value.ExtendedConnectedTime = new(ngapType.ExtendedConnectedTime)
	*ie12.Value.ExtendedConnectedTime = BuildExtendedConnectedTime()

	pduContentIEs.List = append(pduContentIEs.List, ie12)

	ie13 := ngapType.DownlinkNASTransportIEs{}
	ie13.Id.Value = ngapType.ProtocolIEIDUEDifferentiationInfo
	ie13.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie13.Value.Present = ngapType.DownlinkNASTransportIEsPresentUEDifferentiationInfo
	ie13.Value.UEDifferentiationInfo = new(ngapType.UEDifferentiationInfo)
	*ie13.Value.UEDifferentiationInfo = BuildUEDifferentiationInfo()

	pduContentIEs.List = append(pduContentIEs.List, ie13)

	ie14 := ngapType.DownlinkNASTransportIEs{}
	ie14.Id.Value = ngapType.ProtocolIEIDCEmodeBrestricted
	ie14.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie14.Value.Present = ngapType.DownlinkNASTransportIEsPresentCEmodeBrestricted
	ie14.Value.CEmodeBrestricted = new(ngapType.CEmodeBrestricted)
	*ie14.Value.CEmodeBrestricted = BuildCEmodeBrestricted()

	pduContentIEs.List = append(pduContentIEs.List, ie14)

	ie15 := ngapType.DownlinkNASTransportIEs{}
	ie15.Id.Value = ngapType.ProtocolIEIDUERadioCapability
	ie15.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie15.Value.Present = ngapType.DownlinkNASTransportIEsPresentUERadioCapability
	ie15.Value.UERadioCapability = new(ngapType.UERadioCapability)
	*ie15.Value.UERadioCapability = BuildUERadioCapability()

	pduContentIEs.List = append(pduContentIEs.List, ie15)

	ie16 := ngapType.DownlinkNASTransportIEs{}
	ie16.Id.Value = ngapType.ProtocolIEIDUECapabilityInfoRequest
	ie16.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie16.Value.Present = ngapType.DownlinkNASTransportIEsPresentUECapabilityInfoRequest
	ie16.Value.UECapabilityInfoRequest = new(ngapType.UECapabilityInfoRequest)
	*ie16.Value.UECapabilityInfoRequest = BuildUECapabilityInfoRequest()

	pduContentIEs.List = append(pduContentIEs.List, ie16)

	ie17 := ngapType.DownlinkNASTransportIEs{}
	ie17.Id.Value = ngapType.ProtocolIEIDEndIndication
	ie17.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie17.Value.Present = ngapType.DownlinkNASTransportIEsPresentEndIndication
	ie17.Value.EndIndication = new(ngapType.EndIndication)
	*ie17.Value.EndIndication = BuildEndIndication()

	pduContentIEs.List = append(pduContentIEs.List, ie17)

	ie18 := ngapType.DownlinkNASTransportIEs{}
	ie18.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityID
	ie18.Criticality.Value = ngapType.CriticalityPresentReject
	ie18.Value.Present = ngapType.DownlinkNASTransportIEsPresentUERadioCapabilityID
	ie18.Value.UERadioCapabilityID = new(ngapType.UERadioCapabilityID)
	*ie18.Value.UERadioCapabilityID = BuildUERadioCapabilityID()

	pduContentIEs.List = append(pduContentIEs.List, ie18)

	return
}

func BuildUplinkNASTransport() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUplinkNASTransport
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUplinkNASTransport
	pduMsg.Value.UplinkNASTransport = new(ngapType.UplinkNASTransport)
	pduContent := pduMsg.Value.UplinkNASTransport
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UplinkNASTransportIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UplinkNASTransportIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UplinkNASTransportIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UplinkNASTransportIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UplinkNASTransportIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDNASPDU
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.UplinkNASTransportIEsPresentNASPDU
	ie3.Value.NASPDU = new(ngapType.NASPDU)
	*ie3.Value.NASPDU = BuildNASPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UplinkNASTransportIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UplinkNASTransportIEsPresentUserLocationInformation
	ie4.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie4.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.UplinkNASTransportIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDWAGFIdentityInformation
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.UplinkNASTransportIEsPresentWAGFIdentityInformation
	ie5.Value.WAGFIdentityInformation = new(ngapType.OctetString)
	*ie5.Value.WAGFIdentityInformation = BuildOctetString()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.UplinkNASTransportIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDTNGFIdentityInformation
	ie6.Criticality.Value = ngapType.CriticalityPresentReject
	ie6.Value.Present = ngapType.UplinkNASTransportIEsPresentTNGFIdentityInformation
	ie6.Value.TNGFIdentityInformation = new(ngapType.OctetString)
	*ie6.Value.TNGFIdentityInformation = BuildOctetString()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.UplinkNASTransportIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDTWIFIdentityInformation
	ie7.Criticality.Value = ngapType.CriticalityPresentReject
	ie7.Value.Present = ngapType.UplinkNASTransportIEsPresentTWIFIdentityInformation
	ie7.Value.TWIFIdentityInformation = new(ngapType.OctetString)
	*ie7.Value.TWIFIdentityInformation = BuildOctetString()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	return
}

func BuildNASNonDeliveryIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeNASNonDeliveryIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentNASNonDeliveryIndication
	pduMsg.Value.NASNonDeliveryIndication = new(ngapType.NASNonDeliveryIndication)
	pduContent := pduMsg.Value.NASNonDeliveryIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.NASNonDeliveryIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.NASNonDeliveryIndicationIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.NASNonDeliveryIndicationIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.NASNonDeliveryIndicationIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.NASNonDeliveryIndicationIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDNASPDU
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.NASNonDeliveryIndicationIEsPresentNASPDU
	ie3.Value.NASPDU = new(ngapType.NASPDU)
	*ie3.Value.NASPDU = BuildNASPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.NASNonDeliveryIndicationIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCause
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.NASNonDeliveryIndicationIEsPresentCause
	ie4.Value.Cause = new(ngapType.Cause)
	*ie4.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildRerouteNASRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeRerouteNASRequest
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentRerouteNASRequest
	pduMsg.Value.RerouteNASRequest = new(ngapType.RerouteNASRequest)
	pduContent := pduMsg.Value.RerouteNASRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.RerouteNASRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.RerouteNASRequestIEsPresentRANUENGAPID
	ie1.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie1.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.RerouteNASRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.RerouteNASRequestIEsPresentAMFUENGAPID
	ie2.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie2.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.RerouteNASRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDNGAPMessage
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.RerouteNASRequestIEsPresentNGAPMessage
	ie3.Value.NGAPMessage = new(ngapType.OctetString)
	*ie3.Value.NGAPMessage = BuildOctetString()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.RerouteNASRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDAMFSetID
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.RerouteNASRequestIEsPresentAMFSetID
	ie4.Value.AMFSetID = new(ngapType.AMFSetID)
	*ie4.Value.AMFSetID = BuildAMFSetID()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.RerouteNASRequestIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDAllowedNSSAI
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.RerouteNASRequestIEsPresentAllowedNSSAI
	ie5.Value.AllowedNSSAI = new(ngapType.AllowedNSSAI)
	*ie5.Value.AllowedNSSAI = BuildAllowedNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.RerouteNASRequestIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDSourceToTargetAMFInformationReroute
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.RerouteNASRequestIEsPresentSourceToTargetAMFInformationReroute
	ie6.Value.SourceToTargetAMFInformationReroute = new(ngapType.SourceToTargetAMFInformationReroute)
	*ie6.Value.SourceToTargetAMFInformationReroute = BuildSourceToTargetAMFInformationReroute()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	return
}

func BuildNGSetupRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeNGSetup
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentNGSetupRequest
	pduMsg.Value.NGSetupRequest = new(ngapType.NGSetupRequest)
	pduContent := pduMsg.Value.NGSetupRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.NGSetupRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDGlobalRANNodeID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.NGSetupRequestIEsPresentGlobalRANNodeID
	ie1.Value.GlobalRANNodeID = new(ngapType.GlobalRANNodeID)
	*ie1.Value.GlobalRANNodeID = BuildGlobalRANNodeID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.NGSetupRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANNodeName
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.NGSetupRequestIEsPresentRANNodeName
	ie2.Value.RANNodeName = new(ngapType.RANNodeName)
	*ie2.Value.RANNodeName = BuildRANNodeName()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.NGSetupRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDSupportedTAList
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.NGSetupRequestIEsPresentSupportedTAList
	ie3.Value.SupportedTAList = new(ngapType.SupportedTAList)
	*ie3.Value.SupportedTAList = BuildSupportedTAList()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.NGSetupRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDDefaultPagingDRX
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.NGSetupRequestIEsPresentDefaultPagingDRX
	ie4.Value.DefaultPagingDRX = new(ngapType.PagingDRX)
	*ie4.Value.DefaultPagingDRX = BuildPagingDRX()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.NGSetupRequestIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDUERetentionInformation
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.NGSetupRequestIEsPresentUERetentionInformation
	ie5.Value.UERetentionInformation = new(ngapType.UERetentionInformation)
	*ie5.Value.UERetentionInformation = BuildUERetentionInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.NGSetupRequestIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDNBIoTDefaultPagingDRX
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.NGSetupRequestIEsPresentNBIoTDefaultPagingDRX
	ie6.Value.NBIoTDefaultPagingDRX = new(ngapType.NBIoTDefaultPagingDRX)
	*ie6.Value.NBIoTDefaultPagingDRX = BuildNBIoTDefaultPagingDRX()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.NGSetupRequestIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDExtendedRANNodeName
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.NGSetupRequestIEsPresentExtendedRANNodeName
	ie7.Value.ExtendedRANNodeName = new(ngapType.ExtendedRANNodeName)
	*ie7.Value.ExtendedRANNodeName = BuildExtendedRANNodeName()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	return
}

func BuildRANConfigurationUpdate() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeRANConfigurationUpdate
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentRANConfigurationUpdate
	pduMsg.Value.RANConfigurationUpdate = new(ngapType.RANConfigurationUpdate)
	pduContent := pduMsg.Value.RANConfigurationUpdate
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.RANConfigurationUpdateIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDRANNodeName
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.RANConfigurationUpdateIEsPresentRANNodeName
	ie1.Value.RANNodeName = new(ngapType.RANNodeName)
	*ie1.Value.RANNodeName = BuildRANNodeName()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.RANConfigurationUpdateIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDSupportedTAList
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.RANConfigurationUpdateIEsPresentSupportedTAList
	ie2.Value.SupportedTAList = new(ngapType.SupportedTAList)
	*ie2.Value.SupportedTAList = BuildSupportedTAList()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.RANConfigurationUpdateIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDDefaultPagingDRX
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.RANConfigurationUpdateIEsPresentDefaultPagingDRX
	ie3.Value.DefaultPagingDRX = new(ngapType.PagingDRX)
	*ie3.Value.DefaultPagingDRX = BuildPagingDRX()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.RANConfigurationUpdateIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDGlobalRANNodeID
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.RANConfigurationUpdateIEsPresentGlobalRANNodeID
	ie4.Value.GlobalRANNodeID = new(ngapType.GlobalRANNodeID)
	*ie4.Value.GlobalRANNodeID = BuildGlobalRANNodeID()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.RANConfigurationUpdateIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDNGRANTNLAssociationToRemoveList
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.RANConfigurationUpdateIEsPresentNGRANTNLAssociationToRemoveList
	ie5.Value.NGRANTNLAssociationToRemoveList = new(ngapType.NGRANTNLAssociationToRemoveList)
	*ie5.Value.NGRANTNLAssociationToRemoveList = BuildNGRANTNLAssociationToRemoveList()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.RANConfigurationUpdateIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDNBIoTDefaultPagingDRX
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.RANConfigurationUpdateIEsPresentNBIoTDefaultPagingDRX
	ie6.Value.NBIoTDefaultPagingDRX = new(ngapType.NBIoTDefaultPagingDRX)
	*ie6.Value.NBIoTDefaultPagingDRX = BuildNBIoTDefaultPagingDRX()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.RANConfigurationUpdateIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDExtendedRANNodeName
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.RANConfigurationUpdateIEsPresentExtendedRANNodeName
	ie7.Value.ExtendedRANNodeName = new(ngapType.ExtendedRANNodeName)
	*ie7.Value.ExtendedRANNodeName = BuildExtendedRANNodeName()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	return
}

func BuildAMFConfigurationUpdate() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeAMFConfigurationUpdate
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentAMFConfigurationUpdate
	pduMsg.Value.AMFConfigurationUpdate = new(ngapType.AMFConfigurationUpdate)
	pduContent := pduMsg.Value.AMFConfigurationUpdate
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.AMFConfigurationUpdateIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFName
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.AMFConfigurationUpdateIEsPresentAMFName
	ie1.Value.AMFName = new(ngapType.AMFName)
	*ie1.Value.AMFName = BuildAMFName()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.AMFConfigurationUpdateIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDServedGUAMIList
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.AMFConfigurationUpdateIEsPresentServedGUAMIList
	ie2.Value.ServedGUAMIList = new(ngapType.ServedGUAMIList)
	*ie2.Value.ServedGUAMIList = BuildServedGUAMIList()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.AMFConfigurationUpdateIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRelativeAMFCapacity
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.AMFConfigurationUpdateIEsPresentRelativeAMFCapacity
	ie3.Value.RelativeAMFCapacity = new(ngapType.RelativeAMFCapacity)
	*ie3.Value.RelativeAMFCapacity = BuildRelativeAMFCapacity()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.AMFConfigurationUpdateIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPLMNSupportList
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.AMFConfigurationUpdateIEsPresentPLMNSupportList
	ie4.Value.PLMNSupportList = new(ngapType.PLMNSupportList)
	*ie4.Value.PLMNSupportList = BuildPLMNSupportList()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.AMFConfigurationUpdateIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDAMFTNLAssociationToAddList
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.AMFConfigurationUpdateIEsPresentAMFTNLAssociationToAddList
	ie5.Value.AMFTNLAssociationToAddList = new(ngapType.AMFTNLAssociationToAddList)
	*ie5.Value.AMFTNLAssociationToAddList = BuildAMFTNLAssociationToAddList()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.AMFConfigurationUpdateIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDAMFTNLAssociationToRemoveList
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.AMFConfigurationUpdateIEsPresentAMFTNLAssociationToRemoveList
	ie6.Value.AMFTNLAssociationToRemoveList = new(ngapType.AMFTNLAssociationToRemoveList)
	*ie6.Value.AMFTNLAssociationToRemoveList = BuildAMFTNLAssociationToRemoveList()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.AMFConfigurationUpdateIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDAMFTNLAssociationToUpdateList
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.AMFConfigurationUpdateIEsPresentAMFTNLAssociationToUpdateList
	ie7.Value.AMFTNLAssociationToUpdateList = new(ngapType.AMFTNLAssociationToUpdateList)
	*ie7.Value.AMFTNLAssociationToUpdateList = BuildAMFTNLAssociationToUpdateList()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.AMFConfigurationUpdateIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDExtendedAMFName
	ie8.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie8.Value.Present = ngapType.AMFConfigurationUpdateIEsPresentExtendedAMFName
	ie8.Value.ExtendedAMFName = new(ngapType.ExtendedAMFName)
	*ie8.Value.ExtendedAMFName = BuildExtendedAMFName()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	return
}

func BuildAMFStatusIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeAMFStatusIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentAMFStatusIndication
	pduMsg.Value.AMFStatusIndication = new(ngapType.AMFStatusIndication)
	pduContent := pduMsg.Value.AMFStatusIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.AMFStatusIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDUnavailableGUAMIList
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.AMFStatusIndicationIEsPresentUnavailableGUAMIList
	ie1.Value.UnavailableGUAMIList = new(ngapType.UnavailableGUAMIList)
	*ie1.Value.UnavailableGUAMIList = BuildUnavailableGUAMIList()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	return
}

func BuildNGReset() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeNGReset
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentNGReset
	pduMsg.Value.NGReset = new(ngapType.NGReset)
	pduContent := pduMsg.Value.NGReset
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.NGResetIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDCause
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.NGResetIEsPresentCause
	ie1.Value.Cause = new(ngapType.Cause)
	*ie1.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.NGResetIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDResetType
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.NGResetIEsPresentResetType
	ie2.Value.ResetType = new(ngapType.ResetType)
	*ie2.Value.ResetType = BuildResetType()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	return
}

func BuildErrorIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeErrorIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentErrorIndication
	pduMsg.Value.ErrorIndication = new(ngapType.ErrorIndication)
	pduContent := pduMsg.Value.ErrorIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.ErrorIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.ErrorIndicationIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.ErrorIndicationIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.ErrorIndicationIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.ErrorIndicationIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCause
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.ErrorIndicationIEsPresentCause
	ie3.Value.Cause = new(ngapType.Cause)
	*ie3.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.ErrorIndicationIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.ErrorIndicationIEsPresentCriticalityDiagnostics
	ie4.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie4.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.ErrorIndicationIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDFiveGSTMSI
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.ErrorIndicationIEsPresentFiveGSTMSI
	ie5.Value.FiveGSTMSI = new(ngapType.FiveGSTMSI)
	*ie5.Value.FiveGSTMSI = BuildFiveGSTMSI()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildOverloadStart() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeOverloadStart
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentOverloadStart
	pduMsg.Value.OverloadStart = new(ngapType.OverloadStart)
	pduContent := pduMsg.Value.OverloadStart
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.OverloadStartIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFOverloadResponse
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.OverloadStartIEsPresentAMFOverloadResponse
	ie1.Value.AMFOverloadResponse = new(ngapType.OverloadResponse)
	*ie1.Value.AMFOverloadResponse = BuildOverloadResponse()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.OverloadStartIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDAMFTrafficLoadReductionIndication
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.OverloadStartIEsPresentAMFTrafficLoadReductionIndication
	ie2.Value.AMFTrafficLoadReductionIndication = new(ngapType.TrafficLoadReductionIndication)
	*ie2.Value.AMFTrafficLoadReductionIndication = BuildTrafficLoadReductionIndication()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.OverloadStartIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDOverloadStartNSSAIList
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.OverloadStartIEsPresentOverloadStartNSSAIList
	ie3.Value.OverloadStartNSSAIList = new(ngapType.OverloadStartNSSAIList)
	*ie3.Value.OverloadStartNSSAIList = BuildOverloadStartNSSAIList()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildOverloadStop() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeOverloadStop
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentOverloadStop
	pduMsg.Value.OverloadStop = new(ngapType.OverloadStop)

	return
}

func BuildUplinkRANConfigurationTransfer() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUplinkRANConfigurationTransfer
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUplinkRANConfigurationTransfer
	pduMsg.Value.UplinkRANConfigurationTransfer = new(ngapType.UplinkRANConfigurationTransfer)
	pduContent := pduMsg.Value.UplinkRANConfigurationTransfer
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UplinkRANConfigurationTransferIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDSONConfigurationTransferUL
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.UplinkRANConfigurationTransferIEsPresentSONConfigurationTransferUL
	ie1.Value.SONConfigurationTransferUL = new(ngapType.SONConfigurationTransfer)
	*ie1.Value.SONConfigurationTransferUL = BuildSONConfigurationTransfer()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UplinkRANConfigurationTransferIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDENDCSONConfigurationTransferUL
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UplinkRANConfigurationTransferIEsPresentENDCSONConfigurationTransferUL
	ie2.Value.ENDCSONConfigurationTransferUL = new(ngapType.ENDCSONConfigurationTransfer)
	*ie2.Value.ENDCSONConfigurationTransferUL = BuildENDCSONConfigurationTransfer()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UplinkRANConfigurationTransferIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDIntersystemSONConfigurationTransferUL
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UplinkRANConfigurationTransferIEsPresentIntersystemSONConfigurationTransferUL
	ie3.Value.IntersystemSONConfigurationTransferUL = new(ngapType.IntersystemSONConfigurationTransfer)
	*ie3.Value.IntersystemSONConfigurationTransferUL = BuildIntersystemSONConfigurationTransfer()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildDownlinkRANConfigurationTransfer() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeDownlinkRANConfigurationTransfer
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentDownlinkRANConfigurationTransfer
	pduMsg.Value.DownlinkRANConfigurationTransfer = new(ngapType.DownlinkRANConfigurationTransfer)
	pduContent := pduMsg.Value.DownlinkRANConfigurationTransfer
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.DownlinkRANConfigurationTransferIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDSONConfigurationTransferDL
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.DownlinkRANConfigurationTransferIEsPresentSONConfigurationTransferDL
	ie1.Value.SONConfigurationTransferDL = new(ngapType.SONConfigurationTransfer)
	*ie1.Value.SONConfigurationTransferDL = BuildSONConfigurationTransfer()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.DownlinkRANConfigurationTransferIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDENDCSONConfigurationTransferDL
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.DownlinkRANConfigurationTransferIEsPresentENDCSONConfigurationTransferDL
	ie2.Value.ENDCSONConfigurationTransferDL = new(ngapType.ENDCSONConfigurationTransfer)
	*ie2.Value.ENDCSONConfigurationTransferDL = BuildENDCSONConfigurationTransfer()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.DownlinkRANConfigurationTransferIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDIntersystemSONConfigurationTransferDL
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.DownlinkRANConfigurationTransferIEsPresentIntersystemSONConfigurationTransferDL
	ie3.Value.IntersystemSONConfigurationTransferDL = new(ngapType.IntersystemSONConfigurationTransfer)
	*ie3.Value.IntersystemSONConfigurationTransferDL = BuildIntersystemSONConfigurationTransfer()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildWriteReplaceWarningRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeWriteReplaceWarning
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentWriteReplaceWarningRequest
	pduMsg.Value.WriteReplaceWarningRequest = new(ngapType.WriteReplaceWarningRequest)
	pduContent := pduMsg.Value.WriteReplaceWarningRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.WriteReplaceWarningRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDMessageIdentifier
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.WriteReplaceWarningRequestIEsPresentMessageIdentifier
	ie1.Value.MessageIdentifier = new(ngapType.MessageIdentifier)
	*ie1.Value.MessageIdentifier = BuildMessageIdentifier()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.WriteReplaceWarningRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDSerialNumber
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.WriteReplaceWarningRequestIEsPresentSerialNumber
	ie2.Value.SerialNumber = new(ngapType.SerialNumber)
	*ie2.Value.SerialNumber = BuildSerialNumber()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.WriteReplaceWarningRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDWarningAreaList
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.WriteReplaceWarningRequestIEsPresentWarningAreaList
	ie3.Value.WarningAreaList = new(ngapType.WarningAreaList)
	*ie3.Value.WarningAreaList = BuildWarningAreaList()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	// TODO: handle RepetitionPeriod
	//ie4 := ngapType.WriteReplaceWarningRequestIEs{}
	//ie4.Id.Value = ngapType.ProtocolIEIDRepetitionPeriod
	//ie4.Criticality.Value = ngapType.CriticalityPresentReject
	//ie4.Value.Present = ngapType.WriteReplaceWarningRequestIEsPresentRepetitionPeriod
	//ie4.Value.RepetitionPeriod = new(ngapType.RepetitionPeriod)
	//*ie4.Value.RepetitionPeriod = BuildRepetitionPeriod()
	//
	//pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.WriteReplaceWarningRequestIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDNumberOfBroadcastsRequested
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.WriteReplaceWarningRequestIEsPresentNumberOfBroadcastsRequested
	ie5.Value.NumberOfBroadcastsRequested = new(ngapType.NumberOfBroadcastsRequested)
	*ie5.Value.NumberOfBroadcastsRequested = BuildNumberOfBroadcastsRequested()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.WriteReplaceWarningRequestIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDWarningType
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.WriteReplaceWarningRequestIEsPresentWarningType
	ie6.Value.WarningType = new(ngapType.WarningType)
	*ie6.Value.WarningType = BuildWarningType()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.WriteReplaceWarningRequestIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDWarningSecurityInfo
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.WriteReplaceWarningRequestIEsPresentWarningSecurityInfo
	ie7.Value.WarningSecurityInfo = new(ngapType.WarningSecurityInfo)
	*ie7.Value.WarningSecurityInfo = BuildWarningSecurityInfo()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.WriteReplaceWarningRequestIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDDataCodingScheme
	ie8.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie8.Value.Present = ngapType.WriteReplaceWarningRequestIEsPresentDataCodingScheme
	ie8.Value.DataCodingScheme = new(ngapType.DataCodingScheme)
	*ie8.Value.DataCodingScheme = BuildDataCodingScheme()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	ie9 := ngapType.WriteReplaceWarningRequestIEs{}
	ie9.Id.Value = ngapType.ProtocolIEIDWarningMessageContents
	ie9.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie9.Value.Present = ngapType.WriteReplaceWarningRequestIEsPresentWarningMessageContents
	ie9.Value.WarningMessageContents = new(ngapType.WarningMessageContents)
	*ie9.Value.WarningMessageContents = BuildWarningMessageContents()

	pduContentIEs.List = append(pduContentIEs.List, ie9)

	ie10 := ngapType.WriteReplaceWarningRequestIEs{}
	ie10.Id.Value = ngapType.ProtocolIEIDConcurrentWarningMessageInd
	ie10.Criticality.Value = ngapType.CriticalityPresentReject
	ie10.Value.Present = ngapType.WriteReplaceWarningRequestIEsPresentConcurrentWarningMessageInd
	ie10.Value.ConcurrentWarningMessageInd = new(ngapType.ConcurrentWarningMessageInd)
	*ie10.Value.ConcurrentWarningMessageInd = BuildConcurrentWarningMessageInd()

	pduContentIEs.List = append(pduContentIEs.List, ie10)

	ie11 := ngapType.WriteReplaceWarningRequestIEs{}
	ie11.Id.Value = ngapType.ProtocolIEIDWarningAreaCoordinates
	ie11.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie11.Value.Present = ngapType.WriteReplaceWarningRequestIEsPresentWarningAreaCoordinates
	ie11.Value.WarningAreaCoordinates = new(ngapType.WarningAreaCoordinates)
	*ie11.Value.WarningAreaCoordinates = BuildWarningAreaCoordinates()

	pduContentIEs.List = append(pduContentIEs.List, ie11)

	return
}

func BuildPWSCancelRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePWSCancel
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentPWSCancelRequest
	pduMsg.Value.PWSCancelRequest = new(ngapType.PWSCancelRequest)
	pduContent := pduMsg.Value.PWSCancelRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PWSCancelRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDMessageIdentifier
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.PWSCancelRequestIEsPresentMessageIdentifier
	ie1.Value.MessageIdentifier = new(ngapType.MessageIdentifier)
	*ie1.Value.MessageIdentifier = BuildMessageIdentifier()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PWSCancelRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDSerialNumber
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.PWSCancelRequestIEsPresentSerialNumber
	ie2.Value.SerialNumber = new(ngapType.SerialNumber)
	*ie2.Value.SerialNumber = BuildSerialNumber()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PWSCancelRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDWarningAreaList
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PWSCancelRequestIEsPresentWarningAreaList
	ie3.Value.WarningAreaList = new(ngapType.WarningAreaList)
	*ie3.Value.WarningAreaList = BuildWarningAreaList()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PWSCancelRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCancelAllWarningMessages
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.PWSCancelRequestIEsPresentCancelAllWarningMessages
	ie4.Value.CancelAllWarningMessages = new(ngapType.CancelAllWarningMessages)
	*ie4.Value.CancelAllWarningMessages = BuildCancelAllWarningMessages()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildPWSRestartIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePWSRestartIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentPWSRestartIndication
	pduMsg.Value.PWSRestartIndication = new(ngapType.PWSRestartIndication)
	pduContent := pduMsg.Value.PWSRestartIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PWSRestartIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDCellIDListForRestart
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.PWSRestartIndicationIEsPresentCellIDListForRestart
	ie1.Value.CellIDListForRestart = new(ngapType.CellIDListForRestart)
	*ie1.Value.CellIDListForRestart = BuildCellIDListForRestart()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PWSRestartIndicationIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDGlobalRANNodeID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.PWSRestartIndicationIEsPresentGlobalRANNodeID
	ie2.Value.GlobalRANNodeID = new(ngapType.GlobalRANNodeID)
	*ie2.Value.GlobalRANNodeID = BuildGlobalRANNodeID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PWSRestartIndicationIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDTAIListForRestart
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.PWSRestartIndicationIEsPresentTAIListForRestart
	ie3.Value.TAIListForRestart = new(ngapType.TAIListForRestart)
	*ie3.Value.TAIListForRestart = BuildTAIListForRestart()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PWSRestartIndicationIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDEmergencyAreaIDListForRestart
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.PWSRestartIndicationIEsPresentEmergencyAreaIDListForRestart
	ie4.Value.EmergencyAreaIDListForRestart = new(ngapType.EmergencyAreaIDListForRestart)
	*ie4.Value.EmergencyAreaIDListForRestart = BuildEmergencyAreaIDListForRestart()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildPWSFailureIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePWSFailureIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentPWSFailureIndication
	pduMsg.Value.PWSFailureIndication = new(ngapType.PWSFailureIndication)
	pduContent := pduMsg.Value.PWSFailureIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PWSFailureIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDPWSFailedCellIDList
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.PWSFailureIndicationIEsPresentPWSFailedCellIDList
	ie1.Value.PWSFailedCellIDList = new(ngapType.PWSFailedCellIDList)
	*ie1.Value.PWSFailedCellIDList = BuildPWSFailedCellIDList()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PWSFailureIndicationIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDGlobalRANNodeID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.PWSFailureIndicationIEsPresentGlobalRANNodeID
	ie2.Value.GlobalRANNodeID = new(ngapType.GlobalRANNodeID)
	*ie2.Value.GlobalRANNodeID = BuildGlobalRANNodeID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	return
}

func BuildDownlinkUEAssociatedNRPPaTransport() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeDownlinkUEAssociatedNRPPaTransport
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentDownlinkUEAssociatedNRPPaTransport
	pduMsg.Value.DownlinkUEAssociatedNRPPaTransport = new(ngapType.DownlinkUEAssociatedNRPPaTransport)
	pduContent := pduMsg.Value.DownlinkUEAssociatedNRPPaTransport
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.DownlinkUEAssociatedNRPPaTransportIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.DownlinkUEAssociatedNRPPaTransportIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.DownlinkUEAssociatedNRPPaTransportIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.DownlinkUEAssociatedNRPPaTransportIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.DownlinkUEAssociatedNRPPaTransportIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRoutingID
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.DownlinkUEAssociatedNRPPaTransportIEsPresentRoutingID
	ie3.Value.RoutingID = new(ngapType.RoutingID)
	*ie3.Value.RoutingID = BuildRoutingID()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.DownlinkUEAssociatedNRPPaTransportIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDNRPPaPDU
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.DownlinkUEAssociatedNRPPaTransportIEsPresentNRPPaPDU
	ie4.Value.NRPPaPDU = new(ngapType.NRPPaPDU)
	*ie4.Value.NRPPaPDU = BuildNRPPaPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildUplinkUEAssociatedNRPPaTransport() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUplinkUEAssociatedNRPPaTransport
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUplinkUEAssociatedNRPPaTransport
	pduMsg.Value.UplinkUEAssociatedNRPPaTransport = new(ngapType.UplinkUEAssociatedNRPPaTransport)
	pduContent := pduMsg.Value.UplinkUEAssociatedNRPPaTransport
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UplinkUEAssociatedNRPPaTransportIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UplinkUEAssociatedNRPPaTransportIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UplinkUEAssociatedNRPPaTransportIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UplinkUEAssociatedNRPPaTransportIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UplinkUEAssociatedNRPPaTransportIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRoutingID
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.UplinkUEAssociatedNRPPaTransportIEsPresentRoutingID
	ie3.Value.RoutingID = new(ngapType.RoutingID)
	*ie3.Value.RoutingID = BuildRoutingID()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UplinkUEAssociatedNRPPaTransportIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDNRPPaPDU
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.UplinkUEAssociatedNRPPaTransportIEsPresentNRPPaPDU
	ie4.Value.NRPPaPDU = new(ngapType.NRPPaPDU)
	*ie4.Value.NRPPaPDU = BuildNRPPaPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildDownlinkNonUEAssociatedNRPPaTransport() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeDownlinkNonUEAssociatedNRPPaTransport
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentDownlinkNonUEAssociatedNRPPaTransport
	pduMsg.Value.DownlinkNonUEAssociatedNRPPaTransport = new(ngapType.DownlinkNonUEAssociatedNRPPaTransport)
	pduContent := pduMsg.Value.DownlinkNonUEAssociatedNRPPaTransport
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.DownlinkNonUEAssociatedNRPPaTransportIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDRoutingID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.DownlinkNonUEAssociatedNRPPaTransportIEsPresentRoutingID
	ie1.Value.RoutingID = new(ngapType.RoutingID)
	*ie1.Value.RoutingID = BuildRoutingID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.DownlinkNonUEAssociatedNRPPaTransportIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDNRPPaPDU
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.DownlinkNonUEAssociatedNRPPaTransportIEsPresentNRPPaPDU
	ie2.Value.NRPPaPDU = new(ngapType.NRPPaPDU)
	*ie2.Value.NRPPaPDU = BuildNRPPaPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	return
}

func BuildUplinkNonUEAssociatedNRPPaTransport() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUplinkNonUEAssociatedNRPPaTransport
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUplinkNonUEAssociatedNRPPaTransport
	pduMsg.Value.UplinkNonUEAssociatedNRPPaTransport = new(ngapType.UplinkNonUEAssociatedNRPPaTransport)
	pduContent := pduMsg.Value.UplinkNonUEAssociatedNRPPaTransport
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UplinkNonUEAssociatedNRPPaTransportIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDRoutingID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UplinkNonUEAssociatedNRPPaTransportIEsPresentRoutingID
	ie1.Value.RoutingID = new(ngapType.RoutingID)
	*ie1.Value.RoutingID = BuildRoutingID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UplinkNonUEAssociatedNRPPaTransportIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDNRPPaPDU
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UplinkNonUEAssociatedNRPPaTransportIEsPresentNRPPaPDU
	ie2.Value.NRPPaPDU = new(ngapType.NRPPaPDU)
	*ie2.Value.NRPPaPDU = BuildNRPPaPDU()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	return
}

func BuildTraceStart() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeTraceStart
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentTraceStart
	pduMsg.Value.TraceStart = new(ngapType.TraceStart)
	pduContent := pduMsg.Value.TraceStart
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.TraceStartIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.TraceStartIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.TraceStartIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.TraceStartIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.TraceStartIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDTraceActivation
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.TraceStartIEsPresentTraceActivation
	ie3.Value.TraceActivation = new(ngapType.TraceActivation)
	*ie3.Value.TraceActivation = BuildTraceActivation()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildTraceFailureIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeTraceFailureIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentTraceFailureIndication
	pduMsg.Value.TraceFailureIndication = new(ngapType.TraceFailureIndication)
	pduContent := pduMsg.Value.TraceFailureIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.TraceFailureIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.TraceFailureIndicationIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.TraceFailureIndicationIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.TraceFailureIndicationIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.TraceFailureIndicationIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDNGRANTraceID
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.TraceFailureIndicationIEsPresentNGRANTraceID
	ie3.Value.NGRANTraceID = new(ngapType.NGRANTraceID)
	*ie3.Value.NGRANTraceID = BuildNGRANTraceID()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.TraceFailureIndicationIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCause
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.TraceFailureIndicationIEsPresentCause
	ie4.Value.Cause = new(ngapType.Cause)
	*ie4.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildDeactivateTrace() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeDeactivateTrace
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentDeactivateTrace
	pduMsg.Value.DeactivateTrace = new(ngapType.DeactivateTrace)
	pduContent := pduMsg.Value.DeactivateTrace
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.DeactivateTraceIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.DeactivateTraceIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.DeactivateTraceIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.DeactivateTraceIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.DeactivateTraceIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDNGRANTraceID
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.DeactivateTraceIEsPresentNGRANTraceID
	ie3.Value.NGRANTraceID = new(ngapType.NGRANTraceID)
	*ie3.Value.NGRANTraceID = BuildNGRANTraceID()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildCellTrafficTrace() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeCellTrafficTrace
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentCellTrafficTrace
	pduMsg.Value.CellTrafficTrace = new(ngapType.CellTrafficTrace)
	pduContent := pduMsg.Value.CellTrafficTrace
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.CellTrafficTraceIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.CellTrafficTraceIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.CellTrafficTraceIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.CellTrafficTraceIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.CellTrafficTraceIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDNGRANTraceID
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.CellTrafficTraceIEsPresentNGRANTraceID
	ie3.Value.NGRANTraceID = new(ngapType.NGRANTraceID)
	*ie3.Value.NGRANTraceID = BuildNGRANTraceID()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.CellTrafficTraceIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDNGRANCGI
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.CellTrafficTraceIEsPresentNGRANCGI
	ie4.Value.NGRANCGI = new(ngapType.NGRANCGI)
	*ie4.Value.NGRANCGI = BuildNGRANCGI()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.CellTrafficTraceIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDTraceCollectionEntityIPAddress
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.CellTrafficTraceIEsPresentTraceCollectionEntityIPAddress
	ie5.Value.TraceCollectionEntityIPAddress = new(ngapType.TransportLayerAddress)
	*ie5.Value.TraceCollectionEntityIPAddress = BuildTransportLayerAddress()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.CellTrafficTraceIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDPrivacyIndicator
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.CellTrafficTraceIEsPresentPrivacyIndicator
	ie6.Value.PrivacyIndicator = new(ngapType.PrivacyIndicator)
	*ie6.Value.PrivacyIndicator = BuildPrivacyIndicator()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.CellTrafficTraceIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDTraceCollectionEntityURI
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.CellTrafficTraceIEsPresentTraceCollectionEntityURI
	ie7.Value.TraceCollectionEntityURI = new(ngapType.URIAddress)
	*ie7.Value.TraceCollectionEntityURI = BuildURIAddress()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	return
}

func BuildLocationReportingControl() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeLocationReportingControl
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentLocationReportingControl
	pduMsg.Value.LocationReportingControl = new(ngapType.LocationReportingControl)
	pduContent := pduMsg.Value.LocationReportingControl
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.LocationReportingControlIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.LocationReportingControlIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.LocationReportingControlIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.LocationReportingControlIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.LocationReportingControlIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDLocationReportingRequestType
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.LocationReportingControlIEsPresentLocationReportingRequestType
	ie3.Value.LocationReportingRequestType = new(ngapType.LocationReportingRequestType)
	*ie3.Value.LocationReportingRequestType = BuildLocationReportingRequestType()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildLocationReportingFailureIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeLocationReportingFailureIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentLocationReportingFailureIndication
	pduMsg.Value.LocationReportingFailureIndication = new(ngapType.LocationReportingFailureIndication)
	pduContent := pduMsg.Value.LocationReportingFailureIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.LocationReportingFailureIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.LocationReportingFailureIndicationIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.LocationReportingFailureIndicationIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.LocationReportingFailureIndicationIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.LocationReportingFailureIndicationIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCause
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.LocationReportingFailureIndicationIEsPresentCause
	ie3.Value.Cause = new(ngapType.Cause)
	*ie3.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildLocationReport() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeLocationReport
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentLocationReport
	pduMsg.Value.LocationReport = new(ngapType.LocationReport)
	pduContent := pduMsg.Value.LocationReport
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.LocationReportIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.LocationReportIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.LocationReportIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.LocationReportIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.LocationReportIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.LocationReportIEsPresentUserLocationInformation
	ie3.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie3.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.LocationReportIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDUEPresenceInAreaOfInterestList
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.LocationReportIEsPresentUEPresenceInAreaOfInterestList
	ie4.Value.UEPresenceInAreaOfInterestList = new(ngapType.UEPresenceInAreaOfInterestList)
	*ie4.Value.UEPresenceInAreaOfInterestList = BuildUEPresenceInAreaOfInterestList()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.LocationReportIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDLocationReportingRequestType
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.LocationReportIEsPresentLocationReportingRequestType
	ie5.Value.LocationReportingRequestType = new(ngapType.LocationReportingRequestType)
	*ie5.Value.LocationReportingRequestType = BuildLocationReportingRequestType()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildUETNLABindingReleaseRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUETNLABindingRelease
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUETNLABindingReleaseRequest
	pduMsg.Value.UETNLABindingReleaseRequest = new(ngapType.UETNLABindingReleaseRequest)
	pduContent := pduMsg.Value.UETNLABindingReleaseRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UETNLABindingReleaseRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UETNLABindingReleaseRequestIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UETNLABindingReleaseRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UETNLABindingReleaseRequestIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	return
}

func BuildUERadioCapabilityInfoIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUERadioCapabilityInfoIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUERadioCapabilityInfoIndication
	pduMsg.Value.UERadioCapabilityInfoIndication = new(ngapType.UERadioCapabilityInfoIndication)
	pduContent := pduMsg.Value.UERadioCapabilityInfoIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UERadioCapabilityInfoIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UERadioCapabilityInfoIndicationIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UERadioCapabilityInfoIndicationIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UERadioCapabilityInfoIndicationIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UERadioCapabilityInfoIndicationIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDUERadioCapability
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UERadioCapabilityInfoIndicationIEsPresentUERadioCapability
	ie3.Value.UERadioCapability = new(ngapType.UERadioCapability)
	*ie3.Value.UERadioCapability = BuildUERadioCapability()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UERadioCapabilityInfoIndicationIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityForPaging
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UERadioCapabilityInfoIndicationIEsPresentUERadioCapabilityForPaging
	ie4.Value.UERadioCapabilityForPaging = new(ngapType.UERadioCapabilityForPaging)
	*ie4.Value.UERadioCapabilityForPaging = BuildUERadioCapabilityForPaging()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.UERadioCapabilityInfoIndicationIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityEUTRAFormat
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.UERadioCapabilityInfoIndicationIEsPresentUERadioCapabilityEUTRAFormat
	ie5.Value.UERadioCapabilityEUTRAFormat = new(ngapType.UERadioCapability)
	*ie5.Value.UERadioCapabilityEUTRAFormat = BuildUERadioCapability()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildUERadioCapabilityCheckRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUERadioCapabilityCheck
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUERadioCapabilityCheckRequest
	pduMsg.Value.UERadioCapabilityCheckRequest = new(ngapType.UERadioCapabilityCheckRequest)
	pduContent := pduMsg.Value.UERadioCapabilityCheckRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UERadioCapabilityCheckRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UERadioCapabilityCheckRequestIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UERadioCapabilityCheckRequestIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.UERadioCapabilityCheckRequestIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UERadioCapabilityCheckRequestIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDUERadioCapability
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UERadioCapabilityCheckRequestIEsPresentUERadioCapability
	ie3.Value.UERadioCapability = new(ngapType.UERadioCapability)
	*ie3.Value.UERadioCapability = BuildUERadioCapability()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UERadioCapabilityCheckRequestIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityID
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.UERadioCapabilityCheckRequestIEsPresentUERadioCapabilityID
	ie4.Value.UERadioCapabilityID = new(ngapType.UERadioCapabilityID)
	*ie4.Value.UERadioCapabilityID = BuildUERadioCapabilityID()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildPrivateMessage() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePrivateMessage
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentPrivateMessage
	pduMsg.Value.PrivateMessage = new(ngapType.PrivateMessage)
	//pduContent := pduMsg.Value.PrivateMessage
	//pduContentIEs := &pduContent.ProtocolIEs

	return
}

func BuildSecondaryRATDataUsageReport() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeSecondaryRATDataUsageReport
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentSecondaryRATDataUsageReport
	pduMsg.Value.SecondaryRATDataUsageReport = new(ngapType.SecondaryRATDataUsageReport)
	pduContent := pduMsg.Value.SecondaryRATDataUsageReport
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.SecondaryRATDataUsageReportIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.SecondaryRATDataUsageReportIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.SecondaryRATDataUsageReportIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.SecondaryRATDataUsageReportIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.SecondaryRATDataUsageReportIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceSecondaryRATUsageList
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.SecondaryRATDataUsageReportIEsPresentPDUSessionResourceSecondaryRATUsageList
	ie3.Value.PDUSessionResourceSecondaryRATUsageList = new(ngapType.PDUSessionResourceSecondaryRATUsageList)
	*ie3.Value.PDUSessionResourceSecondaryRATUsageList = BuildPDUSessionResourceSecondaryRATUsageList()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.SecondaryRATDataUsageReportIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDHandoverFlag
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.SecondaryRATDataUsageReportIEsPresentHandoverFlag
	ie4.Value.HandoverFlag = new(ngapType.HandoverFlag)
	*ie4.Value.HandoverFlag = BuildHandoverFlag()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.SecondaryRATDataUsageReportIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.SecondaryRATDataUsageReportIEsPresentUserLocationInformation
	ie5.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie5.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildUplinkRIMInformationTransfer() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUplinkRIMInformationTransfer
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUplinkRIMInformationTransfer
	pduMsg.Value.UplinkRIMInformationTransfer = new(ngapType.UplinkRIMInformationTransfer)
	pduContent := pduMsg.Value.UplinkRIMInformationTransfer
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UplinkRIMInformationTransferIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDRIMInformationTransfer
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.UplinkRIMInformationTransferIEsPresentRIMInformationTransfer
	ie1.Value.RIMInformationTransfer = new(ngapType.RIMInformationTransfer)
	*ie1.Value.RIMInformationTransfer = BuildRIMInformationTransfer()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	return
}

func BuildDownlinkRIMInformationTransfer() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeDownlinkRIMInformationTransfer
	pduMsg.Criticality.Value = ngapType.CriticalityPresentIgnore
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentDownlinkRIMInformationTransfer
	pduMsg.Value.DownlinkRIMInformationTransfer = new(ngapType.DownlinkRIMInformationTransfer)
	pduContent := pduMsg.Value.DownlinkRIMInformationTransfer
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.DownlinkRIMInformationTransferIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDRIMInformationTransfer
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.DownlinkRIMInformationTransferIEsPresentRIMInformationTransfer
	ie1.Value.RIMInformationTransfer = new(ngapType.RIMInformationTransfer)
	*ie1.Value.RIMInformationTransfer = BuildRIMInformationTransfer()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	return
}

func BuildConnectionEstablishmentIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeConnectionEstablishmentIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentConnectionEstablishmentIndication
	pduMsg.Value.ConnectionEstablishmentIndication = new(ngapType.ConnectionEstablishmentIndication)
	pduContent := pduMsg.Value.ConnectionEstablishmentIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.ConnectionEstablishmentIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.ConnectionEstablishmentIndicationIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.ConnectionEstablishmentIndicationIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.ConnectionEstablishmentIndicationIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.ConnectionEstablishmentIndicationIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDUERadioCapability
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.ConnectionEstablishmentIndicationIEsPresentUERadioCapability
	ie3.Value.UERadioCapability = new(ngapType.UERadioCapability)
	*ie3.Value.UERadioCapability = BuildUERadioCapability()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.ConnectionEstablishmentIndicationIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDEndIndication
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.ConnectionEstablishmentIndicationIEsPresentEndIndication
	ie4.Value.EndIndication = new(ngapType.EndIndication)
	*ie4.Value.EndIndication = BuildEndIndication()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.ConnectionEstablishmentIndicationIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDSNSSAI
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.ConnectionEstablishmentIndicationIEsPresentSNSSAI
	ie5.Value.SNSSAI = new(ngapType.SNSSAI)
	*ie5.Value.SNSSAI = BuildSNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.ConnectionEstablishmentIndicationIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDAllowedNSSAI
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.ConnectionEstablishmentIndicationIEsPresentAllowedNSSAI
	ie6.Value.AllowedNSSAI = new(ngapType.AllowedNSSAI)
	*ie6.Value.AllowedNSSAI = BuildAllowedNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.ConnectionEstablishmentIndicationIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDUEDifferentiationInfo
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.ConnectionEstablishmentIndicationIEsPresentUEDifferentiationInfo
	ie7.Value.UEDifferentiationInfo = new(ngapType.UEDifferentiationInfo)
	*ie7.Value.UEDifferentiationInfo = BuildUEDifferentiationInfo()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.ConnectionEstablishmentIndicationIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDDLCPSecurityInformation
	ie8.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie8.Value.Present = ngapType.ConnectionEstablishmentIndicationIEsPresentDLCPSecurityInformation
	ie8.Value.DLCPSecurityInformation = new(ngapType.DLCPSecurityInformation)
	*ie8.Value.DLCPSecurityInformation = BuildDLCPSecurityInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	ie9 := ngapType.ConnectionEstablishmentIndicationIEs{}
	ie9.Id.Value = ngapType.ProtocolIEIDNBIoTUEPriority
	ie9.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie9.Value.Present = ngapType.ConnectionEstablishmentIndicationIEsPresentNBIoTUEPriority
	ie9.Value.NBIoTUEPriority = new(ngapType.NBIoTUEPriority)
	*ie9.Value.NBIoTUEPriority = BuildNBIoTUEPriority()

	pduContentIEs.List = append(pduContentIEs.List, ie9)

	return
}

func BuildUERadioCapabilityIDMappingRequest() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUERadioCapabilityIDMapping
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentUERadioCapabilityIDMappingRequest
	pduMsg.Value.UERadioCapabilityIDMappingRequest = new(ngapType.UERadioCapabilityIDMappingRequest)
	pduContent := pduMsg.Value.UERadioCapabilityIDMappingRequest
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UERadioCapabilityIDMappingRequestIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UERadioCapabilityIDMappingRequestIEsPresentUERadioCapabilityID
	ie1.Value.UERadioCapabilityID = new(ngapType.UERadioCapabilityID)
	*ie1.Value.UERadioCapabilityID = BuildUERadioCapabilityID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	return
}

func BuildAMFCPRelocationIndication() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeAMFCPRelocationIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.InitiatingMessagePresentAMFCPRelocationIndication
	pduMsg.Value.AMFCPRelocationIndication = new(ngapType.AMFCPRelocationIndication)
	pduContent := pduMsg.Value.AMFCPRelocationIndication
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.AMFCPRelocationIndicationIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.AMFCPRelocationIndicationIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.AMFCPRelocationIndicationIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.AMFCPRelocationIndicationIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.AMFCPRelocationIndicationIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDSNSSAI
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.AMFCPRelocationIndicationIEsPresentSNSSAI
	ie3.Value.SNSSAI = new(ngapType.SNSSAI)
	*ie3.Value.SNSSAI = BuildSNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.AMFCPRelocationIndicationIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDAllowedNSSAI
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.AMFCPRelocationIndicationIEsPresentAllowedNSSAI
	ie4.Value.AllowedNSSAI = new(ngapType.AllowedNSSAI)
	*ie4.Value.AllowedNSSAI = BuildAllowedNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

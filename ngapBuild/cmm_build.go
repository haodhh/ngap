package ngapBuild

import (
	"ngap/ngapType"
)

func BuildOctetString() (value ngapType.OctetString) {
	value = []byte{171, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildCriticality() (value ngapType.Criticality) {
	value.Value = 0
	value.Value = 2
	return
}

func BuildPresence() (value ngapType.Presence) {
	value.Value = 0
	value.Value = 2
	return
}

func BuildPrivateIEID() (value ngapType.PrivateIEID) {
	value.Present = ngapType.PrivateIEIDPresentLocal
	value.Local = new(int64)
	*value.Local = 65535
	value.Present = ngapType.PrivateIEIDPresentGlobal
	value.Global = new(ngapType.ObjectIdentifier)
	*value.Global = []byte{11, 22, 33, 44, 55, 66, 77, 88, 99}
	return
}

func BuildProcedureCode() (value ngapType.ProcedureCode) {
	value.Value = 0
	value.Value = 255
	return
}

func BuildProtocolExtensionID() (value ngapType.ProtocolExtensionID) {
	value.Value = 0
	value.Value = 65535
	return
}

func BuildProtocolIEID() (value ngapType.ProtocolIEID) {
	value.Value = 0
	value.Value = 65535
	return
}

func BuildTriggeringMessage() (value ngapType.TriggeringMessage) {
	value.Value = 0
	value.Value = 2
	return
}

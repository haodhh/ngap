package ngapBuild

import (
	"ngap/ngapType"
)

func BuildHandoverRequestAcknowledgeTransfer() (value ngapType.HandoverRequestAcknowledgeTransfer) {
	value.DLNGUUPTNLInformation = BuildUPTransportLayerInformation()
	value.DLForwardingUPTNLInformation = new(ngapType.UPTransportLayerInformation)
	*value.DLForwardingUPTNLInformation = BuildUPTransportLayerInformation()
	value.SecurityResult = new(ngapType.SecurityResult)
	*value.SecurityResult = BuildSecurityResult()
	value.QosFlowSetupResponseList = BuildQosFlowListWithDataForwarding()
	value.QosFlowFailedToSetupList = new(ngapType.QosFlowListWithCause)
	*value.QosFlowFailedToSetupList = BuildQosFlowListWithCause()
	value.DataForwardingResponseDRBList = new(ngapType.DataForwardingResponseDRBList)
	*value.DataForwardingResponseDRBList = BuildDataForwardingResponseDRBList()
	return
}

func BuildPDUSessionResourceModifyIndicationUnsuccessfulTransfer() (value ngapType.PDUSessionResourceModifyIndicationUnsuccessfulTransfer) {
	value.Cause = BuildCause()
	return
}

func BuildPDUSessionResourceModifyUnsuccessfulTransfer() (value ngapType.PDUSessionResourceModifyUnsuccessfulTransfer) {
	value.Cause = BuildCause()
	value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*value.CriticalityDiagnostics = BuildCriticalityDiagnostics()
	return
}

func BuildHandoverResourceAllocationUnsuccessfulTransfer() (value ngapType.HandoverResourceAllocationUnsuccessfulTransfer) {
	value.Cause = BuildCause()
	value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*value.CriticalityDiagnostics = BuildCriticalityDiagnostics()
	return
}

func BuildPathSwitchRequestSetupFailedTransfer() (value ngapType.PathSwitchRequestSetupFailedTransfer) {
	value.Cause = BuildCause()
	return
}

func BuildPDUSessionResourceSetupUnsuccessfulTransfer() (value ngapType.PDUSessionResourceSetupUnsuccessfulTransfer) {
	value.Cause = BuildCause()
	value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*value.CriticalityDiagnostics = BuildCriticalityDiagnostics()
	return
}

func BuildHandoverCommandTransfer() (value ngapType.HandoverCommandTransfer) {
	value.DLForwardingUPTNLInformation = new(ngapType.UPTransportLayerInformation)
	*value.DLForwardingUPTNLInformation = BuildUPTransportLayerInformation()
	value.QosFlowToBeForwardedList = new(ngapType.QosFlowToBeForwardedList)
	*value.QosFlowToBeForwardedList = BuildQosFlowToBeForwardedList()
	value.DataForwardingResponseDRBList = new(ngapType.DataForwardingResponseDRBList)
	*value.DataForwardingResponseDRBList = BuildDataForwardingResponseDRBList()
	return
}

func BuildHandoverRequiredTransfer() (value ngapType.HandoverRequiredTransfer) {
	value.DirectForwardingPathAvailability = new(ngapType.DirectForwardingPathAvailability)
	*value.DirectForwardingPathAvailability = BuildDirectForwardingPathAvailability()
	return
}

func BuildPDUSessionResourceModifyConfirmTransfer() (value ngapType.PDUSessionResourceModifyConfirmTransfer) {
	value.QosFlowModifyConfirmList = BuildQosFlowModifyConfirmList()
	value.ULNGUUPTNLInformation = BuildUPTransportLayerInformation()
	value.AdditionalNGUUPTNLInformation = new(ngapType.UPTransportLayerInformationPairList)
	*value.AdditionalNGUUPTNLInformation = BuildUPTransportLayerInformationPairList()
	value.QosFlowFailedToModifyList = new(ngapType.QosFlowListWithCause)
	*value.QosFlowFailedToModifyList = BuildQosFlowListWithCause()
	return
}

func BuildPDUSessionResourceModifyIndicationTransfer() (value ngapType.PDUSessionResourceModifyIndicationTransfer) {
	value.DLQosFlowPerTNLInformation = BuildQosFlowPerTNLInformation()
	value.AdditionalDLQosFlowPerTNLInformation = new(ngapType.QosFlowPerTNLInformationList)
	*value.AdditionalDLQosFlowPerTNLInformation = BuildQosFlowPerTNLInformationList()
	return
}

func BuildPDUSessionResourceModifyRequestTransfer() (value ngapType.PDUSessionResourceModifyRequestTransfer) {

	ie1 := ngapType.PDUSessionResourceModifyRequestTransferIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDPDUSessionAggregateMaximumBitRate
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.IESingleValue.Present = ngapType.PDUSessionResourceModifyRequestTransferIEsPresentPDUSessionAggregateMaximumBitRate
	ie1.IESingleValue.PDUSessionAggregateMaximumBitRate = new(ngapType.PDUSessionAggregateMaximumBitRate)
	*ie1.IESingleValue.PDUSessionAggregateMaximumBitRate = BuildPDUSessionAggregateMaximumBitRate()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie1)

	ie2 := ngapType.PDUSessionResourceModifyRequestTransferIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDULNGUUPTNLModifyList
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.IESingleValue.Present = ngapType.PDUSessionResourceModifyRequestTransferIEsPresentULNGUUPTNLModifyList
	ie2.IESingleValue.ULNGUUPTNLModifyList = new(ngapType.ULNGUUPTNLModifyList)
	*ie2.IESingleValue.ULNGUUPTNLModifyList = BuildULNGUUPTNLModifyList()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie2)

	ie3 := ngapType.PDUSessionResourceModifyRequestTransferIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDNetworkInstance
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.IESingleValue.Present = ngapType.PDUSessionResourceModifyRequestTransferIEsPresentNetworkInstance
	ie3.IESingleValue.NetworkInstance = new(ngapType.NetworkInstance)
	*ie3.IESingleValue.NetworkInstance = BuildNetworkInstance()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie3)

	ie4 := ngapType.PDUSessionResourceModifyRequestTransferIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDQosFlowAddOrModifyRequestList
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.IESingleValue.Present = ngapType.PDUSessionResourceModifyRequestTransferIEsPresentQosFlowAddOrModifyRequestList
	ie4.IESingleValue.QosFlowAddOrModifyRequestList = new(ngapType.QosFlowAddOrModifyRequestList)
	*ie4.IESingleValue.QosFlowAddOrModifyRequestList = BuildQosFlowAddOrModifyRequestList()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie4)

	ie5 := ngapType.PDUSessionResourceModifyRequestTransferIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDQosFlowToReleaseList
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.IESingleValue.Present = ngapType.PDUSessionResourceModifyRequestTransferIEsPresentQosFlowToReleaseList
	ie5.IESingleValue.QosFlowToReleaseList = new(ngapType.QosFlowListWithCause)
	*ie5.IESingleValue.QosFlowToReleaseList = BuildQosFlowListWithCause()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie5)

	ie6 := ngapType.PDUSessionResourceModifyRequestTransferIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDAdditionalULNGUUPTNLInformation
	ie6.Criticality.Value = ngapType.CriticalityPresentReject
	ie6.IESingleValue.Present = ngapType.PDUSessionResourceModifyRequestTransferIEsPresentAdditionalULNGUUPTNLInformation
	ie6.IESingleValue.AdditionalULNGUUPTNLInformation = new(ngapType.UPTransportLayerInformationList)
	*ie6.IESingleValue.AdditionalULNGUUPTNLInformation = BuildUPTransportLayerInformationList()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie6)

	ie7 := ngapType.PDUSessionResourceModifyRequestTransferIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDCommonNetworkInstance
	ie7.Criticality.Value = ngapType.CriticalityPresentReject
	ie7.IESingleValue.Present = ngapType.PDUSessionResourceModifyRequestTransferIEsPresentCommonNetworkInstance
	ie7.IESingleValue.CommonNetworkInstance = new(ngapType.CommonNetworkInstance)
	*ie7.IESingleValue.CommonNetworkInstance = BuildCommonNetworkInstance()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie7)

	ie8 := ngapType.PDUSessionResourceModifyRequestTransferIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDAdditionalRedundantULNGUUPTNLInformation
	ie8.Criticality.Value = ngapType.CriticalityPresentReject
	ie8.IESingleValue.Present = ngapType.PDUSessionResourceModifyRequestTransferIEsPresentAdditionalRedundantULNGUUPTNLInformation
	ie8.IESingleValue.AdditionalRedundantULNGUUPTNLInformation = new(ngapType.UPTransportLayerInformationList)
	*ie8.IESingleValue.AdditionalRedundantULNGUUPTNLInformation = BuildUPTransportLayerInformationList()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie8)

	ie9 := ngapType.PDUSessionResourceModifyRequestTransferIEs{}
	ie9.Id.Value = ngapType.ProtocolIEIDRedundantCommonNetworkInstance
	ie9.Criticality.Value = ngapType.CriticalityPresentReject
	ie9.IESingleValue.Present = ngapType.PDUSessionResourceModifyRequestTransferIEsPresentRedundantCommonNetworkInstance
	ie9.IESingleValue.RedundantCommonNetworkInstance = new(ngapType.CommonNetworkInstance)
	*ie9.IESingleValue.RedundantCommonNetworkInstance = BuildCommonNetworkInstance()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie9)

	return
}

func BuildPDUSessionResourceModifyResponseTransfer() (value ngapType.PDUSessionResourceModifyResponseTransfer) {
	value.DLNGUUPTNLInformation = new(ngapType.UPTransportLayerInformation)
	*value.DLNGUUPTNLInformation = BuildUPTransportLayerInformation()
	value.ULNGUUPTNLInformation = new(ngapType.UPTransportLayerInformation)
	*value.ULNGUUPTNLInformation = BuildUPTransportLayerInformation()
	value.QosFlowAddOrModifyResponseList = new(ngapType.QosFlowAddOrModifyResponseList)
	*value.QosFlowAddOrModifyResponseList = BuildQosFlowAddOrModifyResponseList()
	value.AdditionalDLQosFlowPerTNLInformation = new(ngapType.QosFlowPerTNLInformationList)
	*value.AdditionalDLQosFlowPerTNLInformation = BuildQosFlowPerTNLInformationList()
	value.QosFlowFailedToAddOrModifyList = new(ngapType.QosFlowListWithCause)
	*value.QosFlowFailedToAddOrModifyList = BuildQosFlowListWithCause()
	return
}

func BuildPDUSessionResourceNotifyTransfer() (value ngapType.PDUSessionResourceNotifyTransfer) {
	value.QosFlowNotifyList = new(ngapType.QosFlowNotifyList)
	*value.QosFlowNotifyList = BuildQosFlowNotifyList()
	value.QosFlowReleasedList = new(ngapType.QosFlowListWithCause)
	*value.QosFlowReleasedList = BuildQosFlowListWithCause()
	return
}

func BuildPDUSessionResourceNotifyReleasedTransfer() (value ngapType.PDUSessionResourceNotifyReleasedTransfer) {
	value.Cause = BuildCause()
	return
}

func BuildPathSwitchRequestUnsuccessfulTransfer() (value ngapType.PathSwitchRequestUnsuccessfulTransfer) {
	value.Cause = BuildCause()
	return
}

func BuildPDUSessionResourceReleaseResponseTransfer() (value ngapType.PDUSessionResourceReleaseResponseTransfer) {
	return
}

func BuildUEContextResumeRequestTransfer() (value ngapType.UEContextResumeRequestTransfer) {
	value.QosFlowFailedToResumeList = new(ngapType.QosFlowListWithCause)
	*value.QosFlowFailedToResumeList = BuildQosFlowListWithCause()
	return
}

func BuildUEContextResumeResponseTransfer() (value ngapType.UEContextResumeResponseTransfer) {
	value.QosFlowFailedToResumeList = new(ngapType.QosFlowListWithCause)
	*value.QosFlowFailedToResumeList = BuildQosFlowListWithCause()
	return
}

func BuildSecondaryRATDataUsageReportTransfer() (value ngapType.SecondaryRATDataUsageReportTransfer) {
	value.SecondaryRATUsageInformation = new(ngapType.SecondaryRATUsageInformation)
	*value.SecondaryRATUsageInformation = BuildSecondaryRATUsageInformation()
	return
}

func BuildPDUSessionResourceSetupRequestTransfer() (value ngapType.PDUSessionResourceSetupRequestTransfer) {

	ie1 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDPDUSessionAggregateMaximumBitRate
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentPDUSessionAggregateMaximumBitRate
	ie1.IEValue.PDUSessionAggregateMaximumBitRate = new(ngapType.PDUSessionAggregateMaximumBitRate)
	*ie1.IEValue.PDUSessionAggregateMaximumBitRate = BuildPDUSessionAggregateMaximumBitRate()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie1)

	ie2 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDULNGUUPTNLInformation
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentULNGUUPTNLInformation
	ie2.IEValue.ULNGUUPTNLInformation = new(ngapType.UPTransportLayerInformation)
	*ie2.IEValue.ULNGUUPTNLInformation = BuildUPTransportLayerInformation()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie2)

	ie3 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDAdditionalULNGUUPTNLInformation
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentAdditionalULNGUUPTNLInformation
	ie3.IEValue.AdditionalULNGUUPTNLInformation = new(ngapType.UPTransportLayerInformationList)
	*ie3.IEValue.AdditionalULNGUUPTNLInformation = BuildUPTransportLayerInformationList()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie3)

	ie4 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDDataForwardingNotPossible
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentDataForwardingNotPossible
	ie4.IEValue.DataForwardingNotPossible = new(ngapType.DataForwardingNotPossible)
	*ie4.IEValue.DataForwardingNotPossible = BuildDataForwardingNotPossible()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie4)

	ie5 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDPDUSessionType
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentPDUSessionType
	ie5.IEValue.PDUSessionType = new(ngapType.PDUSessionType)
	*ie5.IEValue.PDUSessionType = BuildPDUSessionType()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie5)

	ie6 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDSecurityIndication
	ie6.Criticality.Value = ngapType.CriticalityPresentReject
	ie6.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentSecurityIndication
	ie6.IEValue.SecurityIndication = new(ngapType.SecurityIndication)
	*ie6.IEValue.SecurityIndication = BuildSecurityIndication()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie6)

	ie7 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDNetworkInstance
	ie7.Criticality.Value = ngapType.CriticalityPresentReject
	ie7.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentNetworkInstance
	ie7.IEValue.NetworkInstance = new(ngapType.NetworkInstance)
	*ie7.IEValue.NetworkInstance = BuildNetworkInstance()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie7)

	ie8 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDQosFlowSetupRequestList
	ie8.Criticality.Value = ngapType.CriticalityPresentReject
	ie8.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentQosFlowSetupRequestList
	ie8.IEValue.QosFlowSetupRequestList = new(ngapType.QosFlowSetupRequestList)
	*ie8.IEValue.QosFlowSetupRequestList = BuildQosFlowSetupRequestList()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie8)

	ie9 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie9.Id.Value = ngapType.ProtocolIEIDCommonNetworkInstance
	ie9.Criticality.Value = ngapType.CriticalityPresentReject
	ie9.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentCommonNetworkInstance
	ie9.IEValue.CommonNetworkInstance = new(ngapType.CommonNetworkInstance)
	*ie9.IEValue.CommonNetworkInstance = BuildCommonNetworkInstance()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie9)

	ie10 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie10.Id.Value = ngapType.ProtocolIEIDDirectForwardingPathAvailability
	ie10.Criticality.Value = ngapType.CriticalityPresentReject
	ie10.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentDirectForwardingPathAvailability
	ie10.IEValue.DirectForwardingPathAvailability = new(ngapType.DirectForwardingPathAvailability)
	*ie10.IEValue.DirectForwardingPathAvailability = BuildDirectForwardingPathAvailability()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie10)

	ie11 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie11.Id.Value = ngapType.ProtocolIEIDRedundantULNGUUPTNLInformation
	ie11.Criticality.Value = ngapType.CriticalityPresentReject
	ie11.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentRedundantULNGUUPTNLInformation
	ie11.IEValue.RedundantULNGUUPTNLInformation = new(ngapType.UPTransportLayerInformation)
	*ie11.IEValue.RedundantULNGUUPTNLInformation = BuildUPTransportLayerInformation()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie11)

	ie12 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie12.Id.Value = ngapType.ProtocolIEIDAdditionalRedundantULNGUUPTNLInformation
	ie12.Criticality.Value = ngapType.CriticalityPresentReject
	ie12.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentAdditionalRedundantULNGUUPTNLInformation
	ie12.IEValue.AdditionalRedundantULNGUUPTNLInformation = new(ngapType.UPTransportLayerInformationList)
	*ie12.IEValue.AdditionalRedundantULNGUUPTNLInformation = BuildUPTransportLayerInformationList()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie12)

	ie13 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie13.Id.Value = ngapType.ProtocolIEIDRedundantCommonNetworkInstance
	ie13.Criticality.Value = ngapType.CriticalityPresentReject
	ie13.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentRedundantCommonNetworkInstance
	ie13.IEValue.RedundantCommonNetworkInstance = new(ngapType.CommonNetworkInstance)
	*ie13.IEValue.RedundantCommonNetworkInstance = BuildCommonNetworkInstance()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie13)

	ie14 := ngapType.PDUSessionResourceSetupRequestTransferIEs{}
	ie14.Id.Value = ngapType.ProtocolIEIDRedundantPDUSessionInformation
	ie14.Criticality.Value = ngapType.CriticalityPresentReject
	ie14.IEValue.Present = ngapType.PDUSessionResourceSetupRequestTransferIEsPresentRedundantPDUSessionInformation
	ie14.IEValue.RedundantPDUSessionInformation = new(ngapType.RedundantPDUSessionInformation)
	*ie14.IEValue.RedundantPDUSessionInformation = BuildRedundantPDUSessionInformation()

	value.ProtocolIEs.List = append(value.ProtocolIEs.List, ie14)

	return
}

func BuildPDUSessionResourceSetupResponseTransfer() (value ngapType.PDUSessionResourceSetupResponseTransfer) {
	value.DLQosFlowPerTNLInformation = BuildQosFlowPerTNLInformation()
	value.AdditionalDLQosFlowPerTNLInformation = new(ngapType.QosFlowPerTNLInformationList)
	*value.AdditionalDLQosFlowPerTNLInformation = BuildQosFlowPerTNLInformationList()
	value.SecurityResult = new(ngapType.SecurityResult)
	*value.SecurityResult = BuildSecurityResult()
	value.QosFlowFailedToSetupList = new(ngapType.QosFlowListWithCause)
	*value.QosFlowFailedToSetupList = BuildQosFlowListWithCause()
	return
}

func BuildUEContextSuspendRequestTransfer() (value ngapType.UEContextSuspendRequestTransfer) {
	value.SuspendIndicator = new(ngapType.SuspendIndicator)
	*value.SuspendIndicator = BuildSuspendIndicator()
	return
}

func BuildPathSwitchRequestAcknowledgeTransfer() (value ngapType.PathSwitchRequestAcknowledgeTransfer) {
	value.ULNGUUPTNLInformation = new(ngapType.UPTransportLayerInformation)
	*value.ULNGUUPTNLInformation = BuildUPTransportLayerInformation()
	value.SecurityIndication = new(ngapType.SecurityIndication)
	*value.SecurityIndication = BuildSecurityIndication()
	return
}

func BuildPathSwitchRequestTransfer() (value ngapType.PathSwitchRequestTransfer) {
	value.DLNGUUPTNLInformation = BuildUPTransportLayerInformation()
	value.DLNGUTNLInformationReused = new(ngapType.DLNGUTNLInformationReused)
	*value.DLNGUTNLInformationReused = BuildDLNGUTNLInformationReused()
	value.UserPlaneSecurityInformation = new(ngapType.UserPlaneSecurityInformation)
	*value.UserPlaneSecurityInformation = BuildUserPlaneSecurityInformation()
	value.QosFlowAcceptedList = BuildQosFlowAcceptedList()
	return
}

func BuildHandoverPreparationUnsuccessfulTransfer() (value ngapType.HandoverPreparationUnsuccessfulTransfer) {
	value.Cause = BuildCause()
	return
}

func BuildPDUSessionResourceReleaseCommandTransfer() (value ngapType.PDUSessionResourceReleaseCommandTransfer) {
	value.Cause = BuildCause()
	return
}

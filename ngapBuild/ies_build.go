package ngapBuild

import (
	"ngap/ngapType"
)

func BuildAdditionalQosFlowInformation() (value ngapType.AdditionalQosFlowInformation) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildPNINPNRestricted() (value ngapType.PNINPNRestricted) {
	value.Value = 0
	return
}

func BuildAlternativeQoSParaSetIndex() (value ngapType.AlternativeQoSParaSetIndex) {
	value.Value = 1
	value.Value = 8
	return
}

func BuildAlternativeQoSParaSetNotifyIndex() (value ngapType.AlternativeQoSParaSetNotifyIndex) {
	value.Value = 0
	value.Value = 8
	return
}

func BuildAMFName() (value ngapType.AMFName) {
	value.Value = "ngap-vht5gc-PrintableString"
	return
}

func BuildAMFNameVisibleString() (value ngapType.AMFNameVisibleString) {
	value.Value = "ngap-vht5gc-VisibleString"
	return
}

func BuildAMFNameUTF8String() (value ngapType.AMFNameUTF8String) {
	value.Value = "ngap-vht5gc-UTF8String"
	return
}

func BuildAMFPointer() (value ngapType.AMFPointer) {
	value.Value.BitLength = 6
	value.Value.Bytes = []byte{171}
	value.Value.BitLength = 6
	value.Value.Bytes = []byte{0}
	return
}

func BuildAMFRegionID() (value ngapType.AMFRegionID) {
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{171}
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{0}
	return
}

func BuildAMFSetID() (value ngapType.AMFSetID) {
	value.Value.BitLength = 10
	value.Value.Bytes = []byte{171, 1}
	value.Value.BitLength = 10
	value.Value.Bytes = []byte{0, 1}
	return
}

func BuildAMFUENGAPID() (value ngapType.AMFUENGAPID) {
	value.Value = 0
	value.Value = 1099511627775
	return
}

func BuildQosFlowMappingIndication() (value ngapType.QosFlowMappingIndication) {
	value.Value = 0
	return
}

func BuildAuthenticatedIndication() (value ngapType.AuthenticatedIndication) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildAveragingWindow() (value ngapType.AveragingWindow) {
	value.Value = 0
	value.Value = 4095
	return
}

func BuildBitRate() (value ngapType.BitRate) {
	value.Value = 0
	value.Value = 2000000
	return
}

func BuildBtRssi() (value ngapType.BtRssi) {
	value.Value = 0
	return
}

func BuildBluetoothMeasConfig() (value ngapType.BluetoothMeasConfig) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildBluetoothName() (value ngapType.BluetoothName) {
	value.Value = []byte{0}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49}
	return
}

func BuildBurstArrivalTime() (value ngapType.BurstArrivalTime) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildCAGID() (value ngapType.CAGID) {
	value.Value.BitLength = 32
	value.Value.Bytes = []byte{171, 1, 2, 3}
	value.Value.BitLength = 32
	value.Value.Bytes = []byte{0, 1, 2, 3}
	return
}

func BuildCancelAllWarningMessages() (value ngapType.CancelAllWarningMessages) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildCauseMisc() (value ngapType.CauseMisc) {
	value.Value = 0
	value.Value = 5
	return
}

func BuildCauseNas() (value ngapType.CauseNas) {
	value.Value = 0
	value.Value = 3
	return
}

func BuildCauseProtocol() (value ngapType.CauseProtocol) {
	value.Value = 0
	value.Value = 6
	return
}

func BuildCauseRadioNetwork() (value ngapType.CauseRadioNetwork) {
	value.Value = 0
	value.Value = 50
	return
}

func BuildCauseTransport() (value ngapType.CauseTransport) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildCellSize() (value ngapType.CellSize) {
	value.Value = 0
	value.Value = 3
	return
}

func BuildCEmodeBSupportIndicator() (value ngapType.CEmodeBSupportIndicator) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildCEmodeBrestricted() (value ngapType.CEmodeBrestricted) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildCnType() (value ngapType.CnType) {
	value.Value = 0
	return
}

func BuildCNTypeRestrictionsForServing() (value ngapType.CNTypeRestrictionsForServing) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildCommonNetworkInstance() (value ngapType.CommonNetworkInstance) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildConcurrentWarningMessageInd() (value ngapType.ConcurrentWarningMessageInd) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildConfidentialityProtectionIndication() (value ngapType.ConfidentialityProtectionIndication) {
	value.Value = 0
	value.Value = 2
	return
}

func BuildConfidentialityProtectionResult() (value ngapType.ConfidentialityProtectionResult) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildConfiguredTACIndication() (value ngapType.ConfiguredTACIndication) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildCoverageEnhancementLevel() (value ngapType.CoverageEnhancementLevel) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildDataCodingScheme() (value ngapType.DataCodingScheme) {
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{171}
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{0}
	return
}

func BuildDataForwardingAccepted() (value ngapType.DataForwardingAccepted) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildDataForwardingNotPossible() (value ngapType.DataForwardingNotPossible) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildDAPSIndicator() (value ngapType.DAPSIndicator) {
	value.Value = 0
	return
}

func BuildDapsresponseindicator() (value ngapType.Dapsresponseindicator) {
	value.Value = 0
	return
}

func BuildDelayCritical() (value ngapType.DelayCritical) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildDLNASMAC() (value ngapType.DLNASMAC) {
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{171, 1}
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{0, 1}
	return
}

func BuildDLForwarding() (value ngapType.DLForwarding) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildDLNGUTNLInformationReused() (value ngapType.DLNGUTNLInformationReused) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildDirectForwardingPathAvailability() (value ngapType.DirectForwardingPathAvailability) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildDRBID() (value ngapType.DRBID) {
	value.Value = 1
	value.Value = 32
	return
}

func BuildEDTSession() (value ngapType.EDTSession) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildEmergencyAreaID() (value ngapType.EmergencyAreaID) {
	value.Value = []byte{0, 1, 2}
	value.Value = []byte{0, 1, 2}
	return
}

func BuildEmergencyFallbackRequestIndicator() (value ngapType.EmergencyFallbackRequestIndicator) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildEmergencyServiceTargetCN() (value ngapType.EmergencyServiceTargetCN) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildEnhancedCoverageRestriction() (value ngapType.EnhancedCoverageRestriction) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildExtendedConnectedTime() (value ngapType.ExtendedConnectedTime) {
	value.Value = 0
	value.Value = 255
	return
}

func BuildENDCSONConfigurationTransfer() (value ngapType.ENDCSONConfigurationTransfer) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildEndIndication() (value ngapType.EndIndication) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildEPSTAC() (value ngapType.EPSTAC) {
	value.Value = []byte{0, 1}
	value.Value = []byte{0, 1}
	return
}

func BuildERABID() (value ngapType.ERABID) {
	value.Value = 0
	value.Value = 15
	return
}

func BuildEUTRACellIdentity() (value ngapType.EUTRACellIdentity) {
	value.Value.BitLength = 28
	value.Value.Bytes = []byte{171, 1, 2, 3}
	value.Value.BitLength = 28
	value.Value.Bytes = []byte{0, 1, 2, 3}
	return
}

func BuildEUTRAencryptionAlgorithms() (value ngapType.EUTRAencryptionAlgorithms) {
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{171, 1}
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{0, 1}
	return
}

func BuildEUTRAintegrityProtectionAlgorithms() (value ngapType.EUTRAintegrityProtectionAlgorithms) {
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{171, 1}
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{0, 1}
	return
}

func BuildEventType() (value ngapType.EventType) {
	value.Value = 0
	value.Value = 5
	return
}

func BuildExpectedActivityPeriod() (value ngapType.ExpectedActivityPeriod) {
	value.Value = 1
	value.Value = 181
	return
}

func BuildExpectedHOInterval() (value ngapType.ExpectedHOInterval) {
	value.Value = 0
	value.Value = 6
	return
}

func BuildExpectedIdlePeriod() (value ngapType.ExpectedIdlePeriod) {
	value.Value = 1
	value.Value = 181
	return
}

func BuildExpectedUEMobility() (value ngapType.ExpectedUEMobility) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildExtendedPacketDelayBudget() (value ngapType.ExtendedPacketDelayBudget) {
	value.Value = 1
	value.Value = 65535
	return
}

func BuildExtendedRNCID() (value ngapType.ExtendedRNCID) {
	value.Value = 4096
	value.Value = 65535
	return
}

func BuildOutOfCoverage() (value ngapType.OutOfCoverage) {
	value.Value = 0
	return
}

func BuildFiveGTMSI() (value ngapType.FiveGTMSI) {
	value.Value = []byte{0, 1, 2, 3}
	value.Value = []byte{0, 1, 2, 3}
	return
}

func BuildFiveQI() (value ngapType.FiveQI) {
	value.Value = 0
	value.Value = 255
	return
}

func BuildGlobalLineIdentity() (value ngapType.GlobalLineIdentity) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildGTPTEID() (value ngapType.GTPTEID) {
	value.Value = []byte{0, 1, 2, 3}
	value.Value = []byte{0, 1, 2, 3}
	return
}

func BuildGUAMIType() (value ngapType.GUAMIType) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildHandoverFlag() (value ngapType.HandoverFlag) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildHandoverType() (value ngapType.HandoverType) {
	value.Value = 0
	value.Value = 3
	return
}

func BuildHFCNodeID() (value ngapType.HFCNodeID) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildHandoverReportType() (value ngapType.HandoverReportType) {
	value.Value = 0
	return
}

func BuildHysteresis() (value ngapType.Hysteresis) {
	value.Value = 0
	value.Value = 30
	return
}

func BuildIABAuthorized() (value ngapType.IABAuthorized) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildIABSupported() (value ngapType.IABSupported) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildIABNodeIndication() (value ngapType.IABNodeIndication) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildIMSVoiceSupportIndicator() (value ngapType.IMSVoiceSupportIndicator) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildIndexToRFSP() (value ngapType.IndexToRFSP) {
	value.Value = 1
	value.Value = 256
	return
}

func BuildIntegrityProtectionIndication() (value ngapType.IntegrityProtectionIndication) {
	value.Value = 0
	value.Value = 2
	return
}

func BuildIntegrityProtectionResult() (value ngapType.IntegrityProtectionResult) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildIntendedNumberOfPagingAttempts() (value ngapType.IntendedNumberOfPagingAttempts) {
	value.Value = 1
	value.Value = 16
	return
}

func BuildInterfacesToTrace() (value ngapType.InterfacesToTrace) {
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{171}
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{0}
	return
}

func BuildEarlyIRATHO() (value ngapType.EarlyIRATHO) {
	value.Value = 0
	return
}

func BuildLAC() (value ngapType.LAC) {
	value.Value = []byte{0, 1}
	value.Value = []byte{0, 1}
	return
}

func BuildLastVisitedEUTRANCellInformation() (value ngapType.LastVisitedEUTRANCellInformation) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildLastVisitedGERANCellInformation() (value ngapType.LastVisitedGERANCellInformation) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildLastVisitedUTRANCellInformation() (value ngapType.LastVisitedUTRANCellInformation) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildLineType() (value ngapType.LineType) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildLocationReportingAdditionalInfo() (value ngapType.LocationReportingAdditionalInfo) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildLocationReportingReferenceID() (value ngapType.LocationReportingReferenceID) {
	value.Value = 1
	value.Value = 64
	return
}

func BuildLoggingInterval() (value ngapType.LoggingInterval) {
	value.Value = 0
	value.Value = 10
	return
}

func BuildLoggingDuration() (value ngapType.LoggingDuration) {
	value.Value = 0
	value.Value = 5
	return
}

func BuildLinksToLog() (value ngapType.LinksToLog) {
	value.Value = 0
	value.Value = 2
	return
}

func BuildLTEMIndication() (value ngapType.LTEMIndication) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildLTEUERLFReportContainer() (value ngapType.LTEUERLFReportContainer) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildMaskedIMEISV() (value ngapType.MaskedIMEISV) {
	value.Value.BitLength = 64
	value.Value.Bytes = []byte{171, 1, 2, 3, 4, 5, 6, 7}
	value.Value.BitLength = 64
	value.Value.Bytes = []byte{0, 1, 2, 3, 4, 5, 6, 7}
	return
}

func BuildMaximumDataBurstVolume() (value ngapType.MaximumDataBurstVolume) {
	value.Value = 0
	value.Value = 2000000
	return
}

func BuildMessageIdentifier() (value ngapType.MessageIdentifier) {
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{171, 1}
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{0, 1}
	return
}

func BuildMaximumIntegrityProtectedDataRate() (value ngapType.MaximumIntegrityProtectedDataRate) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildMICOModeIndication() (value ngapType.MICOModeIndication) {
	value.Value = 0
	return
}

func BuildMobilityInformation() (value ngapType.MobilityInformation) {
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{171, 1}
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{0, 1}
	return
}

func BuildMDTActivation() (value ngapType.MDTActivation) {
	value.Value = 0
	value.Value = 2
	return
}

func BuildMDTModeEutra() (value ngapType.MDTModeEutra) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildMeasurementsToActivate() (value ngapType.MeasurementsToActivate) {
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{171}
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{0}
	return
}

func BuildM1ReportingTrigger() (value ngapType.M1ReportingTrigger) {
	value.Value = 0
	value.Value = 2
	return
}

func BuildM4period() (value ngapType.M4period) {
	value.Value = 0
	value.Value = 4
	return
}

func BuildM5period() (value ngapType.M5period) {
	value.Value = 0
	value.Value = 4
	return
}

func BuildM6reportInterval() (value ngapType.M6reportInterval) {
	value.Value = 0
	value.Value = 13
	return
}

func BuildM7period() (value ngapType.M7period) {
	value.Value = 1
	value.Value = 60
	return
}

func BuildMDTLocationInformation() (value ngapType.MDTLocationInformation) {
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{171}
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{0}
	return
}

func BuildNASPDU() (value ngapType.NASPDU) {
	value.Value = []byte{126, 0, 91, 2}
	return
}

func BuildNASSecurityParametersFromNGRAN() (value ngapType.NASSecurityParametersFromNGRAN) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildNBIoTDefaultPagingDRX() (value ngapType.NBIoTDefaultPagingDRX) {
	value.Value = 0
	value.Value = 3
	return
}

func BuildNBIoTPagingDRX() (value ngapType.NBIoTPagingDRX) {
	value.Value = 0
	value.Value = 5
	return
}

func BuildNBIoTPagingEDRXCycle() (value ngapType.NBIoTPagingEDRXCycle) {
	value.Value = 0
	value.Value = 13
	return
}

func BuildNBIoTPagingTimeWindow() (value ngapType.NBIoTPagingTimeWindow) {
	value.Value = 0
	value.Value = 15
	return
}

func BuildNBIoTUEPriority() (value ngapType.NBIoTUEPriority) {
	value.Value = 0
	value.Value = 255
	return
}

func BuildNetworkInstance() (value ngapType.NetworkInstance) {
	value.Value = 1
	value.Value = 256
	return
}

func BuildNewSecurityContextInd() (value ngapType.NewSecurityContextInd) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildNextHopChainingCount() (value ngapType.NextHopChainingCount) {
	value.Value = 0
	value.Value = 7
	return
}

func BuildNextPagingAreaScope() (value ngapType.NextPagingAreaScope) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildNotifySourceNGRANNode() (value ngapType.NotifySourceNGRANNode) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildNGRANTraceID() (value ngapType.NGRANTraceID) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7}
	return
}

func BuildNID() (value ngapType.NID) {
	value.Value.BitLength = 44
	value.Value.Bytes = []byte{171, 1, 2, 3, 4, 5}
	value.Value.BitLength = 44
	value.Value.Bytes = []byte{0, 1, 2, 3, 4, 5}
	return
}

func BuildNotificationCause() (value ngapType.NotificationCause) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildNotificationControl() (value ngapType.NotificationControl) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildNRCellIdentity() (value ngapType.NRCellIdentity) {
	value.Value.BitLength = 36
	value.Value.Bytes = []byte{171, 1, 2, 3, 4}
	value.Value.BitLength = 36
	value.Value.Bytes = []byte{0, 1, 2, 3, 4}
	return
}

func BuildNRencryptionAlgorithms() (value ngapType.NRencryptionAlgorithms) {
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{171, 1}
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{0, 1}
	return
}

func BuildNRintegrityProtectionAlgorithms() (value ngapType.NRintegrityProtectionAlgorithms) {
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{171, 1}
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{0, 1}
	return
}

func BuildNRMobilityHistoryReport() (value ngapType.NRMobilityHistoryReport) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildNRPPaPDU() (value ngapType.NRPPaPDU) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildNRUERLFReportContainer() (value ngapType.NRUERLFReportContainer) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildNumberOfBroadcasts() (value ngapType.NumberOfBroadcasts) {
	value.Value = 0
	value.Value = 65535
	return
}

func BuildNumberOfBroadcastsRequested() (value ngapType.NumberOfBroadcastsRequested) {
	value.Value = 0
	value.Value = 65535
	return
}

func BuildNRARFCN() (value ngapType.NRARFCN) {
	value.Value = 0
	value.Value = 3279165
	return
}

func BuildNRFrequencyBand() (value ngapType.NRFrequencyBand) {
	value.Value = 1
	value.Value = 1024
	return
}

func BuildNRPCI() (value ngapType.NRPCI) {
	value.Value = 0
	value.Value = 1007
	return
}

func BuildVehicleUE() (value ngapType.VehicleUE) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildPedestrianUE() (value ngapType.PedestrianUE) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildOverloadAction() (value ngapType.OverloadAction) {
	value.Value = 0
	value.Value = 3
	return
}

func BuildPacketDelayBudget() (value ngapType.PacketDelayBudget) {
	value.Value = 0
	value.Value = 1023
	return
}

func BuildPacketLossRate() (value ngapType.PacketLossRate) {
	value.Value = 0
	value.Value = 1000
	return
}

func BuildPagingAttemptCount() (value ngapType.PagingAttemptCount) {
	value.Value = 1
	value.Value = 16
	return
}

func BuildPagingDRX() (value ngapType.PagingDRX) {
	value.Value = 0
	value.Value = 3
	return
}

func BuildPagingOrigin() (value ngapType.PagingOrigin) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildPagingPriority() (value ngapType.PagingPriority) {
	value.Value = 0
	value.Value = 7
	return
}

func BuildPagingEDRXCycle() (value ngapType.PagingEDRXCycle) {
	value.Value = 0
	value.Value = 13
	return
}

func BuildPagingTimeWindow() (value ngapType.PagingTimeWindow) {
	value.Value = 0
	value.Value = 15
	return
}

func BuildPagingProbabilityInformation() (value ngapType.PagingProbabilityInformation) {
	value.Value = 0
	value.Value = 20
	return
}

func BuildPrivacyIndicator() (value ngapType.PrivacyIndicator) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildPDUSessionID() (value ngapType.PDUSessionID) {
	value.Value = 12
	return
}

func BuildPDUSessionType() (value ngapType.PDUSessionType) {
	value.Value = 0
	value.Value = 4
	return
}

func BuildRATType() (value ngapType.RATType) {
	value.Value = 0
	return
}

func BuildPeriodicity() (value ngapType.Periodicity) {
	value.Value = 0
	value.Value = 640000
	return
}

func BuildPeriodicRegistrationUpdateTimer() (value ngapType.PeriodicRegistrationUpdateTimer) {
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{171}
	return
}

func BuildPLMNIdentity() (value ngapType.PLMNIdentity) {
	value.Value = []byte{0, 1, 2}
	value.Value = []byte{0, 1, 2}
	return
}

func BuildPortNumber() (value ngapType.PortNumber) {
	value.Value = []byte{0, 1}
	value.Value = []byte{0, 1}
	return
}

func BuildPreEmptionCapability() (value ngapType.PreEmptionCapability) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildPreEmptionVulnerability() (value ngapType.PreEmptionVulnerability) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildPriorityLevelARP() (value ngapType.PriorityLevelARP) {
	value.Value = 1
	value.Value = 15
	return
}

func BuildPriorityLevelQos() (value ngapType.PriorityLevelQos) {
	value.Value = 1
	value.Value = 127
	return
}

func BuildQosFlowIdentifier() (value ngapType.QosFlowIdentifier) {
	value.Value = 0
	value.Value = 63
	return
}

func BuildQosMonitoringRequest() (value ngapType.QosMonitoringRequest) {
	value.Value = 0
	value.Value = 2
	return
}

func BuildRange() (value ngapType.Range) {
	value.Value = 0
	value.Value = 8
	return
}

func BuildRANNodeName() (value ngapType.RANNodeName) {
	value.Value = "ngap-vht5gc-PrintableString"
	return
}

func BuildRANNodeNameVisibleString() (value ngapType.RANNodeNameVisibleString) {
	value.Value = "ngap-vht5gc-VisibleString"
	return
}

func BuildRANNodeNameUTF8String() (value ngapType.RANNodeNameUTF8String) {
	value.Value = "ngap-vht5gc-UTF8String"
	return
}

func BuildRANPagingPriority() (value ngapType.RANPagingPriority) {
	value.Value = 1
	value.Value = 256
	return
}

func BuildRANUENGAPID() (value ngapType.RANUENGAPID) {
	value.Value = 0
	value.Value = 4294967295
	return
}

func BuildRATInformation() (value ngapType.RATInformation) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildRATRestrictionInformation() (value ngapType.RATRestrictionInformation) {
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{171}
	value.Value.BitLength = 8
	value.Value.Bytes = []byte{0}
	return
}

func BuildRedirectionVoiceFallback() (value ngapType.RedirectionVoiceFallback) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildRedundantQosFlowIndicator() (value ngapType.RedundantQosFlowIndicator) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildReflectiveQosAttribute() (value ngapType.ReflectiveQosAttribute) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildRelativeAMFCapacity() (value ngapType.RelativeAMFCapacity) {
	value.Value = 0
	value.Value = 255
	return
}

func BuildReportArea() (value ngapType.ReportArea) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildRepetitionPeriod() (value ngapType.RepetitionPeriod) {
	value.Value = 0
	value.Value = 131071
	return
}

func BuildResetAll() (value ngapType.ResetAll) {
	value.Value = 0
	return
}

func BuildReportAmountMDT() (value ngapType.ReportAmountMDT) {
	value.Value = 0
	value.Value = 7
	return
}

func BuildReportIntervalMDT() (value ngapType.ReportIntervalMDT) {
	value.Value = 0
	value.Value = 12
	return
}

func BuildRGLevelWirelineAccessCharacteristics() (value ngapType.RGLevelWirelineAccessCharacteristics) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildRNCID() (value ngapType.RNCID) {
	value.Value = 0
	value.Value = 4095
	return
}

func BuildRoutingID() (value ngapType.RoutingID) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildRRCContainer() (value ngapType.RRCContainer) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildRRCEstablishmentCause() (value ngapType.RRCEstablishmentCause) {
	value.Value = 0
	value.Value = 11
	return
}

func BuildRRCInactiveTransitionReportRequest() (value ngapType.RRCInactiveTransitionReportRequest) {
	value.Value = 0
	value.Value = 2
	return
}

func BuildRRCState() (value ngapType.RRCState) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildRSN() (value ngapType.RSN) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildRIMRSDetection() (value ngapType.RIMRSDetection) {
	value.Value = 0
	return
}

func BuildGNBSetID() (value ngapType.GNBSetID) {
	value.Value.BitLength = 22
	value.Value.Bytes = []byte{171, 1, 2}
	value.Value.BitLength = 22
	value.Value.Bytes = []byte{0, 1, 2}
	return
}

func BuildSD() (value ngapType.SD) {
	value.Value = []byte{0, 1, 2}
	value.Value = []byte{0, 1, 2}
	return
}

func BuildSecurityKey() (value ngapType.SecurityKey) {
	value.Value.BitLength = 256
	value.Value.Bytes = []byte{171, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31}
	return
}

func BuildSensorMeasConfig() (value ngapType.SensorMeasConfig) {
	value.Value = 0
	return
}

func BuildUncompensatedBarometricConfig() (value ngapType.UncompensatedBarometricConfig) {
	value.Value = 0
	return
}

func BuildUeSpeedConfig() (value ngapType.UeSpeedConfig) {
	value.Value = 0
	return
}

func BuildUeOrientationConfig() (value ngapType.UeOrientationConfig) {
	value.Value = 0
	return
}

func BuildSerialNumber() (value ngapType.SerialNumber) {
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{171, 183}
	return
}

func BuildSgNBUEX2APID() (value ngapType.SgNBUEX2APID) {
	value.Value = 0
	value.Value = 4294967295
	return
}

func BuildSONInformationRequest() (value ngapType.SONInformationRequest) {
	value.Value = 0
	return
}

func BuildSourceOfUEActivityBehaviourInformation() (value ngapType.SourceOfUEActivityBehaviourInformation) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildSourceToTargetTransparentContainer() (value ngapType.SourceToTargetTransparentContainer) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildSRVCCOperationPossible() (value ngapType.SRVCCOperationPossible) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildConfiguredNSSAI() (value ngapType.ConfiguredNSSAI) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127}
	return
}

func BuildRejectedNSSAIinPLMN() (value ngapType.RejectedNSSAIinPLMN) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31}
	return
}

func BuildRejectedNSSAIinTA() (value ngapType.RejectedNSSAIinTA) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31}
	return
}

func BuildSST() (value ngapType.SST) {
	value.Value = []byte{171}
	return
}

func BuildSuspendIndicator() (value ngapType.SuspendIndicator) {
	value.Value = 0
	return
}

func BuildSuspendRequestIndication() (value ngapType.SuspendRequestIndication) {
	value.Value = 0
	return
}

func BuildSuspendResponseIndication() (value ngapType.SuspendResponseIndication) {
	value.Value = 0
	return
}

func BuildTAC() (value ngapType.TAC) {
	value.Value = []byte{0, 1, 2}
	return
}

func BuildTargetToSourceTransparentContainer() (value ngapType.TargetToSourceTransparentContainer) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildTargettoSourceFailureTransparentContainer() (value ngapType.TargettoSourceFailureTransparentContainer) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildTimerApproachForGUAMIRemoval() (value ngapType.TimerApproachForGUAMIRemoval) {
	value.Value = 0
	return
}

func BuildTimeStamp() (value ngapType.TimeStamp) {
	value.Value = []byte{0, 1, 2, 3}
	return
}

func BuildTimeToWait() (value ngapType.TimeToWait) {
	value.Value = 0
	value.Value = 5
	return
}

func BuildTimeUEStayedInCell() (value ngapType.TimeUEStayedInCell) {
	value.Value = 0
	value.Value = 4095
	return
}

func BuildTimeUEStayedInCellEnhancedGranularity() (value ngapType.TimeUEStayedInCellEnhancedGranularity) {
	value.Value = 0
	value.Value = 40950
	return
}

func BuildTNAPID() (value ngapType.TNAPID) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildTNLAddressWeightFactor() (value ngapType.TNLAddressWeightFactor) {
	value.Value = 0
	value.Value = 255
	return
}

func BuildTNLAssociationUsage() (value ngapType.TNLAssociationUsage) {
	value.Value = 0
	value.Value = 2
	return
}

func BuildTraceDepth() (value ngapType.TraceDepth) {
	value.Value = 0
	value.Value = 5
	return
}

func BuildTrafficLoadReductionIndication() (value ngapType.TrafficLoadReductionIndication) {
	value.Value = 1
	value.Value = 99
	return
}

func BuildTransportLayerAddress() (value ngapType.TransportLayerAddress) {
	value.Value.BitLength = 1
	value.Value.Bytes = []byte{171}
	value.Value.BitLength = 160
	value.Value.Bytes = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19}
	return
}

func BuildTypeOfError() (value ngapType.TypeOfError) {
	value.Value = 0
	value.Value = 1
	return
}

func BuildThresholdRSRP() (value ngapType.ThresholdRSRP) {
	value.Value = 0
	value.Value = 127
	return
}

func BuildThresholdRSRQ() (value ngapType.ThresholdRSRQ) {
	value.Value = 0
	value.Value = 127
	return
}

func BuildThresholdSINR() (value ngapType.ThresholdSINR) {
	value.Value = 0
	value.Value = 127
	return
}

func BuildTimeToTrigger() (value ngapType.TimeToTrigger) {
	value.Value = 0
	value.Value = 15
	return
}

func BuildTWAPID() (value ngapType.TWAPID) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildUECapabilityInfoRequest() (value ngapType.UECapabilityInfoRequest) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildUEContextRequest() (value ngapType.UEContextRequest) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildPeriodicCommunicationIndicator() (value ngapType.PeriodicCommunicationIndicator) {
	value.Value = 0
	return
}

func BuildStationaryIndication() (value ngapType.StationaryIndication) {
	value.Value = 0
	return
}

func BuildTrafficProfile() (value ngapType.TrafficProfile) {
	value.Value = 0
	return
}

func BuildBatteryIndication() (value ngapType.BatteryIndication) {
	value.Value = 0
	return
}

func BuildUEPresence() (value ngapType.UEPresence) {
	value.Value = 0
	value.Value = 2
	return
}

func BuildUERadioCapability() (value ngapType.UERadioCapability) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildUERadioCapabilityForPagingOfNBIoT() (value ngapType.UERadioCapabilityForPagingOfNBIoT) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildUERadioCapabilityForPagingOfNR() (value ngapType.UERadioCapabilityForPagingOfNR) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildUERadioCapabilityForPagingOfEUTRA() (value ngapType.UERadioCapabilityForPagingOfEUTRA) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildUERadioCapabilityID() (value ngapType.UERadioCapabilityID) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	return
}

func BuildUERetentionInformation() (value ngapType.UERetentionInformation) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildUEUPCIoTSupport() (value ngapType.UEUPCIoTSupport) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildULNASMAC() (value ngapType.ULNASMAC) {
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{171, 1}
	value.Value.BitLength = 16
	value.Value.Bytes = []byte{0, 1}
	return
}

func BuildULNASCount() (value ngapType.ULNASCount) {
	value.Value.BitLength = 5
	value.Value.Bytes = []byte{171}
	value.Value.BitLength = 5
	value.Value.Bytes = []byte{0}
	return
}

func BuildULForwarding() (value ngapType.ULForwarding) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildURIAddress() (value ngapType.URIAddress) {
	value.Value = "ngap-vht5gc-VisibleString"
	return
}

func BuildWarningAreaCoordinates() (value ngapType.WarningAreaCoordinates) {
	value.Value = []byte{0}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49}
	return
}

func BuildWarningMessageContents() (value ngapType.WarningMessageContents) {
	value.Value = []byte{0}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49}
	return
}

func BuildWarningSecurityInfo() (value ngapType.WarningSecurityInfo) {
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49}
	return
}

func BuildWarningType() (value ngapType.WarningType) {
	value.Value = []byte{0, 1}
	value.Value = []byte{0, 1}
	return
}

func BuildWlanRssi() (value ngapType.WlanRssi) {
	value.Value = 0
	return
}

func BuildWlanRtt() (value ngapType.WlanRtt) {
	value.Value = 0
	return
}

func BuildWLANMeasConfig() (value ngapType.WLANMeasConfig) {
	value.Value = 0
	value.Value = 0
	return
}

func BuildWLANName() (value ngapType.WLANName) {
	value.Value = []byte{0}
	value.Value = []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31}
	return
}

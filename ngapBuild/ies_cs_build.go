package ngapBuild

import (
	"ngap"
	"ngap/ngapType"
)

func BuildAdditionalDLUPTNLInformationForHOList() (value ngapType.AdditionalDLUPTNLInformationForHOList) {
	item := BuildAdditionalDLUPTNLInformationForHOItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAdditionalDLUPTNLInformationForHOItem() (value ngapType.AdditionalDLUPTNLInformationForHOItem) {
	value.AdditionalDLNGUUPTNLInformation = BuildUPTransportLayerInformation()
	value.AdditionalQosFlowSetupResponseList = BuildQosFlowListWithDataForwarding()
	value.AdditionalDLForwardingUPTNLInformation = new(ngapType.UPTransportLayerInformation)
	*value.AdditionalDLForwardingUPTNLInformation = BuildUPTransportLayerInformation()
	return
}

func BuildAllocationAndRetentionPriority() (value ngapType.AllocationAndRetentionPriority) {
	value.PriorityLevelARP = BuildPriorityLevelARP()
	value.PreEmptionCapability = BuildPreEmptionCapability()
	value.PreEmptionVulnerability = BuildPreEmptionVulnerability()
	return
}

func BuildAllowedCAGListPerPLMN() (value ngapType.AllowedCAGListPerPLMN) {
	item := BuildCAGID()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAllowedNSSAI() (value ngapType.AllowedNSSAI) {
	item := BuildAllowedNSSAIItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAllowedNSSAIItem() (value ngapType.AllowedNSSAIItem) {
	value.SNSSAI = BuildSNSSAI()
	return
}

func BuildAllowedPNINPNList() (value ngapType.AllowedPNINPNList) {
	item := BuildAllowedPNINPNItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAllowedPNINPNItem() (value ngapType.AllowedPNINPNItem) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.PNINPNRestricted = BuildPNINPNRestricted()
	value.AllowedCAGListPerPLMN = BuildAllowedCAGListPerPLMN()
	return
}

func BuildAllowedTACs() (value ngapType.AllowedTACs) {
	item := BuildTAC()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAlternativeQoSParaSetList() (value ngapType.AlternativeQoSParaSetList) {
	item := BuildAlternativeQoSParaSetItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAlternativeQoSParaSetItem() (value ngapType.AlternativeQoSParaSetItem) {
	value.AlternativeQoSParaSetIndex = BuildAlternativeQoSParaSetIndex()
	value.GuaranteedFlowBitRateDL = new(ngapType.BitRate)
	*value.GuaranteedFlowBitRateDL = BuildBitRate()
	value.GuaranteedFlowBitRateUL = new(ngapType.BitRate)
	*value.GuaranteedFlowBitRateUL = BuildBitRate()
	value.PacketDelayBudget = new(ngapType.PacketDelayBudget)
	*value.PacketDelayBudget = BuildPacketDelayBudget()
	value.PacketErrorRate = new(ngapType.PacketErrorRate)
	*value.PacketErrorRate = BuildPacketErrorRate()
	return
}

func BuildAMFPagingTarget() (value ngapType.AMFPagingTarget) {
	value.Present = ngapType.AMFPagingTargetPresentGlobalRANNodeID
	value.GlobalRANNodeID = new(ngapType.GlobalRANNodeID)
	*value.GlobalRANNodeID = BuildGlobalRANNodeID()
	value.Present = ngapType.AMFPagingTargetPresentTAI
	value.TAI = new(ngapType.TAI)
	*value.TAI = BuildTAI()
	return
}

func BuildAMFTNLAssociationSetupList() (value ngapType.AMFTNLAssociationSetupList) {
	item := BuildAMFTNLAssociationSetupItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAMFTNLAssociationSetupItem() (value ngapType.AMFTNLAssociationSetupItem) {
	value.AMFTNLAssociationAddress = BuildCPTransportLayerInformation()
	return
}

func BuildAMFTNLAssociationToAddList() (value ngapType.AMFTNLAssociationToAddList) {
	item := BuildAMFTNLAssociationToAddItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAMFTNLAssociationToAddItem() (value ngapType.AMFTNLAssociationToAddItem) {
	value.AMFTNLAssociationAddress = BuildCPTransportLayerInformation()
	value.TNLAssociationUsage = new(ngapType.TNLAssociationUsage)
	*value.TNLAssociationUsage = BuildTNLAssociationUsage()
	value.TNLAddressWeightFactor = BuildTNLAddressWeightFactor()
	return
}

func BuildAMFTNLAssociationToRemoveList() (value ngapType.AMFTNLAssociationToRemoveList) {
	item := BuildAMFTNLAssociationToRemoveItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAMFTNLAssociationToRemoveItem() (value ngapType.AMFTNLAssociationToRemoveItem) {
	value.AMFTNLAssociationAddress = BuildCPTransportLayerInformation()
	return
}

func BuildAMFTNLAssociationToUpdateList() (value ngapType.AMFTNLAssociationToUpdateList) {
	item := BuildAMFTNLAssociationToUpdateItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAMFTNLAssociationToUpdateItem() (value ngapType.AMFTNLAssociationToUpdateItem) {
	value.AMFTNLAssociationAddress = BuildCPTransportLayerInformation()
	value.TNLAssociationUsage = new(ngapType.TNLAssociationUsage)
	*value.TNLAssociationUsage = BuildTNLAssociationUsage()
	value.TNLAddressWeightFactor = new(ngapType.TNLAddressWeightFactor)
	*value.TNLAddressWeightFactor = BuildTNLAddressWeightFactor()
	return
}

func BuildAreaOfInterest() (value ngapType.AreaOfInterest) {
	value.AreaOfInterestTAIList = new(ngapType.AreaOfInterestTAIList)
	*value.AreaOfInterestTAIList = BuildAreaOfInterestTAIList()
	value.AreaOfInterestCellList = new(ngapType.AreaOfInterestCellList)
	*value.AreaOfInterestCellList = BuildAreaOfInterestCellList()
	value.AreaOfInterestRANNodeList = new(ngapType.AreaOfInterestRANNodeList)
	*value.AreaOfInterestRANNodeList = BuildAreaOfInterestRANNodeList()
	return
}

func BuildAreaOfInterestCellList() (value ngapType.AreaOfInterestCellList) {
	item := BuildAreaOfInterestCellItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAreaOfInterestCellItem() (value ngapType.AreaOfInterestCellItem) {
	value.NGRANCGI = BuildNGRANCGI()
	return
}

func BuildAreaOfInterestList() (value ngapType.AreaOfInterestList) {
	item := BuildAreaOfInterestItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAreaOfInterestItem() (value ngapType.AreaOfInterestItem) {
	value.AreaOfInterest = BuildAreaOfInterest()
	value.LocationReportingReferenceID = BuildLocationReportingReferenceID()
	return
}

func BuildAreaOfInterestRANNodeList() (value ngapType.AreaOfInterestRANNodeList) {
	item := BuildAreaOfInterestRANNodeItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAreaOfInterestRANNodeItem() (value ngapType.AreaOfInterestRANNodeItem) {
	value.GlobalRANNodeID = BuildGlobalRANNodeID()
	return
}

func BuildAreaOfInterestTAIList() (value ngapType.AreaOfInterestTAIList) {
	item := BuildAreaOfInterestTAIItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAreaOfInterestTAIItem() (value ngapType.AreaOfInterestTAIItem) {
	value.TAI = BuildTAI()
	return
}

func BuildAssistanceDataForPaging() (value ngapType.AssistanceDataForPaging) {
	value.AssistanceDataForRecommendedCells = new(ngapType.AssistanceDataForRecommendedCells)
	*value.AssistanceDataForRecommendedCells = BuildAssistanceDataForRecommendedCells()
	value.PagingAttemptInformation = new(ngapType.PagingAttemptInformation)
	*value.PagingAttemptInformation = BuildPagingAttemptInformation()
	return
}

func BuildAssistanceDataForRecommendedCells() (value ngapType.AssistanceDataForRecommendedCells) {
	value.RecommendedCellsForPaging = BuildRecommendedCellsForPaging()
	return
}

func BuildAssociatedQosFlowList() (value ngapType.AssociatedQosFlowList) {
	item := BuildAssociatedQosFlowItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAssociatedQosFlowItem() (value ngapType.AssociatedQosFlowItem) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	value.QosFlowMappingIndication = new(ngapType.QosFlowMappingIndication)
	*value.QosFlowMappingIndication = BuildQosFlowMappingIndication()
	return
}

func BuildAreaScopeOfMDTNR() (value ngapType.AreaScopeOfMDTNR) {
	value.Present = ngapType.AreaScopeOfMDTNRPresentCellBased
	value.CellBased = new(ngapType.CellBasedMDTNR)
	*value.CellBased = BuildCellBasedMDTNR()
	value.Present = ngapType.AreaScopeOfMDTNRPresentTABased
	value.TABased = new(ngapType.TABasedMDT)
	*value.TABased = BuildTABasedMDT()
	value.Present = ngapType.AreaScopeOfMDTNRPresentTAIBased
	value.TAIBased = new(ngapType.TAIBasedMDT)
	*value.TAIBased = BuildTAIBasedMDT()
	return
}

func BuildAreaScopeOfMDTEUTRA() (value ngapType.AreaScopeOfMDTEUTRA) {
	value.Present = ngapType.AreaScopeOfMDTEUTRAPresentCellBased
	value.CellBased = new(ngapType.CellBasedMDTEUTRA)
	*value.CellBased = BuildCellBasedMDTEUTRA()
	value.Present = ngapType.AreaScopeOfMDTEUTRAPresentTABased
	value.TABased = new(ngapType.TABasedMDT)
	*value.TABased = BuildTABasedMDT()
	value.Present = ngapType.AreaScopeOfMDTEUTRAPresentTAIBased
	value.TAIBased = new(ngapType.TAIBasedMDT)
	*value.TAIBased = BuildTAIBasedMDT()
	return
}

func BuildAreaScopeOfNeighCellsList() (value ngapType.AreaScopeOfNeighCellsList) {
	item := BuildAreaScopeOfNeighCellsItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildAreaScopeOfNeighCellsItem() (value ngapType.AreaScopeOfNeighCellsItem) {
	value.NrFrequencyInfo = BuildNRFrequencyInfo()
	value.PciListForMDT = new(ngapType.PCIListForMDT)
	*value.PciListForMDT = BuildPCIListForMDT()
	return
}

func BuildBroadcastCancelledAreaList() (value ngapType.BroadcastCancelledAreaList) {
	value.Present = ngapType.BroadcastCancelledAreaListPresentCellIDCancelledEUTRA
	value.CellIDCancelledEUTRA = new(ngapType.CellIDCancelledEUTRA)
	*value.CellIDCancelledEUTRA = BuildCellIDCancelledEUTRA()
	value.Present = ngapType.BroadcastCancelledAreaListPresentTAICancelledEUTRA
	value.TAICancelledEUTRA = new(ngapType.TAICancelledEUTRA)
	*value.TAICancelledEUTRA = BuildTAICancelledEUTRA()
	value.Present = ngapType.BroadcastCancelledAreaListPresentEmergencyAreaIDCancelledEUTRA
	value.EmergencyAreaIDCancelledEUTRA = new(ngapType.EmergencyAreaIDCancelledEUTRA)
	*value.EmergencyAreaIDCancelledEUTRA = BuildEmergencyAreaIDCancelledEUTRA()
	value.Present = ngapType.BroadcastCancelledAreaListPresentCellIDCancelledNR
	value.CellIDCancelledNR = new(ngapType.CellIDCancelledNR)
	*value.CellIDCancelledNR = BuildCellIDCancelledNR()
	value.Present = ngapType.BroadcastCancelledAreaListPresentTAICancelledNR
	value.TAICancelledNR = new(ngapType.TAICancelledNR)
	*value.TAICancelledNR = BuildTAICancelledNR()
	value.Present = ngapType.BroadcastCancelledAreaListPresentEmergencyAreaIDCancelledNR
	value.EmergencyAreaIDCancelledNR = new(ngapType.EmergencyAreaIDCancelledNR)
	*value.EmergencyAreaIDCancelledNR = BuildEmergencyAreaIDCancelledNR()
	return
}

func BuildBroadcastCompletedAreaList() (value ngapType.BroadcastCompletedAreaList) {
	value.Present = ngapType.BroadcastCompletedAreaListPresentCellIDBroadcastEUTRA
	value.CellIDBroadcastEUTRA = new(ngapType.CellIDBroadcastEUTRA)
	*value.CellIDBroadcastEUTRA = BuildCellIDBroadcastEUTRA()
	value.Present = ngapType.BroadcastCompletedAreaListPresentTAIBroadcastEUTRA
	value.TAIBroadcastEUTRA = new(ngapType.TAIBroadcastEUTRA)
	*value.TAIBroadcastEUTRA = BuildTAIBroadcastEUTRA()
	value.Present = ngapType.BroadcastCompletedAreaListPresentEmergencyAreaIDBroadcastEUTRA
	value.EmergencyAreaIDBroadcastEUTRA = new(ngapType.EmergencyAreaIDBroadcastEUTRA)
	*value.EmergencyAreaIDBroadcastEUTRA = BuildEmergencyAreaIDBroadcastEUTRA()
	value.Present = ngapType.BroadcastCompletedAreaListPresentCellIDBroadcastNR
	value.CellIDBroadcastNR = new(ngapType.CellIDBroadcastNR)
	*value.CellIDBroadcastNR = BuildCellIDBroadcastNR()
	value.Present = ngapType.BroadcastCompletedAreaListPresentTAIBroadcastNR
	value.TAIBroadcastNR = new(ngapType.TAIBroadcastNR)
	*value.TAIBroadcastNR = BuildTAIBroadcastNR()
	value.Present = ngapType.BroadcastCompletedAreaListPresentEmergencyAreaIDBroadcastNR
	value.EmergencyAreaIDBroadcastNR = new(ngapType.EmergencyAreaIDBroadcastNR)
	*value.EmergencyAreaIDBroadcastNR = BuildEmergencyAreaIDBroadcastNR()
	return
}

func BuildBroadcastPLMNList() (value ngapType.BroadcastPLMNList) {
	item := BuildBroadcastPLMNItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildBroadcastPLMNItem() (value ngapType.BroadcastPLMNItem) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.TAISliceSupportList = BuildSliceSupportList()
	return
}

func BuildBluetoothMeasurementConfiguration() (value ngapType.BluetoothMeasurementConfiguration) {
	value.BluetoothMeasConfig = BuildBluetoothMeasConfig()
	value.BluetoothMeasConfigNameList = new(ngapType.BluetoothMeasConfigNameList)
	*value.BluetoothMeasConfigNameList = BuildBluetoothMeasConfigNameList()
	value.BtRssi = new(ngapType.BtRssi)
	*value.BtRssi = BuildBtRssi()
	return
}

func BuildBluetoothMeasConfigNameList() (value ngapType.BluetoothMeasConfigNameList) {
	item := BuildBluetoothMeasConfigNameItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildBluetoothMeasConfigNameItem() (value ngapType.BluetoothMeasConfigNameItem) {
	value.BluetoothName = BuildBluetoothName()
	return
}

func BuildCancelledCellsInEAIEUTRA() (value ngapType.CancelledCellsInEAIEUTRA) {
	item := BuildCancelledCellsInEAIEUTRAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCancelledCellsInEAIEUTRAItem() (value ngapType.CancelledCellsInEAIEUTRAItem) {
	value.EUTRACGI = BuildEUTRACGI()
	value.NumberOfBroadcasts = BuildNumberOfBroadcasts()
	return
}

func BuildCancelledCellsInEAINR() (value ngapType.CancelledCellsInEAINR) {
	item := BuildCancelledCellsInEAINRItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCancelledCellsInEAINRItem() (value ngapType.CancelledCellsInEAINRItem) {
	value.NRCGI = BuildNRCGI()
	value.NumberOfBroadcasts = BuildNumberOfBroadcasts()
	return
}

func BuildCancelledCellsInTAIEUTRA() (value ngapType.CancelledCellsInTAIEUTRA) {
	item := BuildCancelledCellsInTAIEUTRAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCancelledCellsInTAIEUTRAItem() (value ngapType.CancelledCellsInTAIEUTRAItem) {
	value.EUTRACGI = BuildEUTRACGI()
	value.NumberOfBroadcasts = BuildNumberOfBroadcasts()
	return
}

func BuildCancelledCellsInTAINR() (value ngapType.CancelledCellsInTAINR) {
	item := BuildCancelledCellsInTAINRItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCancelledCellsInTAINRItem() (value ngapType.CancelledCellsInTAINRItem) {
	value.NRCGI = BuildNRCGI()
	value.NumberOfBroadcasts = BuildNumberOfBroadcasts()
	return
}

func BuildCandidateCellList() (value ngapType.CandidateCellList) {
	item := BuildCandidateCellItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCandidateCellItem() (value ngapType.CandidateCellItem) {
	value.CandidateCell = BuildCandidateCell()
	return
}

func BuildCandidateCell() (value ngapType.CandidateCell) {
	value.Present = ngapType.CandidateCellPresentCandidateCGI
	value.CandidateCGI = new(ngapType.CandidateCellID)
	*value.CandidateCGI = BuildCandidateCellID()
	value.Present = ngapType.CandidateCellPresentCandidatePCI
	value.CandidatePCI = new(ngapType.CandidatePCI)
	*value.CandidatePCI = BuildCandidatePCI()
	return
}

func BuildCandidateCellID() (value ngapType.CandidateCellID) {
	value.CandidateCellID = BuildNRCGI()
	return
}

func BuildCandidatePCI() (value ngapType.CandidatePCI) {
	value.CandidatePCI = 0
	value.CandidatePCI = 1007
	value.CandidateNRARFCN = 0
	value.CandidateNRARFCN = 3279165
	return
}

func BuildCause() (value ngapType.Cause) {
	value.Present = ngapType.CausePresentRadioNetwork
	value.RadioNetwork = new(ngapType.CauseRadioNetwork)
	*value.RadioNetwork = BuildCauseRadioNetwork()
	value.Present = ngapType.CausePresentTransport
	value.Transport = new(ngapType.CauseTransport)
	*value.Transport = BuildCauseTransport()
	value.Present = ngapType.CausePresentNas
	value.Nas = new(ngapType.CauseNas)
	*value.Nas = BuildCauseNas()
	value.Present = ngapType.CausePresentProtocol
	value.Protocol = new(ngapType.CauseProtocol)
	*value.Protocol = BuildCauseProtocol()
	value.Present = ngapType.CausePresentMisc
	value.Misc = new(ngapType.CauseMisc)
	*value.Misc = BuildCauseMisc()
	return
}

func BuildCellCAGInformation() (value ngapType.CellCAGInformation) {
	value.NGRANCGI = BuildNGRANCGI()
	value.CellCAGList = BuildCellCAGList()
	return
}

func BuildCellCAGList() (value ngapType.CellCAGList) {
	item := BuildCAGID()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCellIDBroadcastEUTRA() (value ngapType.CellIDBroadcastEUTRA) {
	item := BuildCellIDBroadcastEUTRAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCellIDBroadcastEUTRAItem() (value ngapType.CellIDBroadcastEUTRAItem) {
	value.EUTRACGI = BuildEUTRACGI()
	return
}

func BuildCellIDBroadcastNR() (value ngapType.CellIDBroadcastNR) {
	item := BuildCellIDBroadcastNRItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCellIDBroadcastNRItem() (value ngapType.CellIDBroadcastNRItem) {
	value.NRCGI = BuildNRCGI()
	return
}

func BuildCellIDCancelledEUTRA() (value ngapType.CellIDCancelledEUTRA) {
	item := BuildCellIDCancelledEUTRAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCellIDCancelledEUTRAItem() (value ngapType.CellIDCancelledEUTRAItem) {
	value.EUTRACGI = BuildEUTRACGI()
	value.NumberOfBroadcasts = BuildNumberOfBroadcasts()
	return
}

func BuildCellIDCancelledNR() (value ngapType.CellIDCancelledNR) {
	item := BuildCellIDCancelledNRItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCellIDCancelledNRItem() (value ngapType.CellIDCancelledNRItem) {
	value.NRCGI = BuildNRCGI()
	value.NumberOfBroadcasts = BuildNumberOfBroadcasts()
	return
}

func BuildCellIDListForRestart() (value ngapType.CellIDListForRestart) {
	value.Present = ngapType.CellIDListForRestartPresentEUTRACGIListforRestart
	value.EUTRACGIListforRestart = new(ngapType.EUTRACGIList)
	*value.EUTRACGIListforRestart = BuildEUTRACGIList()
	value.Present = ngapType.CellIDListForRestartPresentNRCGIListforRestart
	value.NRCGIListforRestart = new(ngapType.NRCGIList)
	*value.NRCGIListforRestart = BuildNRCGIList()
	return
}

func BuildCellType() (value ngapType.CellType) {
	value.CellSize = BuildCellSize()
	return
}

func BuildCNAssistedRANTuning() (value ngapType.CNAssistedRANTuning) {
	value.ExpectedUEBehaviour = new(ngapType.ExpectedUEBehaviour)
	*value.ExpectedUEBehaviour = BuildExpectedUEBehaviour()
	return
}

func BuildCNTypeRestrictionsForEquivalent() (value ngapType.CNTypeRestrictionsForEquivalent) {
	item := BuildCNTypeRestrictionsForEquivalentItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCNTypeRestrictionsForEquivalentItem() (value ngapType.CNTypeRestrictionsForEquivalentItem) {
	value.PlmnIdentity = BuildPLMNIdentity()
	value.CnType = BuildCnType()
	return
}

func BuildCompletedCellsInEAIEUTRA() (value ngapType.CompletedCellsInEAIEUTRA) {
	item := BuildCompletedCellsInEAIEUTRAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCompletedCellsInEAIEUTRAItem() (value ngapType.CompletedCellsInEAIEUTRAItem) {
	value.EUTRACGI = BuildEUTRACGI()
	return
}

func BuildCompletedCellsInEAINR() (value ngapType.CompletedCellsInEAINR) {
	item := BuildCompletedCellsInEAINRItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCompletedCellsInEAINRItem() (value ngapType.CompletedCellsInEAINRItem) {
	value.NRCGI = BuildNRCGI()
	return
}

func BuildCompletedCellsInTAIEUTRA() (value ngapType.CompletedCellsInTAIEUTRA) {
	item := BuildCompletedCellsInTAIEUTRAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCompletedCellsInTAIEUTRAItem() (value ngapType.CompletedCellsInTAIEUTRAItem) {
	value.EUTRACGI = BuildEUTRACGI()
	return
}

func BuildCompletedCellsInTAINR() (value ngapType.CompletedCellsInTAINR) {
	item := BuildCompletedCellsInTAINRItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCompletedCellsInTAINRItem() (value ngapType.CompletedCellsInTAINRItem) {
	value.NRCGI = BuildNRCGI()
	return
}

func BuildCoreNetworkAssistanceInformationForInactive() (value ngapType.CoreNetworkAssistanceInformationForInactive) {
	value.UEIdentityIndexValue = BuildUEIdentityIndexValue()
	value.UESpecificDRX = new(ngapType.PagingDRX)
	*value.UESpecificDRX = BuildPagingDRX()
	value.PeriodicRegistrationUpdateTimer = BuildPeriodicRegistrationUpdateTimer()
	value.MICOModeIndication = new(ngapType.MICOModeIndication)
	*value.MICOModeIndication = BuildMICOModeIndication()
	value.TAIListForInactive = BuildTAIListForInactive()
	value.ExpectedUEBehaviour = new(ngapType.ExpectedUEBehaviour)
	*value.ExpectedUEBehaviour = BuildExpectedUEBehaviour()
	return
}

func BuildCOUNTValueForPDCPSN12() (value ngapType.COUNTValueForPDCPSN12) {
	value.PDCPSN12 = 0
	value.PDCPSN12 = 4095
	value.HFNPDCPSN12 = 0
	value.HFNPDCPSN12 = 1048575
	return
}

func BuildCOUNTValueForPDCPSN18() (value ngapType.COUNTValueForPDCPSN18) {
	value.PDCPSN18 = 0
	value.PDCPSN18 = 262143
	value.HFNPDCPSN18 = 0
	value.HFNPDCPSN18 = 16383
	return
}

func BuildCPTransportLayerInformation() (value ngapType.CPTransportLayerInformation) {
	value.Present = ngapType.CPTransportLayerInformationPresentEndpointIPAddress
	value.EndpointIPAddress = new(ngapType.TransportLayerAddress)
	*value.EndpointIPAddress = BuildTransportLayerAddress()
	return
}

func BuildCriticalityDiagnostics() (value ngapType.CriticalityDiagnostics) {
	value.ProcedureCode = new(ngapType.ProcedureCode)
	*value.ProcedureCode = BuildProcedureCode()
	value.TriggeringMessage = new(ngapType.TriggeringMessage)
	*value.TriggeringMessage = BuildTriggeringMessage()
	value.ProcedureCriticality = new(ngapType.Criticality)
	*value.ProcedureCriticality = BuildCriticality()
	value.IEsCriticalityDiagnostics = new(ngapType.CriticalityDiagnosticsIEList)
	*value.IEsCriticalityDiagnostics = BuildCriticalityDiagnosticsIEList()
	return
}

func BuildCriticalityDiagnosticsIEList() (value ngapType.CriticalityDiagnosticsIEList) {
	item := BuildCriticalityDiagnosticsIEItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCriticalityDiagnosticsIEItem() (value ngapType.CriticalityDiagnosticsIEItem) {
	value.IECriticality = BuildCriticality()
	value.IEID = BuildProtocolIEID()
	value.TypeOfError = BuildTypeOfError()
	return
}

func BuildCellBasedMDTNR() (value ngapType.CellBasedMDTNR) {
	value.CellIdListforMDT = BuildCellIdListforMDTNR()
	return
}

func BuildCellIdListforMDTNR() (value ngapType.CellIdListforMDTNR) {
	item := BuildNRCGI()
	value.List = append(value.List, item, item, item)
	return
}

func BuildCellBasedMDTEUTRA() (value ngapType.CellBasedMDTEUTRA) {
	value.CellIdListforMDT = BuildCellIdListforMDTEUTRA()
	return
}

func BuildCellIdListforMDTEUTRA() (value ngapType.CellIdListforMDTEUTRA) {
	item := BuildEUTRACGI()
	value.List = append(value.List, item, item, item)
	return
}

func BuildDataForwardingResponseDRBList() (value ngapType.DataForwardingResponseDRBList) {
	item := BuildDataForwardingResponseDRBItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildDataForwardingResponseDRBItem() (value ngapType.DataForwardingResponseDRBItem) {
	value.DRBID = BuildDRBID()
	value.DLForwardingUPTNLInformation = new(ngapType.UPTransportLayerInformation)
	*value.DLForwardingUPTNLInformation = BuildUPTransportLayerInformation()
	value.ULForwardingUPTNLInformation = new(ngapType.UPTransportLayerInformation)
	*value.ULForwardingUPTNLInformation = BuildUPTransportLayerInformation()
	return
}

func BuildDAPSRequestInfo() (value ngapType.DAPSRequestInfo) {
	value.DAPSIndicator = BuildDAPSIndicator()
	return
}

func BuildDAPSResponseInfoList() (value ngapType.DAPSResponseInfoList) {
	item := BuildDAPSResponseInfoItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildDAPSResponseInfoItem() (value ngapType.DAPSResponseInfoItem) {
	value.DRBID = BuildDRBID()
	value.DAPSResponseInfo = BuildDAPSResponseInfo()
	return
}

func BuildDAPSResponseInfo() (value ngapType.DAPSResponseInfo) {
	value.Dapsresponseindicator = BuildDapsresponseindicator()
	return
}

func BuildDataForwardingResponseERABList() (value ngapType.DataForwardingResponseERABList) {
	item := BuildDataForwardingResponseERABListItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildDataForwardingResponseERABListItem() (value ngapType.DataForwardingResponseERABListItem) {
	value.ERABID = BuildERABID()
	value.DLForwardingUPTNLInformation = BuildUPTransportLayerInformation()
	return
}

func BuildDLCPSecurityInformation() (value ngapType.DLCPSecurityInformation) {
	value.DlNASMAC = BuildDLNASMAC()
	return
}

func BuildDRBsSubjectToStatusTransferList() (value ngapType.DRBsSubjectToStatusTransferList) {
	item := BuildDRBsSubjectToStatusTransferItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildDRBsSubjectToStatusTransferItem() (value ngapType.DRBsSubjectToStatusTransferItem) {
	value.DRBID = BuildDRBID()
	value.DRBStatusUL = BuildDRBStatusUL()
	value.DRBStatusDL = BuildDRBStatusDL()
	return
}

func BuildDRBStatusDL() (value ngapType.DRBStatusDL) {
	value.Present = ngapType.DRBStatusDLPresentDRBStatusDL12
	value.DRBStatusDL12 = new(ngapType.DRBStatusDL12)
	*value.DRBStatusDL12 = BuildDRBStatusDL12()
	value.Present = ngapType.DRBStatusDLPresentDRBStatusDL18
	value.DRBStatusDL18 = new(ngapType.DRBStatusDL18)
	*value.DRBStatusDL18 = BuildDRBStatusDL18()
	return
}

func BuildDRBStatusDL12() (value ngapType.DRBStatusDL12) {
	value.DLCOUNTValue = BuildCOUNTValueForPDCPSN12()
	return
}

func BuildDRBStatusDL18() (value ngapType.DRBStatusDL18) {
	value.DLCOUNTValue = BuildCOUNTValueForPDCPSN18()
	return
}

func BuildDRBStatusUL() (value ngapType.DRBStatusUL) {
	value.Present = ngapType.DRBStatusULPresentDRBStatusUL12
	value.DRBStatusUL12 = new(ngapType.DRBStatusUL12)
	*value.DRBStatusUL12 = BuildDRBStatusUL12()
	value.Present = ngapType.DRBStatusULPresentDRBStatusUL18
	value.DRBStatusUL18 = new(ngapType.DRBStatusUL18)
	*value.DRBStatusUL18 = BuildDRBStatusUL18()
	return
}

func BuildDRBStatusUL12() (value ngapType.DRBStatusUL12) {
	value.ULCOUNTValue = BuildCOUNTValueForPDCPSN12()
	value.ReceiveStatusOfULPDCPSDUs = new(ngapType.BitString)
	value.ReceiveStatusOfULPDCPSDUs.BitLength = 50
	value.ReceiveStatusOfULPDCPSDUs.Bytes = []byte{11, 22, 33, 44, 55, 66, 77}
	return
}

func BuildDRBStatusUL18() (value ngapType.DRBStatusUL18) {
	value.ULCOUNTValue = BuildCOUNTValueForPDCPSN18()
	value.ReceiveStatusOfULPDCPSDUs = new(ngapType.BitString)
	value.ReceiveStatusOfULPDCPSDUs.BitLength = 50
	value.ReceiveStatusOfULPDCPSDUs.Bytes = []byte{11, 22, 33, 44, 55, 66, 77}
	return
}

func BuildDRBsToQosFlowsMappingList() (value ngapType.DRBsToQosFlowsMappingList) {
	item := BuildDRBsToQosFlowsMappingItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildDRBsToQosFlowsMappingItem() (value ngapType.DRBsToQosFlowsMappingItem) {
	value.DRBID = BuildDRBID()
	value.AssociatedQosFlowList = BuildAssociatedQosFlowList()
	return
}

func BuildDynamic5QIDescriptor() (value ngapType.Dynamic5QIDescriptor) {
	value.PriorityLevelQos = BuildPriorityLevelQos()
	value.PacketDelayBudget = BuildPacketDelayBudget()
	value.PacketErrorRate = BuildPacketErrorRate()
	value.FiveQI = new(ngapType.FiveQI)
	*value.FiveQI = BuildFiveQI()
	value.DelayCritical = new(ngapType.DelayCritical)
	*value.DelayCritical = BuildDelayCritical()
	value.AveragingWindow = new(ngapType.AveragingWindow)
	*value.AveragingWindow = BuildAveragingWindow()
	value.MaximumDataBurstVolume = new(ngapType.MaximumDataBurstVolume)
	*value.MaximumDataBurstVolume = BuildMaximumDataBurstVolume()
	return
}

func BuildEarlyStatusTransferTransparentContainer() (value ngapType.EarlyStatusTransferTransparentContainer) {
	value.ProcedureStage = BuildProcedureStageChoice()
	return
}

func BuildProcedureStageChoice() (value ngapType.ProcedureStageChoice) {
	value.Present = ngapType.ProcedureStageChoicePresentFirstDlCount
	value.FirstDlCount = new(ngapType.FirstDLCount)
	*value.FirstDlCount = BuildFirstDLCount()
	return
}

func BuildFirstDLCount() (value ngapType.FirstDLCount) {
	value.DRBsSubjectToEarlyStatusTransfer = BuildDRBsSubjectToEarlyStatusTransferList()
	return
}

func BuildDRBsSubjectToEarlyStatusTransferList() (value ngapType.DRBsSubjectToEarlyStatusTransferList) {
	item := BuildDRBsSubjectToEarlyStatusTransferItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildDRBsSubjectToEarlyStatusTransferItem() (value ngapType.DRBsSubjectToEarlyStatusTransferItem) {
	value.DRBID = BuildDRBID()
	value.FirstDLCOUNT = BuildDRBStatusDL()
	return
}

func BuildEmergencyAreaIDBroadcastEUTRA() (value ngapType.EmergencyAreaIDBroadcastEUTRA) {
	item := BuildEmergencyAreaIDBroadcastEUTRAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildEmergencyAreaIDBroadcastEUTRAItem() (value ngapType.EmergencyAreaIDBroadcastEUTRAItem) {
	value.EmergencyAreaID = BuildEmergencyAreaID()
	value.CompletedCellsInEAIEUTRA = BuildCompletedCellsInEAIEUTRA()
	return
}

func BuildEmergencyAreaIDBroadcastNR() (value ngapType.EmergencyAreaIDBroadcastNR) {
	item := BuildEmergencyAreaIDBroadcastNRItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildEmergencyAreaIDBroadcastNRItem() (value ngapType.EmergencyAreaIDBroadcastNRItem) {
	value.EmergencyAreaID = BuildEmergencyAreaID()
	value.CompletedCellsInEAINR = BuildCompletedCellsInEAINR()
	return
}

func BuildEmergencyAreaIDCancelledEUTRA() (value ngapType.EmergencyAreaIDCancelledEUTRA) {
	item := BuildEmergencyAreaIDCancelledEUTRAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildEmergencyAreaIDCancelledEUTRAItem() (value ngapType.EmergencyAreaIDCancelledEUTRAItem) {
	value.EmergencyAreaID = BuildEmergencyAreaID()
	value.CancelledCellsInEAIEUTRA = BuildCancelledCellsInEAIEUTRA()
	return
}

func BuildEmergencyAreaIDCancelledNR() (value ngapType.EmergencyAreaIDCancelledNR) {
	item := BuildEmergencyAreaIDCancelledNRItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildEmergencyAreaIDCancelledNRItem() (value ngapType.EmergencyAreaIDCancelledNRItem) {
	value.EmergencyAreaID = BuildEmergencyAreaID()
	value.CancelledCellsInEAINR = BuildCancelledCellsInEAINR()
	return
}

func BuildEmergencyAreaIDList() (value ngapType.EmergencyAreaIDList) {
	item := BuildEmergencyAreaID()
	value.List = append(value.List, item, item, item)
	return
}

func BuildEmergencyAreaIDListForRestart() (value ngapType.EmergencyAreaIDListForRestart) {
	item := BuildEmergencyAreaID()
	value.List = append(value.List, item, item, item)
	return
}

func BuildEmergencyFallbackIndicator() (value ngapType.EmergencyFallbackIndicator) {
	value.EmergencyFallbackRequestIndicator = BuildEmergencyFallbackRequestIndicator()
	value.EmergencyServiceTargetCN = new(ngapType.EmergencyServiceTargetCN)
	*value.EmergencyServiceTargetCN = BuildEmergencyServiceTargetCN()
	return
}

func BuildENBID() (value ngapType.ENBID) {
	value.Present = ngapType.ENBIDPresentMacroENBID
	value.MacroENBID = new(ngapType.BitString)
	value.MacroENBID.BitLength = 20
	value.MacroENBID.Bytes = []byte{171, 1, 2}
	value.Present = ngapType.ENBIDPresentHomeENBID
	value.HomeENBID = new(ngapType.BitString)
	value.HomeENBID.BitLength = 28
	value.HomeENBID.Bytes = []byte{171, 1, 2, 3}
	value.Present = ngapType.ENBIDPresentShortMacroENBID
	value.ShortMacroENBID = new(ngapType.BitString)
	value.ShortMacroENBID.BitLength = 18
	value.ShortMacroENBID.Bytes = []byte{171, 1, 2}
	value.Present = ngapType.ENBIDPresentLongMacroENBID
	value.LongMacroENBID = new(ngapType.BitString)
	value.LongMacroENBID.BitLength = 21
	value.LongMacroENBID.Bytes = []byte{171, 1, 2}
	return
}

func BuildEndpointIPAddressAndPort() (value ngapType.EndpointIPAddressAndPort) {
	value.EndpointIPAddress = BuildTransportLayerAddress()
	value.PortNumber = BuildPortNumber()
	return
}

func BuildEquivalentPLMNs() (value ngapType.EquivalentPLMNs) {
	item := BuildPLMNIdentity()
	value.List = append(value.List, item, item, item)
	return
}

func BuildEPSTAI() (value ngapType.EPSTAI) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.EPSTAC = BuildEPSTAC()
	return
}

func BuildERABInformationList() (value ngapType.ERABInformationList) {
	item := BuildERABInformationItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildERABInformationItem() (value ngapType.ERABInformationItem) {
	value.ERABID = BuildERABID()
	value.DLForwarding = new(ngapType.DLForwarding)
	*value.DLForwarding = BuildDLForwarding()
	return
}

func BuildEUTRACGI() (value ngapType.EUTRACGI) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.EUTRACellIdentity = BuildEUTRACellIdentity()
	return
}

func BuildEUTRACGIList() (value ngapType.EUTRACGIList) {
	item := BuildEUTRACGI()
	value.List = append(value.List, item, item, item)
	return
}

func BuildEUTRACGIListForWarning() (value ngapType.EUTRACGIListForWarning) {
	item := BuildEUTRACGI()
	value.List = append(value.List, item, item, item)
	return
}

func BuildExpectedUEActivityBehaviour() (value ngapType.ExpectedUEActivityBehaviour) {
	value.ExpectedActivityPeriod = new(ngapType.ExpectedActivityPeriod)
	*value.ExpectedActivityPeriod = BuildExpectedActivityPeriod()
	value.ExpectedIdlePeriod = new(ngapType.ExpectedIdlePeriod)
	*value.ExpectedIdlePeriod = BuildExpectedIdlePeriod()
	value.SourceOfUEActivityBehaviourInformation = new(ngapType.SourceOfUEActivityBehaviourInformation)
	*value.SourceOfUEActivityBehaviourInformation = BuildSourceOfUEActivityBehaviourInformation()
	return
}

func BuildExpectedUEBehaviour() (value ngapType.ExpectedUEBehaviour) {
	value.ExpectedUEActivityBehaviour = new(ngapType.ExpectedUEActivityBehaviour)
	*value.ExpectedUEActivityBehaviour = BuildExpectedUEActivityBehaviour()
	value.ExpectedHOInterval = new(ngapType.ExpectedHOInterval)
	*value.ExpectedHOInterval = BuildExpectedHOInterval()
	value.ExpectedUEMobility = new(ngapType.ExpectedUEMobility)
	*value.ExpectedUEMobility = BuildExpectedUEMobility()
	value.ExpectedUEMovingTrajectory = new(ngapType.ExpectedUEMovingTrajectory)
	*value.ExpectedUEMovingTrajectory = BuildExpectedUEMovingTrajectory()
	return
}

func BuildExpectedUEMovingTrajectory() (value ngapType.ExpectedUEMovingTrajectory) {
	item := BuildExpectedUEMovingTrajectoryItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildExpectedUEMovingTrajectoryItem() (value ngapType.ExpectedUEMovingTrajectoryItem) {
	value.NGRANCGI = BuildNGRANCGI()
	value.TimeStayedInCell = new(int64)
	*value.TimeStayedInCell = 0
	value.TimeStayedInCell = new(int64)
	*value.TimeStayedInCell = 4095
	return
}

func BuildExtendedAMFName() (value ngapType.ExtendedAMFName) {
	value.AMFNameVisibleString = new(ngapType.AMFNameVisibleString)
	*value.AMFNameVisibleString = BuildAMFNameVisibleString()
	value.AMFNameUTF8String = new(ngapType.AMFNameUTF8String)
	*value.AMFNameUTF8String = BuildAMFNameUTF8String()
	return
}

func BuildExtendedRANNodeName() (value ngapType.ExtendedRANNodeName) {
	value.RANNodeNameVisibleString = new(ngapType.RANNodeNameVisibleString)
	*value.RANNodeNameVisibleString = BuildRANNodeNameVisibleString()
	value.RANNodeNameUTF8String = new(ngapType.RANNodeNameUTF8String)
	*value.RANNodeNameUTF8String = BuildRANNodeNameUTF8String()
	return
}

func BuildExtendedRATRestrictionInformation() (value ngapType.ExtendedRATRestrictionInformation) {
	value.PrimaryRATRestriction.BitLength = 8
	value.PrimaryRATRestriction.Bytes = []byte{171}
	value.PrimaryRATRestriction.BitLength = 8
	value.PrimaryRATRestriction.Bytes = []byte{171}
	value.SecondaryRATRestriction.BitLength = 8
	value.SecondaryRATRestriction.Bytes = []byte{171}
	value.SecondaryRATRestriction.BitLength = 8
	value.SecondaryRATRestriction.Bytes = []byte{171}
	return
}

func BuildExtendedSliceSupportList() (value ngapType.ExtendedSliceSupportList) {
	item := BuildSliceSupportItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildEventTrigger() (value ngapType.EventTrigger) {
	value.Present = ngapType.EventTriggerPresentOutOfCoverage
	value.OutOfCoverage = new(ngapType.OutOfCoverage)
	*value.OutOfCoverage = BuildOutOfCoverage()
	value.Present = ngapType.EventTriggerPresentEventL1LoggedMDTConfig
	value.EventL1LoggedMDTConfig = new(ngapType.EventL1LoggedMDTConfig)
	*value.EventL1LoggedMDTConfig = BuildEventL1LoggedMDTConfig()
	return
}

func BuildEventL1LoggedMDTConfig() (value ngapType.EventL1LoggedMDTConfig) {
	value.L1Threshold = BuildMeasurementThresholdL1LoggedMDT()
	value.Hysteresis = BuildHysteresis()
	value.TimeToTrigger = BuildTimeToTrigger()
	return
}

func BuildMeasurementThresholdL1LoggedMDT() (value ngapType.MeasurementThresholdL1LoggedMDT) {
	value.Present = ngapType.MeasurementThresholdL1LoggedMDTPresentThresholdRSRP
	value.ThresholdRSRP = new(ngapType.ThresholdRSRP)
	*value.ThresholdRSRP = BuildThresholdRSRP()
	value.Present = ngapType.MeasurementThresholdL1LoggedMDTPresentThresholdRSRQ
	value.ThresholdRSRQ = new(ngapType.ThresholdRSRQ)
	*value.ThresholdRSRQ = BuildThresholdRSRQ()
	return
}

func BuildFailureIndication() (value ngapType.FailureIndication) {
	value.UERLFReportContainer = BuildUERLFReportContainer()
	return
}

func BuildFiveGSTMSI() (value ngapType.FiveGSTMSI) {
	value.AMFSetID = BuildAMFSetID()
	value.AMFPointer = BuildAMFPointer()
	value.FiveGTMSI = BuildFiveGTMSI()
	return
}

func BuildForbiddenAreaInformation() (value ngapType.ForbiddenAreaInformation) {
	item := BuildForbiddenAreaInformationItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildForbiddenAreaInformationItem() (value ngapType.ForbiddenAreaInformationItem) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.ForbiddenTACs = BuildForbiddenTACs()
	return
}

func BuildForbiddenTACs() (value ngapType.ForbiddenTACs) {
	item := BuildTAC()
	value.List = append(value.List, item, item, item)
	return
}

func BuildFromEUTRANtoNGRAN() (value ngapType.FromEUTRANtoNGRAN) {
	value.SourceeNBID = BuildIntersystemSONeNBID()
	value.TargetNGRANnodeID = BuildIntersystemSONNGRANnodeID()
	return
}

func BuildFromNGRANtoEUTRAN() (value ngapType.FromNGRANtoEUTRAN) {
	value.SourceNGRANnodeID = BuildIntersystemSONNGRANnodeID()
	value.TargeteNBID = BuildIntersystemSONeNBID()
	return
}

func BuildGBRQosInformation() (value ngapType.GBRQosInformation) {
	value.MaximumFlowBitRateDL = BuildBitRate()
	value.MaximumFlowBitRateUL = BuildBitRate()
	value.GuaranteedFlowBitRateDL = BuildBitRate()
	value.GuaranteedFlowBitRateUL = BuildBitRate()
	value.NotificationControl = new(ngapType.NotificationControl)
	*value.NotificationControl = BuildNotificationControl()
	value.MaximumPacketLossRateDL = new(ngapType.PacketLossRate)
	*value.MaximumPacketLossRateDL = BuildPacketLossRate()
	value.MaximumPacketLossRateUL = new(ngapType.PacketLossRate)
	*value.MaximumPacketLossRateUL = BuildPacketLossRate()
	return
}

func BuildGlobalENBID() (value ngapType.GlobalENBID) {
	value.PLMNidentity = BuildPLMNIdentity()
	value.ENBID = BuildENBID()
	return
}

func BuildGlobalGNBID() (value ngapType.GlobalGNBID) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.GNBID = BuildGNBID()
	return
}

func BuildGlobalN3IWFID() (value ngapType.GlobalN3IWFID) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.N3IWFID = BuildN3IWFID()
	return
}

func BuildGlobalLineID() (value ngapType.GlobalLineID) {
	value.GlobalLineIdentity = BuildGlobalLineIdentity()
	value.LineType = new(ngapType.LineType)
	*value.LineType = BuildLineType()
	return
}

func BuildGlobalNgENBID() (value ngapType.GlobalNgENBID) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.NgENBID = BuildNgENBID()
	return
}

func BuildGlobalRANNodeID() (value ngapType.GlobalRANNodeID) {
	value.Present = ngapType.GlobalRANNodeIDPresentGlobalGNBID
	value.GlobalGNBID = new(ngapType.GlobalGNBID)
	*value.GlobalGNBID = BuildGlobalGNBID()
	value.Present = ngapType.GlobalRANNodeIDPresentGlobalNgENBID
	value.GlobalNgENBID = new(ngapType.GlobalNgENBID)
	*value.GlobalNgENBID = BuildGlobalNgENBID()
	value.Present = ngapType.GlobalRANNodeIDPresentGlobalN3IWFID
	value.GlobalN3IWFID = new(ngapType.GlobalN3IWFID)
	*value.GlobalN3IWFID = BuildGlobalN3IWFID()
	return
}

func BuildGlobalTNGFID() (value ngapType.GlobalTNGFID) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.TNGFID = BuildTNGFID()
	return
}

func BuildGlobalTWIFID() (value ngapType.GlobalTWIFID) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.TWIFID = BuildTWIFID()
	return
}

func BuildGlobalWAGFID() (value ngapType.GlobalWAGFID) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.WAGFID = BuildWAGFID()
	return
}

func BuildGNBID() (value ngapType.GNBID) {
	value.Present = ngapType.GNBIDPresentGNBID
	value.GNBID = new(ngapType.BitString)
	value.GNBID.BitLength = 30
	value.GNBID.Bytes = []byte{11, 22, 33, 44}
	return
}

func BuildGTPTunnel() (value ngapType.GTPTunnel) {
	value.TransportLayerAddress = BuildTransportLayerAddress()
	value.GTPTEID = BuildGTPTEID()
	return
}

func BuildGUAMI() (value ngapType.GUAMI) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.AMFRegionID = BuildAMFRegionID()
	value.AMFSetID = BuildAMFSetID()
	value.AMFPointer = BuildAMFPointer()
	return
}

func BuildHOReport() (value ngapType.HOReport) {
	value.HandoverReportType = BuildHandoverReportType()
	value.HandoverCause = BuildCause()
	value.SourcecellCGI = BuildNGRANCGI()
	value.TargetcellCGI = BuildNGRANCGI()
	value.ReestablishmentcellCGI = new(ngapType.NGRANCGI)
	*value.ReestablishmentcellCGI = BuildNGRANCGI()
	value.SourcecellCRNTI = new(ngapType.BitString)
	value.SourcecellCRNTI.BitLength = 16
	value.SourcecellCRNTI.Bytes = []byte{171, 1}
	value.SourcecellCRNTI = new(ngapType.BitString)
	value.SourcecellCRNTI.BitLength = 16
	value.SourcecellCRNTI.Bytes = []byte{171, 1}
	value.TargetcellinEUTRAN = new(ngapType.EUTRACGI)
	*value.TargetcellinEUTRAN = BuildEUTRACGI()
	value.MobilityInformation = new(ngapType.MobilityInformation)
	*value.MobilityInformation = BuildMobilityInformation()
	value.UERLFReportContainer = new(ngapType.UERLFReportContainer)
	*value.UERLFReportContainer = BuildUERLFReportContainer()
	return
}

func BuildInfoOnRecommendedCellsAndRANNodesForPaging() (value ngapType.InfoOnRecommendedCellsAndRANNodesForPaging) {
	value.RecommendedCellsForPaging = BuildRecommendedCellsForPaging()
	value.RecommendRANNodesForPaging = BuildRecommendedRANNodesForPaging()
	return
}

func BuildImmediateMDTNr() (value ngapType.ImmediateMDTNr) {
	value.MeasurementsToActivate = BuildMeasurementsToActivate()
	value.M1Configuration = new(ngapType.M1Configuration)
	*value.M1Configuration = BuildM1Configuration()
	value.M4Configuration = new(ngapType.M4Configuration)
	*value.M4Configuration = BuildM4Configuration()
	value.M5Configuration = new(ngapType.M5Configuration)
	*value.M5Configuration = BuildM5Configuration()
	value.M6Configuration = new(ngapType.M6Configuration)
	*value.M6Configuration = BuildM6Configuration()
	value.M7Configuration = new(ngapType.M7Configuration)
	*value.M7Configuration = BuildM7Configuration()
	value.BluetoothMeasurementConfiguration = new(ngapType.BluetoothMeasurementConfiguration)
	*value.BluetoothMeasurementConfiguration = BuildBluetoothMeasurementConfiguration()
	value.WLANMeasurementConfiguration = new(ngapType.WLANMeasurementConfiguration)
	*value.WLANMeasurementConfiguration = BuildWLANMeasurementConfiguration()
	value.MDTLocationInfo = new(ngapType.MDTLocationInfo)
	*value.MDTLocationInfo = BuildMDTLocationInfo()
	value.SensorMeasurementConfiguration = new(ngapType.SensorMeasurementConfiguration)
	*value.SensorMeasurementConfiguration = BuildSensorMeasurementConfiguration()
	return
}

func BuildInterSystemFailureIndication() (value ngapType.InterSystemFailureIndication) {
	value.UERLFReportContainer = new(ngapType.UERLFReportContainer)
	*value.UERLFReportContainer = BuildUERLFReportContainer()
	return
}

func BuildIntersystemSONConfigurationTransfer() (value ngapType.IntersystemSONConfigurationTransfer) {
	value.TransferType = BuildIntersystemSONTransferType()
	value.IntersystemSONInformation = BuildIntersystemSONInformation()
	return
}

func BuildIntersystemSONTransferType() (value ngapType.IntersystemSONTransferType) {
	value.Present = ngapType.IntersystemSONTransferTypePresentFromEUTRANtoNGRAN
	value.FromEUTRANtoNGRAN = new(ngapType.FromEUTRANtoNGRAN)
	*value.FromEUTRANtoNGRAN = BuildFromEUTRANtoNGRAN()
	value.Present = ngapType.IntersystemSONTransferTypePresentFromNGRANtoEUTRAN
	value.FromNGRANtoEUTRAN = new(ngapType.FromNGRANtoEUTRAN)
	*value.FromNGRANtoEUTRAN = BuildFromNGRANtoEUTRAN()
	return
}

func BuildIntersystemSONeNBID() (value ngapType.IntersystemSONeNBID) {
	value.GlobaleNBID = BuildGlobalENBID()
	value.SelectedEPSTAI = BuildEPSTAI()
	return
}

func BuildIntersystemSONNGRANnodeID() (value ngapType.IntersystemSONNGRANnodeID) {
	value.GlobalRANNodeID = BuildGlobalRANNodeID()
	value.SelectedTAI = BuildTAI()
	return
}

func BuildIntersystemSONInformation() (value ngapType.IntersystemSONInformation) {
	value.Present = ngapType.IntersystemSONInformationPresentIntersystemSONInformationReport
	value.IntersystemSONInformationReport = new(ngapType.IntersystemSONInformationReport)
	*value.IntersystemSONInformationReport = BuildIntersystemSONInformationReport()
	return
}

func BuildIntersystemSONInformationReport() (value ngapType.IntersystemSONInformationReport) {
	value.Present = ngapType.IntersystemSONInformationReportPresentHOReportInformation
	value.HOReportInformation = new(ngapType.InterSystemHOReport)
	*value.HOReportInformation = BuildInterSystemHOReport()
	value.Present = ngapType.IntersystemSONInformationReportPresentFailureIndicationInformation
	value.FailureIndicationInformation = new(ngapType.InterSystemFailureIndication)
	*value.FailureIndicationInformation = BuildInterSystemFailureIndication()
	return
}

func BuildInterSystemHOReport() (value ngapType.InterSystemHOReport) {
	value.HandoverReportType = BuildInterSystemHandoverReportType()
	return
}

func BuildInterSystemHandoverReportType() (value ngapType.InterSystemHandoverReportType) {
	value.Present = ngapType.InterSystemHandoverReportTypePresentTooearlyIntersystemHO
	value.TooearlyIntersystemHO = new(ngapType.TooearlyIntersystemHO)
	*value.TooearlyIntersystemHO = BuildTooearlyIntersystemHO()
	value.Present = ngapType.InterSystemHandoverReportTypePresentIntersystemUnnecessaryHO
	value.IntersystemUnnecessaryHO = new(ngapType.IntersystemUnnecessaryHO)
	*value.IntersystemUnnecessaryHO = BuildIntersystemUnnecessaryHO()
	return
}

func BuildIntersystemUnnecessaryHO() (value ngapType.IntersystemUnnecessaryHO) {
	value.SourcecellID = BuildNGRANCGI()
	value.TargetcellID = BuildEUTRACGI()
	value.EarlyIRATHO = BuildEarlyIRATHO()
	value.CandidateCellList = BuildCandidateCellList()
	return
}

func BuildLAI() (value ngapType.LAI) {
	value.PLMNidentity = BuildPLMNIdentity()
	value.LAC = BuildLAC()
	return
}

func BuildLastVisitedCellInformation() (value ngapType.LastVisitedCellInformation) {
	value.Present = ngapType.LastVisitedCellInformationPresentNGRANCell
	value.NGRANCell = new(ngapType.LastVisitedNGRANCellInformation)
	*value.NGRANCell = BuildLastVisitedNGRANCellInformation()
	value.Present = ngapType.LastVisitedCellInformationPresentEUTRANCell
	value.EUTRANCell = new(ngapType.LastVisitedEUTRANCellInformation)
	*value.EUTRANCell = BuildLastVisitedEUTRANCellInformation()
	value.Present = ngapType.LastVisitedCellInformationPresentUTRANCell
	value.UTRANCell = new(ngapType.LastVisitedUTRANCellInformation)
	*value.UTRANCell = BuildLastVisitedUTRANCellInformation()
	value.Present = ngapType.LastVisitedCellInformationPresentGERANCell
	value.GERANCell = new(ngapType.LastVisitedGERANCellInformation)
	*value.GERANCell = BuildLastVisitedGERANCellInformation()
	return
}

func BuildLastVisitedCellItem() (value ngapType.LastVisitedCellItem) {
	value.LastVisitedCellInformation = BuildLastVisitedCellInformation()
	return
}

func BuildLastVisitedNGRANCellInformation() (value ngapType.LastVisitedNGRANCellInformation) {
	value.GlobalCellID = BuildNGRANCGI()
	value.CellType = BuildCellType()
	value.TimeUEStayedInCell = BuildTimeUEStayedInCell()
	value.TimeUEStayedInCellEnhancedGranularity = new(ngapType.TimeUEStayedInCellEnhancedGranularity)
	*value.TimeUEStayedInCellEnhancedGranularity = BuildTimeUEStayedInCellEnhancedGranularity()
	value.HOCauseValue = new(ngapType.Cause)
	*value.HOCauseValue = BuildCause()
	return
}

func BuildLocationReportingRequestType() (value ngapType.LocationReportingRequestType) {
	value.EventType = BuildEventType()
	value.ReportArea = BuildReportArea()
	value.AreaOfInterestList = new(ngapType.AreaOfInterestList)
	*value.AreaOfInterestList = BuildAreaOfInterestList()
	value.LocationReportingReferenceIDToBeCancelled = new(ngapType.LocationReportingReferenceID)
	*value.LocationReportingReferenceIDToBeCancelled = BuildLocationReportingReferenceID()
	return
}

func BuildLoggedMDTNr() (value ngapType.LoggedMDTNr) {
	value.LoggingInterval = BuildLoggingInterval()
	value.LoggingDuration = BuildLoggingDuration()
	value.LoggedMDTTrigger = BuildLoggedMDTTrigger()
	value.BluetoothMeasurementConfiguration = new(ngapType.BluetoothMeasurementConfiguration)
	*value.BluetoothMeasurementConfiguration = BuildBluetoothMeasurementConfiguration()
	value.WLANMeasurementConfiguration = new(ngapType.WLANMeasurementConfiguration)
	*value.WLANMeasurementConfiguration = BuildWLANMeasurementConfiguration()
	value.SensorMeasurementConfiguration = new(ngapType.SensorMeasurementConfiguration)
	*value.SensorMeasurementConfiguration = BuildSensorMeasurementConfiguration()
	value.AreaScopeOfNeighCellsList = new(ngapType.AreaScopeOfNeighCellsList)
	*value.AreaScopeOfNeighCellsList = BuildAreaScopeOfNeighCellsList()
	return
}

func BuildLoggedMDTTrigger() (value ngapType.LoggedMDTTrigger) {
	value.Present = ngapType.LoggedMDTTriggerPresentEventTrigger
	value.EventTrigger = new(ngapType.EventTrigger)
	*value.EventTrigger = BuildEventTrigger()
	return
}

func BuildLTEV2XServicesAuthorized() (value ngapType.LTEV2XServicesAuthorized) {
	value.VehicleUE = new(ngapType.VehicleUE)
	*value.VehicleUE = BuildVehicleUE()
	value.PedestrianUE = new(ngapType.PedestrianUE)
	*value.PedestrianUE = BuildPedestrianUE()
	return
}

func BuildLTEUESidelinkAggregateMaximumBitrate() (value ngapType.LTEUESidelinkAggregateMaximumBitrate) {
	value.UESidelinkAggregateMaximumBitRate = BuildBitRate()
	return
}

func BuildMobilityRestrictionList() (value ngapType.MobilityRestrictionList) {
	value.ServingPLMN = BuildPLMNIdentity()
	value.EquivalentPLMNs = new(ngapType.EquivalentPLMNs)
	*value.EquivalentPLMNs = BuildEquivalentPLMNs()
	value.RATRestrictions = new(ngapType.RATRestrictions)
	*value.RATRestrictions = BuildRATRestrictions()
	value.ForbiddenAreaInformation = new(ngapType.ForbiddenAreaInformation)
	*value.ForbiddenAreaInformation = BuildForbiddenAreaInformation()
	value.ServiceAreaInformation = new(ngapType.ServiceAreaInformation)
	*value.ServiceAreaInformation = BuildServiceAreaInformation()
	return
}

func BuildMDTPLMNList() (value ngapType.MDTPLMNList) {
	item := BuildPLMNIdentity()
	value.List = append(value.List, item, item, item)
	return
}

func BuildMDTConfiguration() (value ngapType.MDTConfiguration) {
	value.MdtConfigNR = new(ngapType.MDTConfigurationNR)
	*value.MdtConfigNR = BuildMDTConfigurationNR()
	value.MdtConfigEUTRA = new(ngapType.MDTConfigurationEUTRA)
	*value.MdtConfigEUTRA = BuildMDTConfigurationEUTRA()
	return
}

func BuildMDTConfigurationNR() (value ngapType.MDTConfigurationNR) {
	value.MdtActivation = BuildMDTActivation()
	value.AreaScopeOfMDT = BuildAreaScopeOfMDTNR()
	value.MDTModeNr = BuildMDTModeNr()
	value.SignallingBasedMDTPLMNList = new(ngapType.MDTPLMNList)
	*value.SignallingBasedMDTPLMNList = BuildMDTPLMNList()
	return
}

func BuildMDTConfigurationEUTRA() (value ngapType.MDTConfigurationEUTRA) {
	value.MdtActivation = BuildMDTActivation()
	value.AreaScopeOfMDT = BuildAreaScopeOfMDTEUTRA()
	value.MDTMode = BuildMDTModeEutra()
	value.SignallingBasedMDTPLMNList = new(ngapType.MDTPLMNList)
	*value.SignallingBasedMDTPLMNList = BuildMDTPLMNList()
	return
}

func BuildMDTModeNr() (value ngapType.MDTModeNr) {
	value.Present = ngapType.MDTModeNrPresentImmediateMDTNr
	value.ImmediateMDTNr = new(ngapType.ImmediateMDTNr)
	*value.ImmediateMDTNr = BuildImmediateMDTNr()
	value.Present = ngapType.MDTModeNrPresentLoggedMDTNr
	value.LoggedMDTNr = new(ngapType.LoggedMDTNr)
	*value.LoggedMDTNr = BuildLoggedMDTNr()
	return
}

func BuildM1Configuration() (value ngapType.M1Configuration) {
	value.M1reportingTrigger = BuildM1ReportingTrigger()
	value.M1thresholdEventA2 = new(ngapType.M1ThresholdEventA2)
	*value.M1thresholdEventA2 = BuildM1ThresholdEventA2()
	value.M1periodicReporting = new(ngapType.M1PeriodicReporting)
	*value.M1periodicReporting = BuildM1PeriodicReporting()
	return
}

func BuildM1ThresholdEventA2() (value ngapType.M1ThresholdEventA2) {
	value.M1ThresholdType = BuildM1ThresholdType()
	return
}

func BuildM1ThresholdType() (value ngapType.M1ThresholdType) {
	value.Present = ngapType.M1ThresholdTypePresentThresholdRSRP
	value.ThresholdRSRP = new(ngapType.ThresholdRSRP)
	*value.ThresholdRSRP = BuildThresholdRSRP()
	value.Present = ngapType.M1ThresholdTypePresentThresholdRSRQ
	value.ThresholdRSRQ = new(ngapType.ThresholdRSRQ)
	*value.ThresholdRSRQ = BuildThresholdRSRQ()
	value.Present = ngapType.M1ThresholdTypePresentThresholdSINR
	value.ThresholdSINR = new(ngapType.ThresholdSINR)
	*value.ThresholdSINR = BuildThresholdSINR()
	return
}

func BuildM1PeriodicReporting() (value ngapType.M1PeriodicReporting) {
	value.ReportInterval = BuildReportIntervalMDT()
	value.ReportAmount = BuildReportAmountMDT()
	return
}

func BuildM4Configuration() (value ngapType.M4Configuration) {
	value.M4period = BuildM4period()
	value.M4LinksToLog = BuildLinksToLog()
	return
}

func BuildM5Configuration() (value ngapType.M5Configuration) {
	value.M5period = BuildM5period()
	value.M5LinksToLog = BuildLinksToLog()
	return
}

func BuildM6Configuration() (value ngapType.M6Configuration) {
	value.M6reportInterval = BuildM6reportInterval()
	value.M6LinksToLog = BuildLinksToLog()
	return
}

func BuildM7Configuration() (value ngapType.M7Configuration) {
	value.M7period = BuildM7period()
	value.M7LinksToLog = BuildLinksToLog()
	return
}

func BuildMDTLocationInfo() (value ngapType.MDTLocationInfo) {
	value.MDTLocationInformation = BuildMDTLocationInformation()
	return
}

func BuildN3IWFID() (value ngapType.N3IWFID) {
	value.Present = ngapType.N3IWFIDPresentN3IWFID
	value.N3IWFID = new(ngapType.BitString)
	value.N3IWFID.BitLength = 16
	value.N3IWFID.Bytes = []byte{171, 1}
	return
}

func BuildNBIoTPagingEDRXInfo() (value ngapType.NBIoTPagingEDRXInfo) {
	value.NBIoTPagingEDRXCycle = BuildNBIoTPagingEDRXCycle()
	value.NBIoTPagingTimeWindow = new(ngapType.NBIoTPagingTimeWindow)
	*value.NBIoTPagingTimeWindow = BuildNBIoTPagingTimeWindow()
	return
}

func BuildNgENBID() (value ngapType.NgENBID) {
	value.Present = ngapType.NgENBIDPresentMacroNgENBID
	value.MacroNgENBID = new(ngapType.BitString)
	value.MacroNgENBID.BitLength = 20
	value.MacroNgENBID.Bytes = []byte{171, 1, 2}
	value.Present = ngapType.NgENBIDPresentShortMacroNgENBID
	value.ShortMacroNgENBID = new(ngapType.BitString)
	value.ShortMacroNgENBID.BitLength = 18
	value.ShortMacroNgENBID.Bytes = []byte{171, 1, 2}
	value.Present = ngapType.NgENBIDPresentLongMacroNgENBID
	value.LongMacroNgENBID = new(ngapType.BitString)
	value.LongMacroNgENBID.BitLength = 21
	value.LongMacroNgENBID.Bytes = []byte{171, 1, 2}
	return
}

func BuildNGRANCGI() (value ngapType.NGRANCGI) {
	value.Present = ngapType.NGRANCGIPresentNRCGI
	value.NRCGI = new(ngapType.NRCGI)
	*value.NRCGI = BuildNRCGI()
	value.Present = ngapType.NGRANCGIPresentEUTRACGI
	value.EUTRACGI = new(ngapType.EUTRACGI)
	*value.EUTRACGI = BuildEUTRACGI()
	return
}

func BuildNGRANTNLAssociationToRemoveList() (value ngapType.NGRANTNLAssociationToRemoveList) {
	item := BuildNGRANTNLAssociationToRemoveItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildNGRANTNLAssociationToRemoveItem() (value ngapType.NGRANTNLAssociationToRemoveItem) {
	value.TNLAssociationTransportLayerAddress = BuildCPTransportLayerInformation()
	value.TNLAssociationTransportLayerAddressAMF = new(ngapType.CPTransportLayerInformation)
	*value.TNLAssociationTransportLayerAddressAMF = BuildCPTransportLayerInformation()
	return
}

func BuildNonDynamic5QIDescriptor() (value ngapType.NonDynamic5QIDescriptor) {
	value.FiveQI = BuildFiveQI()
	value.PriorityLevelQos = new(ngapType.PriorityLevelQos)
	*value.PriorityLevelQos = BuildPriorityLevelQos()
	value.AveragingWindow = new(ngapType.AveragingWindow)
	*value.AveragingWindow = BuildAveragingWindow()
	value.MaximumDataBurstVolume = new(ngapType.MaximumDataBurstVolume)
	*value.MaximumDataBurstVolume = BuildMaximumDataBurstVolume()
	return
}

func BuildNotAllowedTACs() (value ngapType.NotAllowedTACs) {
	item := BuildTAC()
	value.List = append(value.List, item, item, item)
	return
}

func BuildNPNAccessInformation() (value ngapType.NPNAccessInformation) {
	value.Present = ngapType.NPNAccessInformationPresentPNINPNAccessInformation
	value.PNINPNAccessInformation = new(ngapType.CellCAGList)
	*value.PNINPNAccessInformation = BuildCellCAGList()
	return
}

func BuildNPNMobilityInformation() (value ngapType.NPNMobilityInformation) {
	value.Present = ngapType.NPNMobilityInformationPresentSNPNMobilityInformation
	value.SNPNMobilityInformation = new(ngapType.SNPNMobilityInformation)
	*value.SNPNMobilityInformation = BuildSNPNMobilityInformation()
	value.Present = ngapType.NPNMobilityInformationPresentPNINPNMobilityInformation
	value.PNINPNMobilityInformation = new(ngapType.PNINPNMobilityInformation)
	*value.PNINPNMobilityInformation = BuildPNINPNMobilityInformation()
	return
}

func BuildNPNPagingAssistanceInformation() (value ngapType.NPNPagingAssistanceInformation) {
	value.Present = ngapType.NPNPagingAssistanceInformationPresentPNINPNPagingAssistance
	value.PNINPNPagingAssistance = new(ngapType.AllowedPNINPNList)
	*value.PNINPNPagingAssistance = BuildAllowedPNINPNList()
	return
}

func BuildNPNSupport() (value ngapType.NPNSupport) {
	value.Present = ngapType.NPNSupportPresentSNPN
	value.SNPN = new(ngapType.NID)
	*value.SNPN = BuildNID()
	return
}

func BuildNRCGI() (value ngapType.NRCGI) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.NRCellIdentity = BuildNRCellIdentity()
	return
}

func BuildNRCGIList() (value ngapType.NRCGIList) {
	item := BuildNRCGI()
	value.List = append(value.List, item, item, item)
	return
}

func BuildNRCGIListForWarning() (value ngapType.NRCGIListForWarning) {
	item := BuildNRCGI()
	value.List = append(value.List, item, item, item)
	return
}

func BuildNRFrequencyBandList() (value ngapType.NRFrequencyBandList) {
	item := BuildNRFrequencyBandItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildNRFrequencyBandItem() (value ngapType.NRFrequencyBandItem) {
	value.NrFrequencyBand = BuildNRFrequencyBand()
	return
}

func BuildNRFrequencyInfo() (value ngapType.NRFrequencyInfo) {
	value.NrARFCN = BuildNRARFCN()
	value.FrequencyBandList = BuildNRFrequencyBandList()
	return
}

func BuildNRV2XServicesAuthorized() (value ngapType.NRV2XServicesAuthorized) {
	value.VehicleUE = new(ngapType.VehicleUE)
	*value.VehicleUE = BuildVehicleUE()
	value.PedestrianUE = new(ngapType.PedestrianUE)
	*value.PedestrianUE = BuildPedestrianUE()
	return
}

func BuildNRUESidelinkAggregateMaximumBitrate() (value ngapType.NRUESidelinkAggregateMaximumBitrate) {
	value.UESidelinkAggregateMaximumBitRate = BuildBitRate()
	return
}

func BuildOverloadResponse() (value ngapType.OverloadResponse) {
	value.Present = ngapType.OverloadResponsePresentOverloadAction
	value.OverloadAction = new(ngapType.OverloadAction)
	*value.OverloadAction = BuildOverloadAction()
	return
}

func BuildOverloadStartNSSAIList() (value ngapType.OverloadStartNSSAIList) {
	item := BuildOverloadStartNSSAIItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildOverloadStartNSSAIItem() (value ngapType.OverloadStartNSSAIItem) {
	value.SliceOverloadList = BuildSliceOverloadList()
	value.SliceOverloadResponse = new(ngapType.OverloadResponse)
	*value.SliceOverloadResponse = BuildOverloadResponse()
	value.SliceTrafficLoadReductionIndication = new(ngapType.TrafficLoadReductionIndication)
	*value.SliceTrafficLoadReductionIndication = BuildTrafficLoadReductionIndication()
	return
}

func BuildPacketErrorRate() (value ngapType.PacketErrorRate) {
	value.PERScalar = 0
	value.PERScalar = 9
	value.PERExponent = 0
	value.PERExponent = 9
	return
}

func BuildPagingAssisDataforCEcapabUE() (value ngapType.PagingAssisDataforCEcapabUE) {
	value.EUTRACGI = BuildEUTRACGI()
	value.CoverageEnhancementLevel = BuildCoverageEnhancementLevel()
	return
}

func BuildPagingAttemptInformation() (value ngapType.PagingAttemptInformation) {
	value.PagingAttemptCount = BuildPagingAttemptCount()
	value.IntendedNumberOfPagingAttempts = BuildIntendedNumberOfPagingAttempts()
	value.NextPagingAreaScope = new(ngapType.NextPagingAreaScope)
	*value.NextPagingAreaScope = BuildNextPagingAreaScope()
	return
}

func BuildPagingeDRXInformation() (value ngapType.PagingeDRXInformation) {
	value.PagingEDRXCycle = BuildPagingEDRXCycle()
	value.PagingTimeWindow = new(ngapType.PagingTimeWindow)
	*value.PagingTimeWindow = BuildPagingTimeWindow()
	return
}

func BuildPC5QoSParameters() (value ngapType.PC5QoSParameters) {
	value.Pc5QoSFlowList = BuildPC5QoSFlowList()
	value.Pc5LinkAggregateBitRates = new(ngapType.BitRate)
	*value.Pc5LinkAggregateBitRates = BuildBitRate()
	return
}

func BuildPC5QoSFlowList() (value ngapType.PC5QoSFlowList) {
	item := BuildPC5QoSFlowItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPC5QoSFlowItem() (value ngapType.PC5QoSFlowItem) {
	value.PQI = BuildFiveQI()
	value.Pc5FlowBitRates = new(ngapType.PC5FlowBitRates)
	*value.Pc5FlowBitRates = BuildPC5FlowBitRates()
	value.Range = new(ngapType.Range)
	*value.Range = BuildRange()
	return
}

func BuildPC5FlowBitRates() (value ngapType.PC5FlowBitRates) {
	value.GuaranteedFlowBitRate = BuildBitRate()
	value.MaximumFlowBitRate = BuildBitRate()
	return
}

func BuildPCIListForMDT() (value ngapType.PCIListForMDT) {
	item := BuildNRPCI()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionAggregateMaximumBitRate() (value ngapType.PDUSessionAggregateMaximumBitRate) {
	value.PDUSessionAggregateMaximumBitRateDL = BuildBitRate()
	value.PDUSessionAggregateMaximumBitRateUL = BuildBitRate()
	return
}

func BuildPDUSessionResourceAdmittedList() (value ngapType.PDUSessionResourceAdmittedList) {
	item := BuildPDUSessionResourceAdmittedItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceAdmittedItem() (value ngapType.PDUSessionResourceAdmittedItem) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildHandoverRequestAcknowledgeTransfer()
	value.HandoverRequestAcknowledgeTransfer, _ = ngap.EncodeHandoverRequestAcknowledgeTransfer(p)
	return
}

func BuildPDUSessionResourceFailedToModifyListModCfm() (value ngapType.PDUSessionResourceFailedToModifyListModCfm) {
	item := BuildPDUSessionResourceFailedToModifyItemModCfm()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceFailedToModifyItemModCfm() (value ngapType.PDUSessionResourceFailedToModifyItemModCfm) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceModifyIndicationUnsuccessfulTransfer()
	value.PDUSessionResourceModifyIndicationUnsuccessfulTransfer, _ = ngap.EncodePDUSessionResourceModifyIndicationUnsuccessfulTransfer(p)
	return
}

func BuildPDUSessionResourceFailedToModifyListModRes() (value ngapType.PDUSessionResourceFailedToModifyListModRes) {
	item := BuildPDUSessionResourceFailedToModifyItemModRes()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceFailedToModifyItemModRes() (value ngapType.PDUSessionResourceFailedToModifyItemModRes) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceModifyUnsuccessfulTransfer()
	value.PDUSessionResourceModifyUnsuccessfulTransfer, _ = ngap.EncodePDUSessionResourceModifyUnsuccessfulTransfer(p)
	return
}

func BuildPDUSessionResourceFailedToResumeListRESReq() (value ngapType.PDUSessionResourceFailedToResumeListRESReq) {
	item := BuildPDUSessionResourceFailedToResumeItemRESReq()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceFailedToResumeItemRESReq() (value ngapType.PDUSessionResourceFailedToResumeItemRESReq) {
	value.PDUSessionID = BuildPDUSessionID()
	value.Cause = BuildCause()
	return
}

func BuildPDUSessionResourceFailedToResumeListRESRes() (value ngapType.PDUSessionResourceFailedToResumeListRESRes) {
	item := BuildPDUSessionResourceFailedToResumeItemRESRes()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceFailedToResumeItemRESRes() (value ngapType.PDUSessionResourceFailedToResumeItemRESRes) {
	value.PDUSessionID = BuildPDUSessionID()
	value.Cause = BuildCause()
	return
}

func BuildPDUSessionResourceFailedToSetupListCxtFail() (value ngapType.PDUSessionResourceFailedToSetupListCxtFail) {
	item := BuildPDUSessionResourceFailedToSetupItemCxtFail()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceFailedToSetupItemCxtFail() (value ngapType.PDUSessionResourceFailedToSetupItemCxtFail) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceSetupUnsuccessfulTransfer()
	value.PDUSessionResourceSetupUnsuccessfulTransfer, _ = ngap.EncodePDUSessionResourceSetupUnsuccessfulTransfer(p)
	return
}

func BuildPDUSessionResourceFailedToSetupListCxtRes() (value ngapType.PDUSessionResourceFailedToSetupListCxtRes) {
	item := BuildPDUSessionResourceFailedToSetupItemCxtRes()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceFailedToSetupItemCxtRes() (value ngapType.PDUSessionResourceFailedToSetupItemCxtRes) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceSetupUnsuccessfulTransfer()
	value.PDUSessionResourceSetupUnsuccessfulTransfer, _ = ngap.EncodePDUSessionResourceSetupUnsuccessfulTransfer(p)
	return
}

func BuildPDUSessionResourceFailedToSetupListHOAck() (value ngapType.PDUSessionResourceFailedToSetupListHOAck) {
	item := BuildPDUSessionResourceFailedToSetupItemHOAck()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceFailedToSetupItemHOAck() (value ngapType.PDUSessionResourceFailedToSetupItemHOAck) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildHandoverResourceAllocationUnsuccessfulTransfer()
	value.HandoverResourceAllocationUnsuccessfulTransfer, _ = ngap.EncodeHandoverResourceAllocationUnsuccessfulTransfer(p)
	return
}

func BuildPDUSessionResourceFailedToSetupListPSReq() (value ngapType.PDUSessionResourceFailedToSetupListPSReq) {
	item := BuildPDUSessionResourceFailedToSetupItemPSReq()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceFailedToSetupItemPSReq() (value ngapType.PDUSessionResourceFailedToSetupItemPSReq) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPathSwitchRequestSetupFailedTransfer()
	value.PathSwitchRequestSetupFailedTransfer, _ = ngap.EncodePathSwitchRequestSetupFailedTransfer(p)
	return
}

func BuildPDUSessionResourceFailedToSetupListSURes() (value ngapType.PDUSessionResourceFailedToSetupListSURes) {
	item := BuildPDUSessionResourceFailedToSetupItemSURes()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceFailedToSetupItemSURes() (value ngapType.PDUSessionResourceFailedToSetupItemSURes) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceSetupUnsuccessfulTransfer()
	value.PDUSessionResourceSetupUnsuccessfulTransfer, _ = ngap.EncodePDUSessionResourceSetupUnsuccessfulTransfer(p)
	return
}

func BuildPDUSessionResourceHandoverList() (value ngapType.PDUSessionResourceHandoverList) {
	item := BuildPDUSessionResourceHandoverItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceHandoverItem() (value ngapType.PDUSessionResourceHandoverItem) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildHandoverCommandTransfer()
	value.HandoverCommandTransfer, _ = ngap.EncodeHandoverCommandTransfer(p)
	return
}

func BuildPDUSessionResourceInformationList() (value ngapType.PDUSessionResourceInformationList) {
	item := BuildPDUSessionResourceInformationItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceInformationItem() (value ngapType.PDUSessionResourceInformationItem) {
	value.PDUSessionID = BuildPDUSessionID()
	value.QosFlowInformationList = BuildQosFlowInformationList()
	value.DRBsToQosFlowsMappingList = new(ngapType.DRBsToQosFlowsMappingList)
	*value.DRBsToQosFlowsMappingList = BuildDRBsToQosFlowsMappingList()
	return
}

func BuildPDUSessionResourceListCxtRelCpl() (value ngapType.PDUSessionResourceListCxtRelCpl) {
	item := BuildPDUSessionResourceItemCxtRelCpl()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceItemCxtRelCpl() (value ngapType.PDUSessionResourceItemCxtRelCpl) {
	value.PDUSessionID = BuildPDUSessionID()
	return
}

func BuildPDUSessionResourceListCxtRelReq() (value ngapType.PDUSessionResourceListCxtRelReq) {
	item := BuildPDUSessionResourceItemCxtRelReq()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceItemCxtRelReq() (value ngapType.PDUSessionResourceItemCxtRelReq) {
	value.PDUSessionID = BuildPDUSessionID()
	return
}

func BuildPDUSessionResourceListHORqd() (value ngapType.PDUSessionResourceListHORqd) {
	item := BuildPDUSessionResourceItemHORqd()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceItemHORqd() (value ngapType.PDUSessionResourceItemHORqd) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildHandoverRequiredTransfer()
	value.HandoverRequiredTransfer, _ = ngap.EncodeHandoverRequiredTransfer(p)
	return
}

func BuildPDUSessionResourceModifyListModCfm() (value ngapType.PDUSessionResourceModifyListModCfm) {
	item := BuildPDUSessionResourceModifyItemModCfm()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceModifyItemModCfm() (value ngapType.PDUSessionResourceModifyItemModCfm) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceModifyConfirmTransfer()
	value.PDUSessionResourceModifyConfirmTransfer, _ = ngap.EncodePDUSessionResourceModifyConfirmTransfer(p)
	return
}

func BuildPDUSessionResourceModifyListModInd() (value ngapType.PDUSessionResourceModifyListModInd) {
	item := BuildPDUSessionResourceModifyItemModInd()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceModifyItemModInd() (value ngapType.PDUSessionResourceModifyItemModInd) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceModifyIndicationTransfer()
	value.PDUSessionResourceModifyIndicationTransfer, _ = ngap.EncodePDUSessionResourceModifyIndicationTransfer(p)
	return
}

func BuildPDUSessionResourceModifyListModReq() (value ngapType.PDUSessionResourceModifyListModReq) {
	item := BuildPDUSessionResourceModifyItemModReq()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceModifyItemModReq() (value ngapType.PDUSessionResourceModifyItemModReq) {
	value.PDUSessionID = BuildPDUSessionID()
	value.NASPDU = new(ngapType.NASPDU)
	*value.NASPDU = BuildNASPDU()
	p := BuildPDUSessionResourceModifyRequestTransfer()
	value.PDUSessionResourceModifyRequestTransfer, _ = ngap.EncodePDUSessionResourceModifyRequestTransfer(p)
	return
}

func BuildPDUSessionResourceModifyListModRes() (value ngapType.PDUSessionResourceModifyListModRes) {
	item := BuildPDUSessionResourceModifyItemModRes()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceModifyItemModRes() (value ngapType.PDUSessionResourceModifyItemModRes) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceModifyResponseTransfer()
	value.PDUSessionResourceModifyResponseTransfer, _ = ngap.EncodePDUSessionResourceModifyResponseTransfer(p)
	return
}

func BuildPDUSessionResourceNotifyList() (value ngapType.PDUSessionResourceNotifyList) {
	item := BuildPDUSessionResourceNotifyItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceNotifyItem() (value ngapType.PDUSessionResourceNotifyItem) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceNotifyTransfer()
	value.PDUSessionResourceNotifyTransfer, _ = ngap.EncodePDUSessionResourceNotifyTransfer(p)
	return
}

func BuildPDUSessionResourceReleasedListNot() (value ngapType.PDUSessionResourceReleasedListNot) {
	item := BuildPDUSessionResourceReleasedItemNot()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceReleasedItemNot() (value ngapType.PDUSessionResourceReleasedItemNot) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceNotifyReleasedTransfer()
	value.PDUSessionResourceNotifyReleasedTransfer, _ = ngap.EncodePDUSessionResourceNotifyReleasedTransfer(p)
	return
}

func BuildPDUSessionResourceReleasedListPSAck() (value ngapType.PDUSessionResourceReleasedListPSAck) {
	item := BuildPDUSessionResourceReleasedItemPSAck()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceReleasedItemPSAck() (value ngapType.PDUSessionResourceReleasedItemPSAck) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPathSwitchRequestUnsuccessfulTransfer()
	value.PathSwitchRequestUnsuccessfulTransfer, _ = ngap.EncodePathSwitchRequestUnsuccessfulTransfer(p)
	return
}

func BuildPDUSessionResourceReleasedListPSFail() (value ngapType.PDUSessionResourceReleasedListPSFail) {
	item := BuildPDUSessionResourceReleasedItemPSFail()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceReleasedItemPSFail() (value ngapType.PDUSessionResourceReleasedItemPSFail) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPathSwitchRequestUnsuccessfulTransfer()
	value.PathSwitchRequestUnsuccessfulTransfer, _ = ngap.EncodePathSwitchRequestUnsuccessfulTransfer(p)
	return
}

func BuildPDUSessionResourceReleasedListRelRes() (value ngapType.PDUSessionResourceReleasedListRelRes) {
	item := BuildPDUSessionResourceReleasedItemRelRes()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceReleasedItemRelRes() (value ngapType.PDUSessionResourceReleasedItemRelRes) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceReleaseResponseTransfer()
	value.PDUSessionResourceReleaseResponseTransfer, _ = ngap.EncodePDUSessionResourceReleaseResponseTransfer(p)
	return
}

func BuildPDUSessionResourceResumeListRESReq() (value ngapType.PDUSessionResourceResumeListRESReq) {
	item := BuildPDUSessionResourceResumeItemRESReq()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceResumeItemRESReq() (value ngapType.PDUSessionResourceResumeItemRESReq) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildUEContextResumeRequestTransfer()
	value.UEContextResumeRequestTransfer, _ = ngap.EncodeUEContextResumeRequestTransfer(p)
	return
}

func BuildPDUSessionResourceResumeListRESRes() (value ngapType.PDUSessionResourceResumeListRESRes) {
	item := BuildPDUSessionResourceResumeItemRESRes()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceResumeItemRESRes() (value ngapType.PDUSessionResourceResumeItemRESRes) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildUEContextResumeResponseTransfer()
	value.UEContextResumeResponseTransfer, _ = ngap.EncodeUEContextResumeResponseTransfer(p)
	return
}

func BuildPDUSessionResourceSecondaryRATUsageList() (value ngapType.PDUSessionResourceSecondaryRATUsageList) {
	item := BuildPDUSessionResourceSecondaryRATUsageItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceSecondaryRATUsageItem() (value ngapType.PDUSessionResourceSecondaryRATUsageItem) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildSecondaryRATDataUsageReportTransfer()
	value.SecondaryRATDataUsageReportTransfer, _ = ngap.EncodeSecondaryRATDataUsageReportTransfer(p)
	return
}

func BuildPDUSessionResourceSetupListCxtReq() (value ngapType.PDUSessionResourceSetupListCxtReq) {
	item := BuildPDUSessionResourceSetupItemCxtReq()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceSetupItemCxtReq() (value ngapType.PDUSessionResourceSetupItemCxtReq) {
	value.PDUSessionID = BuildPDUSessionID()
	value.NASPDU = new(ngapType.NASPDU)
	*value.NASPDU = BuildNASPDU()
	value.SNSSAI = BuildSNSSAI()
	p := BuildPDUSessionResourceSetupRequestTransfer()
	value.PDUSessionResourceSetupRequestTransfer, _ = ngap.EncodePDUSessionResourceSetupRequestTransfer(p)
	return
}

func BuildPDUSessionResourceSetupListCxtRes() (value ngapType.PDUSessionResourceSetupListCxtRes) {
	item := BuildPDUSessionResourceSetupItemCxtRes()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceSetupItemCxtRes() (value ngapType.PDUSessionResourceSetupItemCxtRes) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceSetupResponseTransfer()
	value.PDUSessionResourceSetupResponseTransfer, _ = ngap.EncodePDUSessionResourceSetupResponseTransfer(p)
	return
}

func BuildPDUSessionResourceSetupListHOReq() (value ngapType.PDUSessionResourceSetupListHOReq) {
	item := BuildPDUSessionResourceSetupItemHOReq()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceSetupItemHOReq() (value ngapType.PDUSessionResourceSetupItemHOReq) {
	value.PDUSessionID = BuildPDUSessionID()
	value.SNSSAI = BuildSNSSAI()
	p := BuildPDUSessionResourceSetupRequestTransfer()
	value.HandoverRequestTransfer, _ = ngap.EncodePDUSessionResourceSetupRequestTransfer(p)
	return
}

func BuildPDUSessionResourceSetupListSUReq() (value ngapType.PDUSessionResourceSetupListSUReq) {
	item := BuildPDUSessionResourceSetupItemSUReq()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceSetupItemSUReq() (value ngapType.PDUSessionResourceSetupItemSUReq) {
	value.PDUSessionID = BuildPDUSessionID()
	value.PDUSessionNASPDU = new(ngapType.NASPDU)
	*value.PDUSessionNASPDU = BuildNASPDU()
	value.SNSSAI = BuildSNSSAI()
	p := BuildPDUSessionResourceSetupRequestTransfer()
	value.PDUSessionResourceSetupRequestTransfer, _ = ngap.EncodePDUSessionResourceSetupRequestTransfer(p)
	return
}

func BuildPDUSessionResourceSetupListSURes() (value ngapType.PDUSessionResourceSetupListSURes) {
	item := BuildPDUSessionResourceSetupItemSURes()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceSetupItemSURes() (value ngapType.PDUSessionResourceSetupItemSURes) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceSetupResponseTransfer()
	value.PDUSessionResourceSetupResponseTransfer, _ = ngap.EncodePDUSessionResourceSetupResponseTransfer(p)
	return
}

func BuildPDUSessionResourceSuspendListSUSReq() (value ngapType.PDUSessionResourceSuspendListSUSReq) {
	item := BuildPDUSessionResourceSuspendItemSUSReq()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceSuspendItemSUSReq() (value ngapType.PDUSessionResourceSuspendItemSUSReq) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildUEContextSuspendRequestTransfer()
	value.UEContextSuspendRequestTransfer, _ = ngap.EncodeUEContextSuspendRequestTransfer(p)
	return
}

func BuildPDUSessionResourceSwitchedList() (value ngapType.PDUSessionResourceSwitchedList) {
	item := BuildPDUSessionResourceSwitchedItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceSwitchedItem() (value ngapType.PDUSessionResourceSwitchedItem) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPathSwitchRequestAcknowledgeTransfer()
	value.PathSwitchRequestAcknowledgeTransfer, _ = ngap.EncodePathSwitchRequestAcknowledgeTransfer(p)
	return
}

func BuildPDUSessionResourceToBeSwitchedDLList() (value ngapType.PDUSessionResourceToBeSwitchedDLList) {
	item := BuildPDUSessionResourceToBeSwitchedDLItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceToBeSwitchedDLItem() (value ngapType.PDUSessionResourceToBeSwitchedDLItem) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPathSwitchRequestTransfer()
	value.PathSwitchRequestTransfer, _ = ngap.EncodePathSwitchRequestTransfer(p)
	return
}

func BuildPDUSessionResourceToReleaseListHOCmd() (value ngapType.PDUSessionResourceToReleaseListHOCmd) {
	item := BuildPDUSessionResourceToReleaseItemHOCmd()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceToReleaseItemHOCmd() (value ngapType.PDUSessionResourceToReleaseItemHOCmd) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildHandoverPreparationUnsuccessfulTransfer()
	value.HandoverPreparationUnsuccessfulTransfer, _ = ngap.EncodeHandoverPreparationUnsuccessfulTransfer(p)
	return
}

func BuildPDUSessionResourceToReleaseListRelCmd() (value ngapType.PDUSessionResourceToReleaseListRelCmd) {
	item := BuildPDUSessionResourceToReleaseItemRelCmd()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPDUSessionResourceToReleaseItemRelCmd() (value ngapType.PDUSessionResourceToReleaseItemRelCmd) {
	value.PDUSessionID = BuildPDUSessionID()
	p := BuildPDUSessionResourceReleaseCommandTransfer()
	value.PDUSessionResourceReleaseCommandTransfer, _ = ngap.EncodePDUSessionResourceReleaseCommandTransfer(p)
	return
}

func BuildPDUSessionUsageReport() (value ngapType.PDUSessionUsageReport) {
	value.RATType = BuildRATType()
	value.PDUSessionTimedReportList = BuildVolumeTimedReportList()
	return
}

func BuildPLMNSupportList() (value ngapType.PLMNSupportList) {
	item := BuildPLMNSupportItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildPLMNSupportItem() (value ngapType.PLMNSupportItem) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.SliceSupportList = BuildSliceSupportList()
	return
}

func BuildPNINPNMobilityInformation() (value ngapType.PNINPNMobilityInformation) {
	value.AllowedPNINPIList = BuildAllowedPNINPNList()
	return
}

func BuildPWSFailedCellIDList() (value ngapType.PWSFailedCellIDList) {
	value.Present = ngapType.PWSFailedCellIDListPresentEUTRACGIPWSFailedList
	value.EUTRACGIPWSFailedList = new(ngapType.EUTRACGIList)
	*value.EUTRACGIPWSFailedList = BuildEUTRACGIList()
	value.Present = ngapType.PWSFailedCellIDListPresentNRCGIPWSFailedList
	value.NRCGIPWSFailedList = new(ngapType.NRCGIList)
	*value.NRCGIPWSFailedList = BuildNRCGIList()
	return
}

func BuildQosCharacteristics() (value ngapType.QosCharacteristics) {
	value.Present = ngapType.QosCharacteristicsPresentNonDynamic5QI
	value.NonDynamic5QI = new(ngapType.NonDynamic5QIDescriptor)
	*value.NonDynamic5QI = BuildNonDynamic5QIDescriptor()
	value.Present = ngapType.QosCharacteristicsPresentDynamic5QI
	value.Dynamic5QI = new(ngapType.Dynamic5QIDescriptor)
	*value.Dynamic5QI = BuildDynamic5QIDescriptor()
	return
}

func BuildQosFlowAcceptedList() (value ngapType.QosFlowAcceptedList) {
	item := BuildQosFlowAcceptedItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQosFlowAcceptedItem() (value ngapType.QosFlowAcceptedItem) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	return
}

func BuildQosFlowAddOrModifyRequestList() (value ngapType.QosFlowAddOrModifyRequestList) {
	item := BuildQosFlowAddOrModifyRequestItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQosFlowAddOrModifyRequestItem() (value ngapType.QosFlowAddOrModifyRequestItem) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	value.QosFlowLevelQosParameters = new(ngapType.QosFlowLevelQosParameters)
	*value.QosFlowLevelQosParameters = BuildQosFlowLevelQosParameters()
	value.ERABID = new(ngapType.ERABID)
	*value.ERABID = BuildERABID()
	return
}

func BuildQosFlowAddOrModifyResponseList() (value ngapType.QosFlowAddOrModifyResponseList) {
	item := BuildQosFlowAddOrModifyResponseItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQosFlowAddOrModifyResponseItem() (value ngapType.QosFlowAddOrModifyResponseItem) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	return
}

func BuildQosFlowInformationList() (value ngapType.QosFlowInformationList) {
	item := BuildQosFlowInformationItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQosFlowInformationItem() (value ngapType.QosFlowInformationItem) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	value.DLForwarding = new(ngapType.DLForwarding)
	*value.DLForwarding = BuildDLForwarding()
	return
}

func BuildQosFlowLevelQosParameters() (value ngapType.QosFlowLevelQosParameters) {
	value.QosCharacteristics = BuildQosCharacteristics()
	value.AllocationAndRetentionPriority = BuildAllocationAndRetentionPriority()
	value.GBRQosInformation = new(ngapType.GBRQosInformation)
	*value.GBRQosInformation = BuildGBRQosInformation()
	value.ReflectiveQosAttribute = new(ngapType.ReflectiveQosAttribute)
	*value.ReflectiveQosAttribute = BuildReflectiveQosAttribute()
	value.AdditionalQosFlowInformation = new(ngapType.AdditionalQosFlowInformation)
	*value.AdditionalQosFlowInformation = BuildAdditionalQosFlowInformation()
	return
}

func BuildQosFlowListWithCause() (value ngapType.QosFlowListWithCause) {
	item := BuildQosFlowWithCauseItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQosFlowWithCauseItem() (value ngapType.QosFlowWithCauseItem) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	value.Cause = BuildCause()
	return
}

func BuildQosFlowModifyConfirmList() (value ngapType.QosFlowModifyConfirmList) {
	item := BuildQosFlowModifyConfirmItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQosFlowModifyConfirmItem() (value ngapType.QosFlowModifyConfirmItem) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	return
}

func BuildQosFlowNotifyList() (value ngapType.QosFlowNotifyList) {
	item := BuildQosFlowNotifyItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQosFlowNotifyItem() (value ngapType.QosFlowNotifyItem) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	value.NotificationCause = BuildNotificationCause()
	return
}

func BuildQosFlowPerTNLInformation() (value ngapType.QosFlowPerTNLInformation) {
	value.UPTransportLayerInformation = BuildUPTransportLayerInformation()
	value.AssociatedQosFlowList = BuildAssociatedQosFlowList()
	return
}

func BuildQosFlowPerTNLInformationList() (value ngapType.QosFlowPerTNLInformationList) {
	item := BuildQosFlowPerTNLInformationItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQosFlowPerTNLInformationItem() (value ngapType.QosFlowPerTNLInformationItem) {
	value.QosFlowPerTNLInformation = BuildQosFlowPerTNLInformation()
	return
}

func BuildQosFlowSetupRequestList() (value ngapType.QosFlowSetupRequestList) {
	item := BuildQosFlowSetupRequestItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQosFlowSetupRequestItem() (value ngapType.QosFlowSetupRequestItem) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	value.QosFlowLevelQosParameters = BuildQosFlowLevelQosParameters()
	value.ERABID = new(ngapType.ERABID)
	*value.ERABID = BuildERABID()
	return
}

func BuildQosFlowListWithDataForwarding() (value ngapType.QosFlowListWithDataForwarding) {
	item := BuildQosFlowItemWithDataForwarding()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQosFlowItemWithDataForwarding() (value ngapType.QosFlowItemWithDataForwarding) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	value.DataForwardingAccepted = new(ngapType.DataForwardingAccepted)
	*value.DataForwardingAccepted = BuildDataForwardingAccepted()
	return
}

func BuildQosFlowToBeForwardedList() (value ngapType.QosFlowToBeForwardedList) {
	item := BuildQosFlowToBeForwardedItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQosFlowToBeForwardedItem() (value ngapType.QosFlowToBeForwardedItem) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	return
}

func BuildQoSFlowsUsageReportList() (value ngapType.QoSFlowsUsageReportList) {
	item := BuildQoSFlowsUsageReportItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildQoSFlowsUsageReportItem() (value ngapType.QoSFlowsUsageReportItem) {
	value.QosFlowIdentifier = BuildQosFlowIdentifier()
	value.RATType = BuildRATType()
	value.QoSFlowsTimedReportList = BuildVolumeTimedReportList()
	return
}

func BuildRANStatusTransferTransparentContainer() (value ngapType.RANStatusTransferTransparentContainer) {
	value.DRBsSubjectToStatusTransferList = BuildDRBsSubjectToStatusTransferList()
	return
}

func BuildRATRestrictions() (value ngapType.RATRestrictions) {
	item := BuildRATRestrictionsItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildRATRestrictionsItem() (value ngapType.RATRestrictionsItem) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.RATRestrictionInformation = BuildRATRestrictionInformation()
	return
}

func BuildRecommendedCellsForPaging() (value ngapType.RecommendedCellsForPaging) {
	value.RecommendedCellList = BuildRecommendedCellList()
	return
}

func BuildRecommendedCellList() (value ngapType.RecommendedCellList) {
	item := BuildRecommendedCellItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildRecommendedCellItem() (value ngapType.RecommendedCellItem) {
	value.NGRANCGI = BuildNGRANCGI()
	value.TimeStayedInCell = new(int64)
	*value.TimeStayedInCell = 0
	value.TimeStayedInCell = new(int64)
	*value.TimeStayedInCell = 4095
	return
}

func BuildRecommendedRANNodesForPaging() (value ngapType.RecommendedRANNodesForPaging) {
	value.RecommendedRANNodeList = BuildRecommendedRANNodeList()
	return
}

func BuildRecommendedRANNodeList() (value ngapType.RecommendedRANNodeList) {
	item := BuildRecommendedRANNodeItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildRecommendedRANNodeItem() (value ngapType.RecommendedRANNodeItem) {
	value.AMFPagingTarget = BuildAMFPagingTarget()
	return
}

func BuildRedundantPDUSessionInformation() (value ngapType.RedundantPDUSessionInformation) {
	value.RSN = BuildRSN()
	return
}

func BuildResetType() (value ngapType.ResetType) {
	value.Present = ngapType.ResetTypePresentNGInterface
	value.NGInterface = new(ngapType.ResetAll)
	*value.NGInterface = BuildResetAll()
	value.Present = ngapType.ResetTypePresentPartOfNGInterface
	value.PartOfNGInterface = new(ngapType.UEAssociatedLogicalNGConnectionList)
	*value.PartOfNGInterface = BuildUEAssociatedLogicalNGConnectionList()
	return
}

func BuildRIMInformationTransfer() (value ngapType.RIMInformationTransfer) {
	value.TargetRANNodeID = BuildTargetRANNodeID()
	value.SourceRANNodeID = BuildSourceRANNodeID()
	value.RIMInformation = BuildRIMInformation()
	return
}

func BuildRIMInformation() (value ngapType.RIMInformation) {
	value.TargetgNBSetID = BuildGNBSetID()
	value.RIMRSDetection = BuildRIMRSDetection()
	return
}

func BuildScheduledCommunicationTime() (value ngapType.ScheduledCommunicationTime) {
	value.DayofWeek = new(ngapType.BitString)
	value.DayofWeek.BitLength = 7
	value.DayofWeek.Bytes = []byte{171}
	//value.TimeofDayStart = new(ngapType.TimeofDayStart)
	//*value.TimeofDayStart = BuildTimeofDayStart()
	//value.TimeofDayEnd = new(ngapType.TimeofDayEnd)
	//*value.TimeofDayEnd = BuildTimeofDayEnd()
	return
}

func BuildSCTPTLAs() (value ngapType.SCTPTLAs) {
	item := BuildTransportLayerAddress()
	value.List = append(value.List, item, item, item)
	return
}

func BuildSecondaryRATUsageInformation() (value ngapType.SecondaryRATUsageInformation) {
	value.PDUSessionUsageReport = new(ngapType.PDUSessionUsageReport)
	*value.PDUSessionUsageReport = BuildPDUSessionUsageReport()
	value.QosFlowsUsageReportList = new(ngapType.QoSFlowsUsageReportList)
	*value.QosFlowsUsageReportList = BuildQoSFlowsUsageReportList()
	return
}

func BuildSecurityContext() (value ngapType.SecurityContext) {
	value.NextHopChainingCount = BuildNextHopChainingCount()
	value.NextHopNH = BuildSecurityKey()
	return
}

func BuildSecurityIndication() (value ngapType.SecurityIndication) {
	value.IntegrityProtectionIndication = BuildIntegrityProtectionIndication()
	value.ConfidentialityProtectionIndication = BuildConfidentialityProtectionIndication()
	value.MaximumIntegrityProtectedDataRateUL = new(ngapType.MaximumIntegrityProtectedDataRate)
	*value.MaximumIntegrityProtectedDataRateUL = BuildMaximumIntegrityProtectedDataRate()
	return
}

func BuildSecurityResult() (value ngapType.SecurityResult) {
	value.IntegrityProtectionResult = BuildIntegrityProtectionResult()
	value.ConfidentialityProtectionResult = BuildConfidentialityProtectionResult()
	return
}

func BuildSensorMeasurementConfiguration() (value ngapType.SensorMeasurementConfiguration) {
	value.SensorMeasConfig = BuildSensorMeasConfig()
	value.SensorMeasConfigNameList = new(ngapType.SensorMeasConfigNameList)
	*value.SensorMeasConfigNameList = BuildSensorMeasConfigNameList()
	return
}

func BuildSensorMeasConfigNameList() (value ngapType.SensorMeasConfigNameList) {
	item := BuildSensorMeasConfigNameItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildSensorMeasConfigNameItem() (value ngapType.SensorMeasConfigNameItem) {
	value.SensorNameConfig = BuildSensorNameConfig()
	return
}

func BuildSensorNameConfig() (value ngapType.SensorNameConfig) {
	value.Present = ngapType.SensorNameConfigPresentUncompensatedBarometricConfig
	value.UncompensatedBarometricConfig = new(ngapType.UncompensatedBarometricConfig)
	*value.UncompensatedBarometricConfig = BuildUncompensatedBarometricConfig()
	value.Present = ngapType.SensorNameConfigPresentUeSpeedConfig
	value.UeSpeedConfig = new(ngapType.UeSpeedConfig)
	*value.UeSpeedConfig = BuildUeSpeedConfig()
	value.Present = ngapType.SensorNameConfigPresentUeOrientationConfig
	value.UeOrientationConfig = new(ngapType.UeOrientationConfig)
	*value.UeOrientationConfig = BuildUeOrientationConfig()
	return
}

func BuildServedGUAMIList() (value ngapType.ServedGUAMIList) {
	item := BuildServedGUAMIItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildServedGUAMIItem() (value ngapType.ServedGUAMIItem) {
	value.GUAMI = BuildGUAMI()
	value.BackupAMFName = new(ngapType.AMFName)
	*value.BackupAMFName = BuildAMFName()
	return
}

func BuildServiceAreaInformation() (value ngapType.ServiceAreaInformation) {
	item := BuildServiceAreaInformationItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildServiceAreaInformationItem() (value ngapType.ServiceAreaInformationItem) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.AllowedTACs = new(ngapType.AllowedTACs)
	*value.AllowedTACs = BuildAllowedTACs()
	value.NotAllowedTACs = new(ngapType.NotAllowedTACs)
	*value.NotAllowedTACs = BuildNotAllowedTACs()
	return
}

func BuildSliceOverloadList() (value ngapType.SliceOverloadList) {
	item := BuildSliceOverloadItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildSliceOverloadItem() (value ngapType.SliceOverloadItem) {
	value.SNSSAI = BuildSNSSAI()
	return
}

func BuildSliceSupportList() (value ngapType.SliceSupportList) {
	item := BuildSliceSupportItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildSliceSupportItem() (value ngapType.SliceSupportItem) {
	value.SNSSAI = BuildSNSSAI()
	return
}

func BuildSNPNMobilityInformation() (value ngapType.SNPNMobilityInformation) {
	value.ServingNID = BuildNID()
	return
}

func BuildSNSSAI() (value ngapType.SNSSAI) {
	value.SST = BuildSST()
	value.SD = new(ngapType.SD)
	*value.SD = BuildSD()
	return
}

func BuildSONConfigurationTransfer() (value ngapType.SONConfigurationTransfer) {
	value.TargetRANNodeID = BuildTargetRANNodeID()
	value.SourceRANNodeID = BuildSourceRANNodeID()
	value.SONInformation = BuildSONInformation()
	value.XnTNLConfigurationInfo = new(ngapType.XnTNLConfigurationInfo)
	*value.XnTNLConfigurationInfo = BuildXnTNLConfigurationInfo()
	return
}

func BuildSONInformation() (value ngapType.SONInformation) {
	value.Present = ngapType.SONInformationPresentSONInformationRequest
	value.SONInformationRequest = new(ngapType.SONInformationRequest)
	*value.SONInformationRequest = BuildSONInformationRequest()
	value.Present = ngapType.SONInformationPresentSONInformationReply
	value.SONInformationReply = new(ngapType.SONInformationReply)
	*value.SONInformationReply = BuildSONInformationReply()
	return
}

func BuildSONInformationReply() (value ngapType.SONInformationReply) {
	value.XnTNLConfigurationInfo = new(ngapType.XnTNLConfigurationInfo)
	*value.XnTNLConfigurationInfo = BuildXnTNLConfigurationInfo()
	return
}

func BuildSONInformationReport() (value ngapType.SONInformationReport) {
	value.Present = ngapType.SONInformationReportPresentFailureIndicationInformation
	value.FailureIndicationInformation = new(ngapType.FailureIndication)
	*value.FailureIndicationInformation = BuildFailureIndication()
	value.Present = ngapType.SONInformationReportPresentHOReportInformation
	value.HOReportInformation = new(ngapType.HOReport)
	*value.HOReportInformation = BuildHOReport()
	return
}

func BuildSourceNGRANNodeToTargetNGRANNodeTransparentContainer() (value ngapType.SourceNGRANNodeToTargetNGRANNodeTransparentContainer) {
	value.RRCContainer = BuildRRCContainer()
	value.PDUSessionResourceInformationList = new(ngapType.PDUSessionResourceInformationList)
	*value.PDUSessionResourceInformationList = BuildPDUSessionResourceInformationList()
	value.ERABInformationList = new(ngapType.ERABInformationList)
	*value.ERABInformationList = BuildERABInformationList()
	value.TargetCellID = BuildNGRANCGI()
	value.IndexToRFSP = new(ngapType.IndexToRFSP)
	*value.IndexToRFSP = BuildIndexToRFSP()
	value.UEHistoryInformation = BuildUEHistoryInformation()
	return
}

func BuildSourceRANNodeID() (value ngapType.SourceRANNodeID) {
	value.GlobalRANNodeID = BuildGlobalRANNodeID()
	value.SelectedTAI = BuildTAI()
	return
}

func BuildSourceToTargetAMFInformationReroute() (value ngapType.SourceToTargetAMFInformationReroute) {
	value.ConfiguredNSSAI = new(ngapType.ConfiguredNSSAI)
	*value.ConfiguredNSSAI = BuildConfiguredNSSAI()
	value.RejectedNSSAIinPLMN = new(ngapType.RejectedNSSAIinPLMN)
	*value.RejectedNSSAIinPLMN = BuildRejectedNSSAIinPLMN()
	value.RejectedNSSAIinTA = new(ngapType.RejectedNSSAIinTA)
	*value.RejectedNSSAIinTA = BuildRejectedNSSAIinTA()
	return
}

func BuildSupportedTAList() (value ngapType.SupportedTAList) {
	item := BuildSupportedTAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildSupportedTAItem() (value ngapType.SupportedTAItem) {
	value.TAC = BuildTAC()
	value.BroadcastPLMNList = BuildBroadcastPLMNList()
	return
}

func BuildTAI() (value ngapType.TAI) {
	value.PLMNIdentity = BuildPLMNIdentity()
	value.TAC = BuildTAC()
	return
}

func BuildTAIBroadcastEUTRA() (value ngapType.TAIBroadcastEUTRA) {
	item := BuildTAIBroadcastEUTRAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildTAIBroadcastEUTRAItem() (value ngapType.TAIBroadcastEUTRAItem) {
	value.TAI = BuildTAI()
	value.CompletedCellsInTAIEUTRA = BuildCompletedCellsInTAIEUTRA()
	return
}

func BuildTAIBroadcastNR() (value ngapType.TAIBroadcastNR) {
	item := BuildTAIBroadcastNRItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildTAIBroadcastNRItem() (value ngapType.TAIBroadcastNRItem) {
	value.TAI = BuildTAI()
	value.CompletedCellsInTAINR = BuildCompletedCellsInTAINR()
	return
}

func BuildTAICancelledEUTRA() (value ngapType.TAICancelledEUTRA) {
	item := BuildTAICancelledEUTRAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildTAICancelledEUTRAItem() (value ngapType.TAICancelledEUTRAItem) {
	value.TAI = BuildTAI()
	value.CancelledCellsInTAIEUTRA = BuildCancelledCellsInTAIEUTRA()
	return
}

func BuildTAICancelledNR() (value ngapType.TAICancelledNR) {
	item := BuildTAICancelledNRItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildTAICancelledNRItem() (value ngapType.TAICancelledNRItem) {
	value.TAI = BuildTAI()
	value.CancelledCellsInTAINR = BuildCancelledCellsInTAINR()
	return
}

func BuildTAIListForInactive() (value ngapType.TAIListForInactive) {
	item := BuildTAIListForInactiveItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildTAIListForInactiveItem() (value ngapType.TAIListForInactiveItem) {
	value.TAI = BuildTAI()
	return
}

func BuildTAIListForPaging() (value ngapType.TAIListForPaging) {
	item := BuildTAIListForPagingItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildTAIListForPagingItem() (value ngapType.TAIListForPagingItem) {
	value.TAI = BuildTAI()
	return
}

func BuildTAIListForRestart() (value ngapType.TAIListForRestart) {
	item := BuildTAI()
	value.List = append(value.List, item, item, item)
	return
}

func BuildTAIListForWarning() (value ngapType.TAIListForWarning) {
	item := BuildTAI()
	value.List = append(value.List, item, item, item)
	return
}

func BuildTargeteNBID() (value ngapType.TargeteNBID) {
	value.GlobalENBID = BuildGlobalNgENBID()
	value.SelectedEPSTAI = BuildEPSTAI()
	return
}

func BuildTargetID() (value ngapType.TargetID) {
	value.Present = ngapType.TargetIDPresentTargetRANNodeID
	value.TargetRANNodeID = new(ngapType.TargetRANNodeID)
	*value.TargetRANNodeID = BuildTargetRANNodeID()
	value.Present = ngapType.TargetIDPresentTargeteNBID
	value.TargeteNBID = new(ngapType.TargeteNBID)
	*value.TargeteNBID = BuildTargeteNBID()
	return
}

func BuildTargetNGRANNodeToSourceNGRANNodeTransparentContainer() (value ngapType.TargetNGRANNodeToSourceNGRANNodeTransparentContainer) {
	value.RRCContainer = BuildRRCContainer()
	return
}

func BuildTargetNGRANNodeToSourceNGRANNodeFailureTransparentContainer() (value ngapType.TargetNGRANNodeToSourceNGRANNodeFailureTransparentContainer) {
	value.CellCAGInformation = BuildCellCAGInformation()
	return
}

func BuildTargetRANNodeID() (value ngapType.TargetRANNodeID) {
	value.GlobalRANNodeID = BuildGlobalRANNodeID()
	value.SelectedTAI = BuildTAI()
	return
}

func BuildTargetRNCID() (value ngapType.TargetRNCID) {
	value.LAI = BuildLAI()
	value.RNCID = BuildRNCID()
	value.ExtendedRNCID = new(ngapType.ExtendedRNCID)
	*value.ExtendedRNCID = BuildExtendedRNCID()
	return
}

func BuildTNGFID() (value ngapType.TNGFID) {
	value.Present = ngapType.TNGFIDPresentTNGFID
	value.TNGFID = new(ngapType.BitString)
	value.TNGFID.BitLength = 32
	value.TNGFID.Bytes = []byte{171, 1, 2, 3}
	return
}

func BuildTNLAssociationList() (value ngapType.TNLAssociationList) {
	item := BuildTNLAssociationItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildTNLAssociationItem() (value ngapType.TNLAssociationItem) {
	value.TNLAssociationAddress = BuildCPTransportLayerInformation()
	value.Cause = BuildCause()
	return
}

func BuildTooearlyIntersystemHO() (value ngapType.TooearlyIntersystemHO) {
	value.SourcecellID = BuildEUTRACGI()
	value.FailurecellID = BuildNGRANCGI()
	value.UERLFReportContainer = new(ngapType.UERLFReportContainer)
	*value.UERLFReportContainer = BuildUERLFReportContainer()
	return
}

func BuildTraceActivation() (value ngapType.TraceActivation) {
	value.NGRANTraceID = BuildNGRANTraceID()
	value.InterfacesToTrace = BuildInterfacesToTrace()
	value.TraceDepth = BuildTraceDepth()
	value.TraceCollectionEntityIPAddress = BuildTransportLayerAddress()
	return
}

func BuildTAIBasedMDT() (value ngapType.TAIBasedMDT) {
	value.TAIListforMDT = BuildTAIListforMDT()
	return
}

func BuildTAIListforMDT() (value ngapType.TAIListforMDT) {
	item := BuildTAI()
	value.List = append(value.List, item, item, item)
	return
}

func BuildTABasedMDT() (value ngapType.TABasedMDT) {
	value.TAListforMDT = BuildTAListforMDT()
	return
}

func BuildTAListforMDT() (value ngapType.TAListforMDT) {
	item := BuildTAC()
	value.List = append(value.List, item, item, item)
	return
}

func BuildTWIFID() (value ngapType.TWIFID) {
	value.Present = ngapType.TWIFIDPresentTWIFID
	value.TWIFID = new(ngapType.BitString)
	value.TWIFID.BitLength = 32
	value.TWIFID.Bytes = []byte{171, 1, 2, 3}
	return
}

func BuildTSCAssistanceInformation() (value ngapType.TSCAssistanceInformation) {
	value.Periodicity = BuildPeriodicity()
	value.BurstArrivalTime = new(ngapType.BurstArrivalTime)
	*value.BurstArrivalTime = BuildBurstArrivalTime()
	return
}

func BuildTSCTrafficCharacteristics() (value ngapType.TSCTrafficCharacteristics) {
	value.TSCAssistanceInformationDL = new(ngapType.TSCAssistanceInformation)
	*value.TSCAssistanceInformationDL = BuildTSCAssistanceInformation()
	value.TSCAssistanceInformationUL = new(ngapType.TSCAssistanceInformation)
	*value.TSCAssistanceInformationUL = BuildTSCAssistanceInformation()
	return
}

func BuildUEAggregateMaximumBitRate() (value ngapType.UEAggregateMaximumBitRate) {
	value.UEAggregateMaximumBitRateDL = BuildBitRate()
	value.UEAggregateMaximumBitRateUL = BuildBitRate()
	return
}

func BuildUEAssociatedLogicalNGConnectionList() (value ngapType.UEAssociatedLogicalNGConnectionList) {
	item := BuildUEAssociatedLogicalNGConnectionItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildUEAssociatedLogicalNGConnectionItem() (value ngapType.UEAssociatedLogicalNGConnectionItem) {
	value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*value.AMFUENGAPID = BuildAMFUENGAPID()
	value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*value.RANUENGAPID = BuildRANUENGAPID()
	return
}

func BuildUEDifferentiationInfo() (value ngapType.UEDifferentiationInfo) {
	value.PeriodicCommunicationIndicator = new(ngapType.PeriodicCommunicationIndicator)
	*value.PeriodicCommunicationIndicator = BuildPeriodicCommunicationIndicator()
	value.PeriodicTime = new(int64)
	*value.PeriodicTime = 1
	value.PeriodicTime = new(int64)
	*value.PeriodicTime = 3600
	value.ScheduledCommunicationTime = new(ngapType.ScheduledCommunicationTime)
	*value.ScheduledCommunicationTime = BuildScheduledCommunicationTime()
	value.StationaryIndication = new(ngapType.StationaryIndication)
	*value.StationaryIndication = BuildStationaryIndication()
	value.TrafficProfile = new(ngapType.TrafficProfile)
	*value.TrafficProfile = BuildTrafficProfile()
	value.BatteryIndication = new(ngapType.BatteryIndication)
	*value.BatteryIndication = BuildBatteryIndication()
	return
}

func BuildUEHistoryInformation() (value ngapType.UEHistoryInformation) {
	item := BuildLastVisitedCellItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildUEHistoryInformationFromTheUE() (value ngapType.UEHistoryInformationFromTheUE) {
	value.Present = ngapType.UEHistoryInformationFromTheUEPresentNR
	value.NR = new(ngapType.NRMobilityHistoryReport)
	*value.NR = BuildNRMobilityHistoryReport()
	return
}

func BuildUEIdentityIndexValue() (value ngapType.UEIdentityIndexValue) {
	value.Present = ngapType.UEIdentityIndexValuePresentIndexLength10
	value.IndexLength10 = new(ngapType.BitString)
	value.IndexLength10.BitLength = 10
	value.IndexLength10.Bytes = []byte{171, 11}
	return
}

func BuildUENGAPIDs() (value ngapType.UENGAPIDs) {
	value.Present = ngapType.UENGAPIDsPresentUENGAPIDPair
	value.UENGAPIDPair = new(ngapType.UENGAPIDPair)
	*value.UENGAPIDPair = BuildUENGAPIDPair()
	value.Present = ngapType.UENGAPIDsPresentAMFUENGAPID
	value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*value.AMFUENGAPID = BuildAMFUENGAPID()
	return
}

func BuildUENGAPIDPair() (value ngapType.UENGAPIDPair) {
	value.AMFUENGAPID = BuildAMFUENGAPID()
	value.RANUENGAPID = BuildRANUENGAPID()
	return
}

func BuildUEPagingIdentity() (value ngapType.UEPagingIdentity) {
	value.Present = ngapType.UEPagingIdentityPresentFiveGSTMSI
	value.FiveGSTMSI = new(ngapType.FiveGSTMSI)
	*value.FiveGSTMSI = BuildFiveGSTMSI()
	return
}

func BuildUEPresenceInAreaOfInterestList() (value ngapType.UEPresenceInAreaOfInterestList) {
	item := BuildUEPresenceInAreaOfInterestItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildUEPresenceInAreaOfInterestItem() (value ngapType.UEPresenceInAreaOfInterestItem) {
	value.LocationReportingReferenceID = BuildLocationReportingReferenceID()
	value.UEPresence = BuildUEPresence()
	return
}

func BuildUERadioCapabilityForPaging() (value ngapType.UERadioCapabilityForPaging) {
	value.UERadioCapabilityForPagingOfNR = new(ngapType.UERadioCapabilityForPagingOfNR)
	*value.UERadioCapabilityForPagingOfNR = BuildUERadioCapabilityForPagingOfNR()
	value.UERadioCapabilityForPagingOfEUTRA = new(ngapType.UERadioCapabilityForPagingOfEUTRA)
	*value.UERadioCapabilityForPagingOfEUTRA = BuildUERadioCapabilityForPagingOfEUTRA()
	return
}

func BuildUERLFReportContainer() (value ngapType.UERLFReportContainer) {
	value.Present = ngapType.UERLFReportContainerPresentNR
	value.NR = new(ngapType.NRUERLFReportContainer)
	*value.NR = BuildNRUERLFReportContainer()
	value.Present = ngapType.UERLFReportContainerPresentLTE
	value.LTE = new(ngapType.LTEUERLFReportContainer)
	*value.LTE = BuildLTEUERLFReportContainer()
	return
}

func BuildUESecurityCapabilities() (value ngapType.UESecurityCapabilities) {
	value.NRencryptionAlgorithms = BuildNRencryptionAlgorithms()
	value.NRintegrityProtectionAlgorithms = BuildNRintegrityProtectionAlgorithms()
	value.EUTRAencryptionAlgorithms = BuildEUTRAencryptionAlgorithms()
	value.EUTRAintegrityProtectionAlgorithms = BuildEUTRAintegrityProtectionAlgorithms()
	return
}

func BuildULCPSecurityInformation() (value ngapType.ULCPSecurityInformation) {
	value.UlNASMAC = BuildULNASMAC()
	value.UlNASCount = BuildULNASCount()
	return
}

func BuildULNGUUPTNLModifyList() (value ngapType.ULNGUUPTNLModifyList) {
	item := BuildULNGUUPTNLModifyItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildULNGUUPTNLModifyItem() (value ngapType.ULNGUUPTNLModifyItem) {
	value.ULNGUUPTNLInformation = BuildUPTransportLayerInformation()
	value.DLNGUUPTNLInformation = BuildUPTransportLayerInformation()
	return
}

func BuildUnavailableGUAMIList() (value ngapType.UnavailableGUAMIList) {
	item := BuildUnavailableGUAMIItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildUnavailableGUAMIItem() (value ngapType.UnavailableGUAMIItem) {
	value.GUAMI = BuildGUAMI()
	value.TimerApproachForGUAMIRemoval = new(ngapType.TimerApproachForGUAMIRemoval)
	*value.TimerApproachForGUAMIRemoval = BuildTimerApproachForGUAMIRemoval()
	value.BackupAMFName = new(ngapType.AMFName)
	*value.BackupAMFName = BuildAMFName()
	return
}

func BuildUPTransportLayerInformation() (value ngapType.UPTransportLayerInformation) {
	value.Present = ngapType.UPTransportLayerInformationPresentGTPTunnel
	value.GTPTunnel = new(ngapType.GTPTunnel)
	*value.GTPTunnel = BuildGTPTunnel()
	return
}

func BuildUPTransportLayerInformationList() (value ngapType.UPTransportLayerInformationList) {
	item := BuildUPTransportLayerInformationItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildUPTransportLayerInformationItem() (value ngapType.UPTransportLayerInformationItem) {
	value.NGUUPTNLInformation = BuildUPTransportLayerInformation()
	return
}

func BuildUPTransportLayerInformationPairList() (value ngapType.UPTransportLayerInformationPairList) {
	item := BuildUPTransportLayerInformationPairItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildUPTransportLayerInformationPairItem() (value ngapType.UPTransportLayerInformationPairItem) {
	value.ULNGUUPTNLInformation = BuildUPTransportLayerInformation()
	value.DLNGUUPTNLInformation = BuildUPTransportLayerInformation()
	return
}

func BuildUserLocationInformation() (value ngapType.UserLocationInformation) {
	value.Present = ngapType.UserLocationInformationPresentUserLocationInformationEUTRA
	value.UserLocationInformationEUTRA = new(ngapType.UserLocationInformationEUTRA)
	*value.UserLocationInformationEUTRA = BuildUserLocationInformationEUTRA()
	value.Present = ngapType.UserLocationInformationPresentUserLocationInformationNR
	value.UserLocationInformationNR = new(ngapType.UserLocationInformationNR)
	*value.UserLocationInformationNR = BuildUserLocationInformationNR()
	value.Present = ngapType.UserLocationInformationPresentUserLocationInformationN3IWF
	value.UserLocationInformationN3IWF = new(ngapType.UserLocationInformationN3IWF)
	*value.UserLocationInformationN3IWF = BuildUserLocationInformationN3IWF()
	return
}

func BuildUserLocationInformationEUTRA() (value ngapType.UserLocationInformationEUTRA) {
	value.EUTRACGI = BuildEUTRACGI()
	value.TAI = BuildTAI()
	value.TimeStamp = new(ngapType.TimeStamp)
	*value.TimeStamp = BuildTimeStamp()
	return
}

func BuildUserLocationInformationN3IWF() (value ngapType.UserLocationInformationN3IWF) {
	value.IPAddress = BuildTransportLayerAddress()
	value.PortNumber = BuildPortNumber()
	return
}

func BuildUserLocationInformationTNGF() (value ngapType.UserLocationInformationTNGF) {
	value.TNAPID = BuildTNAPID()
	value.IPAddress = BuildTransportLayerAddress()
	value.PortNumber = new(ngapType.PortNumber)
	*value.PortNumber = BuildPortNumber()
	return
}

func BuildUserLocationInformationTWIF() (value ngapType.UserLocationInformationTWIF) {
	value.TWAPID = BuildTWAPID()
	value.IPAddress = BuildTransportLayerAddress()
	value.PortNumber = new(ngapType.PortNumber)
	*value.PortNumber = BuildPortNumber()
	return
}

func BuildUserLocationInformationWAGF() (value ngapType.UserLocationInformationWAGF) {
	value.Present = ngapType.UserLocationInformationWAGFPresentGlobalLineID
	value.GlobalLineID = new(ngapType.GlobalLineID)
	*value.GlobalLineID = BuildGlobalLineID()
	value.Present = ngapType.UserLocationInformationWAGFPresentHFCNodeID
	value.HFCNodeID = new(ngapType.HFCNodeID)
	*value.HFCNodeID = BuildHFCNodeID()
	return
}

func BuildUserLocationInformationNR() (value ngapType.UserLocationInformationNR) {
	value.NRCGI = BuildNRCGI()
	value.TAI = BuildTAI()
	value.TimeStamp = new(ngapType.TimeStamp)
	*value.TimeStamp = BuildTimeStamp()
	return
}

func BuildUserPlaneSecurityInformation() (value ngapType.UserPlaneSecurityInformation) {
	value.SecurityResult = BuildSecurityResult()
	value.SecurityIndication = BuildSecurityIndication()
	return
}

func BuildVolumeTimedReportList() (value ngapType.VolumeTimedReportList) {
	item := BuildVolumeTimedReportItem()
	value.List = append(value.List, item, item)
	return
}

func BuildVolumeTimedReportItem() (value ngapType.VolumeTimedReportItem) {
	value.StartTimeStamp = []byte{11, 22, 33, 44}
	value.EndTimeStamp = []byte{11, 22, 33, 44}
	value.UsageCountUL = 0
	value.UsageCountUL = 12345678
	value.UsageCountDL = 0
	value.UsageCountDL = 12345678
	return
}

func BuildWAGFID() (value ngapType.WAGFID) {
	value.Present = ngapType.WAGFIDPresentWAGFID
	value.WAGFID = new(ngapType.BitString)
	value.WAGFID.BitLength = 16
	value.WAGFID.Bytes = []byte{171, 1}
	return
}

func BuildWarningAreaList() (value ngapType.WarningAreaList) {
	value.Present = ngapType.WarningAreaListPresentEUTRACGIListForWarning
	value.EUTRACGIListForWarning = new(ngapType.EUTRACGIListForWarning)
	*value.EUTRACGIListForWarning = BuildEUTRACGIListForWarning()
	value.Present = ngapType.WarningAreaListPresentNRCGIListForWarning
	value.NRCGIListForWarning = new(ngapType.NRCGIListForWarning)
	*value.NRCGIListForWarning = BuildNRCGIListForWarning()
	value.Present = ngapType.WarningAreaListPresentTAIListForWarning
	value.TAIListForWarning = new(ngapType.TAIListForWarning)
	*value.TAIListForWarning = BuildTAIListForWarning()
	value.Present = ngapType.WarningAreaListPresentEmergencyAreaIDList
	value.EmergencyAreaIDList = new(ngapType.EmergencyAreaIDList)
	*value.EmergencyAreaIDList = BuildEmergencyAreaIDList()
	return
}

func BuildWLANMeasurementConfiguration() (value ngapType.WLANMeasurementConfiguration) {
	value.WlanMeasConfig = BuildWLANMeasConfig()
	value.WlanMeasConfigNameList = new(ngapType.WLANMeasConfigNameList)
	*value.WlanMeasConfigNameList = BuildWLANMeasConfigNameList()
	value.WlanRssi = new(ngapType.WlanRssi)
	*value.WlanRssi = BuildWlanRssi()
	value.WlanRtt = new(ngapType.WlanRtt)
	*value.WlanRtt = BuildWlanRtt()
	return
}

func BuildWLANMeasConfigNameList() (value ngapType.WLANMeasConfigNameList) {
	item := BuildWLANMeasConfigNameItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildWLANMeasConfigNameItem() (value ngapType.WLANMeasConfigNameItem) {
	value.WLANName = BuildWLANName()
	return
}

func BuildWUSAssistanceInformation() (value ngapType.WUSAssistanceInformation) {
	value.PagingProbabilityInformation = BuildPagingProbabilityInformation()
	return
}

func BuildXnExtTLAs() (value ngapType.XnExtTLAs) {
	item := BuildXnExtTLAItem()
	value.List = append(value.List, item, item, item)
	return
}

func BuildXnExtTLAItem() (value ngapType.XnExtTLAItem) {
	value.IPsecTLA = new(ngapType.TransportLayerAddress)
	*value.IPsecTLA = BuildTransportLayerAddress()
	value.GTPTLAs = new(ngapType.XnGTPTLAs)
	*value.GTPTLAs = BuildXnGTPTLAs()
	return
}

func BuildXnGTPTLAs() (value ngapType.XnGTPTLAs) {
	item := BuildTransportLayerAddress()
	value.List = append(value.List, item, item, item)
	return
}

func BuildXnTLAs() (value ngapType.XnTLAs) {
	item := BuildTransportLayerAddress()
	value.List = append(value.List, item, item)
	return
}

func BuildXnTNLConfigurationInfo() (value ngapType.XnTNLConfigurationInfo) {
	value.XnTransportLayerAddresses = BuildXnTLAs()
	value.XnExtendedTransportLayerAddresses = new(ngapType.XnExtTLAs)
	*value.XnExtendedTransportLayerAddresses = BuildXnExtTLAs()
	return
}

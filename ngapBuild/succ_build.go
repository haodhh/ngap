package ngapBuild

import (
	"ngap/ngapType"
)

func BuildPDUSessionResourceSetupResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePDUSessionResourceSetup
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentPDUSessionResourceSetupResponse
	pduMsg.Value.PDUSessionResourceSetupResponse = new(ngapType.PDUSessionResourceSetupResponse)
	pduContent := pduMsg.Value.PDUSessionResourceSetupResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PDUSessionResourceSetupResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.PDUSessionResourceSetupResponseIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PDUSessionResourceSetupResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.PDUSessionResourceSetupResponseIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PDUSessionResourceSetupResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceSetupListSURes
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PDUSessionResourceSetupResponseIEsPresentPDUSessionResourceSetupListSURes
	ie3.Value.PDUSessionResourceSetupListSURes = new(ngapType.PDUSessionResourceSetupListSURes)
	*ie3.Value.PDUSessionResourceSetupListSURes = BuildPDUSessionResourceSetupListSURes()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PDUSessionResourceSetupResponseIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceFailedToSetupListSURes
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.PDUSessionResourceSetupResponseIEsPresentPDUSessionResourceFailedToSetupListSURes
	ie4.Value.PDUSessionResourceFailedToSetupListSURes = new(ngapType.PDUSessionResourceFailedToSetupListSURes)
	*ie4.Value.PDUSessionResourceFailedToSetupListSURes = BuildPDUSessionResourceFailedToSetupListSURes()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.PDUSessionResourceSetupResponseIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.PDUSessionResourceSetupResponseIEsPresentCriticalityDiagnostics
	ie5.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie5.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildPDUSessionResourceReleaseResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePDUSessionResourceRelease
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentPDUSessionResourceReleaseResponse
	pduMsg.Value.PDUSessionResourceReleaseResponse = new(ngapType.PDUSessionResourceReleaseResponse)
	pduContent := pduMsg.Value.PDUSessionResourceReleaseResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PDUSessionResourceReleaseResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.PDUSessionResourceReleaseResponseIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PDUSessionResourceReleaseResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.PDUSessionResourceReleaseResponseIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PDUSessionResourceReleaseResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceReleasedListRelRes
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PDUSessionResourceReleaseResponseIEsPresentPDUSessionResourceReleasedListRelRes
	ie3.Value.PDUSessionResourceReleasedListRelRes = new(ngapType.PDUSessionResourceReleasedListRelRes)
	*ie3.Value.PDUSessionResourceReleasedListRelRes = BuildPDUSessionResourceReleasedListRelRes()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PDUSessionResourceReleaseResponseIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.PDUSessionResourceReleaseResponseIEsPresentUserLocationInformation
	ie4.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie4.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.PDUSessionResourceReleaseResponseIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.PDUSessionResourceReleaseResponseIEsPresentCriticalityDiagnostics
	ie5.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie5.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildPDUSessionResourceModifyResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePDUSessionResourceModify
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentPDUSessionResourceModifyResponse
	pduMsg.Value.PDUSessionResourceModifyResponse = new(ngapType.PDUSessionResourceModifyResponse)
	pduContent := pduMsg.Value.PDUSessionResourceModifyResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PDUSessionResourceModifyResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.PDUSessionResourceModifyResponseIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PDUSessionResourceModifyResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.PDUSessionResourceModifyResponseIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PDUSessionResourceModifyResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceModifyListModRes
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PDUSessionResourceModifyResponseIEsPresentPDUSessionResourceModifyListModRes
	ie3.Value.PDUSessionResourceModifyListModRes = new(ngapType.PDUSessionResourceModifyListModRes)
	*ie3.Value.PDUSessionResourceModifyListModRes = BuildPDUSessionResourceModifyListModRes()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PDUSessionResourceModifyResponseIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceFailedToModifyListModRes
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.PDUSessionResourceModifyResponseIEsPresentPDUSessionResourceFailedToModifyListModRes
	ie4.Value.PDUSessionResourceFailedToModifyListModRes = new(ngapType.PDUSessionResourceFailedToModifyListModRes)
	*ie4.Value.PDUSessionResourceFailedToModifyListModRes = BuildPDUSessionResourceFailedToModifyListModRes()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.PDUSessionResourceModifyResponseIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.PDUSessionResourceModifyResponseIEsPresentUserLocationInformation
	ie5.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie5.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.PDUSessionResourceModifyResponseIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.PDUSessionResourceModifyResponseIEsPresentCriticalityDiagnostics
	ie6.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie6.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	return
}

func BuildPDUSessionResourceModifyConfirm() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePDUSessionResourceModifyIndication
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentPDUSessionResourceModifyConfirm
	pduMsg.Value.PDUSessionResourceModifyConfirm = new(ngapType.PDUSessionResourceModifyConfirm)
	pduContent := pduMsg.Value.PDUSessionResourceModifyConfirm
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PDUSessionResourceModifyConfirmIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.PDUSessionResourceModifyConfirmIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PDUSessionResourceModifyConfirmIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.PDUSessionResourceModifyConfirmIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PDUSessionResourceModifyConfirmIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceModifyListModCfm
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PDUSessionResourceModifyConfirmIEsPresentPDUSessionResourceModifyListModCfm
	ie3.Value.PDUSessionResourceModifyListModCfm = new(ngapType.PDUSessionResourceModifyListModCfm)
	*ie3.Value.PDUSessionResourceModifyListModCfm = BuildPDUSessionResourceModifyListModCfm()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PDUSessionResourceModifyConfirmIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceFailedToModifyListModCfm
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.PDUSessionResourceModifyConfirmIEsPresentPDUSessionResourceFailedToModifyListModCfm
	ie4.Value.PDUSessionResourceFailedToModifyListModCfm = new(ngapType.PDUSessionResourceFailedToModifyListModCfm)
	*ie4.Value.PDUSessionResourceFailedToModifyListModCfm = BuildPDUSessionResourceFailedToModifyListModCfm()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.PDUSessionResourceModifyConfirmIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.PDUSessionResourceModifyConfirmIEsPresentCriticalityDiagnostics
	ie5.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie5.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildInitialContextSetupResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeInitialContextSetup
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentInitialContextSetupResponse
	pduMsg.Value.InitialContextSetupResponse = new(ngapType.InitialContextSetupResponse)
	pduContent := pduMsg.Value.InitialContextSetupResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.InitialContextSetupResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.InitialContextSetupResponseIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.InitialContextSetupResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.InitialContextSetupResponseIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.InitialContextSetupResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceSetupListCxtRes
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.InitialContextSetupResponseIEsPresentPDUSessionResourceSetupListCxtRes
	ie3.Value.PDUSessionResourceSetupListCxtRes = new(ngapType.PDUSessionResourceSetupListCxtRes)
	*ie3.Value.PDUSessionResourceSetupListCxtRes = BuildPDUSessionResourceSetupListCxtRes()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.InitialContextSetupResponseIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceFailedToSetupListCxtRes
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.InitialContextSetupResponseIEsPresentPDUSessionResourceFailedToSetupListCxtRes
	ie4.Value.PDUSessionResourceFailedToSetupListCxtRes = new(ngapType.PDUSessionResourceFailedToSetupListCxtRes)
	*ie4.Value.PDUSessionResourceFailedToSetupListCxtRes = BuildPDUSessionResourceFailedToSetupListCxtRes()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.InitialContextSetupResponseIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.InitialContextSetupResponseIEsPresentCriticalityDiagnostics
	ie5.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie5.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildUEContextReleaseComplete() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextRelease
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentUEContextReleaseComplete
	pduMsg.Value.UEContextReleaseComplete = new(ngapType.UEContextReleaseComplete)
	pduContent := pduMsg.Value.UEContextReleaseComplete
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextReleaseCompleteIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.UEContextReleaseCompleteIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextReleaseCompleteIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UEContextReleaseCompleteIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEContextReleaseCompleteIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UEContextReleaseCompleteIEsPresentUserLocationInformation
	ie3.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie3.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEContextReleaseCompleteIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDInfoOnRecommendedCellsAndRANNodesForPaging
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UEContextReleaseCompleteIEsPresentInfoOnRecommendedCellsAndRANNodesForPaging
	ie4.Value.InfoOnRecommendedCellsAndRANNodesForPaging = new(ngapType.InfoOnRecommendedCellsAndRANNodesForPaging)
	*ie4.Value.InfoOnRecommendedCellsAndRANNodesForPaging = BuildInfoOnRecommendedCellsAndRANNodesForPaging()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.UEContextReleaseCompleteIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceListCxtRelCpl
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.UEContextReleaseCompleteIEsPresentPDUSessionResourceListCxtRelCpl
	ie5.Value.PDUSessionResourceListCxtRelCpl = new(ngapType.PDUSessionResourceListCxtRelCpl)
	*ie5.Value.PDUSessionResourceListCxtRelCpl = BuildPDUSessionResourceListCxtRelCpl()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.UEContextReleaseCompleteIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.UEContextReleaseCompleteIEsPresentCriticalityDiagnostics
	ie6.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie6.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.UEContextReleaseCompleteIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDPagingAssisDataforCEcapabUE
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.UEContextReleaseCompleteIEsPresentPagingAssisDataforCEcapabUE
	ie7.Value.PagingAssisDataforCEcapabUE = new(ngapType.PagingAssisDataforCEcapabUE)
	*ie7.Value.PagingAssisDataforCEcapabUE = BuildPagingAssisDataforCEcapabUE()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	return
}

func BuildUEContextResumeResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextResume
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentUEContextResumeResponse
	pduMsg.Value.UEContextResumeResponse = new(ngapType.UEContextResumeResponse)
	pduContent := pduMsg.Value.UEContextResumeResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextResumeResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.UEContextResumeResponseIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextResumeResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UEContextResumeResponseIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEContextResumeResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceResumeListRESRes
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.UEContextResumeResponseIEsPresentPDUSessionResourceResumeListRESRes
	ie3.Value.PDUSessionResourceResumeListRESRes = new(ngapType.PDUSessionResourceResumeListRESRes)
	*ie3.Value.PDUSessionResourceResumeListRESRes = BuildPDUSessionResourceResumeListRESRes()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEContextResumeResponseIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceFailedToResumeListRESRes
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.UEContextResumeResponseIEsPresentPDUSessionResourceFailedToResumeListRESRes
	ie4.Value.PDUSessionResourceFailedToResumeListRESRes = new(ngapType.PDUSessionResourceFailedToResumeListRESRes)
	*ie4.Value.PDUSessionResourceFailedToResumeListRESRes = BuildPDUSessionResourceFailedToResumeListRESRes()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.UEContextResumeResponseIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDSecurityContext
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.UEContextResumeResponseIEsPresentSecurityContext
	ie5.Value.SecurityContext = new(ngapType.SecurityContext)
	*ie5.Value.SecurityContext = BuildSecurityContext()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.UEContextResumeResponseIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDSuspendResponseIndication
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.UEContextResumeResponseIEsPresentSuspendResponseIndication
	ie6.Value.SuspendResponseIndication = new(ngapType.SuspendResponseIndication)
	*ie6.Value.SuspendResponseIndication = BuildSuspendResponseIndication()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.UEContextResumeResponseIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDExtendedConnectedTime
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.UEContextResumeResponseIEsPresentExtendedConnectedTime
	ie7.Value.ExtendedConnectedTime = new(ngapType.ExtendedConnectedTime)
	*ie7.Value.ExtendedConnectedTime = BuildExtendedConnectedTime()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.UEContextResumeResponseIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie8.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie8.Value.Present = ngapType.UEContextResumeResponseIEsPresentCriticalityDiagnostics
	ie8.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie8.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	return
}

func BuildUEContextSuspendResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextSuspend
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentUEContextSuspendResponse
	pduMsg.Value.UEContextSuspendResponse = new(ngapType.UEContextSuspendResponse)
	pduContent := pduMsg.Value.UEContextSuspendResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextSuspendResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.UEContextSuspendResponseIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextSuspendResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UEContextSuspendResponseIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEContextSuspendResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDSecurityContext
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.UEContextSuspendResponseIEsPresentSecurityContext
	ie3.Value.SecurityContext = new(ngapType.SecurityContext)
	*ie3.Value.SecurityContext = BuildSecurityContext()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEContextSuspendResponseIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UEContextSuspendResponseIEsPresentCriticalityDiagnostics
	ie4.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie4.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildUEContextModificationResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextModification
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentUEContextModificationResponse
	pduMsg.Value.UEContextModificationResponse = new(ngapType.UEContextModificationResponse)
	pduContent := pduMsg.Value.UEContextModificationResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextModificationResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.UEContextModificationResponseIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextModificationResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UEContextModificationResponseIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEContextModificationResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRRCState
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UEContextModificationResponseIEsPresentRRCState
	ie3.Value.RRCState = new(ngapType.RRCState)
	*ie3.Value.RRCState = BuildRRCState()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEContextModificationResponseIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDUserLocationInformation
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UEContextModificationResponseIEsPresentUserLocationInformation
	ie4.Value.UserLocationInformation = new(ngapType.UserLocationInformation)
	*ie4.Value.UserLocationInformation = BuildUserLocationInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.UEContextModificationResponseIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.UEContextModificationResponseIEsPresentCriticalityDiagnostics
	ie5.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie5.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildHandoverCommand() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeHandoverPreparation
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentHandoverCommand
	pduMsg.Value.HandoverCommand = new(ngapType.HandoverCommand)
	pduContent := pduMsg.Value.HandoverCommand
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.HandoverCommandIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.HandoverCommandIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.HandoverCommandIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.HandoverCommandIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.HandoverCommandIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDHandoverType
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.HandoverCommandIEsPresentHandoverType
	ie3.Value.HandoverType = new(ngapType.HandoverType)
	*ie3.Value.HandoverType = BuildHandoverType()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.HandoverCommandIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDNASSecurityParametersFromNGRAN
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.HandoverCommandIEsPresentNASSecurityParametersFromNGRAN
	ie4.Value.NASSecurityParametersFromNGRAN = new(ngapType.NASSecurityParametersFromNGRAN)
	*ie4.Value.NASSecurityParametersFromNGRAN = BuildNASSecurityParametersFromNGRAN()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.HandoverCommandIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceHandoverList
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.HandoverCommandIEsPresentPDUSessionResourceHandoverList
	ie5.Value.PDUSessionResourceHandoverList = new(ngapType.PDUSessionResourceHandoverList)
	*ie5.Value.PDUSessionResourceHandoverList = BuildPDUSessionResourceHandoverList()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.HandoverCommandIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceToReleaseListHOCmd
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.HandoverCommandIEsPresentPDUSessionResourceToReleaseListHOCmd
	ie6.Value.PDUSessionResourceToReleaseListHOCmd = new(ngapType.PDUSessionResourceToReleaseListHOCmd)
	*ie6.Value.PDUSessionResourceToReleaseListHOCmd = BuildPDUSessionResourceToReleaseListHOCmd()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.HandoverCommandIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDTargetToSourceTransparentContainer
	ie7.Criticality.Value = ngapType.CriticalityPresentReject
	ie7.Value.Present = ngapType.HandoverCommandIEsPresentTargetToSourceTransparentContainer
	ie7.Value.TargetToSourceTransparentContainer = new(ngapType.TargetToSourceTransparentContainer)
	*ie7.Value.TargetToSourceTransparentContainer = BuildTargetToSourceTransparentContainer()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.HandoverCommandIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie8.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie8.Value.Present = ngapType.HandoverCommandIEsPresentCriticalityDiagnostics
	ie8.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie8.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	return
}

func BuildHandoverRequestAcknowledge() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeHandoverResourceAllocation
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentHandoverRequestAcknowledge
	pduMsg.Value.HandoverRequestAcknowledge = new(ngapType.HandoverRequestAcknowledge)
	pduContent := pduMsg.Value.HandoverRequestAcknowledge
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.HandoverRequestAcknowledgeIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.HandoverRequestAcknowledgeIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.HandoverRequestAcknowledgeIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.HandoverRequestAcknowledgeIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.HandoverRequestAcknowledgeIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceAdmittedList
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.HandoverRequestAcknowledgeIEsPresentPDUSessionResourceAdmittedList
	ie3.Value.PDUSessionResourceAdmittedList = new(ngapType.PDUSessionResourceAdmittedList)
	*ie3.Value.PDUSessionResourceAdmittedList = BuildPDUSessionResourceAdmittedList()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.HandoverRequestAcknowledgeIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceFailedToSetupListHOAck
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.HandoverRequestAcknowledgeIEsPresentPDUSessionResourceFailedToSetupListHOAck
	ie4.Value.PDUSessionResourceFailedToSetupListHOAck = new(ngapType.PDUSessionResourceFailedToSetupListHOAck)
	*ie4.Value.PDUSessionResourceFailedToSetupListHOAck = BuildPDUSessionResourceFailedToSetupListHOAck()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.HandoverRequestAcknowledgeIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDTargetToSourceTransparentContainer
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.HandoverRequestAcknowledgeIEsPresentTargetToSourceTransparentContainer
	ie5.Value.TargetToSourceTransparentContainer = new(ngapType.TargetToSourceTransparentContainer)
	*ie5.Value.TargetToSourceTransparentContainer = BuildTargetToSourceTransparentContainer()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.HandoverRequestAcknowledgeIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.HandoverRequestAcknowledgeIEsPresentCriticalityDiagnostics
	ie6.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie6.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	return
}

func BuildPathSwitchRequestAcknowledge() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePathSwitchRequest
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentPathSwitchRequestAcknowledge
	pduMsg.Value.PathSwitchRequestAcknowledge = new(ngapType.PathSwitchRequestAcknowledge)
	pduContent := pduMsg.Value.PathSwitchRequestAcknowledge
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDUESecurityCapabilities
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentUESecurityCapabilities
	ie3.Value.UESecurityCapabilities = new(ngapType.UESecurityCapabilities)
	*ie3.Value.UESecurityCapabilities = BuildUESecurityCapabilities()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDSecurityContext
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentSecurityContext
	ie4.Value.SecurityContext = new(ngapType.SecurityContext)
	*ie4.Value.SecurityContext = BuildSecurityContext()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDNewSecurityContextInd
	ie5.Criticality.Value = ngapType.CriticalityPresentReject
	ie5.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentNewSecurityContextInd
	ie5.Value.NewSecurityContextInd = new(ngapType.NewSecurityContextInd)
	*ie5.Value.NewSecurityContextInd = BuildNewSecurityContextInd()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceSwitchedList
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentPDUSessionResourceSwitchedList
	ie6.Value.PDUSessionResourceSwitchedList = new(ngapType.PDUSessionResourceSwitchedList)
	*ie6.Value.PDUSessionResourceSwitchedList = BuildPDUSessionResourceSwitchedList()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceReleasedListPSAck
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentPDUSessionResourceReleasedListPSAck
	ie7.Value.PDUSessionResourceReleasedListPSAck = new(ngapType.PDUSessionResourceReleasedListPSAck)
	*ie7.Value.PDUSessionResourceReleasedListPSAck = BuildPDUSessionResourceReleasedListPSAck()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDAllowedNSSAI
	ie8.Criticality.Value = ngapType.CriticalityPresentReject
	ie8.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentAllowedNSSAI
	ie8.Value.AllowedNSSAI = new(ngapType.AllowedNSSAI)
	*ie8.Value.AllowedNSSAI = BuildAllowedNSSAI()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	ie9 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie9.Id.Value = ngapType.ProtocolIEIDCoreNetworkAssistanceInformationForInactive
	ie9.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie9.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentCoreNetworkAssistanceInformationForInactive
	ie9.Value.CoreNetworkAssistanceInformationForInactive = new(ngapType.CoreNetworkAssistanceInformationForInactive)
	*ie9.Value.CoreNetworkAssistanceInformationForInactive = BuildCoreNetworkAssistanceInformationForInactive()

	pduContentIEs.List = append(pduContentIEs.List, ie9)

	ie10 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie10.Id.Value = ngapType.ProtocolIEIDRRCInactiveTransitionReportRequest
	ie10.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie10.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentRRCInactiveTransitionReportRequest
	ie10.Value.RRCInactiveTransitionReportRequest = new(ngapType.RRCInactiveTransitionReportRequest)
	*ie10.Value.RRCInactiveTransitionReportRequest = BuildRRCInactiveTransitionReportRequest()

	pduContentIEs.List = append(pduContentIEs.List, ie10)

	ie11 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie11.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie11.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie11.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentCriticalityDiagnostics
	ie11.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie11.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie11)

	ie12 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie12.Id.Value = ngapType.ProtocolIEIDRedirectionVoiceFallback
	ie12.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie12.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentRedirectionVoiceFallback
	ie12.Value.RedirectionVoiceFallback = new(ngapType.RedirectionVoiceFallback)
	*ie12.Value.RedirectionVoiceFallback = BuildRedirectionVoiceFallback()

	pduContentIEs.List = append(pduContentIEs.List, ie12)

	ie13 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie13.Id.Value = ngapType.ProtocolIEIDCNAssistedRANTuning
	ie13.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie13.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentCNAssistedRANTuning
	ie13.Value.CNAssistedRANTuning = new(ngapType.CNAssistedRANTuning)
	*ie13.Value.CNAssistedRANTuning = BuildCNAssistedRANTuning()

	pduContentIEs.List = append(pduContentIEs.List, ie13)

	ie14 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie14.Id.Value = ngapType.ProtocolIEIDSRVCCOperationPossible
	ie14.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie14.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentSRVCCOperationPossible
	ie14.Value.SRVCCOperationPossible = new(ngapType.SRVCCOperationPossible)
	*ie14.Value.SRVCCOperationPossible = BuildSRVCCOperationPossible()

	pduContentIEs.List = append(pduContentIEs.List, ie14)

	ie15 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie15.Id.Value = ngapType.ProtocolIEIDEnhancedCoverageRestriction
	ie15.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie15.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentEnhancedCoverageRestriction
	ie15.Value.EnhancedCoverageRestriction = new(ngapType.EnhancedCoverageRestriction)
	*ie15.Value.EnhancedCoverageRestriction = BuildEnhancedCoverageRestriction()

	pduContentIEs.List = append(pduContentIEs.List, ie15)

	ie16 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie16.Id.Value = ngapType.ProtocolIEIDExtendedConnectedTime
	ie16.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie16.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentExtendedConnectedTime
	ie16.Value.ExtendedConnectedTime = new(ngapType.ExtendedConnectedTime)
	*ie16.Value.ExtendedConnectedTime = BuildExtendedConnectedTime()

	pduContentIEs.List = append(pduContentIEs.List, ie16)

	ie17 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie17.Id.Value = ngapType.ProtocolIEIDUEDifferentiationInfo
	ie17.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie17.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentUEDifferentiationInfo
	ie17.Value.UEDifferentiationInfo = new(ngapType.UEDifferentiationInfo)
	*ie17.Value.UEDifferentiationInfo = BuildUEDifferentiationInfo()

	pduContentIEs.List = append(pduContentIEs.List, ie17)

	ie18 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie18.Id.Value = ngapType.ProtocolIEIDNRV2XServicesAuthorized
	ie18.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie18.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentNRV2XServicesAuthorized
	ie18.Value.NRV2XServicesAuthorized = new(ngapType.NRV2XServicesAuthorized)
	*ie18.Value.NRV2XServicesAuthorized = BuildNRV2XServicesAuthorized()

	pduContentIEs.List = append(pduContentIEs.List, ie18)

	ie19 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie19.Id.Value = ngapType.ProtocolIEIDLTEV2XServicesAuthorized
	ie19.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie19.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentLTEV2XServicesAuthorized
	ie19.Value.LTEV2XServicesAuthorized = new(ngapType.LTEV2XServicesAuthorized)
	*ie19.Value.LTEV2XServicesAuthorized = BuildLTEV2XServicesAuthorized()

	pduContentIEs.List = append(pduContentIEs.List, ie19)

	ie20 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie20.Id.Value = ngapType.ProtocolIEIDNRUESidelinkAggregateMaximumBitrate
	ie20.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie20.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentNRUESidelinkAggregateMaximumBitrate
	ie20.Value.NRUESidelinkAggregateMaximumBitrate = new(ngapType.NRUESidelinkAggregateMaximumBitrate)
	*ie20.Value.NRUESidelinkAggregateMaximumBitrate = BuildNRUESidelinkAggregateMaximumBitrate()

	pduContentIEs.List = append(pduContentIEs.List, ie20)

	ie21 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie21.Id.Value = ngapType.ProtocolIEIDLTEUESidelinkAggregateMaximumBitrate
	ie21.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie21.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentLTEUESidelinkAggregateMaximumBitrate
	ie21.Value.LTEUESidelinkAggregateMaximumBitrate = new(ngapType.LTEUESidelinkAggregateMaximumBitrate)
	*ie21.Value.LTEUESidelinkAggregateMaximumBitrate = BuildLTEUESidelinkAggregateMaximumBitrate()

	pduContentIEs.List = append(pduContentIEs.List, ie21)

	ie22 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie22.Id.Value = ngapType.ProtocolIEIDPC5QoSParameters
	ie22.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie22.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentPC5QoSParameters
	ie22.Value.PC5QoSParameters = new(ngapType.PC5QoSParameters)
	*ie22.Value.PC5QoSParameters = BuildPC5QoSParameters()

	pduContentIEs.List = append(pduContentIEs.List, ie22)

	ie23 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie23.Id.Value = ngapType.ProtocolIEIDCEmodeBrestricted
	ie23.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie23.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentCEmodeBrestricted
	ie23.Value.CEmodeBrestricted = new(ngapType.CEmodeBrestricted)
	*ie23.Value.CEmodeBrestricted = BuildCEmodeBrestricted()

	pduContentIEs.List = append(pduContentIEs.List, ie23)

	ie24 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie24.Id.Value = ngapType.ProtocolIEIDUEUPCIoTSupport
	ie24.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie24.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentUEUPCIoTSupport
	ie24.Value.UEUPCIoTSupport = new(ngapType.UEUPCIoTSupport)
	*ie24.Value.UEUPCIoTSupport = BuildUEUPCIoTSupport()

	pduContentIEs.List = append(pduContentIEs.List, ie24)

	ie25 := ngapType.PathSwitchRequestAcknowledgeIEs{}
	ie25.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityID
	ie25.Criticality.Value = ngapType.CriticalityPresentReject
	ie25.Value.Present = ngapType.PathSwitchRequestAcknowledgeIEsPresentUERadioCapabilityID
	ie25.Value.UERadioCapabilityID = new(ngapType.UERadioCapabilityID)
	*ie25.Value.UERadioCapabilityID = BuildUERadioCapabilityID()

	pduContentIEs.List = append(pduContentIEs.List, ie25)

	return
}

func BuildHandoverCancelAcknowledge() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeHandoverCancel
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentHandoverCancelAcknowledge
	pduMsg.Value.HandoverCancelAcknowledge = new(ngapType.HandoverCancelAcknowledge)
	pduContent := pduMsg.Value.HandoverCancelAcknowledge
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.HandoverCancelAcknowledgeIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.HandoverCancelAcknowledgeIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.HandoverCancelAcknowledgeIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.HandoverCancelAcknowledgeIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.HandoverCancelAcknowledgeIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.HandoverCancelAcknowledgeIEsPresentCriticalityDiagnostics
	ie3.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie3.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildNGSetupResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeNGSetup
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentNGSetupResponse
	pduMsg.Value.NGSetupResponse = new(ngapType.NGSetupResponse)
	pduContent := pduMsg.Value.NGSetupResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.NGSetupResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFName
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.NGSetupResponseIEsPresentAMFName
	ie1.Value.AMFName = new(ngapType.AMFName)
	*ie1.Value.AMFName = BuildAMFName()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.NGSetupResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDServedGUAMIList
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.NGSetupResponseIEsPresentServedGUAMIList
	ie2.Value.ServedGUAMIList = new(ngapType.ServedGUAMIList)
	*ie2.Value.ServedGUAMIList = BuildServedGUAMIList()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.NGSetupResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDRelativeAMFCapacity
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.NGSetupResponseIEsPresentRelativeAMFCapacity
	ie3.Value.RelativeAMFCapacity = new(ngapType.RelativeAMFCapacity)
	*ie3.Value.RelativeAMFCapacity = BuildRelativeAMFCapacity()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.NGSetupResponseIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDPLMNSupportList
	ie4.Criticality.Value = ngapType.CriticalityPresentReject
	ie4.Value.Present = ngapType.NGSetupResponseIEsPresentPLMNSupportList
	ie4.Value.PLMNSupportList = new(ngapType.PLMNSupportList)
	*ie4.Value.PLMNSupportList = BuildPLMNSupportList()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.NGSetupResponseIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.NGSetupResponseIEsPresentCriticalityDiagnostics
	ie5.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie5.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	ie6 := ngapType.NGSetupResponseIEs{}
	ie6.Id.Value = ngapType.ProtocolIEIDUERetentionInformation
	ie6.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie6.Value.Present = ngapType.NGSetupResponseIEsPresentUERetentionInformation
	ie6.Value.UERetentionInformation = new(ngapType.UERetentionInformation)
	*ie6.Value.UERetentionInformation = BuildUERetentionInformation()

	pduContentIEs.List = append(pduContentIEs.List, ie6)

	ie7 := ngapType.NGSetupResponseIEs{}
	ie7.Id.Value = ngapType.ProtocolIEIDIABSupported
	ie7.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie7.Value.Present = ngapType.NGSetupResponseIEsPresentIABSupported
	ie7.Value.IABSupported = new(ngapType.IABSupported)
	*ie7.Value.IABSupported = BuildIABSupported()

	pduContentIEs.List = append(pduContentIEs.List, ie7)

	ie8 := ngapType.NGSetupResponseIEs{}
	ie8.Id.Value = ngapType.ProtocolIEIDExtendedAMFName
	ie8.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie8.Value.Present = ngapType.NGSetupResponseIEsPresentExtendedAMFName
	ie8.Value.ExtendedAMFName = new(ngapType.ExtendedAMFName)
	*ie8.Value.ExtendedAMFName = BuildExtendedAMFName()

	pduContentIEs.List = append(pduContentIEs.List, ie8)

	return
}

func BuildRANConfigurationUpdateAcknowledge() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeRANConfigurationUpdate
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentRANConfigurationUpdateAcknowledge
	pduMsg.Value.RANConfigurationUpdateAcknowledge = new(ngapType.RANConfigurationUpdateAcknowledge)
	pduContent := pduMsg.Value.RANConfigurationUpdateAcknowledge
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.RANConfigurationUpdateAcknowledgeIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.RANConfigurationUpdateAcknowledgeIEsPresentCriticalityDiagnostics
	ie1.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie1.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	return
}

func BuildAMFConfigurationUpdateAcknowledge() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeAMFConfigurationUpdate
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentAMFConfigurationUpdateAcknowledge
	pduMsg.Value.AMFConfigurationUpdateAcknowledge = new(ngapType.AMFConfigurationUpdateAcknowledge)
	pduContent := pduMsg.Value.AMFConfigurationUpdateAcknowledge
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.AMFConfigurationUpdateAcknowledgeIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFTNLAssociationSetupList
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.AMFConfigurationUpdateAcknowledgeIEsPresentAMFTNLAssociationSetupList
	ie1.Value.AMFTNLAssociationSetupList = new(ngapType.AMFTNLAssociationSetupList)
	*ie1.Value.AMFTNLAssociationSetupList = BuildAMFTNLAssociationSetupList()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.AMFConfigurationUpdateAcknowledgeIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDAMFTNLAssociationFailedToSetupList
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.AMFConfigurationUpdateAcknowledgeIEsPresentAMFTNLAssociationFailedToSetupList
	ie2.Value.AMFTNLAssociationFailedToSetupList = new(ngapType.TNLAssociationList)
	*ie2.Value.AMFTNLAssociationFailedToSetupList = BuildTNLAssociationList()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.AMFConfigurationUpdateAcknowledgeIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.AMFConfigurationUpdateAcknowledgeIEsPresentCriticalityDiagnostics
	ie3.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie3.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildNGResetAcknowledge() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeNGReset
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentNGResetAcknowledge
	pduMsg.Value.NGResetAcknowledge = new(ngapType.NGResetAcknowledge)
	pduContent := pduMsg.Value.NGResetAcknowledge
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.NGResetAcknowledgeIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDUEAssociatedLogicalNGConnectionList
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.NGResetAcknowledgeIEsPresentUEAssociatedLogicalNGConnectionList
	ie1.Value.UEAssociatedLogicalNGConnectionList = new(ngapType.UEAssociatedLogicalNGConnectionList)
	*ie1.Value.UEAssociatedLogicalNGConnectionList = BuildUEAssociatedLogicalNGConnectionList()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.NGResetAcknowledgeIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.NGResetAcknowledgeIEsPresentCriticalityDiagnostics
	ie2.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie2.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	return
}

func BuildWriteReplaceWarningResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeWriteReplaceWarning
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentWriteReplaceWarningResponse
	pduMsg.Value.WriteReplaceWarningResponse = new(ngapType.WriteReplaceWarningResponse)
	pduContent := pduMsg.Value.WriteReplaceWarningResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.WriteReplaceWarningResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDMessageIdentifier
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.WriteReplaceWarningResponseIEsPresentMessageIdentifier
	ie1.Value.MessageIdentifier = new(ngapType.MessageIdentifier)
	*ie1.Value.MessageIdentifier = BuildMessageIdentifier()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.WriteReplaceWarningResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDSerialNumber
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.WriteReplaceWarningResponseIEsPresentSerialNumber
	ie2.Value.SerialNumber = new(ngapType.SerialNumber)
	*ie2.Value.SerialNumber = BuildSerialNumber()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.WriteReplaceWarningResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDBroadcastCompletedAreaList
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.WriteReplaceWarningResponseIEsPresentBroadcastCompletedAreaList
	ie3.Value.BroadcastCompletedAreaList = new(ngapType.BroadcastCompletedAreaList)
	*ie3.Value.BroadcastCompletedAreaList = BuildBroadcastCompletedAreaList()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.WriteReplaceWarningResponseIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.WriteReplaceWarningResponseIEsPresentCriticalityDiagnostics
	ie4.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie4.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildPWSCancelResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePWSCancel
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentPWSCancelResponse
	pduMsg.Value.PWSCancelResponse = new(ngapType.PWSCancelResponse)
	pduContent := pduMsg.Value.PWSCancelResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PWSCancelResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDMessageIdentifier
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.PWSCancelResponseIEsPresentMessageIdentifier
	ie1.Value.MessageIdentifier = new(ngapType.MessageIdentifier)
	*ie1.Value.MessageIdentifier = BuildMessageIdentifier()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PWSCancelResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDSerialNumber
	ie2.Criticality.Value = ngapType.CriticalityPresentReject
	ie2.Value.Present = ngapType.PWSCancelResponseIEsPresentSerialNumber
	ie2.Value.SerialNumber = new(ngapType.SerialNumber)
	*ie2.Value.SerialNumber = BuildSerialNumber()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PWSCancelResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDBroadcastCancelledAreaList
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PWSCancelResponseIEsPresentBroadcastCancelledAreaList
	ie3.Value.BroadcastCancelledAreaList = new(ngapType.BroadcastCancelledAreaList)
	*ie3.Value.BroadcastCancelledAreaList = BuildBroadcastCancelledAreaList()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PWSCancelResponseIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.PWSCancelResponseIEsPresentCriticalityDiagnostics
	ie4.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie4.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildUERadioCapabilityCheckResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUERadioCapabilityCheck
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentUERadioCapabilityCheckResponse
	pduMsg.Value.UERadioCapabilityCheckResponse = new(ngapType.UERadioCapabilityCheckResponse)
	pduContent := pduMsg.Value.UERadioCapabilityCheckResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UERadioCapabilityCheckResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.UERadioCapabilityCheckResponseIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UERadioCapabilityCheckResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UERadioCapabilityCheckResponseIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UERadioCapabilityCheckResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDIMSVoiceSupportIndicator
	ie3.Criticality.Value = ngapType.CriticalityPresentReject
	ie3.Value.Present = ngapType.UERadioCapabilityCheckResponseIEsPresentIMSVoiceSupportIndicator
	ie3.Value.IMSVoiceSupportIndicator = new(ngapType.IMSVoiceSupportIndicator)
	*ie3.Value.IMSVoiceSupportIndicator = BuildIMSVoiceSupportIndicator()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UERadioCapabilityCheckResponseIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UERadioCapabilityCheckResponseIEsPresentCriticalityDiagnostics
	ie4.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie4.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildUERadioCapabilityIDMappingResponse() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentSuccessfulOutcome
	pdu.SuccessfulOutcome = new(ngapType.SuccessfulOutcome)
	pduMsg := pdu.SuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUERadioCapabilityIDMapping
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentUERadioCapabilityIDMappingResponse
	pduMsg.Value.UERadioCapabilityIDMappingResponse = new(ngapType.UERadioCapabilityIDMappingResponse)
	pduContent := pduMsg.Value.UERadioCapabilityIDMappingResponse
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UERadioCapabilityIDMappingResponseIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDUERadioCapabilityID
	ie1.Criticality.Value = ngapType.CriticalityPresentReject
	ie1.Value.Present = ngapType.UERadioCapabilityIDMappingResponseIEsPresentUERadioCapabilityID
	ie1.Value.UERadioCapabilityID = new(ngapType.UERadioCapabilityID)
	*ie1.Value.UERadioCapabilityID = BuildUERadioCapabilityID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UERadioCapabilityIDMappingResponseIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDUERadioCapability
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UERadioCapabilityIDMappingResponseIEsPresentUERadioCapability
	ie2.Value.UERadioCapability = new(ngapType.UERadioCapability)
	*ie2.Value.UERadioCapability = BuildUERadioCapability()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UERadioCapabilityIDMappingResponseIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UERadioCapabilityIDMappingResponseIEsPresentCriticalityDiagnostics
	ie3.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie3.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

package ngapBuild

import (
	"ngap/ngapType"
)

func BuildInitialContextSetupFailure() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentUnsuccessfulOutcome
	pdu.UnsuccessfulOutcome = new(ngapType.UnsuccessfulOutcome)
	pduMsg := pdu.UnsuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeInitialContextSetup
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.UnsuccessfulOutcomePresentInitialContextSetupFailure
	pduMsg.Value.InitialContextSetupFailure = new(ngapType.InitialContextSetupFailure)
	pduContent := pduMsg.Value.InitialContextSetupFailure
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.InitialContextSetupFailureIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.InitialContextSetupFailureIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.InitialContextSetupFailureIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.InitialContextSetupFailureIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.InitialContextSetupFailureIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceFailedToSetupListCxtFail
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.InitialContextSetupFailureIEsPresentPDUSessionResourceFailedToSetupListCxtFail
	ie3.Value.PDUSessionResourceFailedToSetupListCxtFail = new(ngapType.PDUSessionResourceFailedToSetupListCxtFail)
	*ie3.Value.PDUSessionResourceFailedToSetupListCxtFail = BuildPDUSessionResourceFailedToSetupListCxtFail()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.InitialContextSetupFailureIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCause
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.InitialContextSetupFailureIEsPresentCause
	ie4.Value.Cause = new(ngapType.Cause)
	*ie4.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.InitialContextSetupFailureIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.InitialContextSetupFailureIEsPresentCriticalityDiagnostics
	ie5.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie5.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildUEContextResumeFailure() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentUnsuccessfulOutcome
	pdu.UnsuccessfulOutcome = new(ngapType.UnsuccessfulOutcome)
	pduMsg := pdu.UnsuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextResume
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.UnsuccessfulOutcomePresentUEContextResumeFailure
	pduMsg.Value.UEContextResumeFailure = new(ngapType.UEContextResumeFailure)
	pduContent := pduMsg.Value.UEContextResumeFailure
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextResumeFailureIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.UEContextResumeFailureIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextResumeFailureIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UEContextResumeFailureIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEContextResumeFailureIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCause
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UEContextResumeFailureIEsPresentCause
	ie3.Value.Cause = new(ngapType.Cause)
	*ie3.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEContextResumeFailureIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UEContextResumeFailureIEsPresentCriticalityDiagnostics
	ie4.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie4.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildUEContextSuspendFailure() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentUnsuccessfulOutcome
	pdu.UnsuccessfulOutcome = new(ngapType.UnsuccessfulOutcome)
	pduMsg := pdu.UnsuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextSuspend
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.UnsuccessfulOutcomePresentUEContextSuspendFailure
	pduMsg.Value.UEContextSuspendFailure = new(ngapType.UEContextSuspendFailure)
	pduContent := pduMsg.Value.UEContextSuspendFailure
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextSuspendFailureIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.UEContextSuspendFailureIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextSuspendFailureIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UEContextSuspendFailureIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEContextSuspendFailureIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCause
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UEContextSuspendFailureIEsPresentCause
	ie3.Value.Cause = new(ngapType.Cause)
	*ie3.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEContextSuspendFailureIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UEContextSuspendFailureIEsPresentCriticalityDiagnostics
	ie4.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie4.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildUEContextModificationFailure() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentUnsuccessfulOutcome
	pdu.UnsuccessfulOutcome = new(ngapType.UnsuccessfulOutcome)
	pduMsg := pdu.UnsuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeUEContextModification
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.UnsuccessfulOutcomePresentUEContextModificationFailure
	pduMsg.Value.UEContextModificationFailure = new(ngapType.UEContextModificationFailure)
	pduContent := pduMsg.Value.UEContextModificationFailure
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.UEContextModificationFailureIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.UEContextModificationFailureIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.UEContextModificationFailureIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.UEContextModificationFailureIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.UEContextModificationFailureIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCause
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.UEContextModificationFailureIEsPresentCause
	ie3.Value.Cause = new(ngapType.Cause)
	*ie3.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.UEContextModificationFailureIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.UEContextModificationFailureIEsPresentCriticalityDiagnostics
	ie4.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie4.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildHandoverPreparationFailure() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentUnsuccessfulOutcome
	pdu.UnsuccessfulOutcome = new(ngapType.UnsuccessfulOutcome)
	pduMsg := pdu.UnsuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeHandoverPreparation
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.UnsuccessfulOutcomePresentHandoverPreparationFailure
	pduMsg.Value.HandoverPreparationFailure = new(ngapType.HandoverPreparationFailure)
	pduContent := pduMsg.Value.HandoverPreparationFailure
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.HandoverPreparationFailureIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.HandoverPreparationFailureIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.HandoverPreparationFailureIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.HandoverPreparationFailureIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.HandoverPreparationFailureIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCause
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.HandoverPreparationFailureIEsPresentCause
	ie3.Value.Cause = new(ngapType.Cause)
	*ie3.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.HandoverPreparationFailureIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.HandoverPreparationFailureIEsPresentCriticalityDiagnostics
	ie4.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie4.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	ie5 := ngapType.HandoverPreparationFailureIEs{}
	ie5.Id.Value = ngapType.ProtocolIEIDTargettoSourceFailureTransparentContainer
	ie5.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie5.Value.Present = ngapType.HandoverPreparationFailureIEsPresentTargettoSourceFailureTransparentContainer
	ie5.Value.TargettoSourceFailureTransparentContainer = new(ngapType.TargettoSourceFailureTransparentContainer)
	*ie5.Value.TargettoSourceFailureTransparentContainer = BuildTargettoSourceFailureTransparentContainer()

	pduContentIEs.List = append(pduContentIEs.List, ie5)

	return
}

func BuildHandoverFailure() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentUnsuccessfulOutcome
	pdu.UnsuccessfulOutcome = new(ngapType.UnsuccessfulOutcome)
	pduMsg := pdu.UnsuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeHandoverResourceAllocation
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.UnsuccessfulOutcomePresentHandoverFailure
	pduMsg.Value.HandoverFailure = new(ngapType.HandoverFailure)
	pduContent := pduMsg.Value.HandoverFailure
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.HandoverFailureIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.HandoverFailureIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.HandoverFailureIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDCause
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.HandoverFailureIEsPresentCause
	ie2.Value.Cause = new(ngapType.Cause)
	*ie2.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.HandoverFailureIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.HandoverFailureIEsPresentCriticalityDiagnostics
	ie3.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie3.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.HandoverFailureIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDTargettoSourceFailureTransparentContainer
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.HandoverFailureIEsPresentTargettoSourceFailureTransparentContainer
	ie4.Value.TargettoSourceFailureTransparentContainer = new(ngapType.TargettoSourceFailureTransparentContainer)
	*ie4.Value.TargettoSourceFailureTransparentContainer = BuildTargettoSourceFailureTransparentContainer()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildPathSwitchRequestFailure() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentUnsuccessfulOutcome
	pdu.UnsuccessfulOutcome = new(ngapType.UnsuccessfulOutcome)
	pduMsg := pdu.UnsuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodePathSwitchRequest
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.UnsuccessfulOutcomePresentPathSwitchRequestFailure
	pduMsg.Value.PathSwitchRequestFailure = new(ngapType.PathSwitchRequestFailure)
	pduContent := pduMsg.Value.PathSwitchRequestFailure
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.PathSwitchRequestFailureIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDAMFUENGAPID
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.PathSwitchRequestFailureIEsPresentAMFUENGAPID
	ie1.Value.AMFUENGAPID = new(ngapType.AMFUENGAPID)
	*ie1.Value.AMFUENGAPID = BuildAMFUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.PathSwitchRequestFailureIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDRANUENGAPID
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.PathSwitchRequestFailureIEsPresentRANUENGAPID
	ie2.Value.RANUENGAPID = new(ngapType.RANUENGAPID)
	*ie2.Value.RANUENGAPID = BuildRANUENGAPID()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.PathSwitchRequestFailureIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDPDUSessionResourceReleasedListPSFail
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.PathSwitchRequestFailureIEsPresentPDUSessionResourceReleasedListPSFail
	ie3.Value.PDUSessionResourceReleasedListPSFail = new(ngapType.PDUSessionResourceReleasedListPSFail)
	*ie3.Value.PDUSessionResourceReleasedListPSFail = BuildPDUSessionResourceReleasedListPSFail()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	ie4 := ngapType.PathSwitchRequestFailureIEs{}
	ie4.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie4.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie4.Value.Present = ngapType.PathSwitchRequestFailureIEsPresentCriticalityDiagnostics
	ie4.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie4.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie4)

	return
}

func BuildNGSetupFailure() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentUnsuccessfulOutcome
	pdu.UnsuccessfulOutcome = new(ngapType.UnsuccessfulOutcome)
	pduMsg := pdu.UnsuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeNGSetup
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.UnsuccessfulOutcomePresentNGSetupFailure
	pduMsg.Value.NGSetupFailure = new(ngapType.NGSetupFailure)
	pduContent := pduMsg.Value.NGSetupFailure
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.NGSetupFailureIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDCause
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.NGSetupFailureIEsPresentCause
	ie1.Value.Cause = new(ngapType.Cause)
	*ie1.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.NGSetupFailureIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDTimeToWait
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.NGSetupFailureIEsPresentTimeToWait
	ie2.Value.TimeToWait = new(ngapType.TimeToWait)
	*ie2.Value.TimeToWait = BuildTimeToWait()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.NGSetupFailureIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.NGSetupFailureIEsPresentCriticalityDiagnostics
	ie3.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie3.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildRANConfigurationUpdateFailure() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentUnsuccessfulOutcome
	pdu.UnsuccessfulOutcome = new(ngapType.UnsuccessfulOutcome)
	pduMsg := pdu.UnsuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeRANConfigurationUpdate
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.UnsuccessfulOutcomePresentRANConfigurationUpdateFailure
	pduMsg.Value.RANConfigurationUpdateFailure = new(ngapType.RANConfigurationUpdateFailure)
	pduContent := pduMsg.Value.RANConfigurationUpdateFailure
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.RANConfigurationUpdateFailureIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDCause
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.RANConfigurationUpdateFailureIEsPresentCause
	ie1.Value.Cause = new(ngapType.Cause)
	*ie1.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.RANConfigurationUpdateFailureIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDTimeToWait
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.RANConfigurationUpdateFailureIEsPresentTimeToWait
	ie2.Value.TimeToWait = new(ngapType.TimeToWait)
	*ie2.Value.TimeToWait = BuildTimeToWait()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.RANConfigurationUpdateFailureIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.RANConfigurationUpdateFailureIEsPresentCriticalityDiagnostics
	ie3.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie3.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

func BuildAMFConfigurationUpdateFailure() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentUnsuccessfulOutcome
	pdu.UnsuccessfulOutcome = new(ngapType.UnsuccessfulOutcome)
	pduMsg := pdu.UnsuccessfulOutcome
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeAMFConfigurationUpdate
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.UnsuccessfulOutcomePresentAMFConfigurationUpdateFailure
	pduMsg.Value.AMFConfigurationUpdateFailure = new(ngapType.AMFConfigurationUpdateFailure)
	pduContent := pduMsg.Value.AMFConfigurationUpdateFailure
	pduContentIEs := &pduContent.ProtocolIEs

	ie1 := ngapType.AMFConfigurationUpdateFailureIEs{}
	ie1.Id.Value = ngapType.ProtocolIEIDCause
	ie1.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie1.Value.Present = ngapType.AMFConfigurationUpdateFailureIEsPresentCause
	ie1.Value.Cause = new(ngapType.Cause)
	*ie1.Value.Cause = BuildCause()

	pduContentIEs.List = append(pduContentIEs.List, ie1)

	ie2 := ngapType.AMFConfigurationUpdateFailureIEs{}
	ie2.Id.Value = ngapType.ProtocolIEIDTimeToWait
	ie2.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie2.Value.Present = ngapType.AMFConfigurationUpdateFailureIEsPresentTimeToWait
	ie2.Value.TimeToWait = new(ngapType.TimeToWait)
	*ie2.Value.TimeToWait = BuildTimeToWait()

	pduContentIEs.List = append(pduContentIEs.List, ie2)

	ie3 := ngapType.AMFConfigurationUpdateFailureIEs{}
	ie3.Id.Value = ngapType.ProtocolIEIDCriticalityDiagnostics
	ie3.Criticality.Value = ngapType.CriticalityPresentIgnore
	ie3.Value.Present = ngapType.AMFConfigurationUpdateFailureIEsPresentCriticalityDiagnostics
	ie3.Value.CriticalityDiagnostics = new(ngapType.CriticalityDiagnostics)
	*ie3.Value.CriticalityDiagnostics = BuildCriticalityDiagnostics()

	pduContentIEs.List = append(pduContentIEs.List, ie3)

	return
}

package ngapType

type ProtocolIESingleContainerAMFPagingTargetExtIEs struct {
	List []AMFPagingTargetExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AMFPagingTargetExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue AMFPagingTargetExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AMFPagingTargetExtIEsPresentNothing int = iota /* No components present */
)

type AMFPagingTargetExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerAreaScopeOfMDTNRExtIEs struct {
	List []AreaScopeOfMDTNRExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AreaScopeOfMDTNRExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue AreaScopeOfMDTNRExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AreaScopeOfMDTNRExtIEsPresentNothing int = iota /* No components present */
)

type AreaScopeOfMDTNRExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerAreaScopeOfMDTEUTRAExtIEs struct {
	List []AreaScopeOfMDTEUTRAExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AreaScopeOfMDTEUTRAExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue AreaScopeOfMDTEUTRAExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AreaScopeOfMDTEUTRAExtIEsPresentNothing int = iota /* No components present */
)

type AreaScopeOfMDTEUTRAExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerBroadcastCancelledAreaListExtIEs struct {
	List []BroadcastCancelledAreaListExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type BroadcastCancelledAreaListExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue BroadcastCancelledAreaListExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	BroadcastCancelledAreaListExtIEsPresentNothing int = iota /* No components present */
)

type BroadcastCancelledAreaListExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerBroadcastCompletedAreaListExtIEs struct {
	List []BroadcastCompletedAreaListExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type BroadcastCompletedAreaListExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue BroadcastCompletedAreaListExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	BroadcastCompletedAreaListExtIEsPresentNothing int = iota /* No components present */
)

type BroadcastCompletedAreaListExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerCandidateCellExtIEs struct {
	List []CandidateCellExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CandidateCellExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue CandidateCellExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CandidateCellExtIEsPresentNothing int = iota /* No components present */
)

type CandidateCellExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerCauseExtIEs struct {
	List []CauseExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CauseExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue CauseExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CauseExtIEsPresentNothing int = iota /* No components present */
)

type CauseExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerCellIDListForRestartExtIEs struct {
	List []CellIDListForRestartExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CellIDListForRestartExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue CellIDListForRestartExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CellIDListForRestartExtIEsPresentNothing int = iota /* No components present */
)

type CellIDListForRestartExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerCPTransportLayerInformationExtIEs struct {
	List []CPTransportLayerInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CPTransportLayerInformationExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue CPTransportLayerInformationExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CPTransportLayerInformationExtIEsPresentNothing int = iota /* No components present */
	CPTransportLayerInformationExtIEsPresentEndpointIPAddressAndPort
)

type CPTransportLayerInformationExtIEsIESingleValue struct {
	Present                  int
	EndpointIPAddressAndPort *EndpointIPAddressAndPort `vht5gc:"valueExt,referenceFieldValue:169"`
}

type ProtocolIESingleContainerDRBStatusDLExtIEs struct {
	List []DRBStatusDLExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DRBStatusDLExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue DRBStatusDLExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DRBStatusDLExtIEsPresentNothing int = iota /* No components present */
)

type DRBStatusDLExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerDRBStatusULExtIEs struct {
	List []DRBStatusULExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DRBStatusULExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue DRBStatusULExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DRBStatusULExtIEsPresentNothing int = iota /* No components present */
)

type DRBStatusULExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerProcedureStageChoiceExtIEs struct {
	List []ProcedureStageChoiceExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ProcedureStageChoiceExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue ProcedureStageChoiceExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ProcedureStageChoiceExtIEsPresentNothing int = iota /* No components present */
)

type ProcedureStageChoiceExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerENBIDExtIEs struct {
	List []ENBIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ENBIDExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue ENBIDExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ENBIDExtIEsPresentNothing int = iota /* No components present */
)

type ENBIDExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerEventTriggerExtIEs struct {
	List []EventTriggerExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type EventTriggerExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue EventTriggerExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	EventTriggerExtIEsPresentNothing int = iota /* No components present */
)

type EventTriggerExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerMeasurementThresholdL1LoggedMDTExtIEs struct {
	List []MeasurementThresholdL1LoggedMDTExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type MeasurementThresholdL1LoggedMDTExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue MeasurementThresholdL1LoggedMDTExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	MeasurementThresholdL1LoggedMDTExtIEsPresentNothing int = iota /* No components present */
)

type MeasurementThresholdL1LoggedMDTExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerGlobalRANNodeIDExtIEs struct {
	List []GlobalRANNodeIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GlobalRANNodeIDExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue GlobalRANNodeIDExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GlobalRANNodeIDExtIEsPresentNothing int = iota /* No components present */
	GlobalRANNodeIDExtIEsPresentGlobalTNGFID
	GlobalRANNodeIDExtIEsPresentGlobalTWIFID
	GlobalRANNodeIDExtIEsPresentGlobalWAGFID
)

type GlobalRANNodeIDExtIEsIESingleValue struct {
	Present      int
	GlobalTNGFID *GlobalTNGFID `vht5gc:"valueExt,referenceFieldValue:240"`
	GlobalTWIFID *GlobalTWIFID `vht5gc:"valueExt,referenceFieldValue:241"`
	GlobalWAGFID *GlobalWAGFID `vht5gc:"valueExt,referenceFieldValue:242"`
}

type ProtocolIESingleContainerGNBIDExtIEs struct {
	List []GNBIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GNBIDExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue GNBIDExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GNBIDExtIEsPresentNothing int = iota /* No components present */
)

type GNBIDExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerIntersystemSONTransferTypeExtIEs struct {
	List []IntersystemSONTransferTypeExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type IntersystemSONTransferTypeExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue IntersystemSONTransferTypeExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	IntersystemSONTransferTypeExtIEsPresentNothing int = iota /* No components present */
)

type IntersystemSONTransferTypeExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerIntersystemSONInformationExtIEs struct {
	List []IntersystemSONInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type IntersystemSONInformationExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue IntersystemSONInformationExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	IntersystemSONInformationExtIEsPresentNothing int = iota /* No components present */
)

type IntersystemSONInformationExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerIntersystemSONInformationReportExtIEs struct {
	List []IntersystemSONInformationReportExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type IntersystemSONInformationReportExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue IntersystemSONInformationReportExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	IntersystemSONInformationReportExtIEsPresentNothing int = iota /* No components present */
)

type IntersystemSONInformationReportExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerInterSystemHandoverReportTypeExtIEs struct {
	List []InterSystemHandoverReportTypeExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type InterSystemHandoverReportTypeExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue InterSystemHandoverReportTypeExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	InterSystemHandoverReportTypeExtIEsPresentNothing int = iota /* No components present */
)

type InterSystemHandoverReportTypeExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerLastVisitedCellInformationExtIEs struct {
	List []LastVisitedCellInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type LastVisitedCellInformationExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue LastVisitedCellInformationExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LastVisitedCellInformationExtIEsPresentNothing int = iota /* No components present */
)

type LastVisitedCellInformationExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerLoggedMDTTriggerExtIEs struct {
	List []LoggedMDTTriggerExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type LoggedMDTTriggerExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue LoggedMDTTriggerExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LoggedMDTTriggerExtIEsPresentNothing int = iota /* No components present */
)

type LoggedMDTTriggerExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerMDTModeNrExtIEs struct {
	List []MDTModeNrExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type MDTModeNrExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue MDTModeNrExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	MDTModeNrExtIEsPresentNothing int = iota /* No components present */
)

type MDTModeNrExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerM1ThresholdTypeExtIEs struct {
	List []M1ThresholdTypeExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type M1ThresholdTypeExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue M1ThresholdTypeExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	M1ThresholdTypeExtIEsPresentNothing int = iota /* No components present */
)

type M1ThresholdTypeExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerN3IWFIDExtIEs struct {
	List []N3IWFIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type N3IWFIDExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue N3IWFIDExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	N3IWFIDExtIEsPresentNothing int = iota /* No components present */
)

type N3IWFIDExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerNgENBIDExtIEs struct {
	List []NgENBIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NgENBIDExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue NgENBIDExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NgENBIDExtIEsPresentNothing int = iota /* No components present */
)

type NgENBIDExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerNGRANCGIExtIEs struct {
	List []NGRANCGIExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NGRANCGIExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue NGRANCGIExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NGRANCGIExtIEsPresentNothing int = iota /* No components present */
)

type NGRANCGIExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerNPNAccessInformationExtIEs struct {
	List []NPNAccessInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NPNAccessInformationExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue NPNAccessInformationExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NPNAccessInformationExtIEsPresentNothing int = iota /* No components present */
)

type NPNAccessInformationExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerNPNMobilityInformationExtIEs struct {
	List []NPNMobilityInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NPNMobilityInformationExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue NPNMobilityInformationExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NPNMobilityInformationExtIEsPresentNothing int = iota /* No components present */
)

type NPNMobilityInformationExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerNPNPagingAssistanceInformationExtIEs struct {
	List []NPNPagingAssistanceInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NPNPagingAssistanceInformationExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue NPNPagingAssistanceInformationExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NPNPagingAssistanceInformationExtIEsPresentNothing int = iota /* No components present */
)

type NPNPagingAssistanceInformationExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerNPNSupportExtIEs struct {
	List []NPNSupportExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NPNSupportExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue NPNSupportExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NPNSupportExtIEsPresentNothing int = iota /* No components present */
)

type NPNSupportExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerOverloadResponseExtIEs struct {
	List []OverloadResponseExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type OverloadResponseExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue OverloadResponseExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	OverloadResponseExtIEsPresentNothing int = iota /* No components present */
)

type OverloadResponseExtIEsIESingleValue struct {
	Present int
}

type ProtocolIEContainerPDUSessionResourceModifyRequestTransferIEs struct {
	List []PDUSessionResourceModifyRequestTransferIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceModifyRequestTransferIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue PDUSessionResourceModifyRequestTransferIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyRequestTransferIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceModifyRequestTransferIEsPresentPDUSessionAggregateMaximumBitRate
	PDUSessionResourceModifyRequestTransferIEsPresentULNGUUPTNLModifyList
	PDUSessionResourceModifyRequestTransferIEsPresentNetworkInstance
	PDUSessionResourceModifyRequestTransferIEsPresentQosFlowAddOrModifyRequestList
	PDUSessionResourceModifyRequestTransferIEsPresentQosFlowToReleaseList
	PDUSessionResourceModifyRequestTransferIEsPresentAdditionalULNGUUPTNLInformation
	PDUSessionResourceModifyRequestTransferIEsPresentCommonNetworkInstance
	PDUSessionResourceModifyRequestTransferIEsPresentAdditionalRedundantULNGUUPTNLInformation
	PDUSessionResourceModifyRequestTransferIEsPresentRedundantCommonNetworkInstance
)

type PDUSessionResourceModifyRequestTransferIEsIESingleValue struct {
	Present                                  int
	PDUSessionAggregateMaximumBitRate        *PDUSessionAggregateMaximumBitRate `vht5gc:"valueExt,referenceFieldValue:130"`
	ULNGUUPTNLModifyList                     *ULNGUUPTNLModifyList              `vht5gc:"referenceFieldValue:140"`
	NetworkInstance                          *NetworkInstance                   `vht5gc:"referenceFieldValue:129"`
	QosFlowAddOrModifyRequestList            *QosFlowAddOrModifyRequestList     `vht5gc:"referenceFieldValue:135"`
	QosFlowToReleaseList                     *QosFlowListWithCause              `vht5gc:"referenceFieldValue:137"`
	AdditionalULNGUUPTNLInformation          *UPTransportLayerInformationList   `vht5gc:"referenceFieldValue:126"`
	CommonNetworkInstance                    *CommonNetworkInstance             `vht5gc:"referenceFieldValue:166"`
	AdditionalRedundantULNGUUPTNLInformation *UPTransportLayerInformationList   `vht5gc:"referenceFieldValue:186"`
	RedundantCommonNetworkInstance           *CommonNetworkInstance             `vht5gc:"referenceFieldValue:190"`
}

type ProtocolIEContainerPDUSessionResourceSetupRequestTransferIEs struct {
	List []PDUSessionResourceSetupRequestTransferIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceSetupRequestTransferIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	IEValue     PDUSessionResourceSetupRequestTransferIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSetupRequestTransferIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceSetupRequestTransferIEsPresentPDUSessionAggregateMaximumBitRate
	PDUSessionResourceSetupRequestTransferIEsPresentULNGUUPTNLInformation
	PDUSessionResourceSetupRequestTransferIEsPresentAdditionalULNGUUPTNLInformation
	PDUSessionResourceSetupRequestTransferIEsPresentDataForwardingNotPossible
	PDUSessionResourceSetupRequestTransferIEsPresentPDUSessionType
	PDUSessionResourceSetupRequestTransferIEsPresentSecurityIndication
	PDUSessionResourceSetupRequestTransferIEsPresentNetworkInstance
	PDUSessionResourceSetupRequestTransferIEsPresentQosFlowSetupRequestList
	PDUSessionResourceSetupRequestTransferIEsPresentCommonNetworkInstance
	PDUSessionResourceSetupRequestTransferIEsPresentDirectForwardingPathAvailability
	PDUSessionResourceSetupRequestTransferIEsPresentRedundantULNGUUPTNLInformation
	PDUSessionResourceSetupRequestTransferIEsPresentAdditionalRedundantULNGUUPTNLInformation
	PDUSessionResourceSetupRequestTransferIEsPresentRedundantCommonNetworkInstance
	PDUSessionResourceSetupRequestTransferIEsPresentRedundantPDUSessionInformation
)

type PDUSessionResourceSetupRequestTransferIEsIESingleValue struct {
	Present                                  int
	PDUSessionAggregateMaximumBitRate        *PDUSessionAggregateMaximumBitRate `vht5gc:"valueExt,referenceFieldValue:130"`
	ULNGUUPTNLInformation                    *UPTransportLayerInformation       `vht5gc:"referenceFieldValue:139,valueLB:0,valueUB:1"`
	AdditionalULNGUUPTNLInformation          *UPTransportLayerInformationList   `vht5gc:"referenceFieldValue:126"`
	DataForwardingNotPossible                *DataForwardingNotPossible         `vht5gc:"referenceFieldValue:127"`
	PDUSessionType                           *PDUSessionType                    `vht5gc:"referenceFieldValue:134"`
	SecurityIndication                       *SecurityIndication                `vht5gc:"valueExt,referenceFieldValue:138"`
	NetworkInstance                          *NetworkInstance                   `vht5gc:"referenceFieldValue:129"`
	QosFlowSetupRequestList                  *QosFlowSetupRequestList           `vht5gc:"referenceFieldValue:136"`
	CommonNetworkInstance                    *CommonNetworkInstance             `vht5gc:"referenceFieldValue:166"`
	DirectForwardingPathAvailability         *DirectForwardingPathAvailability  `vht5gc:"referenceFieldValue:22"`
	RedundantULNGUUPTNLInformation           *UPTransportLayerInformation       `vht5gc:"referenceFieldValue:195,valueLB:0,valueUB:1"`
	AdditionalRedundantULNGUUPTNLInformation *UPTransportLayerInformationList   `vht5gc:"referenceFieldValue:186"`
	RedundantCommonNetworkInstance           *CommonNetworkInstance             `vht5gc:"referenceFieldValue:190"`
	RedundantPDUSessionInformation           *RedundantPDUSessionInformation    `vht5gc:"valueExt,referenceFieldValue:197"`
}

type ProtocolIESingleContainerPWSFailedCellIDListExtIEs struct {
	List []PWSFailedCellIDListExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PWSFailedCellIDListExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue PWSFailedCellIDListExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PWSFailedCellIDListExtIEsPresentNothing int = iota /* No components present */
)

type PWSFailedCellIDListExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerQosCharacteristicsExtIEs struct {
	List []QosCharacteristicsExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosCharacteristicsExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue QosCharacteristicsExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosCharacteristicsExtIEsPresentNothing int = iota /* No components present */
)

type QosCharacteristicsExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerResetTypeExtIEs struct {
	List []ResetTypeExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ResetTypeExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue ResetTypeExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ResetTypeExtIEsPresentNothing int = iota /* No components present */
)

type ResetTypeExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerSensorNameConfigExtIEs struct {
	List []SensorNameConfigExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SensorNameConfigExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue SensorNameConfigExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SensorNameConfigExtIEsPresentNothing int = iota /* No components present */
)

type SensorNameConfigExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerSONInformationExtIEs struct {
	List []SONInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SONInformationExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue SONInformationExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SONInformationExtIEsPresentNothing int = iota /* No components present */
	SONInformationExtIEsPresentSONInformationReport
)

type SONInformationExtIEsIESingleValue struct {
	Present              int
	SONInformationReport *SONInformationReport `vht5gc:"referenceFieldValue:252,valueLB:0,valueUB:2"`
}

type ProtocolIESingleContainerSONInformationReportExtIEs struct {
	List []SONInformationReportExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SONInformationReportExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue SONInformationReportExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SONInformationReportExtIEsPresentNothing int = iota /* No components present */
)

type SONInformationReportExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerTargetIDExtIEs struct {
	List []TargetIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TargetIDExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue TargetIDExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TargetIDExtIEsPresentNothing int = iota /* No components present */
	TargetIDExtIEsPresentTargetRNCID
)

type TargetIDExtIEsIESingleValue struct {
	Present     int
	TargetRNCID *TargetRNCID `vht5gc:"valueExt,referenceFieldValue:178"`
}

type ProtocolIESingleContainerTNGFIDExtIEs struct {
	List []TNGFIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TNGFIDExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue TNGFIDExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TNGFIDExtIEsPresentNothing int = iota /* No components present */
)

type TNGFIDExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerTWIFIDExtIEs struct {
	List []TWIFIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TWIFIDExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue TWIFIDExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TWIFIDExtIEsPresentNothing int = iota /* No components present */
)

type TWIFIDExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerUEHistoryInformationFromTheUEExtIEs struct {
	List []UEHistoryInformationFromTheUEExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UEHistoryInformationFromTheUEExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue UEHistoryInformationFromTheUEExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEHistoryInformationFromTheUEExtIEsPresentNothing int = iota /* No components present */
)

type UEHistoryInformationFromTheUEExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerUEIdentityIndexValueExtIEs struct {
	List []UEIdentityIndexValueExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UEIdentityIndexValueExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue UEIdentityIndexValueExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEIdentityIndexValueExtIEsPresentNothing int = iota /* No components present */
)

type UEIdentityIndexValueExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerUENGAPIDsExtIEs struct {
	List []UENGAPIDsExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UENGAPIDsExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue UENGAPIDsExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UENGAPIDsExtIEsPresentNothing int = iota /* No components present */
)

type UENGAPIDsExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerUEPagingIdentityExtIEs struct {
	List []UEPagingIdentityExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UEPagingIdentityExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue UEPagingIdentityExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEPagingIdentityExtIEsPresentNothing int = iota /* No components present */
)

type UEPagingIdentityExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerUERLFReportContainerExtIEs struct {
	List []UERLFReportContainerExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UERLFReportContainerExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue UERLFReportContainerExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UERLFReportContainerExtIEsPresentNothing int = iota /* No components present */
)

type UERLFReportContainerExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerUPTransportLayerInformationExtIEs struct {
	List []UPTransportLayerInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UPTransportLayerInformationExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue UPTransportLayerInformationExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UPTransportLayerInformationExtIEsPresentNothing int = iota /* No components present */
)

type UPTransportLayerInformationExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerUserLocationInformationExtIEs struct {
	List []UserLocationInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UserLocationInformationExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue UserLocationInformationExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UserLocationInformationExtIEsPresentNothing int = iota /* No components present */
	UserLocationInformationExtIEsPresentUserLocationInformationTNGF
	UserLocationInformationExtIEsPresentUserLocationInformationTWIF
	UserLocationInformationExtIEsPresentUserLocationInformationWAGF
)

type UserLocationInformationExtIEsIESingleValue struct {
	Present                     int
	UserLocationInformationTNGF *UserLocationInformationTNGF `vht5gc:"valueExt,referenceFieldValue:244"`
	UserLocationInformationTWIF *UserLocationInformationTWIF `vht5gc:"valueExt,referenceFieldValue:248"`
	UserLocationInformationWAGF *UserLocationInformationWAGF `vht5gc:"referenceFieldValue:243,valueLB:0,valueUB:2"`
}

type ProtocolIESingleContainerUserLocationInformationWAGFExtIEs struct {
	List []UserLocationInformationWAGFExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UserLocationInformationWAGFExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue UserLocationInformationWAGFExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UserLocationInformationWAGFExtIEsPresentNothing int = iota /* No components present */
)

type UserLocationInformationWAGFExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerWAGFIDExtIEs struct {
	List []WAGFIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type WAGFIDExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue WAGFIDExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	WAGFIDExtIEsPresentNothing int = iota /* No components present */
)

type WAGFIDExtIEsIESingleValue struct {
	Present int
}

type ProtocolIESingleContainerWarningAreaListExtIEs struct {
	List []WarningAreaListExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type WarningAreaListExtIEs struct {
	Id            ProtocolIEID
	Criticality   Criticality
	IESingleValue WarningAreaListExtIEsIESingleValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	WarningAreaListExtIEsPresentNothing int = iota /* No components present */
)

type WarningAreaListExtIEsIESingleValue struct {
	Present int
}

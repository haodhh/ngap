package ngapType

type PDUSessionResourceSetupRequest struct {
	ProtocolIEs ProtocolIEContainerPDUSessionResourceSetupRequestIEs
}

type ProtocolIEContainerPDUSessionResourceSetupRequestIEs struct {
	List []PDUSessionResourceSetupRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PDUSessionResourceSetupRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PDUSessionResourceSetupRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSetupRequestIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceSetupRequestIEsPresentAMFUENGAPID
	PDUSessionResourceSetupRequestIEsPresentRANUENGAPID
	PDUSessionResourceSetupRequestIEsPresentRANPagingPriority
	PDUSessionResourceSetupRequestIEsPresentNASPDU
	PDUSessionResourceSetupRequestIEsPresentPDUSessionResourceSetupListSUReq
	PDUSessionResourceSetupRequestIEsPresentUEAggregateMaximumBitRate
)

type PDUSessionResourceSetupRequestIEsValue struct {
	Present                          int
	AMFUENGAPID                      *AMFUENGAPID                      `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                      *RANUENGAPID                      `vht5gc:"referenceFieldValue:85"`
	RANPagingPriority                *RANPagingPriority                `vht5gc:"referenceFieldValue:83"`
	NASPDU                           *NASPDU                           `vht5gc:"referenceFieldValue:38"`
	PDUSessionResourceSetupListSUReq *PDUSessionResourceSetupListSUReq `vht5gc:"referenceFieldValue:74"`
	UEAggregateMaximumBitRate        *UEAggregateMaximumBitRate        `vht5gc:"valueExt,referenceFieldValue:110"`
}

type PDUSessionResourceSetupResponse struct {
	ProtocolIEs ProtocolIEContainerPDUSessionResourceSetupResponseIEs
}

type ProtocolIEContainerPDUSessionResourceSetupResponseIEs struct {
	List []PDUSessionResourceSetupResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PDUSessionResourceSetupResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PDUSessionResourceSetupResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSetupResponseIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceSetupResponseIEsPresentAMFUENGAPID
	PDUSessionResourceSetupResponseIEsPresentRANUENGAPID
	PDUSessionResourceSetupResponseIEsPresentPDUSessionResourceSetupListSURes
	PDUSessionResourceSetupResponseIEsPresentPDUSessionResourceFailedToSetupListSURes
	PDUSessionResourceSetupResponseIEsPresentCriticalityDiagnostics
)

type PDUSessionResourceSetupResponseIEsValue struct {
	Present                                  int
	AMFUENGAPID                              *AMFUENGAPID                              `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                              *RANUENGAPID                              `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceSetupListSURes         *PDUSessionResourceSetupListSURes         `vht5gc:"referenceFieldValue:75"`
	PDUSessionResourceFailedToSetupListSURes *PDUSessionResourceFailedToSetupListSURes `vht5gc:"referenceFieldValue:58"`
	CriticalityDiagnostics                   *CriticalityDiagnostics                   `vht5gc:"valueExt,referenceFieldValue:19"`
}

type PDUSessionResourceReleaseCommand struct {
	ProtocolIEs ProtocolIEContainerPDUSessionResourceReleaseCommandIEs
}

type ProtocolIEContainerPDUSessionResourceReleaseCommandIEs struct {
	List []PDUSessionResourceReleaseCommandIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PDUSessionResourceReleaseCommandIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PDUSessionResourceReleaseCommandIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceReleaseCommandIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceReleaseCommandIEsPresentAMFUENGAPID
	PDUSessionResourceReleaseCommandIEsPresentRANUENGAPID
	PDUSessionResourceReleaseCommandIEsPresentRANPagingPriority
	PDUSessionResourceReleaseCommandIEsPresentNASPDU
	PDUSessionResourceReleaseCommandIEsPresentPDUSessionResourceToReleaseListRelCmd
)

type PDUSessionResourceReleaseCommandIEsValue struct {
	Present                               int
	AMFUENGAPID                           *AMFUENGAPID                           `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                           *RANUENGAPID                           `vht5gc:"referenceFieldValue:85"`
	RANPagingPriority                     *RANPagingPriority                     `vht5gc:"referenceFieldValue:83"`
	NASPDU                                *NASPDU                                `vht5gc:"referenceFieldValue:38"`
	PDUSessionResourceToReleaseListRelCmd *PDUSessionResourceToReleaseListRelCmd `vht5gc:"referenceFieldValue:79"`
}

type PDUSessionResourceReleaseResponse struct {
	ProtocolIEs ProtocolIEContainerPDUSessionResourceReleaseResponseIEs
}

type ProtocolIEContainerPDUSessionResourceReleaseResponseIEs struct {
	List []PDUSessionResourceReleaseResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PDUSessionResourceReleaseResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PDUSessionResourceReleaseResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceReleaseResponseIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceReleaseResponseIEsPresentAMFUENGAPID
	PDUSessionResourceReleaseResponseIEsPresentRANUENGAPID
	PDUSessionResourceReleaseResponseIEsPresentPDUSessionResourceReleasedListRelRes
	PDUSessionResourceReleaseResponseIEsPresentUserLocationInformation
	PDUSessionResourceReleaseResponseIEsPresentCriticalityDiagnostics
)

type PDUSessionResourceReleaseResponseIEsValue struct {
	Present                              int
	AMFUENGAPID                          *AMFUENGAPID                          `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                          *RANUENGAPID                          `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceReleasedListRelRes *PDUSessionResourceReleasedListRelRes `vht5gc:"referenceFieldValue:70"`
	UserLocationInformation              *UserLocationInformation              `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
	CriticalityDiagnostics               *CriticalityDiagnostics               `vht5gc:"valueExt,referenceFieldValue:19"`
}

type PDUSessionResourceModifyRequest struct {
	ProtocolIEs ProtocolIEContainerPDUSessionResourceModifyRequestIEs
}

type ProtocolIEContainerPDUSessionResourceModifyRequestIEs struct {
	List []PDUSessionResourceModifyRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PDUSessionResourceModifyRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PDUSessionResourceModifyRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyRequestIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceModifyRequestIEsPresentAMFUENGAPID
	PDUSessionResourceModifyRequestIEsPresentRANUENGAPID
	PDUSessionResourceModifyRequestIEsPresentRANPagingPriority
	PDUSessionResourceModifyRequestIEsPresentPDUSessionResourceModifyListModReq
)

type PDUSessionResourceModifyRequestIEsValue struct {
	Present                            int
	AMFUENGAPID                        *AMFUENGAPID                        `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                        *RANUENGAPID                        `vht5gc:"referenceFieldValue:85"`
	RANPagingPriority                  *RANPagingPriority                  `vht5gc:"referenceFieldValue:83"`
	PDUSessionResourceModifyListModReq *PDUSessionResourceModifyListModReq `vht5gc:"referenceFieldValue:64"`
}

type PDUSessionResourceModifyResponse struct {
	ProtocolIEs ProtocolIEContainerPDUSessionResourceModifyResponseIEs
}

type ProtocolIEContainerPDUSessionResourceModifyResponseIEs struct {
	List []PDUSessionResourceModifyResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PDUSessionResourceModifyResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PDUSessionResourceModifyResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyResponseIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceModifyResponseIEsPresentAMFUENGAPID
	PDUSessionResourceModifyResponseIEsPresentRANUENGAPID
	PDUSessionResourceModifyResponseIEsPresentPDUSessionResourceModifyListModRes
	PDUSessionResourceModifyResponseIEsPresentPDUSessionResourceFailedToModifyListModRes
	PDUSessionResourceModifyResponseIEsPresentUserLocationInformation
	PDUSessionResourceModifyResponseIEsPresentCriticalityDiagnostics
)

type PDUSessionResourceModifyResponseIEsValue struct {
	Present                                    int
	AMFUENGAPID                                *AMFUENGAPID                                `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                                *RANUENGAPID                                `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceModifyListModRes         *PDUSessionResourceModifyListModRes         `vht5gc:"referenceFieldValue:65"`
	PDUSessionResourceFailedToModifyListModRes *PDUSessionResourceFailedToModifyListModRes `vht5gc:"referenceFieldValue:54"`
	UserLocationInformation                    *UserLocationInformation                    `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
	CriticalityDiagnostics                     *CriticalityDiagnostics                     `vht5gc:"valueExt,referenceFieldValue:19"`
}

type PDUSessionResourceNotify struct {
	ProtocolIEs ProtocolIEContainerPDUSessionResourceNotifyIEs
}

type ProtocolIEContainerPDUSessionResourceNotifyIEs struct {
	List []PDUSessionResourceNotifyIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PDUSessionResourceNotifyIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PDUSessionResourceNotifyIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceNotifyIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceNotifyIEsPresentAMFUENGAPID
	PDUSessionResourceNotifyIEsPresentRANUENGAPID
	PDUSessionResourceNotifyIEsPresentPDUSessionResourceNotifyList
	PDUSessionResourceNotifyIEsPresentPDUSessionResourceReleasedListNot
	PDUSessionResourceNotifyIEsPresentUserLocationInformation
)

type PDUSessionResourceNotifyIEsValue struct {
	Present                           int
	AMFUENGAPID                       *AMFUENGAPID                       `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                       *RANUENGAPID                       `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceNotifyList      *PDUSessionResourceNotifyList      `vht5gc:"referenceFieldValue:66"`
	PDUSessionResourceReleasedListNot *PDUSessionResourceReleasedListNot `vht5gc:"referenceFieldValue:67"`
	UserLocationInformation           *UserLocationInformation           `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
}

type PDUSessionResourceModifyIndication struct {
	ProtocolIEs ProtocolIEContainerPDUSessionResourceModifyIndicationIEs
}

type ProtocolIEContainerPDUSessionResourceModifyIndicationIEs struct {
	List []PDUSessionResourceModifyIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PDUSessionResourceModifyIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PDUSessionResourceModifyIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyIndicationIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceModifyIndicationIEsPresentAMFUENGAPID
	PDUSessionResourceModifyIndicationIEsPresentRANUENGAPID
	PDUSessionResourceModifyIndicationIEsPresentPDUSessionResourceModifyListModInd
	PDUSessionResourceModifyIndicationIEsPresentUserLocationInformation
)

type PDUSessionResourceModifyIndicationIEsValue struct {
	Present                            int
	AMFUENGAPID                        *AMFUENGAPID                        `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                        *RANUENGAPID                        `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceModifyListModInd *PDUSessionResourceModifyListModInd `vht5gc:"referenceFieldValue:63"`
	UserLocationInformation            *UserLocationInformation            `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
}

type PDUSessionResourceModifyConfirm struct {
	ProtocolIEs ProtocolIEContainerPDUSessionResourceModifyConfirmIEs
}

type ProtocolIEContainerPDUSessionResourceModifyConfirmIEs struct {
	List []PDUSessionResourceModifyConfirmIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PDUSessionResourceModifyConfirmIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PDUSessionResourceModifyConfirmIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyConfirmIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceModifyConfirmIEsPresentAMFUENGAPID
	PDUSessionResourceModifyConfirmIEsPresentRANUENGAPID
	PDUSessionResourceModifyConfirmIEsPresentPDUSessionResourceModifyListModCfm
	PDUSessionResourceModifyConfirmIEsPresentPDUSessionResourceFailedToModifyListModCfm
	PDUSessionResourceModifyConfirmIEsPresentCriticalityDiagnostics
)

type PDUSessionResourceModifyConfirmIEsValue struct {
	Present                                    int
	AMFUENGAPID                                *AMFUENGAPID                                `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                                *RANUENGAPID                                `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceModifyListModCfm         *PDUSessionResourceModifyListModCfm         `vht5gc:"referenceFieldValue:62"`
	PDUSessionResourceFailedToModifyListModCfm *PDUSessionResourceFailedToModifyListModCfm `vht5gc:"referenceFieldValue:131"`
	CriticalityDiagnostics                     *CriticalityDiagnostics                     `vht5gc:"valueExt,referenceFieldValue:19"`
}

type InitialContextSetupRequest struct {
	ProtocolIEs ProtocolIEContainerInitialContextSetupRequestIEs
}

type ProtocolIEContainerInitialContextSetupRequestIEs struct {
	List []InitialContextSetupRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type InitialContextSetupRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       InitialContextSetupRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	InitialContextSetupRequestIEsPresentNothing int = iota /* No components present */
	InitialContextSetupRequestIEsPresentAMFUENGAPID
	InitialContextSetupRequestIEsPresentRANUENGAPID
	InitialContextSetupRequestIEsPresentOldAMF
	InitialContextSetupRequestIEsPresentUEAggregateMaximumBitRate
	InitialContextSetupRequestIEsPresentCoreNetworkAssistanceInformationForInactive
	InitialContextSetupRequestIEsPresentGUAMI
	InitialContextSetupRequestIEsPresentPDUSessionResourceSetupListCxtReq
	InitialContextSetupRequestIEsPresentAllowedNSSAI
	InitialContextSetupRequestIEsPresentUESecurityCapabilities
	InitialContextSetupRequestIEsPresentSecurityKey
	InitialContextSetupRequestIEsPresentTraceActivation
	InitialContextSetupRequestIEsPresentMobilityRestrictionList
	InitialContextSetupRequestIEsPresentUERadioCapability
	InitialContextSetupRequestIEsPresentIndexToRFSP
	InitialContextSetupRequestIEsPresentMaskedIMEISV
	InitialContextSetupRequestIEsPresentNASPDU
	InitialContextSetupRequestIEsPresentEmergencyFallbackIndicator
	InitialContextSetupRequestIEsPresentRRCInactiveTransitionReportRequest
	InitialContextSetupRequestIEsPresentUERadioCapabilityForPaging
	InitialContextSetupRequestIEsPresentRedirectionVoiceFallback
	InitialContextSetupRequestIEsPresentLocationReportingRequestType
	InitialContextSetupRequestIEsPresentCNAssistedRANTuning
	InitialContextSetupRequestIEsPresentSRVCCOperationPossible
	InitialContextSetupRequestIEsPresentIABAuthorized
	InitialContextSetupRequestIEsPresentEnhancedCoverageRestriction
	InitialContextSetupRequestIEsPresentExtendedConnectedTime
	InitialContextSetupRequestIEsPresentUEDifferentiationInfo
	InitialContextSetupRequestIEsPresentNRV2XServicesAuthorized
	InitialContextSetupRequestIEsPresentLTEV2XServicesAuthorized
	InitialContextSetupRequestIEsPresentNRUESidelinkAggregateMaximumBitrate
	InitialContextSetupRequestIEsPresentLTEUESidelinkAggregateMaximumBitrate
	InitialContextSetupRequestIEsPresentPC5QoSParameters
	InitialContextSetupRequestIEsPresentCEmodeBrestricted
	InitialContextSetupRequestIEsPresentUEUPCIoTSupport
	InitialContextSetupRequestIEsPresentRGLevelWirelineAccessCharacteristics
	InitialContextSetupRequestIEsPresentManagementBasedMDTPLMNList
	InitialContextSetupRequestIEsPresentUERadioCapabilityID
)

type InitialContextSetupRequestIEsValue struct {
	Present                                     int
	AMFUENGAPID                                 *AMFUENGAPID                                 `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                                 *RANUENGAPID                                 `vht5gc:"referenceFieldValue:85"`
	OldAMF                                      *AMFName                                     `vht5gc:"referenceFieldValue:48"`
	UEAggregateMaximumBitRate                   *UEAggregateMaximumBitRate                   `vht5gc:"valueExt,referenceFieldValue:110"`
	CoreNetworkAssistanceInformationForInactive *CoreNetworkAssistanceInformationForInactive `vht5gc:"valueExt,referenceFieldValue:18"`
	GUAMI                                       *GUAMI                                       `vht5gc:"valueExt,referenceFieldValue:28"`
	PDUSessionResourceSetupListCxtReq           *PDUSessionResourceSetupListCxtReq           `vht5gc:"referenceFieldValue:71"`
	AllowedNSSAI                                *AllowedNSSAI                                `vht5gc:"referenceFieldValue:0"`
	UESecurityCapabilities                      *UESecurityCapabilities                      `vht5gc:"valueExt,referenceFieldValue:119"`
	SecurityKey                                 *SecurityKey                                 `vht5gc:"referenceFieldValue:94"`
	TraceActivation                             *TraceActivation                             `vht5gc:"valueExt,referenceFieldValue:108"`
	MobilityRestrictionList                     *MobilityRestrictionList                     `vht5gc:"valueExt,referenceFieldValue:36"`
	UERadioCapability                           *UERadioCapability                           `vht5gc:"referenceFieldValue:117"`
	IndexToRFSP                                 *IndexToRFSP                                 `vht5gc:"referenceFieldValue:31"`
	MaskedIMEISV                                *MaskedIMEISV                                `vht5gc:"referenceFieldValue:34"`
	NASPDU                                      *NASPDU                                      `vht5gc:"referenceFieldValue:38"`
	EmergencyFallbackIndicator                  *EmergencyFallbackIndicator                  `vht5gc:"valueExt,referenceFieldValue:24"`
	RRCInactiveTransitionReportRequest          *RRCInactiveTransitionReportRequest          `vht5gc:"referenceFieldValue:91"`
	UERadioCapabilityForPaging                  *UERadioCapabilityForPaging                  `vht5gc:"valueExt,referenceFieldValue:118"`
	RedirectionVoiceFallback                    *RedirectionVoiceFallback                    `vht5gc:"referenceFieldValue:146"`
	LocationReportingRequestType                *LocationReportingRequestType                `vht5gc:"valueExt,referenceFieldValue:33"`
	CNAssistedRANTuning                         *CNAssistedRANTuning                         `vht5gc:"valueExt,referenceFieldValue:165"`
	SRVCCOperationPossible                      *SRVCCOperationPossible                      `vht5gc:"referenceFieldValue:177"`
	IABAuthorized                               *IABAuthorized                               `vht5gc:"referenceFieldValue:199"`
	EnhancedCoverageRestriction                 *EnhancedCoverageRestriction                 `vht5gc:"referenceFieldValue:205"`
	ExtendedConnectedTime                       *ExtendedConnectedTime                       `vht5gc:"referenceFieldValue:206"`
	UEDifferentiationInfo                       *UEDifferentiationInfo                       `vht5gc:"valueExt,referenceFieldValue:209"`
	NRV2XServicesAuthorized                     *NRV2XServicesAuthorized                     `vht5gc:"valueExt,referenceFieldValue:216"`
	LTEV2XServicesAuthorized                    *LTEV2XServicesAuthorized                    `vht5gc:"valueExt,referenceFieldValue:215"`
	NRUESidelinkAggregateMaximumBitrate         *NRUESidelinkAggregateMaximumBitrate         `vht5gc:"valueExt,referenceFieldValue:218"`
	LTEUESidelinkAggregateMaximumBitrate        *LTEUESidelinkAggregateMaximumBitrate        `vht5gc:"valueExt,referenceFieldValue:217"`
	PC5QoSParameters                            *PC5QoSParameters                            `vht5gc:"valueExt,referenceFieldValue:219"`
	CEmodeBrestricted                           *CEmodeBrestricted                           `vht5gc:"referenceFieldValue:222"`
	UEUPCIoTSupport                             *UEUPCIoTSupport                             `vht5gc:"referenceFieldValue:234"`
	RGLevelWirelineAccessCharacteristics        *RGLevelWirelineAccessCharacteristics        `vht5gc:"referenceFieldValue:238"`
	ManagementBasedMDTPLMNList                  *MDTPLMNList                                 `vht5gc:"referenceFieldValue:254"`
	UERadioCapabilityID                         *UERadioCapabilityID                         `vht5gc:"referenceFieldValue:264"`
}

type InitialContextSetupResponse struct {
	ProtocolIEs ProtocolIEContainerInitialContextSetupResponseIEs
}

type ProtocolIEContainerInitialContextSetupResponseIEs struct {
	List []InitialContextSetupResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type InitialContextSetupResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       InitialContextSetupResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	InitialContextSetupResponseIEsPresentNothing int = iota /* No components present */
	InitialContextSetupResponseIEsPresentAMFUENGAPID
	InitialContextSetupResponseIEsPresentRANUENGAPID
	InitialContextSetupResponseIEsPresentPDUSessionResourceSetupListCxtRes
	InitialContextSetupResponseIEsPresentPDUSessionResourceFailedToSetupListCxtRes
	InitialContextSetupResponseIEsPresentCriticalityDiagnostics
)

type InitialContextSetupResponseIEsValue struct {
	Present                                   int
	AMFUENGAPID                               *AMFUENGAPID                               `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                               *RANUENGAPID                               `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceSetupListCxtRes         *PDUSessionResourceSetupListCxtRes         `vht5gc:"referenceFieldValue:72"`
	PDUSessionResourceFailedToSetupListCxtRes *PDUSessionResourceFailedToSetupListCxtRes `vht5gc:"referenceFieldValue:55"`
	CriticalityDiagnostics                    *CriticalityDiagnostics                    `vht5gc:"valueExt,referenceFieldValue:19"`
}

type InitialContextSetupFailure struct {
	ProtocolIEs ProtocolIEContainerInitialContextSetupFailureIEs
}

type ProtocolIEContainerInitialContextSetupFailureIEs struct {
	List []InitialContextSetupFailureIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type InitialContextSetupFailureIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       InitialContextSetupFailureIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	InitialContextSetupFailureIEsPresentNothing int = iota /* No components present */
	InitialContextSetupFailureIEsPresentAMFUENGAPID
	InitialContextSetupFailureIEsPresentRANUENGAPID
	InitialContextSetupFailureIEsPresentPDUSessionResourceFailedToSetupListCxtFail
	InitialContextSetupFailureIEsPresentCause
	InitialContextSetupFailureIEsPresentCriticalityDiagnostics
)

type InitialContextSetupFailureIEsValue struct {
	Present                                    int
	AMFUENGAPID                                *AMFUENGAPID                                `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                                *RANUENGAPID                                `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceFailedToSetupListCxtFail *PDUSessionResourceFailedToSetupListCxtFail `vht5gc:"referenceFieldValue:132"`
	Cause                                      *Cause                                      `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	CriticalityDiagnostics                     *CriticalityDiagnostics                     `vht5gc:"valueExt,referenceFieldValue:19"`
}

type UEContextReleaseRequest struct {
	ProtocolIEs ProtocolIEContainerUEContextReleaseRequestIEs
}

type ProtocolIEContainerUEContextReleaseRequestIEs struct {
	List []UEContextReleaseRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextReleaseRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextReleaseRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextReleaseRequestIEsPresentNothing int = iota /* No components present */
	UEContextReleaseRequestIEsPresentAMFUENGAPID
	UEContextReleaseRequestIEsPresentRANUENGAPID
	UEContextReleaseRequestIEsPresentPDUSessionResourceListCxtRelReq
	UEContextReleaseRequestIEsPresentCause
)

type UEContextReleaseRequestIEsValue struct {
	Present                         int
	AMFUENGAPID                     *AMFUENGAPID                     `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                     *RANUENGAPID                     `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceListCxtRelReq *PDUSessionResourceListCxtRelReq `vht5gc:"referenceFieldValue:133"`
	Cause                           *Cause                           `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
}

type UEContextReleaseCommand struct {
	ProtocolIEs ProtocolIEContainerUEContextReleaseCommandIEs
}

type ProtocolIEContainerUEContextReleaseCommandIEs struct {
	List []UEContextReleaseCommandIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextReleaseCommandIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextReleaseCommandIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextReleaseCommandIEsPresentNothing int = iota /* No components present */
	UEContextReleaseCommandIEsPresentUENGAPIDs
	UEContextReleaseCommandIEsPresentCause
)

type UEContextReleaseCommandIEsValue struct {
	Present   int
	UENGAPIDs *UENGAPIDs `vht5gc:"referenceFieldValue:114,valueLB:0,valueUB:2"`
	Cause     *Cause     `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
}

type UEContextReleaseComplete struct {
	ProtocolIEs ProtocolIEContainerUEContextReleaseCompleteIEs
}

type ProtocolIEContainerUEContextReleaseCompleteIEs struct {
	List []UEContextReleaseCompleteIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextReleaseCompleteIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextReleaseCompleteIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextReleaseCompleteIEsPresentNothing int = iota /* No components present */
	UEContextReleaseCompleteIEsPresentAMFUENGAPID
	UEContextReleaseCompleteIEsPresentRANUENGAPID
	UEContextReleaseCompleteIEsPresentUserLocationInformation
	UEContextReleaseCompleteIEsPresentInfoOnRecommendedCellsAndRANNodesForPaging
	UEContextReleaseCompleteIEsPresentPDUSessionResourceListCxtRelCpl
	UEContextReleaseCompleteIEsPresentCriticalityDiagnostics
	UEContextReleaseCompleteIEsPresentPagingAssisDataforCEcapabUE
)

type UEContextReleaseCompleteIEsValue struct {
	Present                                    int
	AMFUENGAPID                                *AMFUENGAPID                                `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                                *RANUENGAPID                                `vht5gc:"referenceFieldValue:85"`
	UserLocationInformation                    *UserLocationInformation                    `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
	InfoOnRecommendedCellsAndRANNodesForPaging *InfoOnRecommendedCellsAndRANNodesForPaging `vht5gc:"valueExt,referenceFieldValue:32"`
	PDUSessionResourceListCxtRelCpl            *PDUSessionResourceListCxtRelCpl            `vht5gc:"referenceFieldValue:60"`
	CriticalityDiagnostics                     *CriticalityDiagnostics                     `vht5gc:"valueExt,referenceFieldValue:19"`
	PagingAssisDataforCEcapabUE                *PagingAssisDataforCEcapabUE                `vht5gc:"valueExt,referenceFieldValue:207"`
}

type UEContextResumeRequest struct {
	ProtocolIEs ProtocolIEContainerUEContextResumeRequestIEs
}

type ProtocolIEContainerUEContextResumeRequestIEs struct {
	List []UEContextResumeRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextResumeRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextResumeRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextResumeRequestIEsPresentNothing int = iota /* No components present */
	UEContextResumeRequestIEsPresentAMFUENGAPID
	UEContextResumeRequestIEsPresentRANUENGAPID
	UEContextResumeRequestIEsPresentRRCResumeCause
	UEContextResumeRequestIEsPresentPDUSessionResourceResumeListRESReq
	UEContextResumeRequestIEsPresentPDUSessionResourceFailedToResumeListRESReq
	UEContextResumeRequestIEsPresentSuspendRequestIndication
)

type UEContextResumeRequestIEsValue struct {
	Present                                    int
	AMFUENGAPID                                *AMFUENGAPID                                `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                                *RANUENGAPID                                `vht5gc:"referenceFieldValue:85"`
	RRCResumeCause                             *RRCEstablishmentCause                      `vht5gc:"referenceFieldValue:237"`
	PDUSessionResourceResumeListRESReq         *PDUSessionResourceResumeListRESReq         `vht5gc:"referenceFieldValue:232"`
	PDUSessionResourceFailedToResumeListRESReq *PDUSessionResourceFailedToResumeListRESReq `vht5gc:"referenceFieldValue:229"`
	SuspendRequestIndication                   *SuspendRequestIndication                   `vht5gc:"referenceFieldValue:235"`
}

type UEContextResumeResponse struct {
	ProtocolIEs ProtocolIEContainerUEContextResumeResponseIEs
}

type ProtocolIEContainerUEContextResumeResponseIEs struct {
	List []UEContextResumeResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextResumeResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextResumeResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextResumeResponseIEsPresentNothing int = iota /* No components present */
	UEContextResumeResponseIEsPresentAMFUENGAPID
	UEContextResumeResponseIEsPresentRANUENGAPID
	UEContextResumeResponseIEsPresentPDUSessionResourceResumeListRESRes
	UEContextResumeResponseIEsPresentPDUSessionResourceFailedToResumeListRESRes
	UEContextResumeResponseIEsPresentSecurityContext
	UEContextResumeResponseIEsPresentSuspendResponseIndication
	UEContextResumeResponseIEsPresentExtendedConnectedTime
	UEContextResumeResponseIEsPresentCriticalityDiagnostics
)

type UEContextResumeResponseIEsValue struct {
	Present                                    int
	AMFUENGAPID                                *AMFUENGAPID                                `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                                *RANUENGAPID                                `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceResumeListRESRes         *PDUSessionResourceResumeListRESRes         `vht5gc:"referenceFieldValue:233"`
	PDUSessionResourceFailedToResumeListRESRes *PDUSessionResourceFailedToResumeListRESRes `vht5gc:"referenceFieldValue:230"`
	SecurityContext                            *SecurityContext                            `vht5gc:"valueExt,referenceFieldValue:93"`
	SuspendResponseIndication                  *SuspendResponseIndication                  `vht5gc:"referenceFieldValue:236"`
	ExtendedConnectedTime                      *ExtendedConnectedTime                      `vht5gc:"referenceFieldValue:206"`
	CriticalityDiagnostics                     *CriticalityDiagnostics                     `vht5gc:"valueExt,referenceFieldValue:19"`
}

type UEContextResumeFailure struct {
	ProtocolIEs ProtocolIEContainerUEContextResumeFailureIEs
}

type ProtocolIEContainerUEContextResumeFailureIEs struct {
	List []UEContextResumeFailureIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextResumeFailureIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextResumeFailureIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextResumeFailureIEsPresentNothing int = iota /* No components present */
	UEContextResumeFailureIEsPresentAMFUENGAPID
	UEContextResumeFailureIEsPresentRANUENGAPID
	UEContextResumeFailureIEsPresentCause
	UEContextResumeFailureIEsPresentCriticalityDiagnostics
)

type UEContextResumeFailureIEsValue struct {
	Present                int
	AMFUENGAPID            *AMFUENGAPID            `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID            *RANUENGAPID            `vht5gc:"referenceFieldValue:85"`
	Cause                  *Cause                  `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
}

type UEContextSuspendRequest struct {
	ProtocolIEs ProtocolIEContainerUEContextSuspendRequestIEs
}

type ProtocolIEContainerUEContextSuspendRequestIEs struct {
	List []UEContextSuspendRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextSuspendRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextSuspendRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextSuspendRequestIEsPresentNothing int = iota /* No components present */
	UEContextSuspendRequestIEsPresentAMFUENGAPID
	UEContextSuspendRequestIEsPresentRANUENGAPID
	UEContextSuspendRequestIEsPresentInfoOnRecommendedCellsAndRANNodesForPaging
	UEContextSuspendRequestIEsPresentPagingAssisDataforCEcapabUE
	UEContextSuspendRequestIEsPresentPDUSessionResourceSuspendListSUSReq
)

type UEContextSuspendRequestIEsValue struct {
	Present                                    int
	AMFUENGAPID                                *AMFUENGAPID                                `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                                *RANUENGAPID                                `vht5gc:"referenceFieldValue:85"`
	InfoOnRecommendedCellsAndRANNodesForPaging *InfoOnRecommendedCellsAndRANNodesForPaging `vht5gc:"valueExt,referenceFieldValue:32"`
	PagingAssisDataforCEcapabUE                *PagingAssisDataforCEcapabUE                `vht5gc:"valueExt,referenceFieldValue:207"`
	PDUSessionResourceSuspendListSUSReq        *PDUSessionResourceSuspendListSUSReq        `vht5gc:"referenceFieldValue:231"`
}

type UEContextSuspendResponse struct {
	ProtocolIEs ProtocolIEContainerUEContextSuspendResponseIEs
}

type ProtocolIEContainerUEContextSuspendResponseIEs struct {
	List []UEContextSuspendResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextSuspendResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextSuspendResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextSuspendResponseIEsPresentNothing int = iota /* No components present */
	UEContextSuspendResponseIEsPresentAMFUENGAPID
	UEContextSuspendResponseIEsPresentRANUENGAPID
	UEContextSuspendResponseIEsPresentSecurityContext
	UEContextSuspendResponseIEsPresentCriticalityDiagnostics
)

type UEContextSuspendResponseIEsValue struct {
	Present                int
	AMFUENGAPID            *AMFUENGAPID            `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID            *RANUENGAPID            `vht5gc:"referenceFieldValue:85"`
	SecurityContext        *SecurityContext        `vht5gc:"valueExt,referenceFieldValue:93"`
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
}

type UEContextSuspendFailure struct {
	ProtocolIEs ProtocolIEContainerUEContextSuspendFailureIEs
}

type ProtocolIEContainerUEContextSuspendFailureIEs struct {
	List []UEContextSuspendFailureIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextSuspendFailureIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextSuspendFailureIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextSuspendFailureIEsPresentNothing int = iota /* No components present */
	UEContextSuspendFailureIEsPresentAMFUENGAPID
	UEContextSuspendFailureIEsPresentRANUENGAPID
	UEContextSuspendFailureIEsPresentCause
	UEContextSuspendFailureIEsPresentCriticalityDiagnostics
)

type UEContextSuspendFailureIEsValue struct {
	Present                int
	AMFUENGAPID            *AMFUENGAPID            `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID            *RANUENGAPID            `vht5gc:"referenceFieldValue:85"`
	Cause                  *Cause                  `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
}

type UEContextModificationRequest struct {
	ProtocolIEs ProtocolIEContainerUEContextModificationRequestIEs
}

type ProtocolIEContainerUEContextModificationRequestIEs struct {
	List []UEContextModificationRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextModificationRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextModificationRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextModificationRequestIEsPresentNothing int = iota /* No components present */
	UEContextModificationRequestIEsPresentAMFUENGAPID
	UEContextModificationRequestIEsPresentRANUENGAPID
	UEContextModificationRequestIEsPresentRANPagingPriority
	UEContextModificationRequestIEsPresentSecurityKey
	UEContextModificationRequestIEsPresentIndexToRFSP
	UEContextModificationRequestIEsPresentUEAggregateMaximumBitRate
	UEContextModificationRequestIEsPresentUESecurityCapabilities
	UEContextModificationRequestIEsPresentCoreNetworkAssistanceInformationForInactive
	UEContextModificationRequestIEsPresentEmergencyFallbackIndicator
	UEContextModificationRequestIEsPresentNewAMFUENGAPID
	UEContextModificationRequestIEsPresentRRCInactiveTransitionReportRequest
	UEContextModificationRequestIEsPresentNewGUAMI
	UEContextModificationRequestIEsPresentCNAssistedRANTuning
	UEContextModificationRequestIEsPresentSRVCCOperationPossible
	UEContextModificationRequestIEsPresentIABAuthorized
	UEContextModificationRequestIEsPresentNRV2XServicesAuthorized
	UEContextModificationRequestIEsPresentLTEV2XServicesAuthorized
	UEContextModificationRequestIEsPresentNRUESidelinkAggregateMaximumBitrate
	UEContextModificationRequestIEsPresentLTEUESidelinkAggregateMaximumBitrate
	UEContextModificationRequestIEsPresentPC5QoSParameters
	UEContextModificationRequestIEsPresentUERadioCapabilityID
)

type UEContextModificationRequestIEsValue struct {
	Present                                     int
	AMFUENGAPID                                 *AMFUENGAPID                                 `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                                 *RANUENGAPID                                 `vht5gc:"referenceFieldValue:85"`
	RANPagingPriority                           *RANPagingPriority                           `vht5gc:"referenceFieldValue:83"`
	SecurityKey                                 *SecurityKey                                 `vht5gc:"referenceFieldValue:94"`
	IndexToRFSP                                 *IndexToRFSP                                 `vht5gc:"referenceFieldValue:31"`
	UEAggregateMaximumBitRate                   *UEAggregateMaximumBitRate                   `vht5gc:"valueExt,referenceFieldValue:110"`
	UESecurityCapabilities                      *UESecurityCapabilities                      `vht5gc:"valueExt,referenceFieldValue:119"`
	CoreNetworkAssistanceInformationForInactive *CoreNetworkAssistanceInformationForInactive `vht5gc:"valueExt,referenceFieldValue:18"`
	EmergencyFallbackIndicator                  *EmergencyFallbackIndicator                  `vht5gc:"valueExt,referenceFieldValue:24"`
	NewAMFUENGAPID                              *AMFUENGAPID                                 `vht5gc:"referenceFieldValue:40"`
	RRCInactiveTransitionReportRequest          *RRCInactiveTransitionReportRequest          `vht5gc:"referenceFieldValue:91"`
	NewGUAMI                                    *GUAMI                                       `vht5gc:"valueExt,referenceFieldValue:162"`
	CNAssistedRANTuning                         *CNAssistedRANTuning                         `vht5gc:"valueExt,referenceFieldValue:165"`
	SRVCCOperationPossible                      *SRVCCOperationPossible                      `vht5gc:"referenceFieldValue:177"`
	IABAuthorized                               *IABAuthorized                               `vht5gc:"referenceFieldValue:199"`
	NRV2XServicesAuthorized                     *NRV2XServicesAuthorized                     `vht5gc:"valueExt,referenceFieldValue:216"`
	LTEV2XServicesAuthorized                    *LTEV2XServicesAuthorized                    `vht5gc:"valueExt,referenceFieldValue:215"`
	NRUESidelinkAggregateMaximumBitrate         *NRUESidelinkAggregateMaximumBitrate         `vht5gc:"valueExt,referenceFieldValue:218"`
	LTEUESidelinkAggregateMaximumBitrate        *LTEUESidelinkAggregateMaximumBitrate        `vht5gc:"valueExt,referenceFieldValue:217"`
	PC5QoSParameters                            *PC5QoSParameters                            `vht5gc:"valueExt,referenceFieldValue:219"`
	UERadioCapabilityID                         *UERadioCapabilityID                         `vht5gc:"referenceFieldValue:264"`
}

type UEContextModificationResponse struct {
	ProtocolIEs ProtocolIEContainerUEContextModificationResponseIEs
}

type ProtocolIEContainerUEContextModificationResponseIEs struct {
	List []UEContextModificationResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextModificationResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextModificationResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextModificationResponseIEsPresentNothing int = iota /* No components present */
	UEContextModificationResponseIEsPresentAMFUENGAPID
	UEContextModificationResponseIEsPresentRANUENGAPID
	UEContextModificationResponseIEsPresentRRCState
	UEContextModificationResponseIEsPresentUserLocationInformation
	UEContextModificationResponseIEsPresentCriticalityDiagnostics
)

type UEContextModificationResponseIEsValue struct {
	Present                 int
	AMFUENGAPID             *AMFUENGAPID             `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID             *RANUENGAPID             `vht5gc:"referenceFieldValue:85"`
	RRCState                *RRCState                `vht5gc:"referenceFieldValue:92"`
	UserLocationInformation *UserLocationInformation `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
	CriticalityDiagnostics  *CriticalityDiagnostics  `vht5gc:"valueExt,referenceFieldValue:19"`
}

type UEContextModificationFailure struct {
	ProtocolIEs ProtocolIEContainerUEContextModificationFailureIEs
}

type ProtocolIEContainerUEContextModificationFailureIEs struct {
	List []UEContextModificationFailureIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEContextModificationFailureIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEContextModificationFailureIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextModificationFailureIEsPresentNothing int = iota /* No components present */
	UEContextModificationFailureIEsPresentAMFUENGAPID
	UEContextModificationFailureIEsPresentRANUENGAPID
	UEContextModificationFailureIEsPresentCause
	UEContextModificationFailureIEsPresentCriticalityDiagnostics
)

type UEContextModificationFailureIEsValue struct {
	Present                int
	AMFUENGAPID            *AMFUENGAPID            `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID            *RANUENGAPID            `vht5gc:"referenceFieldValue:85"`
	Cause                  *Cause                  `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
}

type RRCInactiveTransitionReport struct {
	ProtocolIEs ProtocolIEContainerRRCInactiveTransitionReportIEs
}

type ProtocolIEContainerRRCInactiveTransitionReportIEs struct {
	List []RRCInactiveTransitionReportIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type RRCInactiveTransitionReportIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       RRCInactiveTransitionReportIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RRCInactiveTransitionReportIEsPresentNothing int = iota /* No components present */
	RRCInactiveTransitionReportIEsPresentAMFUENGAPID
	RRCInactiveTransitionReportIEsPresentRANUENGAPID
	RRCInactiveTransitionReportIEsPresentRRCState
	RRCInactiveTransitionReportIEsPresentUserLocationInformation
)

type RRCInactiveTransitionReportIEsValue struct {
	Present                 int
	AMFUENGAPID             *AMFUENGAPID             `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID             *RANUENGAPID             `vht5gc:"referenceFieldValue:85"`
	RRCState                *RRCState                `vht5gc:"referenceFieldValue:92"`
	UserLocationInformation *UserLocationInformation `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
}

type RetrieveUEInformation struct {
	ProtocolIEs ProtocolIEContainerRetrieveUEInformationIEs
}

type ProtocolIEContainerRetrieveUEInformationIEs struct {
	List []RetrieveUEInformationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type RetrieveUEInformationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       RetrieveUEInformationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RetrieveUEInformationIEsPresentNothing int = iota /* No components present */
	RetrieveUEInformationIEsPresentFiveGSTMSI
)

type RetrieveUEInformationIEsValue struct {
	Present    int
	FiveGSTMSI *FiveGSTMSI `vht5gc:"valueExt,referenceFieldValue:26"`
}

type UEInformationTransfer struct {
	ProtocolIEs ProtocolIEContainerUEInformationTransferIEs
}

type ProtocolIEContainerUEInformationTransferIEs struct {
	List []UEInformationTransferIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UEInformationTransferIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UEInformationTransferIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEInformationTransferIEsPresentNothing int = iota /* No components present */
	UEInformationTransferIEsPresentFiveGSTMSI
	UEInformationTransferIEsPresentNBIoTUEPriority
	UEInformationTransferIEsPresentUERadioCapability
	UEInformationTransferIEsPresentSNSSAI
	UEInformationTransferIEsPresentAllowedNSSAI
	UEInformationTransferIEsPresentUEDifferentiationInfo
)

type UEInformationTransferIEsValue struct {
	Present               int
	FiveGSTMSI            *FiveGSTMSI            `vht5gc:"valueExt,referenceFieldValue:26"`
	NBIoTUEPriority       *NBIoTUEPriority       `vht5gc:"referenceFieldValue:210"`
	UERadioCapability     *UERadioCapability     `vht5gc:"referenceFieldValue:117"`
	SNSSAI                *SNSSAI                `vht5gc:"valueExt,referenceFieldValue:148"`
	AllowedNSSAI          *AllowedNSSAI          `vht5gc:"referenceFieldValue:0"`
	UEDifferentiationInfo *UEDifferentiationInfo `vht5gc:"valueExt,referenceFieldValue:209"`
}

type RANCPRelocationIndication struct {
	ProtocolIEs ProtocolIEContainerRANCPRelocationIndicationIEs
}

type ProtocolIEContainerRANCPRelocationIndicationIEs struct {
	List []RANCPRelocationIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type RANCPRelocationIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       RANCPRelocationIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RANCPRelocationIndicationIEsPresentNothing int = iota /* No components present */
	RANCPRelocationIndicationIEsPresentRANUENGAPID
	RANCPRelocationIndicationIEsPresentFiveGSTMSI
	RANCPRelocationIndicationIEsPresentEUTRACGI
	RANCPRelocationIndicationIEsPresentTAI
	RANCPRelocationIndicationIEsPresentULCPSecurityInformation
)

type RANCPRelocationIndicationIEsValue struct {
	Present                 int
	RANUENGAPID             *RANUENGAPID             `vht5gc:"referenceFieldValue:85"`
	FiveGSTMSI              *FiveGSTMSI              `vht5gc:"valueExt,referenceFieldValue:26"`
	EUTRACGI                *EUTRACGI                `vht5gc:"valueExt,referenceFieldValue:25"`
	TAI                     *TAI                     `vht5gc:"valueExt,referenceFieldValue:213"`
	ULCPSecurityInformation *ULCPSecurityInformation `vht5gc:"valueExt,referenceFieldValue:211"`
}

type HandoverRequired struct {
	ProtocolIEs ProtocolIEContainerHandoverRequiredIEs
}

type ProtocolIEContainerHandoverRequiredIEs struct {
	List []HandoverRequiredIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type HandoverRequiredIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       HandoverRequiredIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverRequiredIEsPresentNothing int = iota /* No components present */
	HandoverRequiredIEsPresentAMFUENGAPID
	HandoverRequiredIEsPresentRANUENGAPID
	HandoverRequiredIEsPresentHandoverType
	HandoverRequiredIEsPresentCause
	HandoverRequiredIEsPresentTargetID
	HandoverRequiredIEsPresentDirectForwardingPathAvailability
	HandoverRequiredIEsPresentPDUSessionResourceListHORqd
	HandoverRequiredIEsPresentSourceToTargetTransparentContainer
)

type HandoverRequiredIEsValue struct {
	Present                            int
	AMFUENGAPID                        *AMFUENGAPID                        `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                        *RANUENGAPID                        `vht5gc:"referenceFieldValue:85"`
	HandoverType                       *HandoverType                       `vht5gc:"referenceFieldValue:29"`
	Cause                              *Cause                              `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	TargetID                           *TargetID                           `vht5gc:"referenceFieldValue:105,valueLB:0,valueUB:2"`
	DirectForwardingPathAvailability   *DirectForwardingPathAvailability   `vht5gc:"referenceFieldValue:22"`
	PDUSessionResourceListHORqd        *PDUSessionResourceListHORqd        `vht5gc:"referenceFieldValue:61"`
	SourceToTargetTransparentContainer *SourceToTargetTransparentContainer `vht5gc:"referenceFieldValue:101"`
}

type HandoverCommand struct {
	ProtocolIEs ProtocolIEContainerHandoverCommandIEs
}

type ProtocolIEContainerHandoverCommandIEs struct {
	List []HandoverCommandIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type HandoverCommandIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       HandoverCommandIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverCommandIEsPresentNothing int = iota /* No components present */
	HandoverCommandIEsPresentAMFUENGAPID
	HandoverCommandIEsPresentRANUENGAPID
	HandoverCommandIEsPresentHandoverType
	HandoverCommandIEsPresentNASSecurityParametersFromNGRAN
	HandoverCommandIEsPresentPDUSessionResourceHandoverList
	HandoverCommandIEsPresentPDUSessionResourceToReleaseListHOCmd
	HandoverCommandIEsPresentTargetToSourceTransparentContainer
	HandoverCommandIEsPresentCriticalityDiagnostics
)

type HandoverCommandIEsValue struct {
	Present                              int
	AMFUENGAPID                          *AMFUENGAPID                          `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                          *RANUENGAPID                          `vht5gc:"referenceFieldValue:85"`
	HandoverType                         *HandoverType                         `vht5gc:"referenceFieldValue:29"`
	NASSecurityParametersFromNGRAN       *NASSecurityParametersFromNGRAN       `vht5gc:"referenceFieldValue:39"`
	PDUSessionResourceHandoverList       *PDUSessionResourceHandoverList       `vht5gc:"referenceFieldValue:59"`
	PDUSessionResourceToReleaseListHOCmd *PDUSessionResourceToReleaseListHOCmd `vht5gc:"referenceFieldValue:78"`
	TargetToSourceTransparentContainer   *TargetToSourceTransparentContainer   `vht5gc:"referenceFieldValue:106"`
	CriticalityDiagnostics               *CriticalityDiagnostics               `vht5gc:"valueExt,referenceFieldValue:19"`
}

type HandoverPreparationFailure struct {
	ProtocolIEs ProtocolIEContainerHandoverPreparationFailureIEs
}

type ProtocolIEContainerHandoverPreparationFailureIEs struct {
	List []HandoverPreparationFailureIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type HandoverPreparationFailureIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       HandoverPreparationFailureIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverPreparationFailureIEsPresentNothing int = iota /* No components present */
	HandoverPreparationFailureIEsPresentAMFUENGAPID
	HandoverPreparationFailureIEsPresentRANUENGAPID
	HandoverPreparationFailureIEsPresentCause
	HandoverPreparationFailureIEsPresentCriticalityDiagnostics
	HandoverPreparationFailureIEsPresentTargettoSourceFailureTransparentContainer
)

type HandoverPreparationFailureIEsValue struct {
	Present                                   int
	AMFUENGAPID                               *AMFUENGAPID                               `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                               *RANUENGAPID                               `vht5gc:"referenceFieldValue:85"`
	Cause                                     *Cause                                     `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	CriticalityDiagnostics                    *CriticalityDiagnostics                    `vht5gc:"valueExt,referenceFieldValue:19"`
	TargettoSourceFailureTransparentContainer *TargettoSourceFailureTransparentContainer `vht5gc:"referenceFieldValue:262"`
}

type HandoverRequest struct {
	ProtocolIEs ProtocolIEContainerHandoverRequestIEs
}

type ProtocolIEContainerHandoverRequestIEs struct {
	List []HandoverRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type HandoverRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       HandoverRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverRequestIEsPresentNothing int = iota /* No components present */
	HandoverRequestIEsPresentAMFUENGAPID
	HandoverRequestIEsPresentHandoverType
	HandoverRequestIEsPresentCause
	HandoverRequestIEsPresentUEAggregateMaximumBitRate
	HandoverRequestIEsPresentCoreNetworkAssistanceInformationForInactive
	HandoverRequestIEsPresentUESecurityCapabilities
	HandoverRequestIEsPresentSecurityContext
	HandoverRequestIEsPresentNewSecurityContextInd
	HandoverRequestIEsPresentNASC
	HandoverRequestIEsPresentPDUSessionResourceSetupListHOReq
	HandoverRequestIEsPresentAllowedNSSAI
	HandoverRequestIEsPresentTraceActivation
	HandoverRequestIEsPresentMaskedIMEISV
	HandoverRequestIEsPresentSourceToTargetTransparentContainer
	HandoverRequestIEsPresentMobilityRestrictionList
	HandoverRequestIEsPresentLocationReportingRequestType
	HandoverRequestIEsPresentRRCInactiveTransitionReportRequest
	HandoverRequestIEsPresentGUAMI
	HandoverRequestIEsPresentRedirectionVoiceFallback
	HandoverRequestIEsPresentCNAssistedRANTuning
	HandoverRequestIEsPresentSRVCCOperationPossible
	HandoverRequestIEsPresentIABAuthorized
	HandoverRequestIEsPresentEnhancedCoverageRestriction
	HandoverRequestIEsPresentUEDifferentiationInfo
	HandoverRequestIEsPresentNRV2XServicesAuthorized
	HandoverRequestIEsPresentLTEV2XServicesAuthorized
	HandoverRequestIEsPresentNRUESidelinkAggregateMaximumBitrate
	HandoverRequestIEsPresentLTEUESidelinkAggregateMaximumBitrate
	HandoverRequestIEsPresentPC5QoSParameters
	HandoverRequestIEsPresentCEmodeBrestricted
	HandoverRequestIEsPresentUEUPCIoTSupport
	HandoverRequestIEsPresentManagementBasedMDTPLMNList
	HandoverRequestIEsPresentUERadioCapabilityID
)

type HandoverRequestIEsValue struct {
	Present                                     int
	AMFUENGAPID                                 *AMFUENGAPID                                 `vht5gc:"referenceFieldValue:10"`
	HandoverType                                *HandoverType                                `vht5gc:"referenceFieldValue:29"`
	Cause                                       *Cause                                       `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	UEAggregateMaximumBitRate                   *UEAggregateMaximumBitRate                   `vht5gc:"valueExt,referenceFieldValue:110"`
	CoreNetworkAssistanceInformationForInactive *CoreNetworkAssistanceInformationForInactive `vht5gc:"valueExt,referenceFieldValue:18"`
	UESecurityCapabilities                      *UESecurityCapabilities                      `vht5gc:"valueExt,referenceFieldValue:119"`
	SecurityContext                             *SecurityContext                             `vht5gc:"valueExt,referenceFieldValue:93"`
	NewSecurityContextInd                       *NewSecurityContextInd                       `vht5gc:"referenceFieldValue:41"`
	NASC                                        *NASPDU                                      `vht5gc:"referenceFieldValue:37"`
	PDUSessionResourceSetupListHOReq            *PDUSessionResourceSetupListHOReq            `vht5gc:"referenceFieldValue:73"`
	AllowedNSSAI                                *AllowedNSSAI                                `vht5gc:"referenceFieldValue:0"`
	TraceActivation                             *TraceActivation                             `vht5gc:"valueExt,referenceFieldValue:108"`
	MaskedIMEISV                                *MaskedIMEISV                                `vht5gc:"referenceFieldValue:34"`
	SourceToTargetTransparentContainer          *SourceToTargetTransparentContainer          `vht5gc:"referenceFieldValue:101"`
	MobilityRestrictionList                     *MobilityRestrictionList                     `vht5gc:"valueExt,referenceFieldValue:36"`
	LocationReportingRequestType                *LocationReportingRequestType                `vht5gc:"valueExt,referenceFieldValue:33"`
	RRCInactiveTransitionReportRequest          *RRCInactiveTransitionReportRequest          `vht5gc:"referenceFieldValue:91"`
	GUAMI                                       *GUAMI                                       `vht5gc:"valueExt,referenceFieldValue:28"`
	RedirectionVoiceFallback                    *RedirectionVoiceFallback                    `vht5gc:"referenceFieldValue:146"`
	CNAssistedRANTuning                         *CNAssistedRANTuning                         `vht5gc:"valueExt,referenceFieldValue:165"`
	SRVCCOperationPossible                      *SRVCCOperationPossible                      `vht5gc:"referenceFieldValue:177"`
	IABAuthorized                               *IABAuthorized                               `vht5gc:"referenceFieldValue:199"`
	EnhancedCoverageRestriction                 *EnhancedCoverageRestriction                 `vht5gc:"referenceFieldValue:205"`
	UEDifferentiationInfo                       *UEDifferentiationInfo                       `vht5gc:"valueExt,referenceFieldValue:209"`
	NRV2XServicesAuthorized                     *NRV2XServicesAuthorized                     `vht5gc:"valueExt,referenceFieldValue:216"`
	LTEV2XServicesAuthorized                    *LTEV2XServicesAuthorized                    `vht5gc:"valueExt,referenceFieldValue:215"`
	NRUESidelinkAggregateMaximumBitrate         *NRUESidelinkAggregateMaximumBitrate         `vht5gc:"valueExt,referenceFieldValue:218"`
	LTEUESidelinkAggregateMaximumBitrate        *LTEUESidelinkAggregateMaximumBitrate        `vht5gc:"valueExt,referenceFieldValue:217"`
	PC5QoSParameters                            *PC5QoSParameters                            `vht5gc:"valueExt,referenceFieldValue:219"`
	CEmodeBrestricted                           *CEmodeBrestricted                           `vht5gc:"referenceFieldValue:222"`
	UEUPCIoTSupport                             *UEUPCIoTSupport                             `vht5gc:"referenceFieldValue:234"`
	ManagementBasedMDTPLMNList                  *MDTPLMNList                                 `vht5gc:"referenceFieldValue:254"`
	UERadioCapabilityID                         *UERadioCapabilityID                         `vht5gc:"referenceFieldValue:264"`
}

type HandoverRequestAcknowledge struct {
	ProtocolIEs ProtocolIEContainerHandoverRequestAcknowledgeIEs
}

type ProtocolIEContainerHandoverRequestAcknowledgeIEs struct {
	List []HandoverRequestAcknowledgeIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type HandoverRequestAcknowledgeIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       HandoverRequestAcknowledgeIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverRequestAcknowledgeIEsPresentNothing int = iota /* No components present */
	HandoverRequestAcknowledgeIEsPresentAMFUENGAPID
	HandoverRequestAcknowledgeIEsPresentRANUENGAPID
	HandoverRequestAcknowledgeIEsPresentPDUSessionResourceAdmittedList
	HandoverRequestAcknowledgeIEsPresentPDUSessionResourceFailedToSetupListHOAck
	HandoverRequestAcknowledgeIEsPresentTargetToSourceTransparentContainer
	HandoverRequestAcknowledgeIEsPresentCriticalityDiagnostics
)

type HandoverRequestAcknowledgeIEsValue struct {
	Present                                  int
	AMFUENGAPID                              *AMFUENGAPID                              `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                              *RANUENGAPID                              `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceAdmittedList           *PDUSessionResourceAdmittedList           `vht5gc:"referenceFieldValue:53"`
	PDUSessionResourceFailedToSetupListHOAck *PDUSessionResourceFailedToSetupListHOAck `vht5gc:"referenceFieldValue:56"`
	TargetToSourceTransparentContainer       *TargetToSourceTransparentContainer       `vht5gc:"referenceFieldValue:106"`
	CriticalityDiagnostics                   *CriticalityDiagnostics                   `vht5gc:"valueExt,referenceFieldValue:19"`
}

type HandoverFailure struct {
	ProtocolIEs ProtocolIEContainerHandoverFailureIEs
}

type ProtocolIEContainerHandoverFailureIEs struct {
	List []HandoverFailureIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type HandoverFailureIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       HandoverFailureIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverFailureIEsPresentNothing int = iota /* No components present */
	HandoverFailureIEsPresentAMFUENGAPID
	HandoverFailureIEsPresentCause
	HandoverFailureIEsPresentCriticalityDiagnostics
	HandoverFailureIEsPresentTargettoSourceFailureTransparentContainer
)

type HandoverFailureIEsValue struct {
	Present                                   int
	AMFUENGAPID                               *AMFUENGAPID                               `vht5gc:"referenceFieldValue:10"`
	Cause                                     *Cause                                     `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	CriticalityDiagnostics                    *CriticalityDiagnostics                    `vht5gc:"valueExt,referenceFieldValue:19"`
	TargettoSourceFailureTransparentContainer *TargettoSourceFailureTransparentContainer `vht5gc:"referenceFieldValue:262"`
}

type HandoverNotify struct {
	ProtocolIEs ProtocolIEContainerHandoverNotifyIEs
}

type ProtocolIEContainerHandoverNotifyIEs struct {
	List []HandoverNotifyIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type HandoverNotifyIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       HandoverNotifyIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverNotifyIEsPresentNothing int = iota /* No components present */
	HandoverNotifyIEsPresentAMFUENGAPID
	HandoverNotifyIEsPresentRANUENGAPID
	HandoverNotifyIEsPresentUserLocationInformation
	HandoverNotifyIEsPresentNotifySourceNGRANNode
)

type HandoverNotifyIEsValue struct {
	Present                 int
	AMFUENGAPID             *AMFUENGAPID             `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID             *RANUENGAPID             `vht5gc:"referenceFieldValue:85"`
	UserLocationInformation *UserLocationInformation `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
	NotifySourceNGRANNode   *NotifySourceNGRANNode   `vht5gc:"referenceFieldValue:269"`
}

type PathSwitchRequest struct {
	ProtocolIEs ProtocolIEContainerPathSwitchRequestIEs
}

type ProtocolIEContainerPathSwitchRequestIEs struct {
	List []PathSwitchRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PathSwitchRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PathSwitchRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PathSwitchRequestIEsPresentNothing int = iota /* No components present */
	PathSwitchRequestIEsPresentRANUENGAPID
	PathSwitchRequestIEsPresentSourceAMFUENGAPID
	PathSwitchRequestIEsPresentUserLocationInformation
	PathSwitchRequestIEsPresentUESecurityCapabilities
	PathSwitchRequestIEsPresentPDUSessionResourceToBeSwitchedDLList
	PathSwitchRequestIEsPresentPDUSessionResourceFailedToSetupListPSReq
	PathSwitchRequestIEsPresentRRCResumeCause
)

type PathSwitchRequestIEsValue struct {
	Present                                  int
	RANUENGAPID                              *RANUENGAPID                              `vht5gc:"referenceFieldValue:85"`
	SourceAMFUENGAPID                        *AMFUENGAPID                              `vht5gc:"referenceFieldValue:100"`
	UserLocationInformation                  *UserLocationInformation                  `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
	UESecurityCapabilities                   *UESecurityCapabilities                   `vht5gc:"valueExt,referenceFieldValue:119"`
	PDUSessionResourceToBeSwitchedDLList     *PDUSessionResourceToBeSwitchedDLList     `vht5gc:"referenceFieldValue:76"`
	PDUSessionResourceFailedToSetupListPSReq *PDUSessionResourceFailedToSetupListPSReq `vht5gc:"referenceFieldValue:57"`
	RRCResumeCause                           *RRCEstablishmentCause                    `vht5gc:"referenceFieldValue:237"`
}

type PathSwitchRequestAcknowledge struct {
	ProtocolIEs ProtocolIEContainerPathSwitchRequestAcknowledgeIEs
}

type ProtocolIEContainerPathSwitchRequestAcknowledgeIEs struct {
	List []PathSwitchRequestAcknowledgeIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PathSwitchRequestAcknowledgeIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PathSwitchRequestAcknowledgeIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PathSwitchRequestAcknowledgeIEsPresentNothing int = iota /* No components present */
	PathSwitchRequestAcknowledgeIEsPresentAMFUENGAPID
	PathSwitchRequestAcknowledgeIEsPresentRANUENGAPID
	PathSwitchRequestAcknowledgeIEsPresentUESecurityCapabilities
	PathSwitchRequestAcknowledgeIEsPresentSecurityContext
	PathSwitchRequestAcknowledgeIEsPresentNewSecurityContextInd
	PathSwitchRequestAcknowledgeIEsPresentPDUSessionResourceSwitchedList
	PathSwitchRequestAcknowledgeIEsPresentPDUSessionResourceReleasedListPSAck
	PathSwitchRequestAcknowledgeIEsPresentAllowedNSSAI
	PathSwitchRequestAcknowledgeIEsPresentCoreNetworkAssistanceInformationForInactive
	PathSwitchRequestAcknowledgeIEsPresentRRCInactiveTransitionReportRequest
	PathSwitchRequestAcknowledgeIEsPresentCriticalityDiagnostics
	PathSwitchRequestAcknowledgeIEsPresentRedirectionVoiceFallback
	PathSwitchRequestAcknowledgeIEsPresentCNAssistedRANTuning
	PathSwitchRequestAcknowledgeIEsPresentSRVCCOperationPossible
	PathSwitchRequestAcknowledgeIEsPresentEnhancedCoverageRestriction
	PathSwitchRequestAcknowledgeIEsPresentExtendedConnectedTime
	PathSwitchRequestAcknowledgeIEsPresentUEDifferentiationInfo
	PathSwitchRequestAcknowledgeIEsPresentNRV2XServicesAuthorized
	PathSwitchRequestAcknowledgeIEsPresentLTEV2XServicesAuthorized
	PathSwitchRequestAcknowledgeIEsPresentNRUESidelinkAggregateMaximumBitrate
	PathSwitchRequestAcknowledgeIEsPresentLTEUESidelinkAggregateMaximumBitrate
	PathSwitchRequestAcknowledgeIEsPresentPC5QoSParameters
	PathSwitchRequestAcknowledgeIEsPresentCEmodeBrestricted
	PathSwitchRequestAcknowledgeIEsPresentUEUPCIoTSupport
	PathSwitchRequestAcknowledgeIEsPresentUERadioCapabilityID
)

type PathSwitchRequestAcknowledgeIEsValue struct {
	Present                                     int
	AMFUENGAPID                                 *AMFUENGAPID                                 `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                                 *RANUENGAPID                                 `vht5gc:"referenceFieldValue:85"`
	UESecurityCapabilities                      *UESecurityCapabilities                      `vht5gc:"valueExt,referenceFieldValue:119"`
	SecurityContext                             *SecurityContext                             `vht5gc:"valueExt,referenceFieldValue:93"`
	NewSecurityContextInd                       *NewSecurityContextInd                       `vht5gc:"referenceFieldValue:41"`
	PDUSessionResourceSwitchedList              *PDUSessionResourceSwitchedList              `vht5gc:"referenceFieldValue:77"`
	PDUSessionResourceReleasedListPSAck         *PDUSessionResourceReleasedListPSAck         `vht5gc:"referenceFieldValue:68"`
	AllowedNSSAI                                *AllowedNSSAI                                `vht5gc:"referenceFieldValue:0"`
	CoreNetworkAssistanceInformationForInactive *CoreNetworkAssistanceInformationForInactive `vht5gc:"valueExt,referenceFieldValue:18"`
	RRCInactiveTransitionReportRequest          *RRCInactiveTransitionReportRequest          `vht5gc:"referenceFieldValue:91"`
	CriticalityDiagnostics                      *CriticalityDiagnostics                      `vht5gc:"valueExt,referenceFieldValue:19"`
	RedirectionVoiceFallback                    *RedirectionVoiceFallback                    `vht5gc:"referenceFieldValue:146"`
	CNAssistedRANTuning                         *CNAssistedRANTuning                         `vht5gc:"valueExt,referenceFieldValue:165"`
	SRVCCOperationPossible                      *SRVCCOperationPossible                      `vht5gc:"referenceFieldValue:177"`
	EnhancedCoverageRestriction                 *EnhancedCoverageRestriction                 `vht5gc:"referenceFieldValue:205"`
	ExtendedConnectedTime                       *ExtendedConnectedTime                       `vht5gc:"referenceFieldValue:206"`
	UEDifferentiationInfo                       *UEDifferentiationInfo                       `vht5gc:"valueExt,referenceFieldValue:209"`
	NRV2XServicesAuthorized                     *NRV2XServicesAuthorized                     `vht5gc:"valueExt,referenceFieldValue:216"`
	LTEV2XServicesAuthorized                    *LTEV2XServicesAuthorized                    `vht5gc:"valueExt,referenceFieldValue:215"`
	NRUESidelinkAggregateMaximumBitrate         *NRUESidelinkAggregateMaximumBitrate         `vht5gc:"valueExt,referenceFieldValue:218"`
	LTEUESidelinkAggregateMaximumBitrate        *LTEUESidelinkAggregateMaximumBitrate        `vht5gc:"valueExt,referenceFieldValue:217"`
	PC5QoSParameters                            *PC5QoSParameters                            `vht5gc:"valueExt,referenceFieldValue:219"`
	CEmodeBrestricted                           *CEmodeBrestricted                           `vht5gc:"referenceFieldValue:222"`
	UEUPCIoTSupport                             *UEUPCIoTSupport                             `vht5gc:"referenceFieldValue:234"`
	UERadioCapabilityID                         *UERadioCapabilityID                         `vht5gc:"referenceFieldValue:264"`
}

type PathSwitchRequestFailure struct {
	ProtocolIEs ProtocolIEContainerPathSwitchRequestFailureIEs
}

type ProtocolIEContainerPathSwitchRequestFailureIEs struct {
	List []PathSwitchRequestFailureIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PathSwitchRequestFailureIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PathSwitchRequestFailureIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PathSwitchRequestFailureIEsPresentNothing int = iota /* No components present */
	PathSwitchRequestFailureIEsPresentAMFUENGAPID
	PathSwitchRequestFailureIEsPresentRANUENGAPID
	PathSwitchRequestFailureIEsPresentPDUSessionResourceReleasedListPSFail
	PathSwitchRequestFailureIEsPresentCriticalityDiagnostics
)

type PathSwitchRequestFailureIEsValue struct {
	Present                              int
	AMFUENGAPID                          *AMFUENGAPID                          `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                          *RANUENGAPID                          `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceReleasedListPSFail *PDUSessionResourceReleasedListPSFail `vht5gc:"referenceFieldValue:69"`
	CriticalityDiagnostics               *CriticalityDiagnostics               `vht5gc:"valueExt,referenceFieldValue:19"`
}

type HandoverCancel struct {
	ProtocolIEs ProtocolIEContainerHandoverCancelIEs
}

type ProtocolIEContainerHandoverCancelIEs struct {
	List []HandoverCancelIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type HandoverCancelIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       HandoverCancelIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverCancelIEsPresentNothing int = iota /* No components present */
	HandoverCancelIEsPresentAMFUENGAPID
	HandoverCancelIEsPresentRANUENGAPID
	HandoverCancelIEsPresentCause
)

type HandoverCancelIEsValue struct {
	Present     int
	AMFUENGAPID *AMFUENGAPID `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID *RANUENGAPID `vht5gc:"referenceFieldValue:85"`
	Cause       *Cause       `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
}

type HandoverCancelAcknowledge struct {
	ProtocolIEs ProtocolIEContainerHandoverCancelAcknowledgeIEs
}

type ProtocolIEContainerHandoverCancelAcknowledgeIEs struct {
	List []HandoverCancelAcknowledgeIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type HandoverCancelAcknowledgeIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       HandoverCancelAcknowledgeIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverCancelAcknowledgeIEsPresentNothing int = iota /* No components present */
	HandoverCancelAcknowledgeIEsPresentAMFUENGAPID
	HandoverCancelAcknowledgeIEsPresentRANUENGAPID
	HandoverCancelAcknowledgeIEsPresentCriticalityDiagnostics
)

type HandoverCancelAcknowledgeIEsValue struct {
	Present                int
	AMFUENGAPID            *AMFUENGAPID            `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID            *RANUENGAPID            `vht5gc:"referenceFieldValue:85"`
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
}

type HandoverSuccess struct {
	ProtocolIEs ProtocolIEContainerHandoverSuccessIEs
}

type ProtocolIEContainerHandoverSuccessIEs struct {
	List []HandoverSuccessIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type HandoverSuccessIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       HandoverSuccessIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverSuccessIEsPresentNothing int = iota /* No components present */
	HandoverSuccessIEsPresentAMFUENGAPID
	HandoverSuccessIEsPresentRANUENGAPID
)

type HandoverSuccessIEsValue struct {
	Present     int
	AMFUENGAPID *AMFUENGAPID `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID *RANUENGAPID `vht5gc:"referenceFieldValue:85"`
}

type UplinkRANEarlyStatusTransfer struct {
	ProtocolIEs ProtocolIEContainerUplinkRANEarlyStatusTransferIEs
}

type ProtocolIEContainerUplinkRANEarlyStatusTransferIEs struct {
	List []UplinkRANEarlyStatusTransferIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UplinkRANEarlyStatusTransferIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UplinkRANEarlyStatusTransferIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UplinkRANEarlyStatusTransferIEsPresentNothing int = iota /* No components present */
	UplinkRANEarlyStatusTransferIEsPresentAMFUENGAPID
	UplinkRANEarlyStatusTransferIEsPresentRANUENGAPID
	UplinkRANEarlyStatusTransferIEsPresentEarlyStatusTransferTransparentContainer
)

type UplinkRANEarlyStatusTransferIEsValue struct {
	Present                                 int
	AMFUENGAPID                             *AMFUENGAPID                             `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                             *RANUENGAPID                             `vht5gc:"referenceFieldValue:85"`
	EarlyStatusTransferTransparentContainer *EarlyStatusTransferTransparentContainer `vht5gc:"valueExt,referenceFieldValue:268"`
}

type DownlinkRANEarlyStatusTransfer struct {
	ProtocolIEs ProtocolIEContainerDownlinkRANEarlyStatusTransferIEs
}

type ProtocolIEContainerDownlinkRANEarlyStatusTransferIEs struct {
	List []DownlinkRANEarlyStatusTransferIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type DownlinkRANEarlyStatusTransferIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       DownlinkRANEarlyStatusTransferIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DownlinkRANEarlyStatusTransferIEsPresentNothing int = iota /* No components present */
	DownlinkRANEarlyStatusTransferIEsPresentAMFUENGAPID
	DownlinkRANEarlyStatusTransferIEsPresentRANUENGAPID
	DownlinkRANEarlyStatusTransferIEsPresentEarlyStatusTransferTransparentContainer
)

type DownlinkRANEarlyStatusTransferIEsValue struct {
	Present                                 int
	AMFUENGAPID                             *AMFUENGAPID                             `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                             *RANUENGAPID                             `vht5gc:"referenceFieldValue:85"`
	EarlyStatusTransferTransparentContainer *EarlyStatusTransferTransparentContainer `vht5gc:"valueExt,referenceFieldValue:268"`
}

type UplinkRANStatusTransfer struct {
	ProtocolIEs ProtocolIEContainerUplinkRANStatusTransferIEs
}

type ProtocolIEContainerUplinkRANStatusTransferIEs struct {
	List []UplinkRANStatusTransferIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UplinkRANStatusTransferIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UplinkRANStatusTransferIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UplinkRANStatusTransferIEsPresentNothing int = iota /* No components present */
	UplinkRANStatusTransferIEsPresentAMFUENGAPID
	UplinkRANStatusTransferIEsPresentRANUENGAPID
	UplinkRANStatusTransferIEsPresentRANStatusTransferTransparentContainer
)

type UplinkRANStatusTransferIEsValue struct {
	Present                               int
	AMFUENGAPID                           *AMFUENGAPID                           `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                           *RANUENGAPID                           `vht5gc:"referenceFieldValue:85"`
	RANStatusTransferTransparentContainer *RANStatusTransferTransparentContainer `vht5gc:"valueExt,referenceFieldValue:84"`
}

type DownlinkRANStatusTransfer struct {
	ProtocolIEs ProtocolIEContainerDownlinkRANStatusTransferIEs
}

type ProtocolIEContainerDownlinkRANStatusTransferIEs struct {
	List []DownlinkRANStatusTransferIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type DownlinkRANStatusTransferIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       DownlinkRANStatusTransferIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DownlinkRANStatusTransferIEsPresentNothing int = iota /* No components present */
	DownlinkRANStatusTransferIEsPresentAMFUENGAPID
	DownlinkRANStatusTransferIEsPresentRANUENGAPID
	DownlinkRANStatusTransferIEsPresentRANStatusTransferTransparentContainer
)

type DownlinkRANStatusTransferIEsValue struct {
	Present                               int
	AMFUENGAPID                           *AMFUENGAPID                           `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                           *RANUENGAPID                           `vht5gc:"referenceFieldValue:85"`
	RANStatusTransferTransparentContainer *RANStatusTransferTransparentContainer `vht5gc:"valueExt,referenceFieldValue:84"`
}

type Paging struct {
	ProtocolIEs ProtocolIEContainerPagingIEs
}

type ProtocolIEContainerPagingIEs struct {
	List []PagingIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PagingIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PagingIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PagingIEsPresentNothing int = iota /* No components present */
	PagingIEsPresentUEPagingIdentity
	PagingIEsPresentPagingDRX
	PagingIEsPresentTAIListForPaging
	PagingIEsPresentPagingPriority
	PagingIEsPresentUERadioCapabilityForPaging
	PagingIEsPresentPagingOrigin
	PagingIEsPresentAssistanceDataForPaging
	PagingIEsPresentNBIoTPagingEDRXInfo
	PagingIEsPresentNBIoTPagingDRX
	PagingIEsPresentEnhancedCoverageRestriction
	PagingIEsPresentWUSAssistanceInformation
	PagingIEsPresentPagingeDRXInformation
	PagingIEsPresentCEmodeBrestricted
)

type PagingIEsValue struct {
	Present                     int
	UEPagingIdentity            *UEPagingIdentity            `vht5gc:"referenceFieldValue:115,valueLB:0,valueUB:1"`
	PagingDRX                   *PagingDRX                   `vht5gc:"referenceFieldValue:50"`
	TAIListForPaging            *TAIListForPaging            `vht5gc:"referenceFieldValue:103"`
	PagingPriority              *PagingPriority              `vht5gc:"referenceFieldValue:52"`
	UERadioCapabilityForPaging  *UERadioCapabilityForPaging  `vht5gc:"valueExt,referenceFieldValue:118"`
	PagingOrigin                *PagingOrigin                `vht5gc:"referenceFieldValue:51"`
	AssistanceDataForPaging     *AssistanceDataForPaging     `vht5gc:"valueExt,referenceFieldValue:11"`
	NBIoTPagingEDRXInfo         *NBIoTPagingEDRXInfo         `vht5gc:"valueExt,referenceFieldValue:203"`
	NBIoTPagingDRX              *NBIoTPagingDRX              `vht5gc:"referenceFieldValue:202"`
	EnhancedCoverageRestriction *EnhancedCoverageRestriction `vht5gc:"referenceFieldValue:205"`
	WUSAssistanceInformation    *WUSAssistanceInformation    `vht5gc:"valueExt,referenceFieldValue:208"`
	PagingeDRXInformation       *PagingeDRXInformation       `vht5gc:"valueExt,referenceFieldValue:223"`
	CEmodeBrestricted           *CEmodeBrestricted           `vht5gc:"referenceFieldValue:222"`
}

type InitialUEMessage struct {
	ProtocolIEs ProtocolIEContainerInitialUEMessageIEs
}

type ProtocolIEContainerInitialUEMessageIEs struct {
	List []InitialUEMessageIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type InitialUEMessageIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       InitialUEMessageIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	InitialUEMessageIEsPresentNothing int = iota /* No components present */
	InitialUEMessageIEsPresentRANUENGAPID
	InitialUEMessageIEsPresentNASPDU
	InitialUEMessageIEsPresentUserLocationInformation
	InitialUEMessageIEsPresentRRCEstablishmentCause
	InitialUEMessageIEsPresentFiveGSTMSI
	InitialUEMessageIEsPresentAMFSetID
	InitialUEMessageIEsPresentUEContextRequest
	InitialUEMessageIEsPresentAllowedNSSAI
	InitialUEMessageIEsPresentSourceToTargetAMFInformationReroute
	InitialUEMessageIEsPresentSelectedPLMNIdentity
	InitialUEMessageIEsPresentIABNodeIndication
	InitialUEMessageIEsPresentCEmodeBSupportIndicator
	InitialUEMessageIEsPresentLTEMIndication
	InitialUEMessageIEsPresentEDTSession
	InitialUEMessageIEsPresentAuthenticatedIndication
	InitialUEMessageIEsPresentNPNAccessInformation
)

type InitialUEMessageIEsValue struct {
	Present                             int
	RANUENGAPID                         *RANUENGAPID                         `vht5gc:"referenceFieldValue:85"`
	NASPDU                              *NASPDU                              `vht5gc:"referenceFieldValue:38"`
	UserLocationInformation             *UserLocationInformation             `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
	RRCEstablishmentCause               *RRCEstablishmentCause               `vht5gc:"referenceFieldValue:90"`
	FiveGSTMSI                          *FiveGSTMSI                          `vht5gc:"valueExt,referenceFieldValue:26"`
	AMFSetID                            *AMFSetID                            `vht5gc:"referenceFieldValue:3"`
	UEContextRequest                    *UEContextRequest                    `vht5gc:"referenceFieldValue:112"`
	AllowedNSSAI                        *AllowedNSSAI                        `vht5gc:"referenceFieldValue:0"`
	SourceToTargetAMFInformationReroute *SourceToTargetAMFInformationReroute `vht5gc:"valueExt,referenceFieldValue:171"`
	SelectedPLMNIdentity                *PLMNIdentity                        `vht5gc:"referenceFieldValue:174"`
	IABNodeIndication                   *IABNodeIndication                   `vht5gc:"referenceFieldValue:201"`
	CEmodeBSupportIndicator             *CEmodeBSupportIndicator             `vht5gc:"referenceFieldValue:224"`
	LTEMIndication                      *LTEMIndication                      `vht5gc:"referenceFieldValue:225"`
	EDTSession                          *EDTSession                          `vht5gc:"referenceFieldValue:227"`
	AuthenticatedIndication             *AuthenticatedIndication             `vht5gc:"referenceFieldValue:245"`
	NPNAccessInformation                *NPNAccessInformation                `vht5gc:"referenceFieldValue:259,valueLB:0,valueUB:1"`
}

type DownlinkNASTransport struct {
	ProtocolIEs ProtocolIEContainerDownlinkNASTransportIEs
}

type ProtocolIEContainerDownlinkNASTransportIEs struct {
	List []DownlinkNASTransportIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type DownlinkNASTransportIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       DownlinkNASTransportIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DownlinkNASTransportIEsPresentNothing int = iota /* No components present */
	DownlinkNASTransportIEsPresentAMFUENGAPID
	DownlinkNASTransportIEsPresentRANUENGAPID
	DownlinkNASTransportIEsPresentOldAMF
	DownlinkNASTransportIEsPresentRANPagingPriority
	DownlinkNASTransportIEsPresentNASPDU
	DownlinkNASTransportIEsPresentMobilityRestrictionList
	DownlinkNASTransportIEsPresentIndexToRFSP
	DownlinkNASTransportIEsPresentUEAggregateMaximumBitRate
	DownlinkNASTransportIEsPresentAllowedNSSAI
	DownlinkNASTransportIEsPresentSRVCCOperationPossible
	DownlinkNASTransportIEsPresentEnhancedCoverageRestriction
	DownlinkNASTransportIEsPresentExtendedConnectedTime
	DownlinkNASTransportIEsPresentUEDifferentiationInfo
	DownlinkNASTransportIEsPresentCEmodeBrestricted
	DownlinkNASTransportIEsPresentUERadioCapability
	DownlinkNASTransportIEsPresentUECapabilityInfoRequest
	DownlinkNASTransportIEsPresentEndIndication
	DownlinkNASTransportIEsPresentUERadioCapabilityID
)

type DownlinkNASTransportIEsValue struct {
	Present                     int
	AMFUENGAPID                 *AMFUENGAPID                 `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                 *RANUENGAPID                 `vht5gc:"referenceFieldValue:85"`
	OldAMF                      *AMFName                     `vht5gc:"referenceFieldValue:48"`
	RANPagingPriority           *RANPagingPriority           `vht5gc:"referenceFieldValue:83"`
	NASPDU                      *NASPDU                      `vht5gc:"referenceFieldValue:38"`
	MobilityRestrictionList     *MobilityRestrictionList     `vht5gc:"valueExt,referenceFieldValue:36"`
	IndexToRFSP                 *IndexToRFSP                 `vht5gc:"referenceFieldValue:31"`
	UEAggregateMaximumBitRate   *UEAggregateMaximumBitRate   `vht5gc:"valueExt,referenceFieldValue:110"`
	AllowedNSSAI                *AllowedNSSAI                `vht5gc:"referenceFieldValue:0"`
	SRVCCOperationPossible      *SRVCCOperationPossible      `vht5gc:"referenceFieldValue:177"`
	EnhancedCoverageRestriction *EnhancedCoverageRestriction `vht5gc:"referenceFieldValue:205"`
	ExtendedConnectedTime       *ExtendedConnectedTime       `vht5gc:"referenceFieldValue:206"`
	UEDifferentiationInfo       *UEDifferentiationInfo       `vht5gc:"valueExt,referenceFieldValue:209"`
	CEmodeBrestricted           *CEmodeBrestricted           `vht5gc:"referenceFieldValue:222"`
	UERadioCapability           *UERadioCapability           `vht5gc:"referenceFieldValue:117"`
	UECapabilityInfoRequest     *UECapabilityInfoRequest     `vht5gc:"referenceFieldValue:228"`
	EndIndication               *EndIndication               `vht5gc:"referenceFieldValue:226"`
	UERadioCapabilityID         *UERadioCapabilityID         `vht5gc:"referenceFieldValue:264"`
}

type UplinkNASTransport struct {
	ProtocolIEs ProtocolIEContainerUplinkNASTransportIEs
}

type ProtocolIEContainerUplinkNASTransportIEs struct {
	List []UplinkNASTransportIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UplinkNASTransportIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UplinkNASTransportIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UplinkNASTransportIEsPresentNothing int = iota /* No components present */
	UplinkNASTransportIEsPresentAMFUENGAPID
	UplinkNASTransportIEsPresentRANUENGAPID
	UplinkNASTransportIEsPresentNASPDU
	UplinkNASTransportIEsPresentUserLocationInformation
	UplinkNASTransportIEsPresentWAGFIdentityInformation
	UplinkNASTransportIEsPresentTNGFIdentityInformation
	UplinkNASTransportIEsPresentTWIFIdentityInformation
)

type UplinkNASTransportIEsValue struct {
	Present                 int
	AMFUENGAPID             *AMFUENGAPID             `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID             *RANUENGAPID             `vht5gc:"referenceFieldValue:85"`
	NASPDU                  *NASPDU                  `vht5gc:"referenceFieldValue:38"`
	UserLocationInformation *UserLocationInformation `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
	WAGFIdentityInformation *OctetString             `vht5gc:"referenceFieldValue:239"`
	TNGFIdentityInformation *OctetString             `vht5gc:"referenceFieldValue:246"`
	TWIFIdentityInformation *OctetString             `vht5gc:"referenceFieldValue:247"`
}

type NASNonDeliveryIndication struct {
	ProtocolIEs ProtocolIEContainerNASNonDeliveryIndicationIEs
}

type ProtocolIEContainerNASNonDeliveryIndicationIEs struct {
	List []NASNonDeliveryIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type NASNonDeliveryIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       NASNonDeliveryIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NASNonDeliveryIndicationIEsPresentNothing int = iota /* No components present */
	NASNonDeliveryIndicationIEsPresentAMFUENGAPID
	NASNonDeliveryIndicationIEsPresentRANUENGAPID
	NASNonDeliveryIndicationIEsPresentNASPDU
	NASNonDeliveryIndicationIEsPresentCause
)

type NASNonDeliveryIndicationIEsValue struct {
	Present     int
	AMFUENGAPID *AMFUENGAPID `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID *RANUENGAPID `vht5gc:"referenceFieldValue:85"`
	NASPDU      *NASPDU      `vht5gc:"referenceFieldValue:38"`
	Cause       *Cause       `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
}

type RerouteNASRequest struct {
	ProtocolIEs ProtocolIEContainerRerouteNASRequestIEs
}

type ProtocolIEContainerRerouteNASRequestIEs struct {
	List []RerouteNASRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type RerouteNASRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       RerouteNASRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RerouteNASRequestIEsPresentNothing int = iota /* No components present */
	RerouteNASRequestIEsPresentRANUENGAPID
	RerouteNASRequestIEsPresentAMFUENGAPID
	RerouteNASRequestIEsPresentNGAPMessage
	RerouteNASRequestIEsPresentAMFSetID
	RerouteNASRequestIEsPresentAllowedNSSAI
	RerouteNASRequestIEsPresentSourceToTargetAMFInformationReroute
)

type RerouteNASRequestIEsValue struct {
	Present                             int
	RANUENGAPID                         *RANUENGAPID                         `vht5gc:"referenceFieldValue:85"`
	AMFUENGAPID                         *AMFUENGAPID                         `vht5gc:"referenceFieldValue:10"`
	NGAPMessage                         *OctetString                         `vht5gc:"referenceFieldValue:42"`
	AMFSetID                            *AMFSetID                            `vht5gc:"referenceFieldValue:3"`
	AllowedNSSAI                        *AllowedNSSAI                        `vht5gc:"referenceFieldValue:0"`
	SourceToTargetAMFInformationReroute *SourceToTargetAMFInformationReroute `vht5gc:"valueExt,referenceFieldValue:171"`
}

type NGSetupRequest struct {
	ProtocolIEs ProtocolIEContainerNGSetupRequestIEs
}

type ProtocolIEContainerNGSetupRequestIEs struct {
	List []NGSetupRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type NGSetupRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       NGSetupRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NGSetupRequestIEsPresentNothing int = iota /* No components present */
	NGSetupRequestIEsPresentGlobalRANNodeID
	NGSetupRequestIEsPresentRANNodeName
	NGSetupRequestIEsPresentSupportedTAList
	NGSetupRequestIEsPresentDefaultPagingDRX
	NGSetupRequestIEsPresentUERetentionInformation
	NGSetupRequestIEsPresentNBIoTDefaultPagingDRX
	NGSetupRequestIEsPresentExtendedRANNodeName
)

type NGSetupRequestIEsValue struct {
	Present                int
	GlobalRANNodeID        *GlobalRANNodeID        `vht5gc:"referenceFieldValue:27,valueLB:0,valueUB:3"`
	RANNodeName            *RANNodeName            `vht5gc:"referenceFieldValue:82"`
	SupportedTAList        *SupportedTAList        `vht5gc:"referenceFieldValue:102"`
	DefaultPagingDRX       *PagingDRX              `vht5gc:"referenceFieldValue:21"`
	UERetentionInformation *UERetentionInformation `vht5gc:"referenceFieldValue:147"`
	NBIoTDefaultPagingDRX  *NBIoTDefaultPagingDRX  `vht5gc:"referenceFieldValue:204"`
	ExtendedRANNodeName    *ExtendedRANNodeName    `vht5gc:"valueExt,referenceFieldValue:273"`
}

type NGSetupResponse struct {
	ProtocolIEs ProtocolIEContainerNGSetupResponseIEs
}

type ProtocolIEContainerNGSetupResponseIEs struct {
	List []NGSetupResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type NGSetupResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       NGSetupResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NGSetupResponseIEsPresentNothing int = iota /* No components present */
	NGSetupResponseIEsPresentAMFName
	NGSetupResponseIEsPresentServedGUAMIList
	NGSetupResponseIEsPresentRelativeAMFCapacity
	NGSetupResponseIEsPresentPLMNSupportList
	NGSetupResponseIEsPresentCriticalityDiagnostics
	NGSetupResponseIEsPresentUERetentionInformation
	NGSetupResponseIEsPresentIABSupported
	NGSetupResponseIEsPresentExtendedAMFName
)

type NGSetupResponseIEsValue struct {
	Present                int
	AMFName                *AMFName                `vht5gc:"referenceFieldValue:1"`
	ServedGUAMIList        *ServedGUAMIList        `vht5gc:"referenceFieldValue:96"`
	RelativeAMFCapacity    *RelativeAMFCapacity    `vht5gc:"referenceFieldValue:86"`
	PLMNSupportList        *PLMNSupportList        `vht5gc:"referenceFieldValue:80"`
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
	UERetentionInformation *UERetentionInformation `vht5gc:"referenceFieldValue:147"`
	IABSupported           *IABSupported           `vht5gc:"referenceFieldValue:200"`
	ExtendedAMFName        *ExtendedAMFName        `vht5gc:"valueExt,referenceFieldValue:274"`
}

type NGSetupFailure struct {
	ProtocolIEs ProtocolIEContainerNGSetupFailureIEs
}

type ProtocolIEContainerNGSetupFailureIEs struct {
	List []NGSetupFailureIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type NGSetupFailureIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       NGSetupFailureIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NGSetupFailureIEsPresentNothing int = iota /* No components present */
	NGSetupFailureIEsPresentCause
	NGSetupFailureIEsPresentTimeToWait
	NGSetupFailureIEsPresentCriticalityDiagnostics
)

type NGSetupFailureIEsValue struct {
	Present                int
	Cause                  *Cause                  `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	TimeToWait             *TimeToWait             `vht5gc:"referenceFieldValue:107"`
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
}

type RANConfigurationUpdate struct {
	ProtocolIEs ProtocolIEContainerRANConfigurationUpdateIEs
}

type ProtocolIEContainerRANConfigurationUpdateIEs struct {
	List []RANConfigurationUpdateIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type RANConfigurationUpdateIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       RANConfigurationUpdateIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RANConfigurationUpdateIEsPresentNothing int = iota /* No components present */
	RANConfigurationUpdateIEsPresentRANNodeName
	RANConfigurationUpdateIEsPresentSupportedTAList
	RANConfigurationUpdateIEsPresentDefaultPagingDRX
	RANConfigurationUpdateIEsPresentGlobalRANNodeID
	RANConfigurationUpdateIEsPresentNGRANTNLAssociationToRemoveList
	RANConfigurationUpdateIEsPresentNBIoTDefaultPagingDRX
	RANConfigurationUpdateIEsPresentExtendedRANNodeName
)

type RANConfigurationUpdateIEsValue struct {
	Present                         int
	RANNodeName                     *RANNodeName                     `vht5gc:"referenceFieldValue:82"`
	SupportedTAList                 *SupportedTAList                 `vht5gc:"referenceFieldValue:102"`
	DefaultPagingDRX                *PagingDRX                       `vht5gc:"referenceFieldValue:21"`
	GlobalRANNodeID                 *GlobalRANNodeID                 `vht5gc:"referenceFieldValue:27,valueLB:0,valueUB:3"`
	NGRANTNLAssociationToRemoveList *NGRANTNLAssociationToRemoveList `vht5gc:"referenceFieldValue:167"`
	NBIoTDefaultPagingDRX           *NBIoTDefaultPagingDRX           `vht5gc:"referenceFieldValue:204"`
	ExtendedRANNodeName             *ExtendedRANNodeName             `vht5gc:"valueExt,referenceFieldValue:273"`
}

type RANConfigurationUpdateAcknowledge struct {
	ProtocolIEs ProtocolIEContainerRANConfigurationUpdateAcknowledgeIEs
}

type ProtocolIEContainerRANConfigurationUpdateAcknowledgeIEs struct {
	List []RANConfigurationUpdateAcknowledgeIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type RANConfigurationUpdateAcknowledgeIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       RANConfigurationUpdateAcknowledgeIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RANConfigurationUpdateAcknowledgeIEsPresentNothing int = iota /* No components present */
	RANConfigurationUpdateAcknowledgeIEsPresentCriticalityDiagnostics
)

type RANConfigurationUpdateAcknowledgeIEsValue struct {
	Present                int
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
}

type RANConfigurationUpdateFailure struct {
	ProtocolIEs ProtocolIEContainerRANConfigurationUpdateFailureIEs
}

type ProtocolIEContainerRANConfigurationUpdateFailureIEs struct {
	List []RANConfigurationUpdateFailureIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type RANConfigurationUpdateFailureIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       RANConfigurationUpdateFailureIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RANConfigurationUpdateFailureIEsPresentNothing int = iota /* No components present */
	RANConfigurationUpdateFailureIEsPresentCause
	RANConfigurationUpdateFailureIEsPresentTimeToWait
	RANConfigurationUpdateFailureIEsPresentCriticalityDiagnostics
)

type RANConfigurationUpdateFailureIEsValue struct {
	Present                int
	Cause                  *Cause                  `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	TimeToWait             *TimeToWait             `vht5gc:"referenceFieldValue:107"`
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
}

type AMFConfigurationUpdate struct {
	ProtocolIEs ProtocolIEContainerAMFConfigurationUpdateIEs
}

type ProtocolIEContainerAMFConfigurationUpdateIEs struct {
	List []AMFConfigurationUpdateIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type AMFConfigurationUpdateIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       AMFConfigurationUpdateIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AMFConfigurationUpdateIEsPresentNothing int = iota /* No components present */
	AMFConfigurationUpdateIEsPresentAMFName
	AMFConfigurationUpdateIEsPresentServedGUAMIList
	AMFConfigurationUpdateIEsPresentRelativeAMFCapacity
	AMFConfigurationUpdateIEsPresentPLMNSupportList
	AMFConfigurationUpdateIEsPresentAMFTNLAssociationToAddList
	AMFConfigurationUpdateIEsPresentAMFTNLAssociationToRemoveList
	AMFConfigurationUpdateIEsPresentAMFTNLAssociationToUpdateList
	AMFConfigurationUpdateIEsPresentExtendedAMFName
)

type AMFConfigurationUpdateIEsValue struct {
	Present                       int
	AMFName                       *AMFName                       `vht5gc:"referenceFieldValue:1"`
	ServedGUAMIList               *ServedGUAMIList               `vht5gc:"referenceFieldValue:96"`
	RelativeAMFCapacity           *RelativeAMFCapacity           `vht5gc:"referenceFieldValue:86"`
	PLMNSupportList               *PLMNSupportList               `vht5gc:"referenceFieldValue:80"`
	AMFTNLAssociationToAddList    *AMFTNLAssociationToAddList    `vht5gc:"referenceFieldValue:6"`
	AMFTNLAssociationToRemoveList *AMFTNLAssociationToRemoveList `vht5gc:"referenceFieldValue:7"`
	AMFTNLAssociationToUpdateList *AMFTNLAssociationToUpdateList `vht5gc:"referenceFieldValue:8"`
	ExtendedAMFName               *ExtendedAMFName               `vht5gc:"valueExt,referenceFieldValue:274"`
}

type AMFConfigurationUpdateAcknowledge struct {
	ProtocolIEs ProtocolIEContainerAMFConfigurationUpdateAcknowledgeIEs
}

type ProtocolIEContainerAMFConfigurationUpdateAcknowledgeIEs struct {
	List []AMFConfigurationUpdateAcknowledgeIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type AMFConfigurationUpdateAcknowledgeIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       AMFConfigurationUpdateAcknowledgeIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AMFConfigurationUpdateAcknowledgeIEsPresentNothing int = iota /* No components present */
	AMFConfigurationUpdateAcknowledgeIEsPresentAMFTNLAssociationSetupList
	AMFConfigurationUpdateAcknowledgeIEsPresentAMFTNLAssociationFailedToSetupList
	AMFConfigurationUpdateAcknowledgeIEsPresentCriticalityDiagnostics
)

type AMFConfigurationUpdateAcknowledgeIEsValue struct {
	Present                            int
	AMFTNLAssociationSetupList         *AMFTNLAssociationSetupList `vht5gc:"referenceFieldValue:5"`
	AMFTNLAssociationFailedToSetupList *TNLAssociationList         `vht5gc:"referenceFieldValue:4"`
	CriticalityDiagnostics             *CriticalityDiagnostics     `vht5gc:"valueExt,referenceFieldValue:19"`
}

type AMFConfigurationUpdateFailure struct {
	ProtocolIEs ProtocolIEContainerAMFConfigurationUpdateFailureIEs
}

type ProtocolIEContainerAMFConfigurationUpdateFailureIEs struct {
	List []AMFConfigurationUpdateFailureIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type AMFConfigurationUpdateFailureIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       AMFConfigurationUpdateFailureIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AMFConfigurationUpdateFailureIEsPresentNothing int = iota /* No components present */
	AMFConfigurationUpdateFailureIEsPresentCause
	AMFConfigurationUpdateFailureIEsPresentTimeToWait
	AMFConfigurationUpdateFailureIEsPresentCriticalityDiagnostics
)

type AMFConfigurationUpdateFailureIEsValue struct {
	Present                int
	Cause                  *Cause                  `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	TimeToWait             *TimeToWait             `vht5gc:"referenceFieldValue:107"`
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
}

type AMFStatusIndication struct {
	ProtocolIEs ProtocolIEContainerAMFStatusIndicationIEs
}

type ProtocolIEContainerAMFStatusIndicationIEs struct {
	List []AMFStatusIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type AMFStatusIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       AMFStatusIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AMFStatusIndicationIEsPresentNothing int = iota /* No components present */
	AMFStatusIndicationIEsPresentUnavailableGUAMIList
)

type AMFStatusIndicationIEsValue struct {
	Present              int
	UnavailableGUAMIList *UnavailableGUAMIList `vht5gc:"referenceFieldValue:120"`
}

type NGReset struct {
	ProtocolIEs ProtocolIEContainerNGResetIEs
}

type ProtocolIEContainerNGResetIEs struct {
	List []NGResetIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type NGResetIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       NGResetIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NGResetIEsPresentNothing int = iota /* No components present */
	NGResetIEsPresentCause
	NGResetIEsPresentResetType
)

type NGResetIEsValue struct {
	Present   int
	Cause     *Cause     `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	ResetType *ResetType `vht5gc:"referenceFieldValue:88,valueLB:0,valueUB:2"`
}

type NGResetAcknowledge struct {
	ProtocolIEs ProtocolIEContainerNGResetAcknowledgeIEs
}

type ProtocolIEContainerNGResetAcknowledgeIEs struct {
	List []NGResetAcknowledgeIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type NGResetAcknowledgeIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       NGResetAcknowledgeIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NGResetAcknowledgeIEsPresentNothing int = iota /* No components present */
	NGResetAcknowledgeIEsPresentUEAssociatedLogicalNGConnectionList
	NGResetAcknowledgeIEsPresentCriticalityDiagnostics
)

type NGResetAcknowledgeIEsValue struct {
	Present                             int
	UEAssociatedLogicalNGConnectionList *UEAssociatedLogicalNGConnectionList `vht5gc:"referenceFieldValue:111"`
	CriticalityDiagnostics              *CriticalityDiagnostics              `vht5gc:"valueExt,referenceFieldValue:19"`
}

type ErrorIndication struct {
	ProtocolIEs ProtocolIEContainerErrorIndicationIEs
}

type ProtocolIEContainerErrorIndicationIEs struct {
	List []ErrorIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type ErrorIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       ErrorIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ErrorIndicationIEsPresentNothing int = iota /* No components present */
	ErrorIndicationIEsPresentAMFUENGAPID
	ErrorIndicationIEsPresentRANUENGAPID
	ErrorIndicationIEsPresentCause
	ErrorIndicationIEsPresentCriticalityDiagnostics
	ErrorIndicationIEsPresentFiveGSTMSI
)

type ErrorIndicationIEsValue struct {
	Present                int
	AMFUENGAPID            *AMFUENGAPID            `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID            *RANUENGAPID            `vht5gc:"referenceFieldValue:85"`
	Cause                  *Cause                  `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
	FiveGSTMSI             *FiveGSTMSI             `vht5gc:"valueExt,referenceFieldValue:26"`
}

type OverloadStart struct {
	ProtocolIEs ProtocolIEContainerOverloadStartIEs
}

type ProtocolIEContainerOverloadStartIEs struct {
	List []OverloadStartIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type OverloadStartIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       OverloadStartIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	OverloadStartIEsPresentNothing int = iota /* No components present */
	OverloadStartIEsPresentAMFOverloadResponse
	OverloadStartIEsPresentAMFTrafficLoadReductionIndication
	OverloadStartIEsPresentOverloadStartNSSAIList
)

type OverloadStartIEsValue struct {
	Present                           int
	AMFOverloadResponse               *OverloadResponse               `vht5gc:"referenceFieldValue:2,valueLB:0,valueUB:1"`
	AMFTrafficLoadReductionIndication *TrafficLoadReductionIndication `vht5gc:"referenceFieldValue:9"`
	OverloadStartNSSAIList            *OverloadStartNSSAIList         `vht5gc:"referenceFieldValue:49"`
}

type OverloadStop struct {
	ProtocolIEs ProtocolIEContainerOverloadStopIEs
}

type ProtocolIEContainerOverloadStopIEs struct {
	List []OverloadStopIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type OverloadStopIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       OverloadStopIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	OverloadStopIEsPresentNothing int = iota /* No components present */
)

type OverloadStopIEsValue struct {
	Present int
}

type UplinkRANConfigurationTransfer struct {
	ProtocolIEs ProtocolIEContainerUplinkRANConfigurationTransferIEs
}

type ProtocolIEContainerUplinkRANConfigurationTransferIEs struct {
	List []UplinkRANConfigurationTransferIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UplinkRANConfigurationTransferIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UplinkRANConfigurationTransferIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UplinkRANConfigurationTransferIEsPresentNothing int = iota /* No components present */
	UplinkRANConfigurationTransferIEsPresentSONConfigurationTransferUL
	UplinkRANConfigurationTransferIEsPresentENDCSONConfigurationTransferUL
	UplinkRANConfigurationTransferIEsPresentIntersystemSONConfigurationTransferUL
)

type UplinkRANConfigurationTransferIEsValue struct {
	Present                               int
	SONConfigurationTransferUL            *SONConfigurationTransfer            `vht5gc:"valueExt,referenceFieldValue:99"`
	ENDCSONConfigurationTransferUL        *ENDCSONConfigurationTransfer        `vht5gc:"referenceFieldValue:158"`
	IntersystemSONConfigurationTransferUL *IntersystemSONConfigurationTransfer `vht5gc:"valueExt,referenceFieldValue:251"`
}

type DownlinkRANConfigurationTransfer struct {
	ProtocolIEs ProtocolIEContainerDownlinkRANConfigurationTransferIEs
}

type ProtocolIEContainerDownlinkRANConfigurationTransferIEs struct {
	List []DownlinkRANConfigurationTransferIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type DownlinkRANConfigurationTransferIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       DownlinkRANConfigurationTransferIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DownlinkRANConfigurationTransferIEsPresentNothing int = iota /* No components present */
	DownlinkRANConfigurationTransferIEsPresentSONConfigurationTransferDL
	DownlinkRANConfigurationTransferIEsPresentENDCSONConfigurationTransferDL
	DownlinkRANConfigurationTransferIEsPresentIntersystemSONConfigurationTransferDL
)

type DownlinkRANConfigurationTransferIEsValue struct {
	Present                               int
	SONConfigurationTransferDL            *SONConfigurationTransfer            `vht5gc:"valueExt,referenceFieldValue:98"`
	ENDCSONConfigurationTransferDL        *ENDCSONConfigurationTransfer        `vht5gc:"referenceFieldValue:157"`
	IntersystemSONConfigurationTransferDL *IntersystemSONConfigurationTransfer `vht5gc:"valueExt,referenceFieldValue:250"`
}

type WriteReplaceWarningRequest struct {
	ProtocolIEs ProtocolIEContainerWriteReplaceWarningRequestIEs
}

type ProtocolIEContainerWriteReplaceWarningRequestIEs struct {
	List []WriteReplaceWarningRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type WriteReplaceWarningRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       WriteReplaceWarningRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	WriteReplaceWarningRequestIEsPresentNothing int = iota /* No components present */
	WriteReplaceWarningRequestIEsPresentMessageIdentifier
	WriteReplaceWarningRequestIEsPresentSerialNumber
	WriteReplaceWarningRequestIEsPresentWarningAreaList
	WriteReplaceWarningRequestIEsPresentRepetitionPeriod
	WriteReplaceWarningRequestIEsPresentNumberOfBroadcastsRequested
	WriteReplaceWarningRequestIEsPresentWarningType
	WriteReplaceWarningRequestIEsPresentWarningSecurityInfo
	WriteReplaceWarningRequestIEsPresentDataCodingScheme
	WriteReplaceWarningRequestIEsPresentWarningMessageContents
	WriteReplaceWarningRequestIEsPresentConcurrentWarningMessageInd
	WriteReplaceWarningRequestIEsPresentWarningAreaCoordinates
)

type WriteReplaceWarningRequestIEsValue struct {
	Present                     int
	MessageIdentifier           *MessageIdentifier           `vht5gc:"referenceFieldValue:35"`
	SerialNumber                *SerialNumber                `vht5gc:"referenceFieldValue:95"`
	WarningAreaList             *WarningAreaList             `vht5gc:"referenceFieldValue:122,valueLB:0,valueUB:4"`
	RepetitionPeriod            *RepetitionPeriod            `vht5gc:"referenceFieldValue:87"`
	NumberOfBroadcastsRequested *NumberOfBroadcastsRequested `vht5gc:"referenceFieldValue:47"`
	WarningType                 *WarningType                 `vht5gc:"referenceFieldValue:125"`
	WarningSecurityInfo         *WarningSecurityInfo         `vht5gc:"referenceFieldValue:124"`
	DataCodingScheme            *DataCodingScheme            `vht5gc:"referenceFieldValue:20"`
	WarningMessageContents      *WarningMessageContents      `vht5gc:"referenceFieldValue:123"`
	ConcurrentWarningMessageInd *ConcurrentWarningMessageInd `vht5gc:"referenceFieldValue:17"`
	WarningAreaCoordinates      *WarningAreaCoordinates      `vht5gc:"referenceFieldValue:141"`
}

type WriteReplaceWarningResponse struct {
	ProtocolIEs ProtocolIEContainerWriteReplaceWarningResponseIEs
}

type ProtocolIEContainerWriteReplaceWarningResponseIEs struct {
	List []WriteReplaceWarningResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type WriteReplaceWarningResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       WriteReplaceWarningResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	WriteReplaceWarningResponseIEsPresentNothing int = iota /* No components present */
	WriteReplaceWarningResponseIEsPresentMessageIdentifier
	WriteReplaceWarningResponseIEsPresentSerialNumber
	WriteReplaceWarningResponseIEsPresentBroadcastCompletedAreaList
	WriteReplaceWarningResponseIEsPresentCriticalityDiagnostics
)

type WriteReplaceWarningResponseIEsValue struct {
	Present                    int
	MessageIdentifier          *MessageIdentifier          `vht5gc:"referenceFieldValue:35"`
	SerialNumber               *SerialNumber               `vht5gc:"referenceFieldValue:95"`
	BroadcastCompletedAreaList *BroadcastCompletedAreaList `vht5gc:"referenceFieldValue:13,valueLB:0,valueUB:6"`
	CriticalityDiagnostics     *CriticalityDiagnostics     `vht5gc:"valueExt,referenceFieldValue:19"`
}

type PWSCancelRequest struct {
	ProtocolIEs ProtocolIEContainerPWSCancelRequestIEs
}

type ProtocolIEContainerPWSCancelRequestIEs struct {
	List []PWSCancelRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PWSCancelRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PWSCancelRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PWSCancelRequestIEsPresentNothing int = iota /* No components present */
	PWSCancelRequestIEsPresentMessageIdentifier
	PWSCancelRequestIEsPresentSerialNumber
	PWSCancelRequestIEsPresentWarningAreaList
	PWSCancelRequestIEsPresentCancelAllWarningMessages
)

type PWSCancelRequestIEsValue struct {
	Present                  int
	MessageIdentifier        *MessageIdentifier        `vht5gc:"referenceFieldValue:35"`
	SerialNumber             *SerialNumber             `vht5gc:"referenceFieldValue:95"`
	WarningAreaList          *WarningAreaList          `vht5gc:"referenceFieldValue:122,valueLB:0,valueUB:4"`
	CancelAllWarningMessages *CancelAllWarningMessages `vht5gc:"referenceFieldValue:14"`
}

type PWSCancelResponse struct {
	ProtocolIEs ProtocolIEContainerPWSCancelResponseIEs
}

type ProtocolIEContainerPWSCancelResponseIEs struct {
	List []PWSCancelResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PWSCancelResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PWSCancelResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PWSCancelResponseIEsPresentNothing int = iota /* No components present */
	PWSCancelResponseIEsPresentMessageIdentifier
	PWSCancelResponseIEsPresentSerialNumber
	PWSCancelResponseIEsPresentBroadcastCancelledAreaList
	PWSCancelResponseIEsPresentCriticalityDiagnostics
)

type PWSCancelResponseIEsValue struct {
	Present                    int
	MessageIdentifier          *MessageIdentifier          `vht5gc:"referenceFieldValue:35"`
	SerialNumber               *SerialNumber               `vht5gc:"referenceFieldValue:95"`
	BroadcastCancelledAreaList *BroadcastCancelledAreaList `vht5gc:"referenceFieldValue:12,valueLB:0,valueUB:6"`
	CriticalityDiagnostics     *CriticalityDiagnostics     `vht5gc:"valueExt,referenceFieldValue:19"`
}

type PWSRestartIndication struct {
	ProtocolIEs ProtocolIEContainerPWSRestartIndicationIEs
}

type ProtocolIEContainerPWSRestartIndicationIEs struct {
	List []PWSRestartIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PWSRestartIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PWSRestartIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PWSRestartIndicationIEsPresentNothing int = iota /* No components present */
	PWSRestartIndicationIEsPresentCellIDListForRestart
	PWSRestartIndicationIEsPresentGlobalRANNodeID
	PWSRestartIndicationIEsPresentTAIListForRestart
	PWSRestartIndicationIEsPresentEmergencyAreaIDListForRestart
)

type PWSRestartIndicationIEsValue struct {
	Present                       int
	CellIDListForRestart          *CellIDListForRestart          `vht5gc:"referenceFieldValue:16,valueLB:0,valueUB:2"`
	GlobalRANNodeID               *GlobalRANNodeID               `vht5gc:"referenceFieldValue:27,valueLB:0,valueUB:3"`
	TAIListForRestart             *TAIListForRestart             `vht5gc:"referenceFieldValue:104"`
	EmergencyAreaIDListForRestart *EmergencyAreaIDListForRestart `vht5gc:"referenceFieldValue:23"`
}

type PWSFailureIndication struct {
	ProtocolIEs ProtocolIEContainerPWSFailureIndicationIEs
}

type ProtocolIEContainerPWSFailureIndicationIEs struct {
	List []PWSFailureIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PWSFailureIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       PWSFailureIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PWSFailureIndicationIEsPresentNothing int = iota /* No components present */
	PWSFailureIndicationIEsPresentPWSFailedCellIDList
	PWSFailureIndicationIEsPresentGlobalRANNodeID
)

type PWSFailureIndicationIEsValue struct {
	Present             int
	PWSFailedCellIDList *PWSFailedCellIDList `vht5gc:"referenceFieldValue:81,valueLB:0,valueUB:2"`
	GlobalRANNodeID     *GlobalRANNodeID     `vht5gc:"referenceFieldValue:27,valueLB:0,valueUB:3"`
}

type DownlinkUEAssociatedNRPPaTransport struct {
	ProtocolIEs ProtocolIEContainerDownlinkUEAssociatedNRPPaTransportIEs
}

type ProtocolIEContainerDownlinkUEAssociatedNRPPaTransportIEs struct {
	List []DownlinkUEAssociatedNRPPaTransportIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type DownlinkUEAssociatedNRPPaTransportIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       DownlinkUEAssociatedNRPPaTransportIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DownlinkUEAssociatedNRPPaTransportIEsPresentNothing int = iota /* No components present */
	DownlinkUEAssociatedNRPPaTransportIEsPresentAMFUENGAPID
	DownlinkUEAssociatedNRPPaTransportIEsPresentRANUENGAPID
	DownlinkUEAssociatedNRPPaTransportIEsPresentRoutingID
	DownlinkUEAssociatedNRPPaTransportIEsPresentNRPPaPDU
)

type DownlinkUEAssociatedNRPPaTransportIEsValue struct {
	Present     int
	AMFUENGAPID *AMFUENGAPID `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID *RANUENGAPID `vht5gc:"referenceFieldValue:85"`
	RoutingID   *RoutingID   `vht5gc:"referenceFieldValue:89"`
	NRPPaPDU    *NRPPaPDU    `vht5gc:"referenceFieldValue:46"`
}

type UplinkUEAssociatedNRPPaTransport struct {
	ProtocolIEs ProtocolIEContainerUplinkUEAssociatedNRPPaTransportIEs
}

type ProtocolIEContainerUplinkUEAssociatedNRPPaTransportIEs struct {
	List []UplinkUEAssociatedNRPPaTransportIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UplinkUEAssociatedNRPPaTransportIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UplinkUEAssociatedNRPPaTransportIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UplinkUEAssociatedNRPPaTransportIEsPresentNothing int = iota /* No components present */
	UplinkUEAssociatedNRPPaTransportIEsPresentAMFUENGAPID
	UplinkUEAssociatedNRPPaTransportIEsPresentRANUENGAPID
	UplinkUEAssociatedNRPPaTransportIEsPresentRoutingID
	UplinkUEAssociatedNRPPaTransportIEsPresentNRPPaPDU
)

type UplinkUEAssociatedNRPPaTransportIEsValue struct {
	Present     int
	AMFUENGAPID *AMFUENGAPID `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID *RANUENGAPID `vht5gc:"referenceFieldValue:85"`
	RoutingID   *RoutingID   `vht5gc:"referenceFieldValue:89"`
	NRPPaPDU    *NRPPaPDU    `vht5gc:"referenceFieldValue:46"`
}

type DownlinkNonUEAssociatedNRPPaTransport struct {
	ProtocolIEs ProtocolIEContainerDownlinkNonUEAssociatedNRPPaTransportIEs
}

type ProtocolIEContainerDownlinkNonUEAssociatedNRPPaTransportIEs struct {
	List []DownlinkNonUEAssociatedNRPPaTransportIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type DownlinkNonUEAssociatedNRPPaTransportIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       DownlinkNonUEAssociatedNRPPaTransportIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DownlinkNonUEAssociatedNRPPaTransportIEsPresentNothing int = iota /* No components present */
	DownlinkNonUEAssociatedNRPPaTransportIEsPresentRoutingID
	DownlinkNonUEAssociatedNRPPaTransportIEsPresentNRPPaPDU
)

type DownlinkNonUEAssociatedNRPPaTransportIEsValue struct {
	Present   int
	RoutingID *RoutingID `vht5gc:"referenceFieldValue:89"`
	NRPPaPDU  *NRPPaPDU  `vht5gc:"referenceFieldValue:46"`
}

type UplinkNonUEAssociatedNRPPaTransport struct {
	ProtocolIEs ProtocolIEContainerUplinkNonUEAssociatedNRPPaTransportIEs
}

type ProtocolIEContainerUplinkNonUEAssociatedNRPPaTransportIEs struct {
	List []UplinkNonUEAssociatedNRPPaTransportIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UplinkNonUEAssociatedNRPPaTransportIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UplinkNonUEAssociatedNRPPaTransportIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UplinkNonUEAssociatedNRPPaTransportIEsPresentNothing int = iota /* No components present */
	UplinkNonUEAssociatedNRPPaTransportIEsPresentRoutingID
	UplinkNonUEAssociatedNRPPaTransportIEsPresentNRPPaPDU
)

type UplinkNonUEAssociatedNRPPaTransportIEsValue struct {
	Present   int
	RoutingID *RoutingID `vht5gc:"referenceFieldValue:89"`
	NRPPaPDU  *NRPPaPDU  `vht5gc:"referenceFieldValue:46"`
}

type TraceStart struct {
	ProtocolIEs ProtocolIEContainerTraceStartIEs
}

type ProtocolIEContainerTraceStartIEs struct {
	List []TraceStartIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type TraceStartIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       TraceStartIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TraceStartIEsPresentNothing int = iota /* No components present */
	TraceStartIEsPresentAMFUENGAPID
	TraceStartIEsPresentRANUENGAPID
	TraceStartIEsPresentTraceActivation
)

type TraceStartIEsValue struct {
	Present         int
	AMFUENGAPID     *AMFUENGAPID     `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID     *RANUENGAPID     `vht5gc:"referenceFieldValue:85"`
	TraceActivation *TraceActivation `vht5gc:"valueExt,referenceFieldValue:108"`
}

type TraceFailureIndication struct {
	ProtocolIEs ProtocolIEContainerTraceFailureIndicationIEs
}

type ProtocolIEContainerTraceFailureIndicationIEs struct {
	List []TraceFailureIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type TraceFailureIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       TraceFailureIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TraceFailureIndicationIEsPresentNothing int = iota /* No components present */
	TraceFailureIndicationIEsPresentAMFUENGAPID
	TraceFailureIndicationIEsPresentRANUENGAPID
	TraceFailureIndicationIEsPresentNGRANTraceID
	TraceFailureIndicationIEsPresentCause
)

type TraceFailureIndicationIEsValue struct {
	Present      int
	AMFUENGAPID  *AMFUENGAPID  `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID  *RANUENGAPID  `vht5gc:"referenceFieldValue:85"`
	NGRANTraceID *NGRANTraceID `vht5gc:"referenceFieldValue:44"`
	Cause        *Cause        `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
}

type DeactivateTrace struct {
	ProtocolIEs ProtocolIEContainerDeactivateTraceIEs
}

type ProtocolIEContainerDeactivateTraceIEs struct {
	List []DeactivateTraceIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type DeactivateTraceIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       DeactivateTraceIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DeactivateTraceIEsPresentNothing int = iota /* No components present */
	DeactivateTraceIEsPresentAMFUENGAPID
	DeactivateTraceIEsPresentRANUENGAPID
	DeactivateTraceIEsPresentNGRANTraceID
)

type DeactivateTraceIEsValue struct {
	Present      int
	AMFUENGAPID  *AMFUENGAPID  `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID  *RANUENGAPID  `vht5gc:"referenceFieldValue:85"`
	NGRANTraceID *NGRANTraceID `vht5gc:"referenceFieldValue:44"`
}

type CellTrafficTrace struct {
	ProtocolIEs ProtocolIEContainerCellTrafficTraceIEs
}

type ProtocolIEContainerCellTrafficTraceIEs struct {
	List []CellTrafficTraceIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type CellTrafficTraceIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       CellTrafficTraceIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CellTrafficTraceIEsPresentNothing int = iota /* No components present */
	CellTrafficTraceIEsPresentAMFUENGAPID
	CellTrafficTraceIEsPresentRANUENGAPID
	CellTrafficTraceIEsPresentNGRANTraceID
	CellTrafficTraceIEsPresentNGRANCGI
	CellTrafficTraceIEsPresentTraceCollectionEntityIPAddress
	CellTrafficTraceIEsPresentPrivacyIndicator
	CellTrafficTraceIEsPresentTraceCollectionEntityURI
)

type CellTrafficTraceIEsValue struct {
	Present                        int
	AMFUENGAPID                    *AMFUENGAPID           `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                    *RANUENGAPID           `vht5gc:"referenceFieldValue:85"`
	NGRANTraceID                   *NGRANTraceID          `vht5gc:"referenceFieldValue:44"`
	NGRANCGI                       *NGRANCGI              `vht5gc:"referenceFieldValue:43,valueLB:0,valueUB:2"`
	TraceCollectionEntityIPAddress *TransportLayerAddress `vht5gc:"referenceFieldValue:109"`
	PrivacyIndicator               *PrivacyIndicator      `vht5gc:"referenceFieldValue:256"`
	TraceCollectionEntityURI       *URIAddress            `vht5gc:"referenceFieldValue:257"`
}

type LocationReportingControl struct {
	ProtocolIEs ProtocolIEContainerLocationReportingControlIEs
}

type ProtocolIEContainerLocationReportingControlIEs struct {
	List []LocationReportingControlIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type LocationReportingControlIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       LocationReportingControlIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LocationReportingControlIEsPresentNothing int = iota /* No components present */
	LocationReportingControlIEsPresentAMFUENGAPID
	LocationReportingControlIEsPresentRANUENGAPID
	LocationReportingControlIEsPresentLocationReportingRequestType
)

type LocationReportingControlIEsValue struct {
	Present                      int
	AMFUENGAPID                  *AMFUENGAPID                  `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                  *RANUENGAPID                  `vht5gc:"referenceFieldValue:85"`
	LocationReportingRequestType *LocationReportingRequestType `vht5gc:"valueExt,referenceFieldValue:33"`
}

type LocationReportingFailureIndication struct {
	ProtocolIEs ProtocolIEContainerLocationReportingFailureIndicationIEs
}

type ProtocolIEContainerLocationReportingFailureIndicationIEs struct {
	List []LocationReportingFailureIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type LocationReportingFailureIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       LocationReportingFailureIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LocationReportingFailureIndicationIEsPresentNothing int = iota /* No components present */
	LocationReportingFailureIndicationIEsPresentAMFUENGAPID
	LocationReportingFailureIndicationIEsPresentRANUENGAPID
	LocationReportingFailureIndicationIEsPresentCause
)

type LocationReportingFailureIndicationIEsValue struct {
	Present     int
	AMFUENGAPID *AMFUENGAPID `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID *RANUENGAPID `vht5gc:"referenceFieldValue:85"`
	Cause       *Cause       `vht5gc:"referenceFieldValue:15,valueLB:0,valueUB:5"`
}

type LocationReport struct {
	ProtocolIEs ProtocolIEContainerLocationReportIEs
}

type ProtocolIEContainerLocationReportIEs struct {
	List []LocationReportIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type LocationReportIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       LocationReportIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LocationReportIEsPresentNothing int = iota /* No components present */
	LocationReportIEsPresentAMFUENGAPID
	LocationReportIEsPresentRANUENGAPID
	LocationReportIEsPresentUserLocationInformation
	LocationReportIEsPresentUEPresenceInAreaOfInterestList
	LocationReportIEsPresentLocationReportingRequestType
)

type LocationReportIEsValue struct {
	Present                        int
	AMFUENGAPID                    *AMFUENGAPID                    `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                    *RANUENGAPID                    `vht5gc:"referenceFieldValue:85"`
	UserLocationInformation        *UserLocationInformation        `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
	UEPresenceInAreaOfInterestList *UEPresenceInAreaOfInterestList `vht5gc:"referenceFieldValue:116"`
	LocationReportingRequestType   *LocationReportingRequestType   `vht5gc:"valueExt,referenceFieldValue:33"`
}

type UETNLABindingReleaseRequest struct {
	ProtocolIEs ProtocolIEContainerUETNLABindingReleaseRequestIEs
}

type ProtocolIEContainerUETNLABindingReleaseRequestIEs struct {
	List []UETNLABindingReleaseRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UETNLABindingReleaseRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UETNLABindingReleaseRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UETNLABindingReleaseRequestIEsPresentNothing int = iota /* No components present */
	UETNLABindingReleaseRequestIEsPresentAMFUENGAPID
	UETNLABindingReleaseRequestIEsPresentRANUENGAPID
)

type UETNLABindingReleaseRequestIEsValue struct {
	Present     int
	AMFUENGAPID *AMFUENGAPID `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID *RANUENGAPID `vht5gc:"referenceFieldValue:85"`
}

type UERadioCapabilityInfoIndication struct {
	ProtocolIEs ProtocolIEContainerUERadioCapabilityInfoIndicationIEs
}

type ProtocolIEContainerUERadioCapabilityInfoIndicationIEs struct {
	List []UERadioCapabilityInfoIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UERadioCapabilityInfoIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UERadioCapabilityInfoIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UERadioCapabilityInfoIndicationIEsPresentNothing int = iota /* No components present */
	UERadioCapabilityInfoIndicationIEsPresentAMFUENGAPID
	UERadioCapabilityInfoIndicationIEsPresentRANUENGAPID
	UERadioCapabilityInfoIndicationIEsPresentUERadioCapability
	UERadioCapabilityInfoIndicationIEsPresentUERadioCapabilityForPaging
	UERadioCapabilityInfoIndicationIEsPresentUERadioCapabilityEUTRAFormat
)

type UERadioCapabilityInfoIndicationIEsValue struct {
	Present                      int
	AMFUENGAPID                  *AMFUENGAPID                `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                  *RANUENGAPID                `vht5gc:"referenceFieldValue:85"`
	UERadioCapability            *UERadioCapability          `vht5gc:"referenceFieldValue:117"`
	UERadioCapabilityForPaging   *UERadioCapabilityForPaging `vht5gc:"valueExt,referenceFieldValue:118"`
	UERadioCapabilityEUTRAFormat *UERadioCapability          `vht5gc:"referenceFieldValue:265"`
}

type UERadioCapabilityCheckRequest struct {
	ProtocolIEs ProtocolIEContainerUERadioCapabilityCheckRequestIEs
}

type ProtocolIEContainerUERadioCapabilityCheckRequestIEs struct {
	List []UERadioCapabilityCheckRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UERadioCapabilityCheckRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UERadioCapabilityCheckRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UERadioCapabilityCheckRequestIEsPresentNothing int = iota /* No components present */
	UERadioCapabilityCheckRequestIEsPresentAMFUENGAPID
	UERadioCapabilityCheckRequestIEsPresentRANUENGAPID
	UERadioCapabilityCheckRequestIEsPresentUERadioCapability
	UERadioCapabilityCheckRequestIEsPresentUERadioCapabilityID
)

type UERadioCapabilityCheckRequestIEsValue struct {
	Present             int
	AMFUENGAPID         *AMFUENGAPID         `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID         *RANUENGAPID         `vht5gc:"referenceFieldValue:85"`
	UERadioCapability   *UERadioCapability   `vht5gc:"referenceFieldValue:117"`
	UERadioCapabilityID *UERadioCapabilityID `vht5gc:"referenceFieldValue:264"`
}

type UERadioCapabilityCheckResponse struct {
	ProtocolIEs ProtocolIEContainerUERadioCapabilityCheckResponseIEs
}

type ProtocolIEContainerUERadioCapabilityCheckResponseIEs struct {
	List []UERadioCapabilityCheckResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UERadioCapabilityCheckResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UERadioCapabilityCheckResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UERadioCapabilityCheckResponseIEsPresentNothing int = iota /* No components present */
	UERadioCapabilityCheckResponseIEsPresentAMFUENGAPID
	UERadioCapabilityCheckResponseIEsPresentRANUENGAPID
	UERadioCapabilityCheckResponseIEsPresentIMSVoiceSupportIndicator
	UERadioCapabilityCheckResponseIEsPresentCriticalityDiagnostics
)

type UERadioCapabilityCheckResponseIEsValue struct {
	Present                  int
	AMFUENGAPID              *AMFUENGAPID              `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID              *RANUENGAPID              `vht5gc:"referenceFieldValue:85"`
	IMSVoiceSupportIndicator *IMSVoiceSupportIndicator `vht5gc:"referenceFieldValue:30"`
	CriticalityDiagnostics   *CriticalityDiagnostics   `vht5gc:"valueExt,referenceFieldValue:19"`
}

type PrivateMessage struct {
	PrivateIEs PrivateIEContainerPrivateMessageIEs
}

type PrivateIEContainerPrivateMessageIEs struct {
	List []PrivateMessageIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type PrivateMessageIEs struct {
	Id          PrivateIEID
	Criticality Criticality
	Value       PrivateMessageIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PrivateMessageIEsPresentNothing int = iota /* No components present */
)

type PrivateMessageIEsValue struct {
	Present int
}

type SecondaryRATDataUsageReport struct {
	ProtocolIEs ProtocolIEContainerSecondaryRATDataUsageReportIEs
}

type ProtocolIEContainerSecondaryRATDataUsageReportIEs struct {
	List []SecondaryRATDataUsageReportIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type SecondaryRATDataUsageReportIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       SecondaryRATDataUsageReportIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SecondaryRATDataUsageReportIEsPresentNothing int = iota /* No components present */
	SecondaryRATDataUsageReportIEsPresentAMFUENGAPID
	SecondaryRATDataUsageReportIEsPresentRANUENGAPID
	SecondaryRATDataUsageReportIEsPresentPDUSessionResourceSecondaryRATUsageList
	SecondaryRATDataUsageReportIEsPresentHandoverFlag
	SecondaryRATDataUsageReportIEsPresentUserLocationInformation
)

type SecondaryRATDataUsageReportIEsValue struct {
	Present                                 int
	AMFUENGAPID                             *AMFUENGAPID                             `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID                             *RANUENGAPID                             `vht5gc:"referenceFieldValue:85"`
	PDUSessionResourceSecondaryRATUsageList *PDUSessionResourceSecondaryRATUsageList `vht5gc:"referenceFieldValue:142"`
	HandoverFlag                            *HandoverFlag                            `vht5gc:"referenceFieldValue:143"`
	UserLocationInformation                 *UserLocationInformation                 `vht5gc:"referenceFieldValue:121,valueLB:0,valueUB:3"`
}

type UplinkRIMInformationTransfer struct {
	ProtocolIEs ProtocolIEContainerUplinkRIMInformationTransferIEs
}

type ProtocolIEContainerUplinkRIMInformationTransferIEs struct {
	List []UplinkRIMInformationTransferIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UplinkRIMInformationTransferIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UplinkRIMInformationTransferIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UplinkRIMInformationTransferIEsPresentNothing int = iota /* No components present */
	UplinkRIMInformationTransferIEsPresentRIMInformationTransfer
)

type UplinkRIMInformationTransferIEsValue struct {
	Present                int
	RIMInformationTransfer *RIMInformationTransfer `vht5gc:"valueExt,referenceFieldValue:175"`
}

type DownlinkRIMInformationTransfer struct {
	ProtocolIEs ProtocolIEContainerDownlinkRIMInformationTransferIEs
}

type ProtocolIEContainerDownlinkRIMInformationTransferIEs struct {
	List []DownlinkRIMInformationTransferIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type DownlinkRIMInformationTransferIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       DownlinkRIMInformationTransferIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DownlinkRIMInformationTransferIEsPresentNothing int = iota /* No components present */
	DownlinkRIMInformationTransferIEsPresentRIMInformationTransfer
)

type DownlinkRIMInformationTransferIEsValue struct {
	Present                int
	RIMInformationTransfer *RIMInformationTransfer `vht5gc:"valueExt,referenceFieldValue:175"`
}

type ConnectionEstablishmentIndication struct {
	ProtocolIEs ProtocolIEContainerConnectionEstablishmentIndicationIEs
}

type ProtocolIEContainerConnectionEstablishmentIndicationIEs struct {
	List []ConnectionEstablishmentIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type ConnectionEstablishmentIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       ConnectionEstablishmentIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ConnectionEstablishmentIndicationIEsPresentNothing int = iota /* No components present */
	ConnectionEstablishmentIndicationIEsPresentAMFUENGAPID
	ConnectionEstablishmentIndicationIEsPresentRANUENGAPID
	ConnectionEstablishmentIndicationIEsPresentUERadioCapability
	ConnectionEstablishmentIndicationIEsPresentEndIndication
	ConnectionEstablishmentIndicationIEsPresentSNSSAI
	ConnectionEstablishmentIndicationIEsPresentAllowedNSSAI
	ConnectionEstablishmentIndicationIEsPresentUEDifferentiationInfo
	ConnectionEstablishmentIndicationIEsPresentDLCPSecurityInformation
	ConnectionEstablishmentIndicationIEsPresentNBIoTUEPriority
)

type ConnectionEstablishmentIndicationIEsValue struct {
	Present                 int
	AMFUENGAPID             *AMFUENGAPID             `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID             *RANUENGAPID             `vht5gc:"referenceFieldValue:85"`
	UERadioCapability       *UERadioCapability       `vht5gc:"referenceFieldValue:117"`
	EndIndication           *EndIndication           `vht5gc:"referenceFieldValue:226"`
	SNSSAI                  *SNSSAI                  `vht5gc:"valueExt,referenceFieldValue:148"`
	AllowedNSSAI            *AllowedNSSAI            `vht5gc:"referenceFieldValue:0"`
	UEDifferentiationInfo   *UEDifferentiationInfo   `vht5gc:"valueExt,referenceFieldValue:209"`
	DLCPSecurityInformation *DLCPSecurityInformation `vht5gc:"valueExt,referenceFieldValue:212"`
	NBIoTUEPriority         *NBIoTUEPriority         `vht5gc:"referenceFieldValue:210"`
}

type UERadioCapabilityIDMappingRequest struct {
	ProtocolIEs ProtocolIEContainerUERadioCapabilityIDMappingRequestIEs
}

type ProtocolIEContainerUERadioCapabilityIDMappingRequestIEs struct {
	List []UERadioCapabilityIDMappingRequestIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UERadioCapabilityIDMappingRequestIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UERadioCapabilityIDMappingRequestIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UERadioCapabilityIDMappingRequestIEsPresentNothing int = iota /* No components present */
	UERadioCapabilityIDMappingRequestIEsPresentUERadioCapabilityID
)

type UERadioCapabilityIDMappingRequestIEsValue struct {
	Present             int
	UERadioCapabilityID *UERadioCapabilityID `vht5gc:"referenceFieldValue:264"`
}

type UERadioCapabilityIDMappingResponse struct {
	ProtocolIEs ProtocolIEContainerUERadioCapabilityIDMappingResponseIEs
}

type ProtocolIEContainerUERadioCapabilityIDMappingResponseIEs struct {
	List []UERadioCapabilityIDMappingResponseIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type UERadioCapabilityIDMappingResponseIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       UERadioCapabilityIDMappingResponseIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UERadioCapabilityIDMappingResponseIEsPresentNothing int = iota /* No components present */
	UERadioCapabilityIDMappingResponseIEsPresentUERadioCapabilityID
	UERadioCapabilityIDMappingResponseIEsPresentUERadioCapability
	UERadioCapabilityIDMappingResponseIEsPresentCriticalityDiagnostics
)

type UERadioCapabilityIDMappingResponseIEsValue struct {
	Present                int
	UERadioCapabilityID    *UERadioCapabilityID    `vht5gc:"referenceFieldValue:264"`
	UERadioCapability      *UERadioCapability      `vht5gc:"referenceFieldValue:117"`
	CriticalityDiagnostics *CriticalityDiagnostics `vht5gc:"valueExt,referenceFieldValue:19"`
}

type AMFCPRelocationIndication struct {
	ProtocolIEs ProtocolIEContainerAMFCPRelocationIndicationIEs
}

type ProtocolIEContainerAMFCPRelocationIndicationIEs struct {
	List []AMFCPRelocationIndicationIEs `vht5gc:"sizeLB:0,sizeUB:65535"`
}

type AMFCPRelocationIndicationIEs struct {
	Id          ProtocolIEID
	Criticality Criticality
	Value       AMFCPRelocationIndicationIEsValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AMFCPRelocationIndicationIEsPresentNothing int = iota /* No components present */
	AMFCPRelocationIndicationIEsPresentAMFUENGAPID
	AMFCPRelocationIndicationIEsPresentRANUENGAPID
	AMFCPRelocationIndicationIEsPresentSNSSAI
	AMFCPRelocationIndicationIEsPresentAllowedNSSAI
)

type AMFCPRelocationIndicationIEsValue struct {
	Present      int
	AMFUENGAPID  *AMFUENGAPID  `vht5gc:"referenceFieldValue:10"`
	RANUENGAPID  *RANUENGAPID  `vht5gc:"referenceFieldValue:85"`
	SNSSAI       *SNSSAI       `vht5gc:"valueExt,referenceFieldValue:148"`
	AllowedNSSAI *AllowedNSSAI `vht5gc:"referenceFieldValue:0"`
}

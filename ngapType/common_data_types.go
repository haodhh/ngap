package ngapType

const (
	CriticalityPresentReject Enumerated = 0
	CriticalityPresentIgnore Enumerated = 1
	CriticalityPresentNotify Enumerated = 2
)

type Criticality struct {
	Value Enumerated `vht5gc:"valueLB:0,valueUB:2"`
}

const (
	PresencePresentOptional    Enumerated = 0
	PresencePresentConditional Enumerated = 1
	PresencePresentMandatory   Enumerated = 2
)

type Presence struct {
	Value Enumerated `vht5gc:"valueLB:0,valueUB:2"`
}

const (
	PrivateIEIDPresentNothing int = iota /* No components present */
	PrivateIEIDPresentLocal
	PrivateIEIDPresentGlobal
)

type PrivateIEID struct {
	Present int
	Local   *int64 `vht5gc:"valueLB:0,valueUB:65535"`
	Global  *ObjectIdentifier
}

type ProcedureCode struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:255"`
}

type ProtocolExtensionID struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:65535"`
}

type ProtocolIEID struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:65535"`
}

const (
	TriggeringMessagePresentInitiatingMessage    Enumerated = 0
	TriggeringMessagePresentSuccessfulOutcome    Enumerated = 1
	TriggeringMessagePresentUnsuccessfullOutcome Enumerated = 2
)

type TriggeringMessage struct {
	Value Enumerated `vht5gc:"valueLB:0,valueUB:2"`
}

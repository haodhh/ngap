package ngapType

const (
	AdditionalQosFlowInformationPresentMoreLikely Enumerated = 0
)

type AdditionalQosFlowInformation struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	PNINPNRestrictedPresentRestricted Enumerated = 0
	PNINPNRestrictedPresentNotRestricted Enumerated = 1
)

type PNINPNRestricted struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:1"`
}

type AlternativeQoSParaSetIndex struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:8"`
}

type AlternativeQoSParaSetNotifyIndex struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:8"`
}

type AMFName struct {
	Value string `vht5gc:"sizeExt,sizeLB:1,sizeUB:150"`
}

type AMFNameVisibleString struct {
	Value string
}

type AMFNameUTF8String struct {
	Value string `vht5gc:"sizeExt,sizeLB:1,sizeUB:150"`
}

type AMFPointer struct {
	Value BitString `vht5gc:"sizeLB:6,sizeUB:6"`
}

type AMFRegionID struct {
	Value BitString `vht5gc:"sizeLB:8,sizeUB:8"`
}

type AMFSetID struct {
	Value BitString `vht5gc:"sizeLB:10,sizeUB:10"`
}

type AMFUENGAPID struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:1099511627775"`
}

const (
	QosFlowMappingIndicationPresentUl Enumerated = 0
	QosFlowMappingIndicationPresentDl Enumerated = 1
)

type QosFlowMappingIndication struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:1"`
}

const (
	AuthenticatedIndicationPresentTrue Enumerated = 0
)

type AuthenticatedIndication struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type AveragingWindow struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:4095"`
}

type BitRate struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:4000000000000"`
}

const (
	BtRssiPresentTrue Enumerated = 0
)

type BtRssi struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:0"`
}

const (
	BluetoothMeasConfigPresentSetup Enumerated = 0
)

type BluetoothMeasConfig struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type BluetoothName struct {
	Value OctetString `vht5gc:"sizeLB:1,sizeUB:248"`
}

type BurstArrivalTime struct {
	Value OctetString
}

type CAGID struct {
	Value BitString `vht5gc:"sizeLB:32,sizeUB:32"`
}

const (
	CancelAllWarningMessagesPresentTrue Enumerated = 0
)

type CancelAllWarningMessages struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	CauseMiscPresentControlProcessingOverload Enumerated = 0
	CauseMiscPresentNotEnoughUserPlaneProcessingResources Enumerated = 1
	CauseMiscPresentHardwareFailure Enumerated = 2
	CauseMiscPresentOmIntervention Enumerated = 3
	CauseMiscPresentUnknownPLMN Enumerated = 4
	CauseMiscPresentUnspecified Enumerated = 5
)

type CauseMisc struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:5"`
}

const (
	CauseNasPresentNormalRelease Enumerated = 0
	CauseNasPresentAuthenticationFailure Enumerated = 1
	CauseNasPresentDeregister Enumerated = 2
	CauseNasPresentUnspecified Enumerated = 3
)

type CauseNas struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:3"`
}

const (
	CauseProtocolPresentTransferSyntaxError Enumerated = 0
	CauseProtocolPresentAbstractSyntaxErrorReject Enumerated = 1
	CauseProtocolPresentAbstractSyntaxErrorIgnoreAndNotify Enumerated = 2
	CauseProtocolPresentMessageNotCompatibleWithReceiverState Enumerated = 3
	CauseProtocolPresentSemanticError Enumerated = 4
	CauseProtocolPresentAbstractSyntaxErrorFalselyConstructedMessage Enumerated = 5
	CauseProtocolPresentUnspecified Enumerated = 6
)

type CauseProtocol struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:6"`
}

const (
	CauseRadioNetworkPresentUnspecified Enumerated = 0
	CauseRadioNetworkPresentTxnrelocoverallExpiry Enumerated = 1
	CauseRadioNetworkPresentSuccessfulHandover Enumerated = 2
	CauseRadioNetworkPresentReleaseDueToNgranGeneratedReason Enumerated = 3
	CauseRadioNetworkPresentReleaseDueTo5gcGeneratedReason Enumerated = 4
	CauseRadioNetworkPresentHandoverCancelled Enumerated = 5
	CauseRadioNetworkPresentPartialHandover Enumerated = 6
	CauseRadioNetworkPresentHoFailureInTarget5GCNgranNodeOrTargetSystem Enumerated = 7
	CauseRadioNetworkPresentHoTargetNotAllowed Enumerated = 8
	CauseRadioNetworkPresentTngrelocoverallExpiry Enumerated = 9
	CauseRadioNetworkPresentTngrelocprepExpiry Enumerated = 10
	CauseRadioNetworkPresentCellNotAvailable Enumerated = 11
	CauseRadioNetworkPresentUnknownTargetID Enumerated = 12
	CauseRadioNetworkPresentNoRadioResourcesAvailableInTargetCell Enumerated = 13
	CauseRadioNetworkPresentUnknownLocalUENGAPID Enumerated = 14
	CauseRadioNetworkPresentInconsistentRemoteUENGAPID Enumerated = 15
	CauseRadioNetworkPresentHandoverDesirableForRadioReason Enumerated = 16
	CauseRadioNetworkPresentTimeCriticalHandover Enumerated = 17
	CauseRadioNetworkPresentResourceOptimisationHandover Enumerated = 18
	CauseRadioNetworkPresentReduceLoadInServingCell Enumerated = 19
	CauseRadioNetworkPresentUserInactivity Enumerated = 20
	CauseRadioNetworkPresentRadioConnectionWithUeLost Enumerated = 21
	CauseRadioNetworkPresentRadioResourcesNotAvailable Enumerated = 22
	CauseRadioNetworkPresentInvalidQosCombination Enumerated = 23
	CauseRadioNetworkPresentFailureInRadioInterfaceProcedure Enumerated = 24
	CauseRadioNetworkPresentInteractionWithOtherProcedure Enumerated = 25
	CauseRadioNetworkPresentUnknownPDUSessionID Enumerated = 26
	CauseRadioNetworkPresentUnkownQosFlowID Enumerated = 27
	CauseRadioNetworkPresentMultiplePDUSessionIDInstances Enumerated = 28
	CauseRadioNetworkPresentMultipleQosFlowIDInstances Enumerated = 29
	CauseRadioNetworkPresentEncryptionAndOrIntegrityProtectionAlgorithmsNotSupported Enumerated = 30
	CauseRadioNetworkPresentNgIntraSystemHandoverTriggered Enumerated = 31
	CauseRadioNetworkPresentNgInterSystemHandoverTriggered Enumerated = 32
	CauseRadioNetworkPresentXnHandoverTriggered Enumerated = 33
	CauseRadioNetworkPresentNotSupported5QIValue Enumerated = 34
	CauseRadioNetworkPresentUeContextTransfer Enumerated = 35
	CauseRadioNetworkPresentImsVoiceEpsFallbackOrRatFallbackTriggered Enumerated = 36
	CauseRadioNetworkPresentUpIntegrityProtectionNotPossible Enumerated = 37
	CauseRadioNetworkPresentUpConfidentialityProtectionNotPossible Enumerated = 38
	CauseRadioNetworkPresentSliceNotSupported Enumerated = 39
	CauseRadioNetworkPresentUeInRrcInactiveStateNotReachable Enumerated = 40
	CauseRadioNetworkPresentRedirection Enumerated = 41
	CauseRadioNetworkPresentResourcesNotAvailableForTheSlice Enumerated = 42
	CauseRadioNetworkPresentUeMaxIntegrityProtectedDataRateReason Enumerated = 43
	CauseRadioNetworkPresentReleaseDueToCnDetectedMobility Enumerated = 44
	CauseRadioNetworkPresentN26InterfaceNotAvailable Enumerated = 45
	CauseRadioNetworkPresentReleaseDueToPreEmption Enumerated = 46
	CauseRadioNetworkPresentMultipleLocationReportingReferenceIDInstances Enumerated = 47
	CauseRadioNetworkPresentRsnNotAvailableForTheUp Enumerated = 48
	CauseRadioNetworkPresentNpnAccessDenied Enumerated = 49
	CauseRadioNetworkPresentCagOnlyAccessDenied Enumerated = 50
)

type CauseRadioNetwork struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:50"`
}

const (
	CauseTransportPresentTransportResourceUnavailable Enumerated = 0
	CauseTransportPresentUnspecified Enumerated = 1
)

type CauseTransport struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	CellSizePresentVerysmall Enumerated = 0
	CellSizePresentSmall Enumerated = 1
	CellSizePresentMedium Enumerated = 2
	CellSizePresentLarge Enumerated = 3
)

type CellSize struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:3"`
}

const (
	CEmodeBSupportIndicatorPresentSupported Enumerated = 0
)

type CEmodeBSupportIndicator struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	CEmodeBrestrictedPresentRestricted Enumerated = 0
	CEmodeBrestrictedPresentNotRestricted Enumerated = 1
)

type CEmodeBrestricted struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	CnTypePresentEpcForbidden Enumerated = 0
	CnTypePresentFiveGCForbidden Enumerated = 1
)

type CnType struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:1"`
}

const (
	CNTypeRestrictionsForServingPresentEpcForbidden Enumerated = 0
)

type CNTypeRestrictionsForServing struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type CommonNetworkInstance struct {
	Value OctetString
}

const (
	ConcurrentWarningMessageIndPresentTrue Enumerated = 0
)

type ConcurrentWarningMessageInd struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	ConfidentialityProtectionIndicationPresentRequired Enumerated = 0
	ConfidentialityProtectionIndicationPresentPreferred Enumerated = 1
	ConfidentialityProtectionIndicationPresentNotNeeded Enumerated = 2
)

type ConfidentialityProtectionIndication struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:2"`
}

const (
	ConfidentialityProtectionResultPresentPerformed Enumerated = 0
	ConfidentialityProtectionResultPresentNotPerformed Enumerated = 1
)

type ConfidentialityProtectionResult struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	ConfiguredTACIndicationPresentTrue Enumerated = 0
)

type ConfiguredTACIndication struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type CoverageEnhancementLevel struct {
	Value OctetString
}

type DataCodingScheme struct {
	Value BitString `vht5gc:"sizeLB:8,sizeUB:8"`
}

const (
	DataForwardingAcceptedPresentDataForwardingAccepted Enumerated = 0
)

type DataForwardingAccepted struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	DataForwardingNotPossiblePresentDataForwardingNotPossible Enumerated = 0
)

type DataForwardingNotPossible struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	DAPSIndicatorPresentDapsHoRequired Enumerated = 0
)

type DAPSIndicator struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:0"`
}

const (
	DapsresponseindicatorPresentDapsHoAccepted Enumerated = 0
	DapsresponseindicatorPresentDapsHoNotAccepted Enumerated = 1
)

type Dapsresponseindicator struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:1"`
}

const (
	DelayCriticalPresentDelayCritical Enumerated = 0
	DelayCriticalPresentNonDelayCritical Enumerated = 1
)

type DelayCritical struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

type DLNASMAC struct {
	Value BitString `vht5gc:"sizeLB:16,sizeUB:16"`
}

const (
	DLForwardingPresentDlForwardingProposed Enumerated = 0
)

type DLForwarding struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	DLNGUTNLInformationReusedPresentTrue Enumerated = 0
)

type DLNGUTNLInformationReused struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	DirectForwardingPathAvailabilityPresentDirectPathAvailable Enumerated = 0
)

type DirectForwardingPathAvailability struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type DRBID struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:32"`
}

const (
	EDTSessionPresentTrue Enumerated = 0
)

type EDTSession struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type EmergencyAreaID struct {
	Value OctetString `vht5gc:"sizeLB:3,sizeUB:3"`
}

const (
	EmergencyFallbackRequestIndicatorPresentEmergencyFallbackRequested Enumerated = 0
)

type EmergencyFallbackRequestIndicator struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	EmergencyServiceTargetCNPresentFiveGC Enumerated = 0
	EmergencyServiceTargetCNPresentEpc Enumerated = 1
)

type EmergencyServiceTargetCN struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	EnhancedCoverageRestrictionPresentRestricted Enumerated = 0
)

type EnhancedCoverageRestriction struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type ExtendedConnectedTime struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:255"`
}

type ENDCSONConfigurationTransfer struct {
	Value OctetString
}

const (
	EndIndicationPresentNoFurtherData Enumerated = 0
	EndIndicationPresentFurtherDataExists Enumerated = 1
)

type EndIndication struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

type EPSTAC struct {
	Value OctetString `vht5gc:"sizeLB:2,sizeUB:2"`
}

type ERABID struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:15"`
}

type EUTRACellIdentity struct {
	Value BitString `vht5gc:"sizeLB:28,sizeUB:28"`
}

type EUTRAencryptionAlgorithms struct {
	Value BitString `vht5gc:"sizeExt,sizeLB:16,sizeUB:16"`
}

type EUTRAintegrityProtectionAlgorithms struct {
	Value BitString `vht5gc:"sizeExt,sizeLB:16,sizeUB:16"`
}

const (
	EventTypePresentDirect Enumerated = 0
	EventTypePresentChangeOfServeCell Enumerated = 1
	EventTypePresentUePresenceInAreaOfInterest Enumerated = 2
	EventTypePresentStopChangeOfServeCell Enumerated = 3
	EventTypePresentStopUePresenceInAreaOfInterest Enumerated = 4
	EventTypePresentCancelLocationReportingForTheUe Enumerated = 5
)

type EventType struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:5"`
}

type ExpectedActivityPeriod struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:181"`
}

const (
	ExpectedHOIntervalPresentSec15 Enumerated = 0
	ExpectedHOIntervalPresentSec30 Enumerated = 1
	ExpectedHOIntervalPresentSec60 Enumerated = 2
	ExpectedHOIntervalPresentSec90 Enumerated = 3
	ExpectedHOIntervalPresentSec120 Enumerated = 4
	ExpectedHOIntervalPresentSec180 Enumerated = 5
	ExpectedHOIntervalPresentLongTime Enumerated = 6
)

type ExpectedHOInterval struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:6"`
}

type ExpectedIdlePeriod struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:181"`
}

const (
	ExpectedUEMobilityPresentStationary Enumerated = 0
	ExpectedUEMobilityPresentMobile Enumerated = 1
)

type ExpectedUEMobility struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

type ExtendedPacketDelayBudget struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:65535"`
}

type ExtendedRNCID struct {
	Value int64 `vht5gc:"valueLB:4096,valueUB:65535"`
}

const (
	OutOfCoveragePresentTrue Enumerated = 0
)

type OutOfCoverage struct {
	Value Enumerated `vht5gc:"valueLB:0,valueUB:0"`
}

type FiveGTMSI struct {
	Value OctetString `vht5gc:"sizeLB:4,sizeUB:4"`
}

type FiveQI struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:255"`
}

type GlobalLineIdentity struct {
	Value OctetString
}

type GTPTEID struct {
	Value OctetString `vht5gc:"sizeLB:4,sizeUB:4"`
}

const (
	GUAMITypePresentNative Enumerated = 0
	GUAMITypePresentMapped Enumerated = 1
)

type GUAMIType struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	HandoverFlagPresentHandoverPreparation Enumerated = 0
)

type HandoverFlag struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	HandoverTypePresentIntra5gs Enumerated = 0
	HandoverTypePresentFivegsToEps Enumerated = 1
	HandoverTypePresentEpsTo5gs Enumerated = 2
	HandoverTypePresentFivegsToUtran Enumerated = 3
)

type HandoverType struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:3"`
}

type HFCNodeID struct {
	Value OctetString
}

const (
	HandoverReportTypePresentHoTooEarly Enumerated = 0
	HandoverReportTypePresentHoToWrongCell Enumerated = 1
	HandoverReportTypePresentIntersystemPingPong Enumerated = 2
)

type HandoverReportType struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:2"`
}

type Hysteresis struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:30"`
}

const (
	IABAuthorizedPresentAuthorized Enumerated = 0
	IABAuthorizedPresentNotAuthorized Enumerated = 1
)

type IABAuthorized struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	IABSupportedPresentTrue Enumerated = 0
)

type IABSupported struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	IABNodeIndicationPresentTrue Enumerated = 0
)

type IABNodeIndication struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	IMSVoiceSupportIndicatorPresentSupported Enumerated = 0
	IMSVoiceSupportIndicatorPresentNotSupported Enumerated = 1
)

type IMSVoiceSupportIndicator struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

type IndexToRFSP struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:256"`
}

const (
	IntegrityProtectionIndicationPresentRequired Enumerated = 0
	IntegrityProtectionIndicationPresentPreferred Enumerated = 1
	IntegrityProtectionIndicationPresentNotNeeded Enumerated = 2
)

type IntegrityProtectionIndication struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:2"`
}

const (
	IntegrityProtectionResultPresentPerformed Enumerated = 0
	IntegrityProtectionResultPresentNotPerformed Enumerated = 1
)

type IntegrityProtectionResult struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

type IntendedNumberOfPagingAttempts struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:16"`
}

type InterfacesToTrace struct {
	Value BitString `vht5gc:"sizeLB:8,sizeUB:8"`
}

const (
	EarlyIRATHOPresentTrue Enumerated = 0
	EarlyIRATHOPresentFalse Enumerated = 1
)

type EarlyIRATHO struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:1"`
}

type LAC struct {
	Value OctetString `vht5gc:"sizeLB:2,sizeUB:2"`
}

type LastVisitedEUTRANCellInformation struct {
	Value OctetString
}

type LastVisitedGERANCellInformation struct {
	Value OctetString
}

type LastVisitedUTRANCellInformation struct {
	Value OctetString
}

const (
	LineTypePresentDsl Enumerated = 0
	LineTypePresentPon Enumerated = 1
)

type LineType struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	LocationReportingAdditionalInfoPresentIncludePSCell Enumerated = 0
)

type LocationReportingAdditionalInfo struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type LocationReportingReferenceID struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:64"`
}

const (
	LoggingIntervalPresentMs320 Enumerated = 0
	LoggingIntervalPresentMs640 Enumerated = 1
	LoggingIntervalPresentMs1280 Enumerated = 2
	LoggingIntervalPresentMs2560 Enumerated = 3
	LoggingIntervalPresentMs5120 Enumerated = 4
	LoggingIntervalPresentMs10240 Enumerated = 5
	LoggingIntervalPresentMs20480 Enumerated = 6
	LoggingIntervalPresentMs30720 Enumerated = 7
	LoggingIntervalPresentMs40960 Enumerated = 8
	LoggingIntervalPresentMs61440 Enumerated = 9
	LoggingIntervalPresentInfinity Enumerated = 10
)

type LoggingInterval struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:10"`
}

const (
	LoggingDurationPresentM10 Enumerated = 0
	LoggingDurationPresentM20 Enumerated = 1
	LoggingDurationPresentM40 Enumerated = 2
	LoggingDurationPresentM60 Enumerated = 3
	LoggingDurationPresentM90 Enumerated = 4
	LoggingDurationPresentM120 Enumerated = 5
)

type LoggingDuration struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:5"`
}

const (
	LinksToLogPresentUplink Enumerated = 0
	LinksToLogPresentDownlink Enumerated = 1
	LinksToLogPresentBothUplinkAndDownlink Enumerated = 2
)

type LinksToLog struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:2"`
}

const (
	LTEMIndicationPresentLteM Enumerated = 0
)

type LTEMIndication struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type LTEUERLFReportContainer struct {
	Value OctetString
}

type MaskedIMEISV struct {
	Value BitString `vht5gc:"sizeLB:64,sizeUB:64"`
}

type MaximumDataBurstVolume struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:2000000"`
}

type MessageIdentifier struct {
	Value BitString `vht5gc:"sizeLB:16,sizeUB:16"`
}

const (
	MaximumIntegrityProtectedDataRatePresentBitrate64kbs Enumerated = 0
	MaximumIntegrityProtectedDataRatePresentMaximumUERate Enumerated = 1
)

type MaximumIntegrityProtectedDataRate struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	MICOModeIndicationPresentTrue Enumerated = 0
)

type MICOModeIndication struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type MobilityInformation struct {
	Value BitString `vht5gc:"sizeLB:16,sizeUB:16"`
}

const (
	MDTActivationPresentImmediateMDTOnly Enumerated = 0
	MDTActivationPresentLoggedMDTOnly Enumerated = 1
	MDTActivationPresentImmediateMDTAndTrace Enumerated = 2
)

type MDTActivation struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:2"`
}

type MDTModeEutra struct {
	Value OctetString
}

type MeasurementsToActivate struct {
	Value BitString `vht5gc:"sizeLB:8,sizeUB:8"`
}

const (
	M1ReportingTriggerPresentPeriodic Enumerated = 0
	M1ReportingTriggerPresentA2eventtriggered Enumerated = 1
	M1ReportingTriggerPresentA2eventtriggeredPeriodic Enumerated = 2
)

type M1ReportingTrigger struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:2"`
}

const (
	M4periodPresentMs1024 Enumerated = 0
	M4periodPresentMs2048 Enumerated = 1
	M4periodPresentMs5120 Enumerated = 2
	M4periodPresentMs10240 Enumerated = 3
	M4periodPresentMin1 Enumerated = 4
)

type M4period struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:4"`
}

const (
	M5periodPresentMs1024 Enumerated = 0
	M5periodPresentMs2048 Enumerated = 1
	M5periodPresentMs5120 Enumerated = 2
	M5periodPresentMs10240 Enumerated = 3
	M5periodPresentMin1 Enumerated = 4
)

type M5period struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:4"`
}

const (
	M6reportIntervalPresentMs120 Enumerated = 0
	M6reportIntervalPresentMs240 Enumerated = 1
	M6reportIntervalPresentMs480 Enumerated = 2
	M6reportIntervalPresentMs640 Enumerated = 3
	M6reportIntervalPresentMs1024 Enumerated = 4
	M6reportIntervalPresentMs2048 Enumerated = 5
	M6reportIntervalPresentMs5120 Enumerated = 6
	M6reportIntervalPresentMs10240 Enumerated = 7
	M6reportIntervalPresentMs20480 Enumerated = 8
	M6reportIntervalPresentMs40960 Enumerated = 9
	M6reportIntervalPresentMin1 Enumerated = 10
	M6reportIntervalPresentMin6 Enumerated = 11
	M6reportIntervalPresentMin12 Enumerated = 12
	M6reportIntervalPresentMin30 Enumerated = 13
)

type M6reportInterval struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:13"`
}

type M7period struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:60"`
}

type MDTLocationInformation struct {
	Value BitString `vht5gc:"sizeLB:8,sizeUB:8"`
}

type NASPDU struct {
	Value OctetString
}

type NASSecurityParametersFromNGRAN struct {
	Value OctetString
}

const (
	NBIoTDefaultPagingDRXPresentRf128 Enumerated = 0
	NBIoTDefaultPagingDRXPresentRf256 Enumerated = 1
	NBIoTDefaultPagingDRXPresentRf512 Enumerated = 2
	NBIoTDefaultPagingDRXPresentRf1024 Enumerated = 3
)

type NBIoTDefaultPagingDRX struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:3"`
}

const (
	NBIoTPagingDRXPresentRf32 Enumerated = 0
	NBIoTPagingDRXPresentRf64 Enumerated = 1
	NBIoTPagingDRXPresentRf128 Enumerated = 2
	NBIoTPagingDRXPresentRf256 Enumerated = 3
	NBIoTPagingDRXPresentRf512 Enumerated = 4
	NBIoTPagingDRXPresentRf1024 Enumerated = 5
)

type NBIoTPagingDRX struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:5"`
}

const (
	NBIoTPagingEDRXCyclePresentHf2 Enumerated = 0
	NBIoTPagingEDRXCyclePresentHf4 Enumerated = 1
	NBIoTPagingEDRXCyclePresentHf6 Enumerated = 2
	NBIoTPagingEDRXCyclePresentHf8 Enumerated = 3
	NBIoTPagingEDRXCyclePresentHf10 Enumerated = 4
	NBIoTPagingEDRXCyclePresentHf12 Enumerated = 5
	NBIoTPagingEDRXCyclePresentHf14 Enumerated = 6
	NBIoTPagingEDRXCyclePresentHf16 Enumerated = 7
	NBIoTPagingEDRXCyclePresentHf32 Enumerated = 8
	NBIoTPagingEDRXCyclePresentHf64 Enumerated = 9
	NBIoTPagingEDRXCyclePresentHf128 Enumerated = 10
	NBIoTPagingEDRXCyclePresentHf256 Enumerated = 11
	NBIoTPagingEDRXCyclePresentHf512 Enumerated = 12
	NBIoTPagingEDRXCyclePresentHf1024 Enumerated = 13
)

type NBIoTPagingEDRXCycle struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:13"`
}

const (
	NBIoTPagingTimeWindowPresentS1 Enumerated = 0
	NBIoTPagingTimeWindowPresentS2 Enumerated = 1
	NBIoTPagingTimeWindowPresentS3 Enumerated = 2
	NBIoTPagingTimeWindowPresentS4 Enumerated = 3
	NBIoTPagingTimeWindowPresentS5 Enumerated = 4
	NBIoTPagingTimeWindowPresentS6 Enumerated = 5
	NBIoTPagingTimeWindowPresentS7 Enumerated = 6
	NBIoTPagingTimeWindowPresentS8 Enumerated = 7
	NBIoTPagingTimeWindowPresentS9 Enumerated = 8
	NBIoTPagingTimeWindowPresentS10 Enumerated = 9
	NBIoTPagingTimeWindowPresentS11 Enumerated = 10
	NBIoTPagingTimeWindowPresentS12 Enumerated = 11
	NBIoTPagingTimeWindowPresentS13 Enumerated = 12
	NBIoTPagingTimeWindowPresentS14 Enumerated = 13
	NBIoTPagingTimeWindowPresentS15 Enumerated = 14
	NBIoTPagingTimeWindowPresentS16 Enumerated = 15
)

type NBIoTPagingTimeWindow struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:15"`
}

type NBIoTUEPriority struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:255"`
}

type NetworkInstance struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:256"`
}

const (
	NewSecurityContextIndPresentTrue Enumerated = 0
)

type NewSecurityContextInd struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type NextHopChainingCount struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:7"`
}

const (
	NextPagingAreaScopePresentSame Enumerated = 0
	NextPagingAreaScopePresentChanged Enumerated = 1
)

type NextPagingAreaScope struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	NotifySourceNGRANNodePresentNotifySource Enumerated = 0
)

type NotifySourceNGRANNode struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type NGRANTraceID struct {
	Value OctetString `vht5gc:"sizeLB:8,sizeUB:8"`
}

type NID struct {
	Value BitString `vht5gc:"sizeLB:44,sizeUB:44"`
}

const (
	NotificationCausePresentFulfilled Enumerated = 0
	NotificationCausePresentNotFulfilled Enumerated = 1
)

type NotificationCause struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	NotificationControlPresentNotificationRequested Enumerated = 0
)

type NotificationControl struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type NRCellIdentity struct {
	Value BitString `vht5gc:"sizeLB:36,sizeUB:36"`
}

type NRencryptionAlgorithms struct {
	Value BitString `vht5gc:"sizeExt,sizeLB:16,sizeUB:16"`
}

type NRintegrityProtectionAlgorithms struct {
	Value BitString `vht5gc:"sizeExt,sizeLB:16,sizeUB:16"`
}

type NRMobilityHistoryReport struct {
	Value OctetString
}

type NRPPaPDU struct {
	Value OctetString
}

type NRUERLFReportContainer struct {
	Value OctetString
}

type NumberOfBroadcasts struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:65535"`
}

type NumberOfBroadcastsRequested struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:65535"`
}

type NRARFCN struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:"`
}

type NRFrequencyBand struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:1024"`
}

type NRPCI struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:1007"`
}

const (
	VehicleUEPresentAuthorized Enumerated = 0
	VehicleUEPresentNotAuthorized Enumerated = 1
)

type VehicleUE struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	PedestrianUEPresentAuthorized Enumerated = 0
	PedestrianUEPresentNotAuthorized Enumerated = 1
)

type PedestrianUE struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	OverloadActionPresentRejectNonEmergencyMoDt Enumerated = 0
	OverloadActionPresentRejectRrcCrSignalling Enumerated = 1
	OverloadActionPresentPermitEmergencySessionsAndMobileTerminatedServicesOnly Enumerated = 2
	OverloadActionPresentPermitHighPrioritySessionsAndMobileTerminatedServicesOnly Enumerated = 3
)

type OverloadAction struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:3"`
}

type PacketDelayBudget struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:1023"`
}

type PacketLossRate struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:1000"`
}

type PagingAttemptCount struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:16"`
}

const (
	PagingDRXPresentV32 Enumerated = 0
	PagingDRXPresentV64 Enumerated = 1
	PagingDRXPresentV128 Enumerated = 2
	PagingDRXPresentV256 Enumerated = 3
)

type PagingDRX struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:3"`
}

const (
	PagingOriginPresentNon3gpp Enumerated = 0
)

type PagingOrigin struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	PagingPriorityPresentPriolevel1 Enumerated = 0
	PagingPriorityPresentPriolevel2 Enumerated = 1
	PagingPriorityPresentPriolevel3 Enumerated = 2
	PagingPriorityPresentPriolevel4 Enumerated = 3
	PagingPriorityPresentPriolevel5 Enumerated = 4
	PagingPriorityPresentPriolevel6 Enumerated = 5
	PagingPriorityPresentPriolevel7 Enumerated = 6
	PagingPriorityPresentPriolevel8 Enumerated = 7
)

type PagingPriority struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:7"`
}

const (
	PagingEDRXCyclePresentHfhalf Enumerated = 0
	PagingEDRXCyclePresentHf1 Enumerated = 1
	PagingEDRXCyclePresentHf2 Enumerated = 2
	PagingEDRXCyclePresentHf4 Enumerated = 3
	PagingEDRXCyclePresentHf6 Enumerated = 4
	PagingEDRXCyclePresentHf8 Enumerated = 5
	PagingEDRXCyclePresentHf10 Enumerated = 6
	PagingEDRXCyclePresentHf12 Enumerated = 7
	PagingEDRXCyclePresentHf14 Enumerated = 8
	PagingEDRXCyclePresentHf16 Enumerated = 9
	PagingEDRXCyclePresentHf32 Enumerated = 10
	PagingEDRXCyclePresentHf64 Enumerated = 11
	PagingEDRXCyclePresentHf128 Enumerated = 12
	PagingEDRXCyclePresentHf256 Enumerated = 13
)

type PagingEDRXCycle struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:13"`
}

const (
	PagingTimeWindowPresentS1 Enumerated = 0
	PagingTimeWindowPresentS2 Enumerated = 1
	PagingTimeWindowPresentS3 Enumerated = 2
	PagingTimeWindowPresentS4 Enumerated = 3
	PagingTimeWindowPresentS5 Enumerated = 4
	PagingTimeWindowPresentS6 Enumerated = 5
	PagingTimeWindowPresentS7 Enumerated = 6
	PagingTimeWindowPresentS8 Enumerated = 7
	PagingTimeWindowPresentS9 Enumerated = 8
	PagingTimeWindowPresentS10 Enumerated = 9
	PagingTimeWindowPresentS11 Enumerated = 10
	PagingTimeWindowPresentS12 Enumerated = 11
	PagingTimeWindowPresentS13 Enumerated = 12
	PagingTimeWindowPresentS14 Enumerated = 13
	PagingTimeWindowPresentS15 Enumerated = 14
	PagingTimeWindowPresentS16 Enumerated = 15
)

type PagingTimeWindow struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:15"`
}

const (
	PagingProbabilityInformationPresentP00 Enumerated = 0
	PagingProbabilityInformationPresentP05 Enumerated = 1
	PagingProbabilityInformationPresentP10 Enumerated = 2
	PagingProbabilityInformationPresentP15 Enumerated = 3
	PagingProbabilityInformationPresentP20 Enumerated = 4
	PagingProbabilityInformationPresentP25 Enumerated = 5
	PagingProbabilityInformationPresentP30 Enumerated = 6
	PagingProbabilityInformationPresentP35 Enumerated = 7
	PagingProbabilityInformationPresentP40 Enumerated = 8
	PagingProbabilityInformationPresentP45 Enumerated = 9
	PagingProbabilityInformationPresentP50 Enumerated = 10
	PagingProbabilityInformationPresentP55 Enumerated = 11
	PagingProbabilityInformationPresentP60 Enumerated = 12
	PagingProbabilityInformationPresentP65 Enumerated = 13
	PagingProbabilityInformationPresentP70 Enumerated = 14
	PagingProbabilityInformationPresentP75 Enumerated = 15
	PagingProbabilityInformationPresentP80 Enumerated = 16
	PagingProbabilityInformationPresentP85 Enumerated = 17
	PagingProbabilityInformationPresentP90 Enumerated = 18
	PagingProbabilityInformationPresentP95 Enumerated = 19
	PagingProbabilityInformationPresentP100 Enumerated = 20
)

type PagingProbabilityInformation struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:20"`
}

const (
	PrivacyIndicatorPresentImmediateMDT Enumerated = 0
	PrivacyIndicatorPresentLoggedMDT Enumerated = 1
)

type PrivacyIndicator struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

type PDUSessionID struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:255"`
}

const (
	PDUSessionTypePresentIpv4 Enumerated = 0
	PDUSessionTypePresentIpv6 Enumerated = 1
	PDUSessionTypePresentIpv4v6 Enumerated = 2
	PDUSessionTypePresentEthernet Enumerated = 3
	PDUSessionTypePresentUnstructured Enumerated = 4
)

type PDUSessionType struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:4"`
}

type Periodicity struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:640000"`
}

type PeriodicRegistrationUpdateTimer struct {
	Value BitString `vht5gc:"sizeLB:8,sizeUB:8"`
}

type PLMNIdentity struct {
	Value OctetString `vht5gc:"sizeLB:3,sizeUB:3"`
}

type PortNumber struct {
	Value OctetString `vht5gc:"sizeLB:2,sizeUB:2"`
}

const (
	PreEmptionCapabilityPresentShallNotTriggerPreEmption Enumerated = 0
	PreEmptionCapabilityPresentMayTriggerPreEmption Enumerated = 1
)

type PreEmptionCapability struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	PreEmptionVulnerabilityPresentNotPreEmptable Enumerated = 0
	PreEmptionVulnerabilityPresentPreEmptable Enumerated = 1
)

type PreEmptionVulnerability struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

type PriorityLevelARP struct {
	Value int64 `vht5gc:"valueLB:1,valueUB:15"`
}

type PriorityLevelQos struct {
	Value int64 `vht5gc:"valueExt,valueLB:1,valueUB:127"`
}

type QosFlowIdentifier struct {
	Value int64 `vht5gc:"valueExt,valueLB:0,valueUB:63"`
}

const (
	QosMonitoringRequestPresentUl Enumerated = 0
	QosMonitoringRequestPresentDl Enumerated = 1
	QosMonitoringRequestPresentBoth Enumerated = 2
)

type QosMonitoringRequest struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:2"`
}

const (
	RATTypePresentNr Enumerated = 0
	RATTypePresentEutra Enumerated = 1
	RATTypePresentNrUnlicensed Enumerated = 2
	RATTypePresentEUtraUnlicensed Enumerated = 3
)

type RATType struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:3"`
}

const (
	RangePresentM50 Enumerated = 0
	RangePresentM80 Enumerated = 1
	RangePresentM180 Enumerated = 2
	RangePresentM200 Enumerated = 3
	RangePresentM350 Enumerated = 4
	RangePresentM400 Enumerated = 5
	RangePresentM500 Enumerated = 6
	RangePresentM700 Enumerated = 7
	RangePresentM1000 Enumerated = 8
)

type Range struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:8"`
}

type RANNodeName struct {
	Value string `vht5gc:"sizeExt,sizeLB:1,sizeUB:150"`
}

type RANNodeNameVisibleString struct {
	Value string
}

type RANNodeNameUTF8String struct {
	Value string `vht5gc:"sizeExt,sizeLB:1,sizeUB:150"`
}

type RANPagingPriority struct {
	Value int64 `vht5gc:"valueLB:1,valueUB:256"`
}

type RANUENGAPID struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:4294967295"`
}

const (
	RATInformationPresentUnlicensed Enumerated = 0
	RATInformationPresentNbIoT Enumerated = 1
)

type RATInformation struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

type RATRestrictionInformation struct {
	Value BitString `vht5gc:"sizeExt,sizeLB:8,sizeUB:8"`
}

const (
	RedirectionVoiceFallbackPresentPossible Enumerated = 0
	RedirectionVoiceFallbackPresentNotPossible Enumerated = 1
)

type RedirectionVoiceFallback struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	RedundantQosFlowIndicatorPresentTrue Enumerated = 0
	RedundantQosFlowIndicatorPresentFalse Enumerated = 1
)

type RedundantQosFlowIndicator struct {
	Value Enumerated `vht5gc:"valueLB:0,valueUB:1"`
}

const (
	ReflectiveQosAttributePresentSubjectTo Enumerated = 0
)

type ReflectiveQosAttribute struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type RelativeAMFCapacity struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:255"`
}

const (
	ReportAreaPresentCell Enumerated = 0
)

type ReportArea struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type RepetitionPeriod struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:131071"`
}

const (
	ResetAllPresentResetAll Enumerated = 0
)

type ResetAll struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	ReportAmountMDTPresentR1 Enumerated = 0
	ReportAmountMDTPresentR2 Enumerated = 1
	ReportAmountMDTPresentR4 Enumerated = 2
	ReportAmountMDTPresentR8 Enumerated = 3
	ReportAmountMDTPresentR16 Enumerated = 4
	ReportAmountMDTPresentR32 Enumerated = 5
	ReportAmountMDTPresentR64 Enumerated = 6
	ReportAmountMDTPresentRinfinity Enumerated = 7
)

type ReportAmountMDT struct {
	Value Enumerated `vht5gc:"valueLB:0,valueUB:7"`
}

const (
	ReportIntervalMDTPresentMs120 Enumerated = 0
	ReportIntervalMDTPresentMs240 Enumerated = 1
	ReportIntervalMDTPresentMs480 Enumerated = 2
	ReportIntervalMDTPresentMs640 Enumerated = 3
	ReportIntervalMDTPresentMs1024 Enumerated = 4
	ReportIntervalMDTPresentMs2048 Enumerated = 5
	ReportIntervalMDTPresentMs5120 Enumerated = 6
	ReportIntervalMDTPresentMs10240 Enumerated = 7
	ReportIntervalMDTPresentMin1 Enumerated = 8
	ReportIntervalMDTPresentMin6 Enumerated = 9
	ReportIntervalMDTPresentMin12 Enumerated = 10
	ReportIntervalMDTPresentMin30 Enumerated = 11
	ReportIntervalMDTPresentMin60 Enumerated = 12
)

type ReportIntervalMDT struct {
	Value Enumerated `vht5gc:"valueLB:0,valueUB:12"`
}

type RGLevelWirelineAccessCharacteristics struct {
	Value OctetString
}

type RNCID struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:4095"`
}

type RoutingID struct {
	Value OctetString
}

type RRCContainer struct {
	Value OctetString
}

const (
	RRCEstablishmentCausePresentEmergency Enumerated = 0
	RRCEstablishmentCausePresentHighPriorityAccess Enumerated = 1
	RRCEstablishmentCausePresentMtAccess Enumerated = 2
	RRCEstablishmentCausePresentMoSignalling Enumerated = 3
	RRCEstablishmentCausePresentMoData Enumerated = 4
	RRCEstablishmentCausePresentMoVoiceCall Enumerated = 5
	RRCEstablishmentCausePresentMoVideoCall Enumerated = 6
	RRCEstablishmentCausePresentMoSMS Enumerated = 7
	RRCEstablishmentCausePresentMpsPriorityAccess Enumerated = 8
	RRCEstablishmentCausePresentMcsPriorityAccess Enumerated = 9
	RRCEstablishmentCausePresentNotAvailable Enumerated = 10
	RRCEstablishmentCausePresentMoExceptionData Enumerated = 11
)

type RRCEstablishmentCause struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:11"`
}

const (
	RRCInactiveTransitionReportRequestPresentSubsequentStateTransitionReport Enumerated = 0
	RRCInactiveTransitionReportRequestPresentSingleRrcConnectedStateReport Enumerated = 1
	RRCInactiveTransitionReportRequestPresentCancelReport Enumerated = 2
)

type RRCInactiveTransitionReportRequest struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:2"`
}

const (
	RRCStatePresentInactive Enumerated = 0
	RRCStatePresentConnected Enumerated = 1
)

type RRCState struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	RSNPresentV1 Enumerated = 0
	RSNPresentV2 Enumerated = 1
)

type RSN struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

const (
	RIMRSDetectionPresentRsDetected Enumerated = 0
	RIMRSDetectionPresentRsDisappeared Enumerated = 1
)

type RIMRSDetection struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:1"`
}

type GNBSetID struct {
	Value BitString `vht5gc:"sizeLB:22,sizeUB:22"`
}

type SD struct {
	Value OctetString `vht5gc:"sizeLB:3,sizeUB:3"`
}

type SecurityKey struct {
	Value BitString `vht5gc:"sizeLB:256,sizeUB:256"`
}

const (
	SensorMeasConfigPresentSetup Enumerated = 0
)

type SensorMeasConfig struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	UncompensatedBarometricConfigPresentTrue Enumerated = 0
)

type UncompensatedBarometricConfig struct {
	Value Enumerated `vht5gc:"valueLB:0,valueUB:0"`
}

const (
	UeSpeedConfigPresentTrue Enumerated = 0
)

type UeSpeedConfig struct {
	Value Enumerated `vht5gc:"valueLB:0,valueUB:0"`
}

const (
	UeOrientationConfigPresentTrue Enumerated = 0
)

type UeOrientationConfig struct {
	Value Enumerated `vht5gc:"valueLB:0,valueUB:0"`
}

type SerialNumber struct {
	Value BitString `vht5gc:"sizeLB:16,sizeUB:16"`
}

type SgNBUEX2APID struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:4294967295"`
}

const (
	SONInformationRequestPresentXnTNLConfigurationInfo Enumerated = 0
)

type SONInformationRequest struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	SourceOfUEActivityBehaviourInformationPresentSubscriptionInformation Enumerated = 0
	SourceOfUEActivityBehaviourInformationPresentStatistics Enumerated = 1
)

type SourceOfUEActivityBehaviourInformation struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

type SourceToTargetTransparentContainer struct {
	Value OctetString
}

const (
	SRVCCOperationPossiblePresentPossible Enumerated = 0
	SRVCCOperationPossiblePresentNotPossible Enumerated = 1
)

type SRVCCOperationPossible struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

type ConfiguredNSSAI struct {
	Value OctetString `vht5gc:"sizeLB:128,sizeUB:128"`
}

type RejectedNSSAIinPLMN struct {
	Value OctetString `vht5gc:"sizeLB:32,sizeUB:32"`
}

type RejectedNSSAIinTA struct {
	Value OctetString `vht5gc:"sizeLB:32,sizeUB:32"`
}

type SST struct {
	Value OctetString `vht5gc:"sizeLB:1,sizeUB:1"`
}

const (
	SuspendIndicatorPresentTrue Enumerated = 0
)

type SuspendIndicator struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	SuspendRequestIndicationPresentSuspendRequested Enumerated = 0
)

type SuspendRequestIndication struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	SuspendResponseIndicationPresentSuspendIndicated Enumerated = 0
)

type SuspendResponseIndication struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type TAC struct {
	Value OctetString `vht5gc:"sizeLB:3,sizeUB:3"`
}

type TargetToSourceTransparentContainer struct {
	Value OctetString
}

type TargettoSourceFailureTransparentContainer struct {
	Value OctetString
}

const (
	TimerApproachForGUAMIRemovalPresentApplyTimer Enumerated = 0
)

type TimerApproachForGUAMIRemoval struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type TimeStamp struct {
	Value OctetString `vht5gc:"sizeLB:4,sizeUB:4"`
}

const (
	TimeToWaitPresentV1s Enumerated = 0
	TimeToWaitPresentV2s Enumerated = 1
	TimeToWaitPresentV5s Enumerated = 2
	TimeToWaitPresentV10s Enumerated = 3
	TimeToWaitPresentV20s Enumerated = 4
	TimeToWaitPresentV60s Enumerated = 5
)

type TimeToWait struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:5"`
}

type TimeUEStayedInCell struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:4095"`
}

type TimeUEStayedInCellEnhancedGranularity struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:40950"`
}

type TNAPID struct {
	Value OctetString
}

type TNLAddressWeightFactor struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:255"`
}

const (
	TNLAssociationUsagePresentUe Enumerated = 0
	TNLAssociationUsagePresentNonUe Enumerated = 1
	TNLAssociationUsagePresentBoth Enumerated = 2
)

type TNLAssociationUsage struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:2"`
}

const (
	TraceDepthPresentMinimum Enumerated = 0
	TraceDepthPresentMedium Enumerated = 1
	TraceDepthPresentMaximum Enumerated = 2
	TraceDepthPresentMinimumWithoutVendorSpecificExtension Enumerated = 3
	TraceDepthPresentMediumWithoutVendorSpecificExtension Enumerated = 4
	TraceDepthPresentMaximumWithoutVendorSpecificExtension Enumerated = 5
)

type TraceDepth struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:5"`
}

type TrafficLoadReductionIndication struct {
	Value int64 `vht5gc:"valueLB:1,valueUB:99"`
}

type TransportLayerAddress struct {
	Value BitString `vht5gc:"sizeExt,sizeLB:1,sizeUB:160"`
}

const (
	TypeOfErrorPresentNotUnderstood Enumerated = 0
	TypeOfErrorPresentMissing Enumerated = 1
)

type TypeOfError struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:1"`
}

type ThresholdRSRP struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:127"`
}

type ThresholdRSRQ struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:127"`
}

type ThresholdSINR struct {
	Value int64 `vht5gc:"valueLB:0,valueUB:127"`
}

const (
	TimeToTriggerPresentMs0 Enumerated = 0
	TimeToTriggerPresentMs40 Enumerated = 1
	TimeToTriggerPresentMs64 Enumerated = 2
	TimeToTriggerPresentMs80 Enumerated = 3
	TimeToTriggerPresentMs100 Enumerated = 4
	TimeToTriggerPresentMs128 Enumerated = 5
	TimeToTriggerPresentMs160 Enumerated = 6
	TimeToTriggerPresentMs256 Enumerated = 7
	TimeToTriggerPresentMs320 Enumerated = 8
	TimeToTriggerPresentMs480 Enumerated = 9
	TimeToTriggerPresentMs512 Enumerated = 10
	TimeToTriggerPresentMs640 Enumerated = 11
	TimeToTriggerPresentMs1024 Enumerated = 12
	TimeToTriggerPresentMs1280 Enumerated = 13
	TimeToTriggerPresentMs2560 Enumerated = 14
	TimeToTriggerPresentMs5120 Enumerated = 15
)

type TimeToTrigger struct {
	Value Enumerated `vht5gc:"valueLB:0,valueUB:15"`
}

type TWAPID struct {
	Value OctetString
}

const (
	UECapabilityInfoRequestPresentRequested Enumerated = 0
)

type UECapabilityInfoRequest struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	UEContextRequestPresentRequested Enumerated = 0
)

type UEContextRequest struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	PeriodicCommunicationIndicatorPresentPeriodically Enumerated = 0
	PeriodicCommunicationIndicatorPresentOndemand Enumerated = 1
)

type PeriodicCommunicationIndicator struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:1"`
}

const (
	StationaryIndicationPresentStationary Enumerated = 0
	StationaryIndicationPresentMobile Enumerated = 1
)

type StationaryIndication struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:1"`
}

const (
	TrafficProfilePresentSinglePacket Enumerated = 0
	TrafficProfilePresentDualPackets Enumerated = 1
	TrafficProfilePresentMultiplePackets Enumerated = 2
)

type TrafficProfile struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:2"`
}

const (
	BatteryIndicationPresentBatteryPowered Enumerated = 0
	BatteryIndicationPresentBatteryPoweredNotRechargeableOrReplaceable Enumerated = 1
	BatteryIndicationPresentNotBatteryPowered Enumerated = 2
)

type BatteryIndication struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:2"`
}

const (
	UEPresencePresentIn Enumerated = 0
	UEPresencePresentOut Enumerated = 1
	UEPresencePresentUnknown Enumerated = 2
)

type UEPresence struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:2"`
}

type UERadioCapability struct {
	Value OctetString
}

type UERadioCapabilityForPagingOfNBIoT struct {
	Value OctetString
}

type UERadioCapabilityForPagingOfNR struct {
	Value OctetString
}

type UERadioCapabilityForPagingOfEUTRA struct {
	Value OctetString
}

type UERadioCapabilityID struct {
	Value OctetString
}

const (
	UERetentionInformationPresentUesRetained Enumerated = 0
)

type UERetentionInformation struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

const (
	UEUPCIoTSupportPresentSupported Enumerated = 0
)

type UEUPCIoTSupport struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type ULNASMAC struct {
	Value BitString `vht5gc:"sizeLB:16,sizeUB:16"`
}

type ULNASCount struct {
	Value BitString `vht5gc:"sizeLB:5,sizeUB:5"`
}

const (
	ULForwardingPresentUlForwardingProposed Enumerated = 0
)

type ULForwarding struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type URIAddress struct {
	Value string
}

type WarningAreaCoordinates struct {
	Value OctetString `vht5gc:"sizeLB:1,sizeUB:1024"`
}

type WarningMessageContents struct {
	Value OctetString `vht5gc:"sizeLB:1,sizeUB:9600"`
}

type WarningSecurityInfo struct {
	Value OctetString `vht5gc:"sizeLB:50,sizeUB:50"`
}

type WarningType struct {
	Value OctetString `vht5gc:"sizeLB:2,sizeUB:2"`
}

const (
	WlanRssiPresentTrue Enumerated = 0
)

type WlanRssi struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:0"`
}

const (
	WlanRttPresentTrue Enumerated = 0
)

type WlanRtt struct {
	Value Enumerated `vht5gc:"valueExt,,valueLB:0,valueUB:0"`
}

const (
	WLANMeasConfigPresentSetup Enumerated = 0
)

type WLANMeasConfig struct {
	Value Enumerated `vht5gc:"valueExt,valueLB:0,valueUB:0"`
}

type WLANName struct {
	Value OctetString `vht5gc:"sizeLB:1,sizeUB:32"`
}

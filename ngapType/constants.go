package ngapType

const (
	ProcedureCodeAMFConfigurationUpdate                int64 = 0
	ProcedureCodeAMFConfigurationUpdateAcknowledge     int64 = 0
	ProcedureCodeAMFConfigurationUpdateFailure         int64 = 0
	ProcedureCodeAMFStatusIndication                   int64 = 1
	ProcedureCodeCellTrafficTrace                      int64 = 2
	ProcedureCodeDeactivateTrace                       int64 = 3
	ProcedureCodeDownlinkNASTransport                  int64 = 4
	ProcedureCodeDownlinkNonUEAssociatedNRPPaTransport int64 = 5
	ProcedureCodeDownlinkRANConfigurationTransfer      int64 = 6
	ProcedureCodeDownlinkRANStatusTransfer             int64 = 7
	ProcedureCodeDownlinkUEAssociatedNRPPaTransport    int64 = 8
	ProcedureCodeErrorIndication                       int64 = 9
	ProcedureCodeHandoverCancel                        int64 = 10
	ProcedureCodeHandoverCancelAcknowledge             int64 = 10
	ProcedureCodeHandoverNotification                  int64 = 11
	ProcedureCodeHandoverNotify                        int64 = 11
	ProcedureCodeHandoverPreparation                   int64 = 12
	ProcedureCodeHandoverRequired                      int64 = 12
	ProcedureCodeHandoverCommand                       int64 = 12
	ProcedureCodeHandoverPreparationFailure            int64 = 12
	ProcedureCodeHandoverResourceAllocation            int64 = 13
	ProcedureCodeHandoverRequest                       int64 = 13
	ProcedureCodeHandoverRequestAcknowledge            int64 = 13
	ProcedureCodeHandoverFailure                       int64 = 13
	ProcedureCodeInitialContextSetup                   int64 = 14
	ProcedureCodeInitialContextSetupRequest            int64 = 14
	ProcedureCodeInitialContextSetupResponse           int64 = 14
	ProcedureCodeInitialContextSetupFailure            int64 = 14
	ProcedureCodeInitialUEMessage                      int64 = 15
	ProcedureCodeLocationReportingControl              int64 = 16
	ProcedureCodeLocationReportingFailureIndication    int64 = 17
	ProcedureCodeLocationReport                        int64 = 18
	ProcedureCodeNASNonDeliveryIndication              int64 = 19
	ProcedureCodeNGReset                               int64 = 20
	ProcedureCodeNGResetAcknowledge                    int64 = 20
	ProcedureCodeNGSetup                               int64 = 21
	ProcedureCodeNGSetupRequest                        int64 = 21
	ProcedureCodeNGSetupResponse                       int64 = 21
	ProcedureCodeNGSetupFailure                        int64 = 21
	ProcedureCodeOverloadStart                         int64 = 22
	ProcedureCodeOverloadStop                          int64 = 23
	ProcedureCodePaging                                int64 = 24
	ProcedureCodePathSwitchRequest                     int64 = 25
	ProcedureCodePathSwitchRequestAcknowledge          int64 = 25
	ProcedureCodePathSwitchRequestFailure              int64 = 25
	ProcedureCodePDUSessionResourceModify              int64 = 26
	ProcedureCodePDUSessionResourceModifyRequest       int64 = 26
	ProcedureCodePDUSessionResourceModifyResponse      int64 = 26
	ProcedureCodePDUSessionResourceModifyIndication    int64 = 27
	ProcedureCodePDUSessionResourceModifyConfirm       int64 = 27
	ProcedureCodePDUSessionResourceRelease             int64 = 28
	ProcedureCodePDUSessionResourceReleaseCommand      int64 = 28
	ProcedureCodePDUSessionResourceReleaseResponse     int64 = 28
	ProcedureCodePDUSessionResourceSetup               int64 = 29
	ProcedureCodePDUSessionResourceSetupRequest        int64 = 29
	ProcedureCodePDUSessionResourceSetupResponse       int64 = 29
	ProcedureCodePDUSessionResourceNotify              int64 = 30
	ProcedureCodePrivateMessage                        int64 = 31
	ProcedureCodePWSCancel                             int64 = 32
	ProcedureCodePWSCancelRequest                      int64 = 32
	ProcedureCodePWSCancelResponse                     int64 = 32
	ProcedureCodePWSFailureIndication                  int64 = 33
	ProcedureCodePWSRestartIndication                  int64 = 34
	ProcedureCodeRANConfigurationUpdate                int64 = 35
	ProcedureCodeRANConfigurationUpdateAcknowledge     int64 = 35
	ProcedureCodeRANConfigurationUpdateFailure         int64 = 35
	ProcedureCodeRerouteNASRequest                     int64 = 36
	ProcedureCodeRRCInactiveTransitionReport           int64 = 37
	ProcedureCodeTraceFailureIndication                int64 = 38
	ProcedureCodeTraceStart                            int64 = 39
	ProcedureCodeUEContextModification                 int64 = 40
	ProcedureCodeUEContextModificationRequest          int64 = 40
	ProcedureCodeUEContextModificationResponse         int64 = 40
	ProcedureCodeUEContextModificationFailure          int64 = 40
	ProcedureCodeUEContextRelease                      int64 = 41
	ProcedureCodeUEContextReleaseCommand               int64 = 41
	ProcedureCodeUEContextReleaseComplete              int64 = 41
	ProcedureCodeUEContextReleaseRequest               int64 = 42
	ProcedureCodeUERadioCapabilityCheck                int64 = 43
	ProcedureCodeUERadioCapabilityCheckRequest         int64 = 43
	ProcedureCodeUERadioCapabilityCheckResponse        int64 = 43
	ProcedureCodeUERadioCapabilityInfoIndication       int64 = 44
	ProcedureCodeUETNLABindingRelease                  int64 = 45
	ProcedureCodeUETNLABindingReleaseRequest           int64 = 45
	ProcedureCodeUplinkNASTransport                    int64 = 46
	ProcedureCodeUplinkNonUEAssociatedNRPPaTransport   int64 = 47
	ProcedureCodeUplinkRANConfigurationTransfer        int64 = 48
	ProcedureCodeUplinkRANStatusTransfer               int64 = 49
	ProcedureCodeUplinkUEAssociatedNRPPaTransport      int64 = 50
	ProcedureCodeWriteReplaceWarning                   int64 = 51
	ProcedureCodeWriteReplaceWarningRequest            int64 = 51
	ProcedureCodeWriteReplaceWarningResponse           int64 = 51
	ProcedureCodeSecondaryRATDataUsageReport           int64 = 52
	ProcedureCodeUplinkRIMInformationTransfer          int64 = 53
	ProcedureCodeDownlinkRIMInformationTransfer        int64 = 54
	ProcedureCodeRetrieveUEInformation                 int64 = 55
	ProcedureCodeUEInformationTransfer                 int64 = 56
	ProcedureCodeRANCPRelocationIndication             int64 = 57
	ProcedureCodeUEContextResume                       int64 = 58
	ProcedureCodeUEContextResumeRequest                int64 = 58
	ProcedureCodeUEContextResumeResponse               int64 = 58
	ProcedureCodeUEContextResumeFailure                int64 = 58
	ProcedureCodeUEContextSuspend                      int64 = 59
	ProcedureCodeUEContextSuspendRequest               int64 = 59
	ProcedureCodeUEContextSuspendResponse              int64 = 59
	ProcedureCodeUEContextSuspendFailure               int64 = 59
	ProcedureCodeUERadioCapabilityIDMapping            int64 = 60
	ProcedureCodeUERadioCapabilityIDMappingRequest     int64 = 60
	ProcedureCodeUERadioCapabilityIDMappingResponse    int64 = 60
	ProcedureCodeHandoverSuccess                       int64 = 61
	ProcedureCodeUplinkRANEarlyStatusTransfer          int64 = 62
	ProcedureCodeDownlinkRANEarlyStatusTransfer        int64 = 63
	ProcedureCodeAMFCPRelocationIndication             int64 = 64
	ProcedureCodeConnectionEstablishmentIndication     int64 = 65
)

const (
	MaxPrivateIEs         int64 = 65535
	MaxProtocolExtensions int64 = 65535
	MaxProtocolIEs        int64 = 65535
)

const (
	MaxnoofAllowedAreas              int64 = 16
	MaxnoofAllowedCAGsperPLMN        int64 = 256
	MaxnoofAllowedSNSSAIs            int64 = 8
	MaxnoofBluetoothName             int64 = 4
	MaxnoofBPLMNs                    int64 = 12
	MaxnoofCAGSperCell               int64 = 64
	MaxnoofCellIDforMDT              int64 = 32
	MaxnoofCellIDforWarning          int64 = 65535
	MaxnoofCellinAoI                 int64 = 256
	MaxnoofCellinEAI                 int64 = 65535
	MaxnoofCellinTAI                 int64 = 65535
	MaxnoofCellsingNB                int64 = 16384
	MaxnoofCellsinngeNB              int64 = 256
	MaxnoofCellsinUEHistoryInfo      int64 = 16
	MaxnoofCellsUEMovingTrajectory   int64 = 16
	MaxnoofDRBs                      int64 = 32
	MaxnoofEmergencyAreaID           int64 = 65535
	MaxnoofEAIforRestart             int64 = 256
	MaxnoofEPLMNs                    int64 = 15
	MaxnoofEPLMNsPlusOne             int64 = 16
	MaxnoofERABs                     int64 = 256
	MaxnoofErrors                    int64 = 256
	MaxnoofExtSliceItems             int64 = 65535
	MaxnoofForbTACs                  int64 = 4096
	MaxnoofFreqforMDT                int64 = 8
	MaxnoofMDTPLMNs                  int64 = 16
	MaxnoofMultiConnectivity         int64 = 4
	MaxnoofMultiConnectivityMinusOne int64 = 3
	MaxnoofNeighPCIforMDT            int64 = 32
	MaxnoofNGConnectionsToReset      int64 = 65536
	MaxnoofNRCellBands               int64 = 32
	MaxnoofPC5QoSFlows               int64 = 2048
	MaxnoofPDUSessions               int64 = 256
	MaxnoofPLMNs                     int64 = 12
	MaxnoofQosFlows                  int64 = 64
	MaxnoofQosParaSets               int64 = 8
	MaxnoofRANNodeinAoI              int64 = 64
	MaxnoofRecommendedCells          int64 = 16
	MaxnoofRecommendedRANNodes       int64 = 16
	MaxnoofAoI                       int64 = 64
	MaxnoofSensorName                int64 = 3
	MaxnoofServedGUAMIs              int64 = 256
	MaxnoofSliceItems                int64 = 1024
	MaxnoofTACs                      int64 = 256
	MaxnoofTAforMDT                  int64 = 8
	MaxnoofTAIforInactive            int64 = 16
	MaxnoofTAIforPaging              int64 = 16
	MaxnoofTAIforRestart             int64 = 2048
	MaxnoofTAIforWarning             int64 = 65535
	MaxnoofTAIinAoI                  int64 = 16
	MaxnoofTimePeriods               int64 = 2
	MaxnoofTNLAssociations           int64 = 32
	MaxnoofWLANName                  int64 = 4
	MaxnoofXnExtTLAs                 int64 = 16
	MaxnoofXnGTPTLAs                 int64 = 16
	MaxnoofXnTLAs                    int64 = 2
	MaxnoofCandidateCells            int64 = 32
	MaxNRARFCN                       int64 = 3279165
)

const (
	ProtocolIEIDAllowedNSSAI                                  int64 = 0
	ProtocolIEIDAMFName                                       int64 = 1
	ProtocolIEIDAMFOverloadResponse                           int64 = 2
	ProtocolIEIDAMFSetID                                      int64 = 3
	ProtocolIEIDAMFTNLAssociationFailedToSetupList            int64 = 4
	ProtocolIEIDAMFTNLAssociationSetupList                    int64 = 5
	ProtocolIEIDAMFTNLAssociationToAddList                    int64 = 6
	ProtocolIEIDAMFTNLAssociationToRemoveList                 int64 = 7
	ProtocolIEIDAMFTNLAssociationToUpdateList                 int64 = 8
	ProtocolIEIDAMFTrafficLoadReductionIndication             int64 = 9
	ProtocolIEIDAMFUENGAPID                                   int64 = 10
	ProtocolIEIDAssistanceDataForPaging                       int64 = 11
	ProtocolIEIDBroadcastCancelledAreaList                    int64 = 12
	ProtocolIEIDBroadcastCompletedAreaList                    int64 = 13
	ProtocolIEIDCancelAllWarningMessages                      int64 = 14
	ProtocolIEIDCause                                         int64 = 15
	ProtocolIEIDCellIDListForRestart                          int64 = 16
	ProtocolIEIDConcurrentWarningMessageInd                   int64 = 17
	ProtocolIEIDCoreNetworkAssistanceInformationForInactive   int64 = 18
	ProtocolIEIDCriticalityDiagnostics                        int64 = 19
	ProtocolIEIDDataCodingScheme                              int64 = 20
	ProtocolIEIDDefaultPagingDRX                              int64 = 21
	ProtocolIEIDDirectForwardingPathAvailability              int64 = 22
	ProtocolIEIDEmergencyAreaIDListForRestart                 int64 = 23
	ProtocolIEIDEmergencyFallbackIndicator                    int64 = 24
	ProtocolIEIDEUTRACGI                                      int64 = 25
	ProtocolIEIDFiveGSTMSI                                    int64 = 26
	ProtocolIEIDGlobalRANNodeID                               int64 = 27
	ProtocolIEIDGUAMI                                         int64 = 28
	ProtocolIEIDHandoverType                                  int64 = 29
	ProtocolIEIDIMSVoiceSupportIndicator                      int64 = 30
	ProtocolIEIDIndexToRFSP                                   int64 = 31
	ProtocolIEIDInfoOnRecommendedCellsAndRANNodesForPaging    int64 = 32
	ProtocolIEIDLocationReportingRequestType                  int64 = 33
	ProtocolIEIDMaskedIMEISV                                  int64 = 34
	ProtocolIEIDMessageIdentifier                             int64 = 35
	ProtocolIEIDMobilityRestrictionList                       int64 = 36
	ProtocolIEIDNASC                                          int64 = 37
	ProtocolIEIDNASPDU                                        int64 = 38
	ProtocolIEIDNASSecurityParametersFromNGRAN                int64 = 39
	ProtocolIEIDNewAMFUENGAPID                                int64 = 40
	ProtocolIEIDNewSecurityContextInd                         int64 = 41
	ProtocolIEIDNGAPMessage                                   int64 = 42
	ProtocolIEIDNGRANCGI                                      int64 = 43
	ProtocolIEIDNGRANTraceID                                  int64 = 44
	ProtocolIEIDNRCGI                                         int64 = 45
	ProtocolIEIDNRPPaPDU                                      int64 = 46
	ProtocolIEIDNumberOfBroadcastsRequested                   int64 = 47
	ProtocolIEIDOldAMF                                        int64 = 48
	ProtocolIEIDOverloadStartNSSAIList                        int64 = 49
	ProtocolIEIDPagingDRX                                     int64 = 50
	ProtocolIEIDPagingOrigin                                  int64 = 51
	ProtocolIEIDPagingPriority                                int64 = 52
	ProtocolIEIDPDUSessionResourceAdmittedList                int64 = 53
	ProtocolIEIDPDUSessionResourceFailedToModifyListModRes    int64 = 54
	ProtocolIEIDPDUSessionResourceFailedToSetupListCxtRes     int64 = 55
	ProtocolIEIDPDUSessionResourceFailedToSetupListHOAck      int64 = 56
	ProtocolIEIDPDUSessionResourceFailedToSetupListPSReq      int64 = 57
	ProtocolIEIDPDUSessionResourceFailedToSetupListSURes      int64 = 58
	ProtocolIEIDPDUSessionResourceHandoverList                int64 = 59
	ProtocolIEIDPDUSessionResourceListCxtRelCpl               int64 = 60
	ProtocolIEIDPDUSessionResourceListHORqd                   int64 = 61
	ProtocolIEIDPDUSessionResourceModifyListModCfm            int64 = 62
	ProtocolIEIDPDUSessionResourceModifyListModInd            int64 = 63
	ProtocolIEIDPDUSessionResourceModifyListModReq            int64 = 64
	ProtocolIEIDPDUSessionResourceModifyListModRes            int64 = 65
	ProtocolIEIDPDUSessionResourceNotifyList                  int64 = 66
	ProtocolIEIDPDUSessionResourceReleasedListNot             int64 = 67
	ProtocolIEIDPDUSessionResourceReleasedListPSAck           int64 = 68
	ProtocolIEIDPDUSessionResourceReleasedListPSFail          int64 = 69
	ProtocolIEIDPDUSessionResourceReleasedListRelRes          int64 = 70
	ProtocolIEIDPDUSessionResourceSetupListCxtReq             int64 = 71
	ProtocolIEIDPDUSessionResourceSetupListCxtRes             int64 = 72
	ProtocolIEIDPDUSessionResourceSetupListHOReq              int64 = 73
	ProtocolIEIDPDUSessionResourceSetupListSUReq              int64 = 74
	ProtocolIEIDPDUSessionResourceSetupListSURes              int64 = 75
	ProtocolIEIDPDUSessionResourceToBeSwitchedDLList          int64 = 76
	ProtocolIEIDPDUSessionResourceSwitchedList                int64 = 77
	ProtocolIEIDPDUSessionResourceToReleaseListHOCmd          int64 = 78
	ProtocolIEIDPDUSessionResourceToReleaseListRelCmd         int64 = 79
	ProtocolIEIDPLMNSupportList                               int64 = 80
	ProtocolIEIDPWSFailedCellIDList                           int64 = 81
	ProtocolIEIDRANNodeName                                   int64 = 82
	ProtocolIEIDRANPagingPriority                             int64 = 83
	ProtocolIEIDRANStatusTransferTransparentContainer         int64 = 84
	ProtocolIEIDRANUENGAPID                                   int64 = 85
	ProtocolIEIDRelativeAMFCapacity                           int64 = 86
	ProtocolIEIDRepetitionPeriod                              int64 = 87
	ProtocolIEIDResetType                                     int64 = 88
	ProtocolIEIDRoutingID                                     int64 = 89
	ProtocolIEIDRRCEstablishmentCause                         int64 = 90
	ProtocolIEIDRRCInactiveTransitionReportRequest            int64 = 91
	ProtocolIEIDRRCState                                      int64 = 92
	ProtocolIEIDSecurityContext                               int64 = 93
	ProtocolIEIDSecurityKey                                   int64 = 94
	ProtocolIEIDSerialNumber                                  int64 = 95
	ProtocolIEIDServedGUAMIList                               int64 = 96
	ProtocolIEIDSliceSupportList                              int64 = 97
	ProtocolIEIDSONConfigurationTransferDL                    int64 = 98
	ProtocolIEIDSONConfigurationTransferUL                    int64 = 99
	ProtocolIEIDSourceAMFUENGAPID                             int64 = 100
	ProtocolIEIDSourceToTargetTransparentContainer            int64 = 101
	ProtocolIEIDSupportedTAList                               int64 = 102
	ProtocolIEIDTAIListForPaging                              int64 = 103
	ProtocolIEIDTAIListForRestart                             int64 = 104
	ProtocolIEIDTargetID                                      int64 = 105
	ProtocolIEIDTargetToSourceTransparentContainer            int64 = 106
	ProtocolIEIDTimeToWait                                    int64 = 107
	ProtocolIEIDTraceActivation                               int64 = 108
	ProtocolIEIDTraceCollectionEntityIPAddress                int64 = 109
	ProtocolIEIDUEAggregateMaximumBitRate                     int64 = 110
	ProtocolIEIDUEAssociatedLogicalNGConnectionList           int64 = 111
	ProtocolIEIDUEContextRequest                              int64 = 112
	ProtocolIEIDUnknown                                       int64 = 113
	ProtocolIEIDUENGAPIDs                                     int64 = 114
	ProtocolIEIDUEPagingIdentity                              int64 = 115
	ProtocolIEIDUEPresenceInAreaOfInterestList                int64 = 116
	ProtocolIEIDUERadioCapability                             int64 = 117
	ProtocolIEIDUERadioCapabilityForPaging                    int64 = 118
	ProtocolIEIDUESecurityCapabilities                        int64 = 119
	ProtocolIEIDUnavailableGUAMIList                          int64 = 120
	ProtocolIEIDUserLocationInformation                       int64 = 121
	ProtocolIEIDWarningAreaList                               int64 = 122
	ProtocolIEIDWarningMessageContents                        int64 = 123
	ProtocolIEIDWarningSecurityInfo                           int64 = 124
	ProtocolIEIDWarningType                                   int64 = 125
	ProtocolIEIDAdditionalULNGUUPTNLInformation               int64 = 126
	ProtocolIEIDDataForwardingNotPossible                     int64 = 127
	ProtocolIEIDDLNGUUPTNLInformation                         int64 = 128
	ProtocolIEIDNetworkInstance                               int64 = 129
	ProtocolIEIDPDUSessionAggregateMaximumBitRate             int64 = 130
	ProtocolIEIDPDUSessionResourceFailedToModifyListModCfm    int64 = 131
	ProtocolIEIDPDUSessionResourceFailedToSetupListCxtFail    int64 = 132
	ProtocolIEIDPDUSessionResourceListCxtRelReq               int64 = 133
	ProtocolIEIDPDUSessionType                                int64 = 134
	ProtocolIEIDQosFlowAddOrModifyRequestList                 int64 = 135
	ProtocolIEIDQosFlowSetupRequestList                       int64 = 136
	ProtocolIEIDQosFlowToReleaseList                          int64 = 137
	ProtocolIEIDSecurityIndication                            int64 = 138
	ProtocolIEIDULNGUUPTNLInformation                         int64 = 139
	ProtocolIEIDULNGUUPTNLModifyList                          int64 = 140
	ProtocolIEIDWarningAreaCoordinates                        int64 = 141
	ProtocolIEIDPDUSessionResourceSecondaryRATUsageList       int64 = 142
	ProtocolIEIDHandoverFlag                                  int64 = 143
	ProtocolIEIDSecondaryRATUsageInformation                  int64 = 144
	ProtocolIEIDPDUSessionResourceReleaseResponseTransfer     int64 = 145
	ProtocolIEIDRedirectionVoiceFallback                      int64 = 146
	ProtocolIEIDUERetentionInformation                        int64 = 147
	ProtocolIEIDSNSSAI                                        int64 = 148
	ProtocolIEIDPSCellInformation                             int64 = 149
	ProtocolIEIDLastEUTRANPLMNIdentity                        int64 = 150
	ProtocolIEIDMaximumIntegrityProtectedDataRateDL           int64 = 151
	ProtocolIEIDAdditionalDLForwardingUPTNLInformation        int64 = 152
	ProtocolIEIDAdditionalDLUPTNLInformationForHOList         int64 = 153
	ProtocolIEIDAdditionalNGUUPTNLInformation                 int64 = 154
	ProtocolIEIDAdditionalDLQosFlowPerTNLInformation          int64 = 155
	ProtocolIEIDSecurityResult                                int64 = 156
	ProtocolIEIDENDCSONConfigurationTransferDL                int64 = 157
	ProtocolIEIDENDCSONConfigurationTransferUL                int64 = 158
	ProtocolIEIDOldAssociatedQosFlowListULendmarkerexpected   int64 = 159
	ProtocolIEIDCNTypeRestrictionsForEquivalent               int64 = 160
	ProtocolIEIDCNTypeRestrictionsForServing                  int64 = 161
	ProtocolIEIDNewGUAMI                                      int64 = 162
	ProtocolIEIDULForwarding                                  int64 = 163
	ProtocolIEIDULForwardingUPTNLInformation                  int64 = 164
	ProtocolIEIDCNAssistedRANTuning                           int64 = 165
	ProtocolIEIDCommonNetworkInstance                         int64 = 166
	ProtocolIEIDNGRANTNLAssociationToRemoveList               int64 = 167
	ProtocolIEIDTNLAssociationTransportLayerAddressNGRAN      int64 = 168
	ProtocolIEIDEndpointIPAddressAndPort                      int64 = 169
	ProtocolIEIDLocationReportingAdditionalInfo               int64 = 170
	ProtocolIEIDSourceToTargetAMFInformationReroute           int64 = 171
	ProtocolIEIDAdditionalULForwardingUPTNLInformation        int64 = 172
	ProtocolIEIDSCTPTLAs                                      int64 = 173
	ProtocolIEIDSelectedPLMNIdentity                          int64 = 174
	ProtocolIEIDRIMInformationTransfer                        int64 = 175
	ProtocolIEIDGUAMIType                                     int64 = 176
	ProtocolIEIDSRVCCOperationPossible                        int64 = 177
	ProtocolIEIDTargetRNCID                                   int64 = 178
	ProtocolIEIDRATInformation                                int64 = 179
	ProtocolIEIDExtendedRATRestrictionInformation             int64 = 180
	ProtocolIEIDQosMonitoringRequest                          int64 = 181
	ProtocolIEIDSgNBUEX2APID                                  int64 = 182
	ProtocolIEIDAdditionalRedundantDLNGUUPTNLInformation      int64 = 183
	ProtocolIEIDAdditionalRedundantDLQosFlowPerTNLInformation int64 = 184
	ProtocolIEIDAdditionalRedundantNGUUPTNLInformation        int64 = 185
	ProtocolIEIDAdditionalRedundantULNGUUPTNLInformation      int64 = 186
	ProtocolIEIDCNPacketDelayBudgetDL                         int64 = 187
	ProtocolIEIDCNPacketDelayBudgetUL                         int64 = 188
	ProtocolIEIDExtendedPacketDelayBudget                     int64 = 189
	ProtocolIEIDRedundantCommonNetworkInstance                int64 = 190
	ProtocolIEIDRedundantDLNGUTNLInformationReused            int64 = 191
	ProtocolIEIDRedundantDLNGUUPTNLInformation                int64 = 192
	ProtocolIEIDRedundantDLQosFlowPerTNLInformation           int64 = 193
	ProtocolIEIDRedundantQosFlowIndicator                     int64 = 194
	ProtocolIEIDRedundantULNGUUPTNLInformation                int64 = 195
	ProtocolIEIDTSCTrafficCharacteristics                     int64 = 196
	ProtocolIEIDRedundantPDUSessionInformation                int64 = 197
	ProtocolIEIDUsedRSNInformation                            int64 = 198
	ProtocolIEIDIABAuthorized                                 int64 = 199
	ProtocolIEIDIABSupported                                  int64 = 200
	ProtocolIEIDIABNodeIndication                             int64 = 201
	ProtocolIEIDNBIoTPagingDRX                                int64 = 202
	ProtocolIEIDNBIoTPagingEDRXInfo                           int64 = 203
	ProtocolIEIDNBIoTDefaultPagingDRX                         int64 = 204
	ProtocolIEIDEnhancedCoverageRestriction                   int64 = 205
	ProtocolIEIDExtendedConnectedTime                         int64 = 206
	ProtocolIEIDPagingAssisDataforCEcapabUE                   int64 = 207
	ProtocolIEIDWUSAssistanceInformation                      int64 = 208
	ProtocolIEIDUEDifferentiationInfo                         int64 = 209
	ProtocolIEIDNBIoTUEPriority                               int64 = 210
	ProtocolIEIDULCPSecurityInformation                       int64 = 211
	ProtocolIEIDDLCPSecurityInformation                       int64 = 212
	ProtocolIEIDTAI                                           int64 = 213
	ProtocolIEIDUERadioCapabilityForPagingOfNBIoT             int64 = 214
	ProtocolIEIDLTEV2XServicesAuthorized                      int64 = 215
	ProtocolIEIDNRV2XServicesAuthorized                       int64 = 216
	ProtocolIEIDLTEUESidelinkAggregateMaximumBitrate          int64 = 217
	ProtocolIEIDNRUESidelinkAggregateMaximumBitrate           int64 = 218
	ProtocolIEIDPC5QoSParameters                              int64 = 219
	ProtocolIEIDAlternativeQoSParaSetList                     int64 = 220
	ProtocolIEIDCurrentQoSParaSetIndex                        int64 = 221
	ProtocolIEIDCEmodeBrestricted                             int64 = 222
	ProtocolIEIDPagingeDRXInformation                         int64 = 223
	ProtocolIEIDCEmodeBSupportIndicator                       int64 = 224
	ProtocolIEIDLTEMIndication                                int64 = 225
	ProtocolIEIDEndIndication                                 int64 = 226
	ProtocolIEIDEDTSession                                    int64 = 227
	ProtocolIEIDUECapabilityInfoRequest                       int64 = 228
	ProtocolIEIDPDUSessionResourceFailedToResumeListRESReq    int64 = 229
	ProtocolIEIDPDUSessionResourceFailedToResumeListRESRes    int64 = 230
	ProtocolIEIDPDUSessionResourceSuspendListSUSReq           int64 = 231
	ProtocolIEIDPDUSessionResourceResumeListRESReq            int64 = 232
	ProtocolIEIDPDUSessionResourceResumeListRESRes            int64 = 233
	ProtocolIEIDUEUPCIoTSupport                               int64 = 234
	ProtocolIEIDSuspendRequestIndication                      int64 = 235
	ProtocolIEIDSuspendResponseIndication                     int64 = 236
	ProtocolIEIDRRCResumeCause                                int64 = 237
	ProtocolIEIDRGLevelWirelineAccessCharacteristics          int64 = 238
	ProtocolIEIDWAGFIdentityInformation                       int64 = 239
	ProtocolIEIDGlobalTNGFID                                  int64 = 240
	ProtocolIEIDGlobalTWIFID                                  int64 = 241
	ProtocolIEIDGlobalWAGFID                                  int64 = 242
	ProtocolIEIDUserLocationInformationWAGF                   int64 = 243
	ProtocolIEIDUserLocationInformationTNGF                   int64 = 244
	ProtocolIEIDAuthenticatedIndication                       int64 = 245
	ProtocolIEIDTNGFIdentityInformation                       int64 = 246
	ProtocolIEIDTWIFIdentityInformation                       int64 = 247
	ProtocolIEIDUserLocationInformationTWIF                   int64 = 248
	ProtocolIEIDDataForwardingResponseERABList                int64 = 249
	ProtocolIEIDIntersystemSONConfigurationTransferDL         int64 = 250
	ProtocolIEIDIntersystemSONConfigurationTransferUL         int64 = 251
	ProtocolIEIDSONInformationReport                          int64 = 252
	ProtocolIEIDUEHistoryInformationFromTheUE                 int64 = 253
	ProtocolIEIDManagementBasedMDTPLMNList                    int64 = 254
	ProtocolIEIDMDTConfiguration                              int64 = 255
	ProtocolIEIDPrivacyIndicator                              int64 = 256
	ProtocolIEIDTraceCollectionEntityURI                      int64 = 257
	ProtocolIEIDNPNSupport                                    int64 = 258
	ProtocolIEIDNPNAccessInformation                          int64 = 259
	ProtocolIEIDNPNPagingAssistanceInformation                int64 = 260
	ProtocolIEIDNPNMobilityInformation                        int64 = 261
	ProtocolIEIDTargettoSourceFailureTransparentContainer     int64 = 262
	ProtocolIEIDNID                                           int64 = 263
	ProtocolIEIDUERadioCapabilityID                           int64 = 264
	ProtocolIEIDUERadioCapabilityEUTRAFormat                  int64 = 265
	ProtocolIEIDDAPSRequestInfo                               int64 = 266
	ProtocolIEIDDAPSResponseInfoList                          int64 = 267
	ProtocolIEIDEarlyStatusTransferTransparentContainer       int64 = 268
	ProtocolIEIDNotifySourceNGRANNode                         int64 = 269
	ProtocolIEIDExtendedSliceSupportList                      int64 = 270
	ProtocolIEIDExtendedTAISliceSupportList                   int64 = 271
	ProtocolIEIDConfiguredTACIndication                       int64 = 272
	ProtocolIEIDExtendedRANNodeName                           int64 = 273
	ProtocolIEIDExtendedAMFName                               int64 = 274
)

package ngapType

type ProtocolExtensionContainerAdditionalDLUPTNLInformationForHOItemExtIEs struct {
	List []AdditionalDLUPTNLInformationForHOItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AdditionalDLUPTNLInformationForHOItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AdditionalDLUPTNLInformationForHOItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AdditionalDLUPTNLInformationForHOItemExtIEsPresentNothing int = iota /* No components present */
	AdditionalDLUPTNLInformationForHOItemExtIEsPresentAdditionalRedundantDLNGUUPTNLInformation
)

type AdditionalDLUPTNLInformationForHOItemExtIEsExtensionValue struct {
	Present                                  int
	AdditionalRedundantDLNGUUPTNLInformation *UPTransportLayerInformation `vht5gc:"referenceFieldValue:183,valueLB:0,valueUB:1"`
}

type ProtocolExtensionContainerAllocationAndRetentionPriorityExtIEs struct {
	List []AllocationAndRetentionPriorityExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AllocationAndRetentionPriorityExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AllocationAndRetentionPriorityExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AllocationAndRetentionPriorityExtIEsPresentNothing int = iota /* No components present */
)

type AllocationAndRetentionPriorityExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAllowedNSSAIItemExtIEs struct {
	List []AllowedNSSAIItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AllowedNSSAIItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AllowedNSSAIItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AllowedNSSAIItemExtIEsPresentNothing int = iota /* No components present */
)

type AllowedNSSAIItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAllowedPNINPNItemExtIEs struct {
	List []AllowedPNINPNItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AllowedPNINPNItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AllowedPNINPNItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AllowedPNINPNItemExtIEsPresentNothing int = iota /* No components present */
)

type AllowedPNINPNItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAlternativeQoSParaSetItemExtIEs struct {
	List []AlternativeQoSParaSetItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AlternativeQoSParaSetItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AlternativeQoSParaSetItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AlternativeQoSParaSetItemExtIEsPresentNothing int = iota /* No components present */
)

type AlternativeQoSParaSetItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAMFTNLAssociationSetupItemExtIEs struct {
	List []AMFTNLAssociationSetupItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AMFTNLAssociationSetupItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AMFTNLAssociationSetupItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AMFTNLAssociationSetupItemExtIEsPresentNothing int = iota /* No components present */
)

type AMFTNLAssociationSetupItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAMFTNLAssociationToAddItemExtIEs struct {
	List []AMFTNLAssociationToAddItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AMFTNLAssociationToAddItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AMFTNLAssociationToAddItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AMFTNLAssociationToAddItemExtIEsPresentNothing int = iota /* No components present */
)

type AMFTNLAssociationToAddItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAMFTNLAssociationToRemoveItemExtIEs struct {
	List []AMFTNLAssociationToRemoveItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AMFTNLAssociationToRemoveItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AMFTNLAssociationToRemoveItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AMFTNLAssociationToRemoveItemExtIEsPresentNothing int = iota /* No components present */
	AMFTNLAssociationToRemoveItemExtIEsPresentTNLAssociationTransportLayerAddressNGRAN
)

type AMFTNLAssociationToRemoveItemExtIEsExtensionValue struct {
	Present                                  int
	TNLAssociationTransportLayerAddressNGRAN *CPTransportLayerInformation `vht5gc:"referenceFieldValue:168,valueLB:0,valueUB:1"`
}

type ProtocolExtensionContainerAMFTNLAssociationToUpdateItemExtIEs struct {
	List []AMFTNLAssociationToUpdateItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AMFTNLAssociationToUpdateItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AMFTNLAssociationToUpdateItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AMFTNLAssociationToUpdateItemExtIEsPresentNothing int = iota /* No components present */
)

type AMFTNLAssociationToUpdateItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAreaOfInterestExtIEs struct {
	List []AreaOfInterestExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AreaOfInterestExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AreaOfInterestExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AreaOfInterestExtIEsPresentNothing int = iota /* No components present */
)

type AreaOfInterestExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAreaOfInterestCellItemExtIEs struct {
	List []AreaOfInterestCellItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AreaOfInterestCellItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AreaOfInterestCellItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AreaOfInterestCellItemExtIEsPresentNothing int = iota /* No components present */
)

type AreaOfInterestCellItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAreaOfInterestItemExtIEs struct {
	List []AreaOfInterestItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AreaOfInterestItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AreaOfInterestItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AreaOfInterestItemExtIEsPresentNothing int = iota /* No components present */
)

type AreaOfInterestItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAreaOfInterestRANNodeItemExtIEs struct {
	List []AreaOfInterestRANNodeItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AreaOfInterestRANNodeItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AreaOfInterestRANNodeItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AreaOfInterestRANNodeItemExtIEsPresentNothing int = iota /* No components present */
)

type AreaOfInterestRANNodeItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAreaOfInterestTAIItemExtIEs struct {
	List []AreaOfInterestTAIItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AreaOfInterestTAIItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AreaOfInterestTAIItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AreaOfInterestTAIItemExtIEsPresentNothing int = iota /* No components present */
)

type AreaOfInterestTAIItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAssistanceDataForPagingExtIEs struct {
	List []AssistanceDataForPagingExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AssistanceDataForPagingExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AssistanceDataForPagingExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AssistanceDataForPagingExtIEsPresentNothing int = iota /* No components present */
	AssistanceDataForPagingExtIEsPresentNPNPagingAssistanceInformation
	AssistanceDataForPagingExtIEsPresentPagingAssisDataforCEcapabUE
)

type AssistanceDataForPagingExtIEsExtensionValue struct {
	Present                        int
	NPNPagingAssistanceInformation *NPNPagingAssistanceInformation `vht5gc:"referenceFieldValue:260,valueLB:0,valueUB:1"`
	PagingAssisDataforCEcapabUE    *PagingAssisDataforCEcapabUE    `vht5gc:"valueExt,referenceFieldValue:207"`
}

type ProtocolExtensionContainerAssistanceDataForRecommendedCellsExtIEs struct {
	List []AssistanceDataForRecommendedCellsExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AssistanceDataForRecommendedCellsExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AssistanceDataForRecommendedCellsExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AssistanceDataForRecommendedCellsExtIEsPresentNothing int = iota /* No components present */
)

type AssistanceDataForRecommendedCellsExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerAssociatedQosFlowItemExtIEs struct {
	List []AssociatedQosFlowItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AssociatedQosFlowItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AssociatedQosFlowItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AssociatedQosFlowItemExtIEsPresentNothing int = iota /* No components present */
	AssociatedQosFlowItemExtIEsPresentCurrentQoSParaSetIndex
)

type AssociatedQosFlowItemExtIEsExtensionValue struct {
	Present                int
	CurrentQoSParaSetIndex *AlternativeQoSParaSetIndex `vht5gc:"referenceFieldValue:221"`
}

type ProtocolExtensionContainerAreaScopeOfNeighCellsItemExtIEs struct {
	List []AreaScopeOfNeighCellsItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type AreaScopeOfNeighCellsItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue AreaScopeOfNeighCellsItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	AreaScopeOfNeighCellsItemExtIEsPresentNothing int = iota /* No components present */
)

type AreaScopeOfNeighCellsItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerBroadcastPLMNItemExtIEs struct {
	List []BroadcastPLMNItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type BroadcastPLMNItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue BroadcastPLMNItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	BroadcastPLMNItemExtIEsPresentNothing int = iota /* No components present */
	BroadcastPLMNItemExtIEsPresentNPNSupport
	BroadcastPLMNItemExtIEsPresentExtendedTAISliceSupportList
)

type BroadcastPLMNItemExtIEsExtensionValue struct {
	Present                     int
	NPNSupport                  *NPNSupport               `vht5gc:"referenceFieldValue:258,valueLB:0,valueUB:1"`
	ExtendedTAISliceSupportList *ExtendedSliceSupportList `vht5gc:"referenceFieldValue:271"`
}

type ProtocolExtensionContainerBluetoothMeasurementConfigurationExtIEs struct {
	List []BluetoothMeasurementConfigurationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type BluetoothMeasurementConfigurationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue BluetoothMeasurementConfigurationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	BluetoothMeasurementConfigurationExtIEsPresentNothing int = iota /* No components present */
)

type BluetoothMeasurementConfigurationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerBluetoothMeasConfigNameItemExtIEs struct {
	List []BluetoothMeasConfigNameItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type BluetoothMeasConfigNameItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue BluetoothMeasConfigNameItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	BluetoothMeasConfigNameItemExtIEsPresentNothing int = iota /* No components present */
)

type BluetoothMeasConfigNameItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCancelledCellsInEAIEUTRAItemExtIEs struct {
	List []CancelledCellsInEAIEUTRAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CancelledCellsInEAIEUTRAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CancelledCellsInEAIEUTRAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CancelledCellsInEAIEUTRAItemExtIEsPresentNothing int = iota /* No components present */
)

type CancelledCellsInEAIEUTRAItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCancelledCellsInEAINRItemExtIEs struct {
	List []CancelledCellsInEAINRItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CancelledCellsInEAINRItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CancelledCellsInEAINRItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CancelledCellsInEAINRItemExtIEsPresentNothing int = iota /* No components present */
)

type CancelledCellsInEAINRItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCancelledCellsInTAIEUTRAItemExtIEs struct {
	List []CancelledCellsInTAIEUTRAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CancelledCellsInTAIEUTRAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CancelledCellsInTAIEUTRAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CancelledCellsInTAIEUTRAItemExtIEsPresentNothing int = iota /* No components present */
)

type CancelledCellsInTAIEUTRAItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCancelledCellsInTAINRItemExtIEs struct {
	List []CancelledCellsInTAINRItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CancelledCellsInTAINRItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CancelledCellsInTAINRItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CancelledCellsInTAINRItemExtIEsPresentNothing int = iota /* No components present */
)

type CancelledCellsInTAINRItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCandidateCellItemExtIEs struct {
	List []CandidateCellItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CandidateCellItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CandidateCellItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CandidateCellItemExtIEsPresentNothing int = iota /* No components present */
)

type CandidateCellItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCandidateCellIDExtIEs struct {
	List []CandidateCellIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CandidateCellIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CandidateCellIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CandidateCellIDExtIEsPresentNothing int = iota /* No components present */
)

type CandidateCellIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCandidatePCIExtIEs struct {
	List []CandidatePCIExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CandidatePCIExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CandidatePCIExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CandidatePCIExtIEsPresentNothing int = iota /* No components present */
)

type CandidatePCIExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCellCAGInformationExtIEs struct {
	List []CellCAGInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CellCAGInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CellCAGInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CellCAGInformationExtIEsPresentNothing int = iota /* No components present */
)

type CellCAGInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCellIDBroadcastEUTRAItemExtIEs struct {
	List []CellIDBroadcastEUTRAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CellIDBroadcastEUTRAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CellIDBroadcastEUTRAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CellIDBroadcastEUTRAItemExtIEsPresentNothing int = iota /* No components present */
)

type CellIDBroadcastEUTRAItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCellIDBroadcastNRItemExtIEs struct {
	List []CellIDBroadcastNRItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CellIDBroadcastNRItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CellIDBroadcastNRItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CellIDBroadcastNRItemExtIEsPresentNothing int = iota /* No components present */
)

type CellIDBroadcastNRItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCellIDCancelledEUTRAItemExtIEs struct {
	List []CellIDCancelledEUTRAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CellIDCancelledEUTRAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CellIDCancelledEUTRAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CellIDCancelledEUTRAItemExtIEsPresentNothing int = iota /* No components present */
)

type CellIDCancelledEUTRAItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCellIDCancelledNRItemExtIEs struct {
	List []CellIDCancelledNRItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CellIDCancelledNRItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CellIDCancelledNRItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CellIDCancelledNRItemExtIEsPresentNothing int = iota /* No components present */
)

type CellIDCancelledNRItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCellTypeExtIEs struct {
	List []CellTypeExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CellTypeExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CellTypeExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CellTypeExtIEsPresentNothing int = iota /* No components present */
)

type CellTypeExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCNAssistedRANTuningExtIEs struct {
	List []CNAssistedRANTuningExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CNAssistedRANTuningExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CNAssistedRANTuningExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CNAssistedRANTuningExtIEsPresentNothing int = iota /* No components present */
)

type CNAssistedRANTuningExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCNTypeRestrictionsForEquivalentItemExtIEs struct {
	List []CNTypeRestrictionsForEquivalentItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CNTypeRestrictionsForEquivalentItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CNTypeRestrictionsForEquivalentItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CNTypeRestrictionsForEquivalentItemExtIEsPresentNothing int = iota /* No components present */
)

type CNTypeRestrictionsForEquivalentItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCompletedCellsInEAIEUTRAItemExtIEs struct {
	List []CompletedCellsInEAIEUTRAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CompletedCellsInEAIEUTRAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CompletedCellsInEAIEUTRAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CompletedCellsInEAIEUTRAItemExtIEsPresentNothing int = iota /* No components present */
)

type CompletedCellsInEAIEUTRAItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCompletedCellsInEAINRItemExtIEs struct {
	List []CompletedCellsInEAINRItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CompletedCellsInEAINRItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CompletedCellsInEAINRItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CompletedCellsInEAINRItemExtIEsPresentNothing int = iota /* No components present */
)

type CompletedCellsInEAINRItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCompletedCellsInTAIEUTRAItemExtIEs struct {
	List []CompletedCellsInTAIEUTRAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CompletedCellsInTAIEUTRAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CompletedCellsInTAIEUTRAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CompletedCellsInTAIEUTRAItemExtIEsPresentNothing int = iota /* No components present */
)

type CompletedCellsInTAIEUTRAItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCompletedCellsInTAINRItemExtIEs struct {
	List []CompletedCellsInTAINRItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CompletedCellsInTAINRItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CompletedCellsInTAINRItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CompletedCellsInTAINRItemExtIEsPresentNothing int = iota /* No components present */
)

type CompletedCellsInTAINRItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCoreNetworkAssistanceInformationForInactiveExtIEs struct {
	List []CoreNetworkAssistanceInformationForInactiveExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CoreNetworkAssistanceInformationForInactiveExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CoreNetworkAssistanceInformationForInactiveExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CoreNetworkAssistanceInformationForInactiveExtIEsPresentNothing int = iota /* No components present */
	CoreNetworkAssistanceInformationForInactiveExtIEsPresentPagingeDRXInformation
)

type CoreNetworkAssistanceInformationForInactiveExtIEsExtensionValue struct {
	Present               int
	PagingeDRXInformation *PagingeDRXInformation `vht5gc:"valueExt,referenceFieldValue:223"`
}

type ProtocolExtensionContainerCOUNTValueForPDCPSN12ExtIEs struct {
	List []COUNTValueForPDCPSN12ExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type COUNTValueForPDCPSN12ExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue COUNTValueForPDCPSN12ExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	COUNTValueForPDCPSN12ExtIEsPresentNothing int = iota /* No components present */
)

type COUNTValueForPDCPSN12ExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCOUNTValueForPDCPSN18ExtIEs struct {
	List []COUNTValueForPDCPSN18ExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type COUNTValueForPDCPSN18ExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue COUNTValueForPDCPSN18ExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	COUNTValueForPDCPSN18ExtIEsPresentNothing int = iota /* No components present */
)

type COUNTValueForPDCPSN18ExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCriticalityDiagnosticsExtIEs struct {
	List []CriticalityDiagnosticsExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CriticalityDiagnosticsExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CriticalityDiagnosticsExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CriticalityDiagnosticsExtIEsPresentNothing int = iota /* No components present */
)

type CriticalityDiagnosticsExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCriticalityDiagnosticsIEItemExtIEs struct {
	List []CriticalityDiagnosticsIEItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CriticalityDiagnosticsIEItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CriticalityDiagnosticsIEItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CriticalityDiagnosticsIEItemExtIEsPresentNothing int = iota /* No components present */
)

type CriticalityDiagnosticsIEItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCellBasedMDTNRExtIEs struct {
	List []CellBasedMDTNRExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CellBasedMDTNRExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CellBasedMDTNRExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CellBasedMDTNRExtIEsPresentNothing int = iota /* No components present */
)

type CellBasedMDTNRExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerCellBasedMDTEUTRAExtIEs struct {
	List []CellBasedMDTEUTRAExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type CellBasedMDTEUTRAExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue CellBasedMDTEUTRAExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	CellBasedMDTEUTRAExtIEsPresentNothing int = iota /* No components present */
)

type CellBasedMDTEUTRAExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDataForwardingResponseDRBItemExtIEs struct {
	List []DataForwardingResponseDRBItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DataForwardingResponseDRBItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DataForwardingResponseDRBItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DataForwardingResponseDRBItemExtIEsPresentNothing int = iota /* No components present */
)

type DataForwardingResponseDRBItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDAPSRequestInfoExtIEs struct {
	List []DAPSRequestInfoExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DAPSRequestInfoExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DAPSRequestInfoExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DAPSRequestInfoExtIEsPresentNothing int = iota /* No components present */
)

type DAPSRequestInfoExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDAPSResponseInfoItemExtIEs struct {
	List []DAPSResponseInfoItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DAPSResponseInfoItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DAPSResponseInfoItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DAPSResponseInfoItemExtIEsPresentNothing int = iota /* No components present */
)

type DAPSResponseInfoItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDAPSResponseInfoExtIEs struct {
	List []DAPSResponseInfoExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DAPSResponseInfoExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DAPSResponseInfoExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DAPSResponseInfoExtIEsPresentNothing int = iota /* No components present */
)

type DAPSResponseInfoExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDataForwardingResponseERABListItemExtIEs struct {
	List []DataForwardingResponseERABListItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DataForwardingResponseERABListItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DataForwardingResponseERABListItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DataForwardingResponseERABListItemExtIEsPresentNothing int = iota /* No components present */
)

type DataForwardingResponseERABListItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDLCPSecurityInformationExtIEs struct {
	List []DLCPSecurityInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DLCPSecurityInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DLCPSecurityInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DLCPSecurityInformationExtIEsPresentNothing int = iota /* No components present */
)

type DLCPSecurityInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDRBsSubjectToStatusTransferItemExtIEs struct {
	List []DRBsSubjectToStatusTransferItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DRBsSubjectToStatusTransferItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DRBsSubjectToStatusTransferItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DRBsSubjectToStatusTransferItemExtIEsPresentNothing int = iota /* No components present */
	DRBsSubjectToStatusTransferItemExtIEsPresentOldAssociatedQosFlowListULendmarkerexpected
)

type DRBsSubjectToStatusTransferItemExtIEsExtensionValue struct {
	Present                                     int
	OldAssociatedQosFlowListULendmarkerexpected *AssociatedQosFlowList `vht5gc:"referenceFieldValue:159"`
}

type ProtocolExtensionContainerDRBStatusDL12ExtIEs struct {
	List []DRBStatusDL12ExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DRBStatusDL12ExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DRBStatusDL12ExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DRBStatusDL12ExtIEsPresentNothing int = iota /* No components present */
)

type DRBStatusDL12ExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDRBStatusDL18ExtIEs struct {
	List []DRBStatusDL18ExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DRBStatusDL18ExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DRBStatusDL18ExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DRBStatusDL18ExtIEsPresentNothing int = iota /* No components present */
)

type DRBStatusDL18ExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDRBStatusUL12ExtIEs struct {
	List []DRBStatusUL12ExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DRBStatusUL12ExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DRBStatusUL12ExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DRBStatusUL12ExtIEsPresentNothing int = iota /* No components present */
)

type DRBStatusUL12ExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDRBStatusUL18ExtIEs struct {
	List []DRBStatusUL18ExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DRBStatusUL18ExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DRBStatusUL18ExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DRBStatusUL18ExtIEsPresentNothing int = iota /* No components present */
)

type DRBStatusUL18ExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDRBsToQosFlowsMappingItemExtIEs struct {
	List []DRBsToQosFlowsMappingItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DRBsToQosFlowsMappingItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DRBsToQosFlowsMappingItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DRBsToQosFlowsMappingItemExtIEsPresentNothing int = iota /* No components present */
	DRBsToQosFlowsMappingItemExtIEsPresentDAPSRequestInfo
)

type DRBsToQosFlowsMappingItemExtIEsExtensionValue struct {
	Present         int
	DAPSRequestInfo *DAPSRequestInfo `vht5gc:"valueExt,referenceFieldValue:266"`
}

type ProtocolExtensionContainerDynamic5QIDescriptorExtIEs struct {
	List []Dynamic5QIDescriptorExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type Dynamic5QIDescriptorExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue Dynamic5QIDescriptorExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	Dynamic5QIDescriptorExtIEsPresentNothing int = iota /* No components present */
	Dynamic5QIDescriptorExtIEsPresentExtendedPacketDelayBudget
	Dynamic5QIDescriptorExtIEsPresentCNPacketDelayBudgetDL
	Dynamic5QIDescriptorExtIEsPresentCNPacketDelayBudgetUL
)

type Dynamic5QIDescriptorExtIEsExtensionValue struct {
	Present                   int
	ExtendedPacketDelayBudget *ExtendedPacketDelayBudget `vht5gc:"referenceFieldValue:189"`
	CNPacketDelayBudgetDL     *ExtendedPacketDelayBudget `vht5gc:"referenceFieldValue:187"`
	CNPacketDelayBudgetUL     *ExtendedPacketDelayBudget `vht5gc:"referenceFieldValue:188"`
}

type ProtocolExtensionContainerEarlyStatusTransferTransparentContainerExtIEs struct {
	List []EarlyStatusTransferTransparentContainerExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type EarlyStatusTransferTransparentContainerExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue EarlyStatusTransferTransparentContainerExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	EarlyStatusTransferTransparentContainerExtIEsPresentNothing int = iota /* No components present */
)

type EarlyStatusTransferTransparentContainerExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerFirstDLCountExtIEs struct {
	List []FirstDLCountExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type FirstDLCountExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue FirstDLCountExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	FirstDLCountExtIEsPresentNothing int = iota /* No components present */
)

type FirstDLCountExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerDRBsSubjectToEarlyStatusTransferItemExtIEs struct {
	List []DRBsSubjectToEarlyStatusTransferItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type DRBsSubjectToEarlyStatusTransferItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue DRBsSubjectToEarlyStatusTransferItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	DRBsSubjectToEarlyStatusTransferItemExtIEsPresentNothing int = iota /* No components present */
)

type DRBsSubjectToEarlyStatusTransferItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerEmergencyAreaIDBroadcastEUTRAItemExtIEs struct {
	List []EmergencyAreaIDBroadcastEUTRAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type EmergencyAreaIDBroadcastEUTRAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue EmergencyAreaIDBroadcastEUTRAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	EmergencyAreaIDBroadcastEUTRAItemExtIEsPresentNothing int = iota /* No components present */
)

type EmergencyAreaIDBroadcastEUTRAItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerEmergencyAreaIDBroadcastNRItemExtIEs struct {
	List []EmergencyAreaIDBroadcastNRItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type EmergencyAreaIDBroadcastNRItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue EmergencyAreaIDBroadcastNRItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	EmergencyAreaIDBroadcastNRItemExtIEsPresentNothing int = iota /* No components present */
)

type EmergencyAreaIDBroadcastNRItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerEmergencyAreaIDCancelledEUTRAItemExtIEs struct {
	List []EmergencyAreaIDCancelledEUTRAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type EmergencyAreaIDCancelledEUTRAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue EmergencyAreaIDCancelledEUTRAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	EmergencyAreaIDCancelledEUTRAItemExtIEsPresentNothing int = iota /* No components present */
)

type EmergencyAreaIDCancelledEUTRAItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerEmergencyAreaIDCancelledNRItemExtIEs struct {
	List []EmergencyAreaIDCancelledNRItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type EmergencyAreaIDCancelledNRItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue EmergencyAreaIDCancelledNRItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	EmergencyAreaIDCancelledNRItemExtIEsPresentNothing int = iota /* No components present */
)

type EmergencyAreaIDCancelledNRItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerEmergencyFallbackIndicatorExtIEs struct {
	List []EmergencyFallbackIndicatorExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type EmergencyFallbackIndicatorExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue EmergencyFallbackIndicatorExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	EmergencyFallbackIndicatorExtIEsPresentNothing int = iota /* No components present */
)

type EmergencyFallbackIndicatorExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerEndpointIPAddressAndPortExtIEs struct {
	List []EndpointIPAddressAndPortExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type EndpointIPAddressAndPortExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue EndpointIPAddressAndPortExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	EndpointIPAddressAndPortExtIEsPresentNothing int = iota /* No components present */
)

type EndpointIPAddressAndPortExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerEPSTAIExtIEs struct {
	List []EPSTAIExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type EPSTAIExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue EPSTAIExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	EPSTAIExtIEsPresentNothing int = iota /* No components present */
)

type EPSTAIExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerERABInformationItemExtIEs struct {
	List []ERABInformationItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ERABInformationItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ERABInformationItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ERABInformationItemExtIEsPresentNothing int = iota /* No components present */
)

type ERABInformationItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerEUTRACGIExtIEs struct {
	List []EUTRACGIExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type EUTRACGIExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue EUTRACGIExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	EUTRACGIExtIEsPresentNothing int = iota /* No components present */
)

type EUTRACGIExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerExpectedUEActivityBehaviourExtIEs struct {
	List []ExpectedUEActivityBehaviourExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ExpectedUEActivityBehaviourExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ExpectedUEActivityBehaviourExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ExpectedUEActivityBehaviourExtIEsPresentNothing int = iota /* No components present */
)

type ExpectedUEActivityBehaviourExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerExpectedUEBehaviourExtIEs struct {
	List []ExpectedUEBehaviourExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ExpectedUEBehaviourExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ExpectedUEBehaviourExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ExpectedUEBehaviourExtIEsPresentNothing int = iota /* No components present */
)

type ExpectedUEBehaviourExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerExpectedUEMovingTrajectoryItemExtIEs struct {
	List []ExpectedUEMovingTrajectoryItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ExpectedUEMovingTrajectoryItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ExpectedUEMovingTrajectoryItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ExpectedUEMovingTrajectoryItemExtIEsPresentNothing int = iota /* No components present */
)

type ExpectedUEMovingTrajectoryItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerExtendedAMFNameExtIEs struct {
	List []ExtendedAMFNameExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ExtendedAMFNameExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ExtendedAMFNameExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ExtendedAMFNameExtIEsPresentNothing int = iota /* No components present */
)

type ExtendedAMFNameExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerExtendedRANNodeNameExtIEs struct {
	List []ExtendedRANNodeNameExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ExtendedRANNodeNameExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ExtendedRANNodeNameExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ExtendedRANNodeNameExtIEsPresentNothing int = iota /* No components present */
)

type ExtendedRANNodeNameExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerExtendedRATRestrictionInformationExtIEs struct {
	List []ExtendedRATRestrictionInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ExtendedRATRestrictionInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ExtendedRATRestrictionInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ExtendedRATRestrictionInformationExtIEsPresentNothing int = iota /* No components present */
)

type ExtendedRATRestrictionInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerEventL1LoggedMDTConfigExtIEs struct {
	List []EventL1LoggedMDTConfigExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type EventL1LoggedMDTConfigExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue EventL1LoggedMDTConfigExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	EventL1LoggedMDTConfigExtIEsPresentNothing int = iota /* No components present */
)

type EventL1LoggedMDTConfigExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerFailureIndicationExtIEs struct {
	List []FailureIndicationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type FailureIndicationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue FailureIndicationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	FailureIndicationExtIEsPresentNothing int = iota /* No components present */
)

type FailureIndicationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerFiveGSTMSIExtIEs struct {
	List []FiveGSTMSIExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type FiveGSTMSIExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue FiveGSTMSIExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	FiveGSTMSIExtIEsPresentNothing int = iota /* No components present */
)

type FiveGSTMSIExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerForbiddenAreaInformationItemExtIEs struct {
	List []ForbiddenAreaInformationItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ForbiddenAreaInformationItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ForbiddenAreaInformationItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ForbiddenAreaInformationItemExtIEsPresentNothing int = iota /* No components present */
)

type ForbiddenAreaInformationItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerFromEUTRANtoNGRANExtIEs struct {
	List []FromEUTRANtoNGRANExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type FromEUTRANtoNGRANExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue FromEUTRANtoNGRANExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	FromEUTRANtoNGRANExtIEsPresentNothing int = iota /* No components present */
)

type FromEUTRANtoNGRANExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerFromNGRANtoEUTRANExtIEs struct {
	List []FromNGRANtoEUTRANExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type FromNGRANtoEUTRANExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue FromNGRANtoEUTRANExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	FromNGRANtoEUTRANExtIEsPresentNothing int = iota /* No components present */
)

type FromNGRANtoEUTRANExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerGBRQosInformationExtIEs struct {
	List []GBRQosInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GBRQosInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue GBRQosInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GBRQosInformationExtIEsPresentNothing int = iota /* No components present */
	GBRQosInformationExtIEsPresentAlternativeQoSParaSetList
)

type GBRQosInformationExtIEsExtensionValue struct {
	Present                   int
	AlternativeQoSParaSetList *AlternativeQoSParaSetList `vht5gc:"referenceFieldValue:220"`
}

type ProtocolExtensionContainerGlobalENBIDExtIEs struct {
	List []GlobalENBIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GlobalENBIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue GlobalENBIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GlobalENBIDExtIEsPresentNothing int = iota /* No components present */
)

type GlobalENBIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerGlobalGNBIDExtIEs struct {
	List []GlobalGNBIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GlobalGNBIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue GlobalGNBIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GlobalGNBIDExtIEsPresentNothing int = iota /* No components present */
)

type GlobalGNBIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerGlobalN3IWFIDExtIEs struct {
	List []GlobalN3IWFIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GlobalN3IWFIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue GlobalN3IWFIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GlobalN3IWFIDExtIEsPresentNothing int = iota /* No components present */
)

type GlobalN3IWFIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerGlobalLineIDExtIEs struct {
	List []GlobalLineIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GlobalLineIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue GlobalLineIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GlobalLineIDExtIEsPresentNothing int = iota /* No components present */
)

type GlobalLineIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerGlobalNgENBIDExtIEs struct {
	List []GlobalNgENBIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GlobalNgENBIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue GlobalNgENBIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GlobalNgENBIDExtIEsPresentNothing int = iota /* No components present */
)

type GlobalNgENBIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerGlobalTNGFIDExtIEs struct {
	List []GlobalTNGFIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GlobalTNGFIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue GlobalTNGFIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GlobalTNGFIDExtIEsPresentNothing int = iota /* No components present */
)

type GlobalTNGFIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerGlobalTWIFIDExtIEs struct {
	List []GlobalTWIFIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GlobalTWIFIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue GlobalTWIFIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GlobalTWIFIDExtIEsPresentNothing int = iota /* No components present */
)

type GlobalTWIFIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerGlobalWAGFIDExtIEs struct {
	List []GlobalWAGFIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GlobalWAGFIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue GlobalWAGFIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GlobalWAGFIDExtIEsPresentNothing int = iota /* No components present */
)

type GlobalWAGFIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerGTPTunnelExtIEs struct {
	List []GTPTunnelExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GTPTunnelExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue GTPTunnelExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GTPTunnelExtIEsPresentNothing int = iota /* No components present */
)

type GTPTunnelExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerGUAMIExtIEs struct {
	List []GUAMIExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type GUAMIExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue GUAMIExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	GUAMIExtIEsPresentNothing int = iota /* No components present */
)

type GUAMIExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerHandoverCommandTransferExtIEs struct {
	List []HandoverCommandTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type HandoverCommandTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue HandoverCommandTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverCommandTransferExtIEsPresentNothing int = iota /* No components present */
	HandoverCommandTransferExtIEsPresentAdditionalDLForwardingUPTNLInformation
	HandoverCommandTransferExtIEsPresentULForwardingUPTNLInformation
	HandoverCommandTransferExtIEsPresentAdditionalULForwardingUPTNLInformation
	HandoverCommandTransferExtIEsPresentDataForwardingResponseERABList
)

type HandoverCommandTransferExtIEsExtensionValue struct {
	Present                                int
	AdditionalDLForwardingUPTNLInformation *QosFlowPerTNLInformationList    `vht5gc:"referenceFieldValue:152"`
	ULForwardingUPTNLInformation           *UPTransportLayerInformation     `vht5gc:"referenceFieldValue:164,valueLB:0,valueUB:1"`
	AdditionalULForwardingUPTNLInformation *UPTransportLayerInformationList `vht5gc:"referenceFieldValue:172"`
	DataForwardingResponseERABList         *DataForwardingResponseERABList  `vht5gc:"referenceFieldValue:249"`
}

type ProtocolExtensionContainerHandoverPreparationUnsuccessfulTransferExtIEs struct {
	List []HandoverPreparationUnsuccessfulTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type HandoverPreparationUnsuccessfulTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue HandoverPreparationUnsuccessfulTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverPreparationUnsuccessfulTransferExtIEsPresentNothing int = iota /* No components present */
)

type HandoverPreparationUnsuccessfulTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerHandoverRequestAcknowledgeTransferExtIEs struct {
	List []HandoverRequestAcknowledgeTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type HandoverRequestAcknowledgeTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue HandoverRequestAcknowledgeTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverRequestAcknowledgeTransferExtIEsPresentNothing int = iota /* No components present */
	HandoverRequestAcknowledgeTransferExtIEsPresentAdditionalDLUPTNLInformationForHOList
	HandoverRequestAcknowledgeTransferExtIEsPresentULForwardingUPTNLInformation
	HandoverRequestAcknowledgeTransferExtIEsPresentAdditionalULForwardingUPTNLInformation
	HandoverRequestAcknowledgeTransferExtIEsPresentDataForwardingResponseERABList
	HandoverRequestAcknowledgeTransferExtIEsPresentRedundantDLNGUUPTNLInformation
	HandoverRequestAcknowledgeTransferExtIEsPresentUsedRSNInformation
	HandoverRequestAcknowledgeTransferExtIEsPresentGlobalRANNodeID
)

type HandoverRequestAcknowledgeTransferExtIEsExtensionValue struct {
	Present                                int
	AdditionalDLUPTNLInformationForHOList  *AdditionalDLUPTNLInformationForHOList `vht5gc:"referenceFieldValue:153"`
	ULForwardingUPTNLInformation           *UPTransportLayerInformation           `vht5gc:"referenceFieldValue:164,valueLB:0,valueUB:1"`
	AdditionalULForwardingUPTNLInformation *UPTransportLayerInformationList       `vht5gc:"referenceFieldValue:172"`
	DataForwardingResponseERABList         *DataForwardingResponseERABList        `vht5gc:"referenceFieldValue:249"`
	RedundantDLNGUUPTNLInformation         *UPTransportLayerInformation           `vht5gc:"referenceFieldValue:192,valueLB:0,valueUB:1"`
	UsedRSNInformation                     *RedundantPDUSessionInformation        `vht5gc:"valueExt,referenceFieldValue:198"`
	GlobalRANNodeID                        *GlobalRANNodeID                       `vht5gc:"referenceFieldValue:27,valueLB:0,valueUB:3"`
}

type ProtocolExtensionContainerHandoverRequiredTransferExtIEs struct {
	List []HandoverRequiredTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type HandoverRequiredTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue HandoverRequiredTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverRequiredTransferExtIEsPresentNothing int = iota /* No components present */
)

type HandoverRequiredTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerHandoverResourceAllocationUnsuccessfulTransferExtIEs struct {
	List []HandoverResourceAllocationUnsuccessfulTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type HandoverResourceAllocationUnsuccessfulTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue HandoverResourceAllocationUnsuccessfulTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HandoverResourceAllocationUnsuccessfulTransferExtIEsPresentNothing int = iota /* No components present */
)

type HandoverResourceAllocationUnsuccessfulTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerHOReportExtIEs struct {
	List []HOReportExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type HOReportExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue HOReportExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	HOReportExtIEsPresentNothing int = iota /* No components present */
)

type HOReportExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerInfoOnRecommendedCellsAndRANNodesForPagingExtIEs struct {
	List []InfoOnRecommendedCellsAndRANNodesForPagingExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type InfoOnRecommendedCellsAndRANNodesForPagingExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue InfoOnRecommendedCellsAndRANNodesForPagingExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	InfoOnRecommendedCellsAndRANNodesForPagingExtIEsPresentNothing int = iota /* No components present */
)

type InfoOnRecommendedCellsAndRANNodesForPagingExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerImmediateMDTNrExtIEs struct {
	List []ImmediateMDTNrExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ImmediateMDTNrExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ImmediateMDTNrExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ImmediateMDTNrExtIEsPresentNothing int = iota /* No components present */
)

type ImmediateMDTNrExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerInterSystemFailureIndicationExtIEs struct {
	List []InterSystemFailureIndicationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type InterSystemFailureIndicationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue InterSystemFailureIndicationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	InterSystemFailureIndicationExtIEsPresentNothing int = iota /* No components present */
)

type InterSystemFailureIndicationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerIntersystemSONConfigurationTransferExtIEs struct {
	List []IntersystemSONConfigurationTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type IntersystemSONConfigurationTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue IntersystemSONConfigurationTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	IntersystemSONConfigurationTransferExtIEsPresentNothing int = iota /* No components present */
)

type IntersystemSONConfigurationTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerIntersystemSONeNBIDExtIEs struct {
	List []IntersystemSONeNBIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type IntersystemSONeNBIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue IntersystemSONeNBIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	IntersystemSONeNBIDExtIEsPresentNothing int = iota /* No components present */
)

type IntersystemSONeNBIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerIntersystemSONNGRANnodeIDExtIEs struct {
	List []IntersystemSONNGRANnodeIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type IntersystemSONNGRANnodeIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue IntersystemSONNGRANnodeIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	IntersystemSONNGRANnodeIDExtIEsPresentNothing int = iota /* No components present */
)

type IntersystemSONNGRANnodeIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerInterSystemHOReportExtIEs struct {
	List []InterSystemHOReportExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type InterSystemHOReportExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue InterSystemHOReportExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	InterSystemHOReportExtIEsPresentNothing int = iota /* No components present */
)

type InterSystemHOReportExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerIntersystemUnnecessaryHOExtIEs struct {
	List []IntersystemUnnecessaryHOExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type IntersystemUnnecessaryHOExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue IntersystemUnnecessaryHOExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	IntersystemUnnecessaryHOExtIEsPresentNothing int = iota /* No components present */
)

type IntersystemUnnecessaryHOExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerLAIExtIEs struct {
	List []LAIExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type LAIExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue LAIExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LAIExtIEsPresentNothing int = iota /* No components present */
)

type LAIExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerLastVisitedCellItemExtIEs struct {
	List []LastVisitedCellItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type LastVisitedCellItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue LastVisitedCellItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LastVisitedCellItemExtIEsPresentNothing int = iota /* No components present */
)

type LastVisitedCellItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerLastVisitedNGRANCellInformationExtIEs struct {
	List []LastVisitedNGRANCellInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type LastVisitedNGRANCellInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue LastVisitedNGRANCellInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LastVisitedNGRANCellInformationExtIEsPresentNothing int = iota /* No components present */
)

type LastVisitedNGRANCellInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerLocationReportingRequestTypeExtIEs struct {
	List []LocationReportingRequestTypeExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type LocationReportingRequestTypeExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue LocationReportingRequestTypeExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LocationReportingRequestTypeExtIEsPresentNothing int = iota /* No components present */
	LocationReportingRequestTypeExtIEsPresentLocationReportingAdditionalInfo
)

type LocationReportingRequestTypeExtIEsExtensionValue struct {
	Present                         int
	LocationReportingAdditionalInfo *LocationReportingAdditionalInfo `vht5gc:"referenceFieldValue:170"`
}

type ProtocolExtensionContainerLoggedMDTNrExtIEs struct {
	List []LoggedMDTNrExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type LoggedMDTNrExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue LoggedMDTNrExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LoggedMDTNrExtIEsPresentNothing int = iota /* No components present */
)

type LoggedMDTNrExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerLTEV2XServicesAuthorizedExtIEs struct {
	List []LTEV2XServicesAuthorizedExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type LTEV2XServicesAuthorizedExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue LTEV2XServicesAuthorizedExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LTEV2XServicesAuthorizedExtIEsPresentNothing int = iota /* No components present */
)

type LTEV2XServicesAuthorizedExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerLTEUESidelinkAggregateMaximumBitratesExtIEs struct {
	List []LTEUESidelinkAggregateMaximumBitratesExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type LTEUESidelinkAggregateMaximumBitratesExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue LTEUESidelinkAggregateMaximumBitratesExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	LTEUESidelinkAggregateMaximumBitratesExtIEsPresentNothing int = iota /* No components present */
)

type LTEUESidelinkAggregateMaximumBitratesExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerMobilityRestrictionListExtIEs struct {
	List []MobilityRestrictionListExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type MobilityRestrictionListExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue MobilityRestrictionListExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	MobilityRestrictionListExtIEsPresentNothing int = iota /* No components present */
	MobilityRestrictionListExtIEsPresentLastEUTRANPLMNIdentity
	MobilityRestrictionListExtIEsPresentCNTypeRestrictionsForServing
	MobilityRestrictionListExtIEsPresentCNTypeRestrictionsForEquivalent
	MobilityRestrictionListExtIEsPresentNPNMobilityInformation
)

type MobilityRestrictionListExtIEsExtensionValue struct {
	Present                         int
	LastEUTRANPLMNIdentity          *PLMNIdentity                    `vht5gc:"referenceFieldValue:150"`
	CNTypeRestrictionsForServing    *CNTypeRestrictionsForServing    `vht5gc:"referenceFieldValue:161"`
	CNTypeRestrictionsForEquivalent *CNTypeRestrictionsForEquivalent `vht5gc:"referenceFieldValue:160"`
	NPNMobilityInformation          *NPNMobilityInformation          `vht5gc:"referenceFieldValue:261,valueLB:0,valueUB:2"`
}

type ProtocolExtensionContainerMDTConfigurationExtIEs struct {
	List []MDTConfigurationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type MDTConfigurationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue MDTConfigurationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	MDTConfigurationExtIEsPresentNothing int = iota /* No components present */
)

type MDTConfigurationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerMDTConfigurationNRExtIEs struct {
	List []MDTConfigurationNRExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type MDTConfigurationNRExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue MDTConfigurationNRExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	MDTConfigurationNRExtIEsPresentNothing int = iota /* No components present */
)

type MDTConfigurationNRExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerMDTConfigurationEUTRAExtIEs struct {
	List []MDTConfigurationEUTRAExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type MDTConfigurationEUTRAExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue MDTConfigurationEUTRAExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	MDTConfigurationEUTRAExtIEsPresentNothing int = iota /* No components present */
)

type MDTConfigurationEUTRAExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerM1ConfigurationExtIEs struct {
	List []M1ConfigurationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type M1ConfigurationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue M1ConfigurationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	M1ConfigurationExtIEsPresentNothing int = iota /* No components present */
)

type M1ConfigurationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerM1ThresholdEventA2ExtIEs struct {
	List []M1ThresholdEventA2ExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type M1ThresholdEventA2ExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue M1ThresholdEventA2ExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	M1ThresholdEventA2ExtIEsPresentNothing int = iota /* No components present */
)

type M1ThresholdEventA2ExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerM1PeriodicReportingExtIEs struct {
	List []M1PeriodicReportingExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type M1PeriodicReportingExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue M1PeriodicReportingExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	M1PeriodicReportingExtIEsPresentNothing int = iota /* No components present */
)

type M1PeriodicReportingExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerM4ConfigurationExtIEs struct {
	List []M4ConfigurationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type M4ConfigurationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue M4ConfigurationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	M4ConfigurationExtIEsPresentNothing int = iota /* No components present */
)

type M4ConfigurationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerM5ConfigurationExtIEs struct {
	List []M5ConfigurationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type M5ConfigurationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue M5ConfigurationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	M5ConfigurationExtIEsPresentNothing int = iota /* No components present */
)

type M5ConfigurationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerM6ConfigurationExtIEs struct {
	List []M6ConfigurationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type M6ConfigurationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue M6ConfigurationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	M6ConfigurationExtIEsPresentNothing int = iota /* No components present */
)

type M6ConfigurationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerM7ConfigurationExtIEs struct {
	List []M7ConfigurationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type M7ConfigurationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue M7ConfigurationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	M7ConfigurationExtIEsPresentNothing int = iota /* No components present */
)

type M7ConfigurationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerMDTLocationInfoExtIEs struct {
	List []MDTLocationInfoExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type MDTLocationInfoExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue MDTLocationInfoExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	MDTLocationInfoExtIEsPresentNothing int = iota /* No components present */
)

type MDTLocationInfoExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerNBIoTPagingEDRXInfoExtIEs struct {
	List []NBIoTPagingEDRXInfoExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NBIoTPagingEDRXInfoExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue NBIoTPagingEDRXInfoExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NBIoTPagingEDRXInfoExtIEsPresentNothing int = iota /* No components present */
)

type NBIoTPagingEDRXInfoExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerNGRANTNLAssociationToRemoveItemExtIEs struct {
	List []NGRANTNLAssociationToRemoveItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NGRANTNLAssociationToRemoveItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue NGRANTNLAssociationToRemoveItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NGRANTNLAssociationToRemoveItemExtIEsPresentNothing int = iota /* No components present */
)

type NGRANTNLAssociationToRemoveItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerNonDynamic5QIDescriptorExtIEs struct {
	List []NonDynamic5QIDescriptorExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NonDynamic5QIDescriptorExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue NonDynamic5QIDescriptorExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NonDynamic5QIDescriptorExtIEsPresentNothing int = iota /* No components present */
	NonDynamic5QIDescriptorExtIEsPresentCNPacketDelayBudgetDL
	NonDynamic5QIDescriptorExtIEsPresentCNPacketDelayBudgetUL
)

type NonDynamic5QIDescriptorExtIEsExtensionValue struct {
	Present               int
	CNPacketDelayBudgetDL *ExtendedPacketDelayBudget `vht5gc:"referenceFieldValue:187"`
	CNPacketDelayBudgetUL *ExtendedPacketDelayBudget `vht5gc:"referenceFieldValue:188"`
}

type ProtocolExtensionContainerNRCGIExtIEs struct {
	List []NRCGIExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NRCGIExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue NRCGIExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NRCGIExtIEsPresentNothing int = iota /* No components present */
)

type NRCGIExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerNRFrequencyBandItemExtIEs struct {
	List []NRFrequencyBandItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NRFrequencyBandItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue NRFrequencyBandItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NRFrequencyBandItemExtIEsPresentNothing int = iota /* No components present */
)

type NRFrequencyBandItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerNRFrequencyInfoExtIEs struct {
	List []NRFrequencyInfoExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NRFrequencyInfoExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue NRFrequencyInfoExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NRFrequencyInfoExtIEsPresentNothing int = iota /* No components present */
)

type NRFrequencyInfoExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerNRV2XServicesAuthorizedExtIEs struct {
	List []NRV2XServicesAuthorizedExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NRV2XServicesAuthorizedExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue NRV2XServicesAuthorizedExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NRV2XServicesAuthorizedExtIEsPresentNothing int = iota /* No components present */
)

type NRV2XServicesAuthorizedExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerNRUESidelinkAggregateMaximumBitrateExtIEs struct {
	List []NRUESidelinkAggregateMaximumBitrateExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type NRUESidelinkAggregateMaximumBitrateExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue NRUESidelinkAggregateMaximumBitrateExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	NRUESidelinkAggregateMaximumBitrateExtIEsPresentNothing int = iota /* No components present */
)

type NRUESidelinkAggregateMaximumBitrateExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerOverloadStartNSSAIItemExtIEs struct {
	List []OverloadStartNSSAIItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type OverloadStartNSSAIItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue OverloadStartNSSAIItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	OverloadStartNSSAIItemExtIEsPresentNothing int = iota /* No components present */
)

type OverloadStartNSSAIItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPacketErrorRateExtIEs struct {
	List []PacketErrorRateExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PacketErrorRateExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PacketErrorRateExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PacketErrorRateExtIEsPresentNothing int = iota /* No components present */
)

type PacketErrorRateExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPagingAssisDataforCEcapabUEExtIEs struct {
	List []PagingAssisDataforCEcapabUEExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PagingAssisDataforCEcapabUEExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PagingAssisDataforCEcapabUEExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PagingAssisDataforCEcapabUEExtIEsPresentNothing int = iota /* No components present */
)

type PagingAssisDataforCEcapabUEExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPagingAttemptInformationExtIEs struct {
	List []PagingAttemptInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PagingAttemptInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PagingAttemptInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PagingAttemptInformationExtIEsPresentNothing int = iota /* No components present */
)

type PagingAttemptInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPagingeDRXInformationExtIEs struct {
	List []PagingeDRXInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PagingeDRXInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PagingeDRXInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PagingeDRXInformationExtIEsPresentNothing int = iota /* No components present */
)

type PagingeDRXInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPathSwitchRequestAcknowledgeTransferExtIEs struct {
	List []PathSwitchRequestAcknowledgeTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PathSwitchRequestAcknowledgeTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PathSwitchRequestAcknowledgeTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PathSwitchRequestAcknowledgeTransferExtIEsPresentNothing int = iota /* No components present */
	PathSwitchRequestAcknowledgeTransferExtIEsPresentAdditionalNGUUPTNLInformation
	PathSwitchRequestAcknowledgeTransferExtIEsPresentRedundantULNGUUPTNLInformation
	PathSwitchRequestAcknowledgeTransferExtIEsPresentAdditionalRedundantNGUUPTNLInformation
)

type PathSwitchRequestAcknowledgeTransferExtIEsExtensionValue struct {
	Present                                int
	AdditionalNGUUPTNLInformation          *UPTransportLayerInformationPairList `vht5gc:"referenceFieldValue:154"`
	RedundantULNGUUPTNLInformation         *UPTransportLayerInformation         `vht5gc:"referenceFieldValue:195,valueLB:0,valueUB:1"`
	AdditionalRedundantNGUUPTNLInformation *UPTransportLayerInformationPairList `vht5gc:"referenceFieldValue:185"`
}

type ProtocolExtensionContainerPathSwitchRequestSetupFailedTransferExtIEs struct {
	List []PathSwitchRequestSetupFailedTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PathSwitchRequestSetupFailedTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PathSwitchRequestSetupFailedTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PathSwitchRequestSetupFailedTransferExtIEsPresentNothing int = iota /* No components present */
)

type PathSwitchRequestSetupFailedTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPathSwitchRequestTransferExtIEs struct {
	List []PathSwitchRequestTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PathSwitchRequestTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PathSwitchRequestTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PathSwitchRequestTransferExtIEsPresentNothing int = iota /* No components present */
	PathSwitchRequestTransferExtIEsPresentAdditionalDLQosFlowPerTNLInformation
	PathSwitchRequestTransferExtIEsPresentRedundantDLNGUUPTNLInformation
	PathSwitchRequestTransferExtIEsPresentRedundantDLNGUTNLInformationReused
	PathSwitchRequestTransferExtIEsPresentAdditionalRedundantDLQosFlowPerTNLInformation
	PathSwitchRequestTransferExtIEsPresentUsedRSNInformation
	PathSwitchRequestTransferExtIEsPresentGlobalRANNodeID
)

type PathSwitchRequestTransferExtIEsExtensionValue struct {
	Present                                       int
	AdditionalDLQosFlowPerTNLInformation          *QosFlowPerTNLInformationList   `vht5gc:"referenceFieldValue:155"`
	RedundantDLNGUUPTNLInformation                *UPTransportLayerInformation    `vht5gc:"referenceFieldValue:192,valueLB:0,valueUB:1"`
	RedundantDLNGUTNLInformationReused            *DLNGUTNLInformationReused      `vht5gc:"referenceFieldValue:191"`
	AdditionalRedundantDLQosFlowPerTNLInformation *QosFlowPerTNLInformationList   `vht5gc:"referenceFieldValue:184"`
	UsedRSNInformation                            *RedundantPDUSessionInformation `vht5gc:"valueExt,referenceFieldValue:198"`
	GlobalRANNodeID                               *GlobalRANNodeID                `vht5gc:"referenceFieldValue:27,valueLB:0,valueUB:3"`
}

type ProtocolExtensionContainerPathSwitchRequestUnsuccessfulTransferExtIEs struct {
	List []PathSwitchRequestUnsuccessfulTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PathSwitchRequestUnsuccessfulTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PathSwitchRequestUnsuccessfulTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PathSwitchRequestUnsuccessfulTransferExtIEsPresentNothing int = iota /* No components present */
)

type PathSwitchRequestUnsuccessfulTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPC5QoSParametersExtIEs struct {
	List []PC5QoSParametersExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PC5QoSParametersExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PC5QoSParametersExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PC5QoSParametersExtIEsPresentNothing int = iota /* No components present */
)

type PC5QoSParametersExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPC5QoSFlowItemExtIEs struct {
	List []PC5QoSFlowItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PC5QoSFlowItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PC5QoSFlowItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PC5QoSFlowItemExtIEsPresentNothing int = iota /* No components present */
)

type PC5QoSFlowItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPC5FlowBitRatesExtIEs struct {
	List []PC5FlowBitRatesExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PC5FlowBitRatesExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PC5FlowBitRatesExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PC5FlowBitRatesExtIEsPresentNothing int = iota /* No components present */
)

type PC5FlowBitRatesExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionAggregateMaximumBitRateExtIEs struct {
	List []PDUSessionAggregateMaximumBitRateExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionAggregateMaximumBitRateExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionAggregateMaximumBitRateExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionAggregateMaximumBitRateExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionAggregateMaximumBitRateExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceAdmittedItemExtIEs struct {
	List []PDUSessionResourceAdmittedItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceAdmittedItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceAdmittedItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceAdmittedItemExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceAdmittedItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceFailedToModifyItemModCfmExtIEs struct {
	List []PDUSessionResourceFailedToModifyItemModCfmExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceFailedToModifyItemModCfmExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceFailedToModifyItemModCfmExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceFailedToModifyItemModCfmExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceFailedToModifyItemModCfmExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceFailedToModifyItemModResExtIEs struct {
	List []PDUSessionResourceFailedToModifyItemModResExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceFailedToModifyItemModResExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceFailedToModifyItemModResExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceFailedToModifyItemModResExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceFailedToModifyItemModResExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceFailedToResumeItemRESReqExtIEs struct {
	List []PDUSessionResourceFailedToResumeItemRESReqExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceFailedToResumeItemRESReqExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceFailedToResumeItemRESReqExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceFailedToResumeItemRESReqExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceFailedToResumeItemRESReqExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceFailedToResumeItemRESResExtIEs struct {
	List []PDUSessionResourceFailedToResumeItemRESResExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceFailedToResumeItemRESResExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceFailedToResumeItemRESResExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceFailedToResumeItemRESResExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceFailedToResumeItemRESResExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceFailedToSetupItemCxtFailExtIEs struct {
	List []PDUSessionResourceFailedToSetupItemCxtFailExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceFailedToSetupItemCxtFailExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceFailedToSetupItemCxtFailExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceFailedToSetupItemCxtFailExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceFailedToSetupItemCxtFailExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceFailedToSetupItemCxtResExtIEs struct {
	List []PDUSessionResourceFailedToSetupItemCxtResExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceFailedToSetupItemCxtResExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceFailedToSetupItemCxtResExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceFailedToSetupItemCxtResExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceFailedToSetupItemCxtResExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceFailedToSetupItemHOAckExtIEs struct {
	List []PDUSessionResourceFailedToSetupItemHOAckExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceFailedToSetupItemHOAckExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceFailedToSetupItemHOAckExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceFailedToSetupItemHOAckExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceFailedToSetupItemHOAckExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceFailedToSetupItemPSReqExtIEs struct {
	List []PDUSessionResourceFailedToSetupItemPSReqExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceFailedToSetupItemPSReqExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceFailedToSetupItemPSReqExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceFailedToSetupItemPSReqExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceFailedToSetupItemPSReqExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceFailedToSetupItemSUResExtIEs struct {
	List []PDUSessionResourceFailedToSetupItemSUResExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceFailedToSetupItemSUResExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceFailedToSetupItemSUResExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceFailedToSetupItemSUResExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceFailedToSetupItemSUResExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceHandoverItemExtIEs struct {
	List []PDUSessionResourceHandoverItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceHandoverItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceHandoverItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceHandoverItemExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceHandoverItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceInformationItemExtIEs struct {
	List []PDUSessionResourceInformationItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceInformationItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceInformationItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceInformationItemExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceInformationItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceItemCxtRelCplExtIEs struct {
	List []PDUSessionResourceItemCxtRelCplExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceItemCxtRelCplExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceItemCxtRelCplExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceItemCxtRelCplExtIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceItemCxtRelCplExtIEsPresentPDUSessionResourceReleaseResponseTransfer
)

type PDUSessionResourceItemCxtRelCplExtIEsExtensionValue struct {
	Present                                   int
	PDUSessionResourceReleaseResponseTransfer *OctetString `vht5gc:"referenceFieldValue:145"`
}

type ProtocolExtensionContainerPDUSessionResourceItemCxtRelReqExtIEs struct {
	List []PDUSessionResourceItemCxtRelReqExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceItemCxtRelReqExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceItemCxtRelReqExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceItemCxtRelReqExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceItemCxtRelReqExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceItemHORqdExtIEs struct {
	List []PDUSessionResourceItemHORqdExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceItemHORqdExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceItemHORqdExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceItemHORqdExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceItemHORqdExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceModifyConfirmTransferExtIEs struct {
	List []PDUSessionResourceModifyConfirmTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceModifyConfirmTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceModifyConfirmTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyConfirmTransferExtIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceModifyConfirmTransferExtIEsPresentRedundantULNGUUPTNLInformation
	PDUSessionResourceModifyConfirmTransferExtIEsPresentAdditionalRedundantNGUUPTNLInformation
)

type PDUSessionResourceModifyConfirmTransferExtIEsExtensionValue struct {
	Present                                int
	RedundantULNGUUPTNLInformation         *UPTransportLayerInformation         `vht5gc:"referenceFieldValue:195,valueLB:0,valueUB:1"`
	AdditionalRedundantNGUUPTNLInformation *UPTransportLayerInformationPairList `vht5gc:"referenceFieldValue:185"`
}

type ProtocolExtensionContainerPDUSessionResourceModifyIndicationUnsuccessfulTransferExtIEs struct {
	List []PDUSessionResourceModifyIndicationUnsuccessfulTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceModifyIndicationUnsuccessfulTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceModifyIndicationUnsuccessfulTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyIndicationUnsuccessfulTransferExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceModifyIndicationUnsuccessfulTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceModifyResponseTransferExtIEs struct {
	List []PDUSessionResourceModifyResponseTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceModifyResponseTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceModifyResponseTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyResponseTransferExtIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceModifyResponseTransferExtIEsPresentAdditionalNGUUPTNLInformation
	PDUSessionResourceModifyResponseTransferExtIEsPresentRedundantDLNGUUPTNLInformation
	PDUSessionResourceModifyResponseTransferExtIEsPresentRedundantULNGUUPTNLInformation
	PDUSessionResourceModifyResponseTransferExtIEsPresentAdditionalRedundantDLQosFlowPerTNLInformation
	PDUSessionResourceModifyResponseTransferExtIEsPresentAdditionalRedundantNGUUPTNLInformation
)

type PDUSessionResourceModifyResponseTransferExtIEsExtensionValue struct {
	Present                                       int
	AdditionalNGUUPTNLInformation                 *UPTransportLayerInformationPairList `vht5gc:"referenceFieldValue:154"`
	RedundantDLNGUUPTNLInformation                *UPTransportLayerInformation         `vht5gc:"referenceFieldValue:192,valueLB:0,valueUB:1"`
	RedundantULNGUUPTNLInformation                *UPTransportLayerInformation         `vht5gc:"referenceFieldValue:195,valueLB:0,valueUB:1"`
	AdditionalRedundantDLQosFlowPerTNLInformation *QosFlowPerTNLInformationList        `vht5gc:"referenceFieldValue:184"`
	AdditionalRedundantNGUUPTNLInformation        *UPTransportLayerInformationPairList `vht5gc:"referenceFieldValue:185"`
}

type ProtocolExtensionContainerPDUSessionResourceModifyIndicationTransferExtIEs struct {
	List []PDUSessionResourceModifyIndicationTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceModifyIndicationTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceModifyIndicationTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyIndicationTransferExtIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceModifyIndicationTransferExtIEsPresentSecondaryRATUsageInformation
	PDUSessionResourceModifyIndicationTransferExtIEsPresentSecurityResult
	PDUSessionResourceModifyIndicationTransferExtIEsPresentRedundantDLQosFlowPerTNLInformation
	PDUSessionResourceModifyIndicationTransferExtIEsPresentAdditionalRedundantDLQosFlowPerTNLInformation
	PDUSessionResourceModifyIndicationTransferExtIEsPresentGlobalRANNodeID
)

type PDUSessionResourceModifyIndicationTransferExtIEsExtensionValue struct {
	Present                                       int
	SecondaryRATUsageInformation                  *SecondaryRATUsageInformation `vht5gc:"valueExt,referenceFieldValue:144"`
	SecurityResult                                *SecurityResult               `vht5gc:"valueExt,referenceFieldValue:156"`
	RedundantDLQosFlowPerTNLInformation           *QosFlowPerTNLInformation     `vht5gc:"valueExt,referenceFieldValue:193"`
	AdditionalRedundantDLQosFlowPerTNLInformation *QosFlowPerTNLInformationList `vht5gc:"referenceFieldValue:184"`
	GlobalRANNodeID                               *GlobalRANNodeID              `vht5gc:"referenceFieldValue:27,valueLB:0,valueUB:3"`
}

type ProtocolExtensionContainerPDUSessionResourceModifyItemModCfmExtIEs struct {
	List []PDUSessionResourceModifyItemModCfmExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceModifyItemModCfmExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceModifyItemModCfmExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyItemModCfmExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceModifyItemModCfmExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceModifyItemModIndExtIEs struct {
	List []PDUSessionResourceModifyItemModIndExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceModifyItemModIndExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceModifyItemModIndExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyItemModIndExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceModifyItemModIndExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceModifyItemModReqExtIEs struct {
	List []PDUSessionResourceModifyItemModReqExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceModifyItemModReqExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceModifyItemModReqExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyItemModReqExtIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceModifyItemModReqExtIEsPresentSNSSAI
)

type PDUSessionResourceModifyItemModReqExtIEsExtensionValue struct {
	Present int
	SNSSAI  *SNSSAI `vht5gc:"valueExt,referenceFieldValue:148"`
}

type ProtocolExtensionContainerPDUSessionResourceModifyItemModResExtIEs struct {
	List []PDUSessionResourceModifyItemModResExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceModifyItemModResExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceModifyItemModResExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyItemModResExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceModifyItemModResExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceModifyUnsuccessfulTransferExtIEs struct {
	List []PDUSessionResourceModifyUnsuccessfulTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceModifyUnsuccessfulTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceModifyUnsuccessfulTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceModifyUnsuccessfulTransferExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceModifyUnsuccessfulTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceNotifyItemExtIEs struct {
	List []PDUSessionResourceNotifyItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceNotifyItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceNotifyItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceNotifyItemExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceNotifyItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceNotifyReleasedTransferExtIEs struct {
	List []PDUSessionResourceNotifyReleasedTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceNotifyReleasedTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceNotifyReleasedTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceNotifyReleasedTransferExtIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceNotifyReleasedTransferExtIEsPresentSecondaryRATUsageInformation
)

type PDUSessionResourceNotifyReleasedTransferExtIEsExtensionValue struct {
	Present                      int
	SecondaryRATUsageInformation *SecondaryRATUsageInformation `vht5gc:"valueExt,referenceFieldValue:144"`
}

type ProtocolExtensionContainerPDUSessionResourceNotifyTransferExtIEs struct {
	List []PDUSessionResourceNotifyTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceNotifyTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceNotifyTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceNotifyTransferExtIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceNotifyTransferExtIEsPresentSecondaryRATUsageInformation
)

type PDUSessionResourceNotifyTransferExtIEsExtensionValue struct {
	Present                      int
	SecondaryRATUsageInformation *SecondaryRATUsageInformation `vht5gc:"valueExt,referenceFieldValue:144"`
}

type ProtocolExtensionContainerPDUSessionResourceReleaseCommandTransferExtIEs struct {
	List []PDUSessionResourceReleaseCommandTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceReleaseCommandTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceReleaseCommandTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceReleaseCommandTransferExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceReleaseCommandTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceReleasedItemNotExtIEs struct {
	List []PDUSessionResourceReleasedItemNotExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceReleasedItemNotExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceReleasedItemNotExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceReleasedItemNotExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceReleasedItemNotExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceReleasedItemPSAckExtIEs struct {
	List []PDUSessionResourceReleasedItemPSAckExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceReleasedItemPSAckExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceReleasedItemPSAckExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceReleasedItemPSAckExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceReleasedItemPSAckExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceReleasedItemPSFailExtIEs struct {
	List []PDUSessionResourceReleasedItemPSFailExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceReleasedItemPSFailExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceReleasedItemPSFailExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceReleasedItemPSFailExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceReleasedItemPSFailExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceReleasedItemRelResExtIEs struct {
	List []PDUSessionResourceReleasedItemRelResExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceReleasedItemRelResExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceReleasedItemRelResExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceReleasedItemRelResExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceReleasedItemRelResExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceReleaseResponseTransferExtIEs struct {
	List []PDUSessionResourceReleaseResponseTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceReleaseResponseTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceReleaseResponseTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceReleaseResponseTransferExtIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceReleaseResponseTransferExtIEsPresentSecondaryRATUsageInformation
)

type PDUSessionResourceReleaseResponseTransferExtIEsExtensionValue struct {
	Present                      int
	SecondaryRATUsageInformation *SecondaryRATUsageInformation `vht5gc:"valueExt,referenceFieldValue:144"`
}

type ProtocolExtensionContainerPDUSessionResourceResumeItemRESReqExtIEs struct {
	List []PDUSessionResourceResumeItemRESReqExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceResumeItemRESReqExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceResumeItemRESReqExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceResumeItemRESReqExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceResumeItemRESReqExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceResumeItemRESResExtIEs struct {
	List []PDUSessionResourceResumeItemRESResExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceResumeItemRESResExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceResumeItemRESResExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceResumeItemRESResExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceResumeItemRESResExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceSecondaryRATUsageItemExtIEs struct {
	List []PDUSessionResourceSecondaryRATUsageItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceSecondaryRATUsageItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceSecondaryRATUsageItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSecondaryRATUsageItemExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceSecondaryRATUsageItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceSetupItemCxtReqExtIEs struct {
	List []PDUSessionResourceSetupItemCxtReqExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceSetupItemCxtReqExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceSetupItemCxtReqExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSetupItemCxtReqExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceSetupItemCxtReqExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceSetupItemCxtResExtIEs struct {
	List []PDUSessionResourceSetupItemCxtResExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceSetupItemCxtResExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceSetupItemCxtResExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSetupItemCxtResExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceSetupItemCxtResExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceSetupItemHOReqExtIEs struct {
	List []PDUSessionResourceSetupItemHOReqExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceSetupItemHOReqExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceSetupItemHOReqExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSetupItemHOReqExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceSetupItemHOReqExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceSetupItemSUReqExtIEs struct {
	List []PDUSessionResourceSetupItemSUReqExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceSetupItemSUReqExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceSetupItemSUReqExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSetupItemSUReqExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceSetupItemSUReqExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceSetupItemSUResExtIEs struct {
	List []PDUSessionResourceSetupItemSUResExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceSetupItemSUResExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceSetupItemSUResExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSetupItemSUResExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceSetupItemSUResExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceSetupResponseTransferExtIEs struct {
	List []PDUSessionResourceSetupResponseTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceSetupResponseTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceSetupResponseTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSetupResponseTransferExtIEsPresentNothing int = iota /* No components present */
	PDUSessionResourceSetupResponseTransferExtIEsPresentRedundantDLQosFlowPerTNLInformation
	PDUSessionResourceSetupResponseTransferExtIEsPresentAdditionalRedundantDLQosFlowPerTNLInformation
	PDUSessionResourceSetupResponseTransferExtIEsPresentUsedRSNInformation
	PDUSessionResourceSetupResponseTransferExtIEsPresentGlobalRANNodeID
)

type PDUSessionResourceSetupResponseTransferExtIEsExtensionValue struct {
	Present                                       int
	RedundantDLQosFlowPerTNLInformation           *QosFlowPerTNLInformation       `vht5gc:"valueExt,referenceFieldValue:193"`
	AdditionalRedundantDLQosFlowPerTNLInformation *QosFlowPerTNLInformationList   `vht5gc:"referenceFieldValue:184"`
	UsedRSNInformation                            *RedundantPDUSessionInformation `vht5gc:"valueExt,referenceFieldValue:198"`
	GlobalRANNodeID                               *GlobalRANNodeID                `vht5gc:"referenceFieldValue:27,valueLB:0,valueUB:3"`
}

type ProtocolExtensionContainerPDUSessionResourceSetupUnsuccessfulTransferExtIEs struct {
	List []PDUSessionResourceSetupUnsuccessfulTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceSetupUnsuccessfulTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceSetupUnsuccessfulTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSetupUnsuccessfulTransferExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceSetupUnsuccessfulTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceSuspendItemSUSReqExtIEs struct {
	List []PDUSessionResourceSuspendItemSUSReqExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceSuspendItemSUSReqExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceSuspendItemSUSReqExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSuspendItemSUSReqExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceSuspendItemSUSReqExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceSwitchedItemExtIEs struct {
	List []PDUSessionResourceSwitchedItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceSwitchedItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceSwitchedItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceSwitchedItemExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceSwitchedItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceToBeSwitchedDLItemExtIEs struct {
	List []PDUSessionResourceToBeSwitchedDLItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceToBeSwitchedDLItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceToBeSwitchedDLItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceToBeSwitchedDLItemExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceToBeSwitchedDLItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceToReleaseItemHOCmdExtIEs struct {
	List []PDUSessionResourceToReleaseItemHOCmdExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceToReleaseItemHOCmdExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceToReleaseItemHOCmdExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceToReleaseItemHOCmdExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceToReleaseItemHOCmdExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionResourceToReleaseItemRelCmdExtIEs struct {
	List []PDUSessionResourceToReleaseItemRelCmdExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionResourceToReleaseItemRelCmdExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionResourceToReleaseItemRelCmdExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionResourceToReleaseItemRelCmdExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionResourceToReleaseItemRelCmdExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPDUSessionUsageReportExtIEs struct {
	List []PDUSessionUsageReportExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PDUSessionUsageReportExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PDUSessionUsageReportExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PDUSessionUsageReportExtIEsPresentNothing int = iota /* No components present */
)

type PDUSessionUsageReportExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerPLMNSupportItemExtIEs struct {
	List []PLMNSupportItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PLMNSupportItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PLMNSupportItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PLMNSupportItemExtIEsPresentNothing int = iota /* No components present */
	PLMNSupportItemExtIEsPresentNPNSupport
	PLMNSupportItemExtIEsPresentExtendedSliceSupportList
)

type PLMNSupportItemExtIEsExtensionValue struct {
	Present                  int
	NPNSupport               *NPNSupport               `vht5gc:"referenceFieldValue:258,valueLB:0,valueUB:1"`
	ExtendedSliceSupportList *ExtendedSliceSupportList `vht5gc:"referenceFieldValue:270"`
}

type ProtocolExtensionContainerPNINPNMobilityInformationExtIEs struct {
	List []PNINPNMobilityInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type PNINPNMobilityInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue PNINPNMobilityInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	PNINPNMobilityInformationExtIEsPresentNothing int = iota /* No components present */
)

type PNINPNMobilityInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerQosFlowAcceptedItemExtIEs struct {
	List []QosFlowAcceptedItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowAcceptedItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowAcceptedItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowAcceptedItemExtIEsPresentNothing int = iota /* No components present */
	QosFlowAcceptedItemExtIEsPresentCurrentQoSParaSetIndex
)

type QosFlowAcceptedItemExtIEsExtensionValue struct {
	Present                int
	CurrentQoSParaSetIndex *AlternativeQoSParaSetIndex `vht5gc:"referenceFieldValue:221"`
}

type ProtocolExtensionContainerQosFlowAddOrModifyRequestItemExtIEs struct {
	List []QosFlowAddOrModifyRequestItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowAddOrModifyRequestItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowAddOrModifyRequestItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowAddOrModifyRequestItemExtIEsPresentNothing int = iota /* No components present */
	QosFlowAddOrModifyRequestItemExtIEsPresentTSCTrafficCharacteristics
	QosFlowAddOrModifyRequestItemExtIEsPresentRedundantQosFlowIndicator
)

type QosFlowAddOrModifyRequestItemExtIEsExtensionValue struct {
	Present                   int
	TSCTrafficCharacteristics *TSCTrafficCharacteristics `vht5gc:"valueExt,referenceFieldValue:196"`
	RedundantQosFlowIndicator *RedundantQosFlowIndicator `vht5gc:"referenceFieldValue:194"`
}

type ProtocolExtensionContainerQosFlowAddOrModifyResponseItemExtIEs struct {
	List []QosFlowAddOrModifyResponseItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowAddOrModifyResponseItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowAddOrModifyResponseItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowAddOrModifyResponseItemExtIEsPresentNothing int = iota /* No components present */
	QosFlowAddOrModifyResponseItemExtIEsPresentCurrentQoSParaSetIndex
)

type QosFlowAddOrModifyResponseItemExtIEsExtensionValue struct {
	Present                int
	CurrentQoSParaSetIndex *AlternativeQoSParaSetIndex `vht5gc:"referenceFieldValue:221"`
}

type ProtocolExtensionContainerQosFlowInformationItemExtIEs struct {
	List []QosFlowInformationItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowInformationItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowInformationItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowInformationItemExtIEsPresentNothing int = iota /* No components present */
	QosFlowInformationItemExtIEsPresentULForwarding
)

type QosFlowInformationItemExtIEsExtensionValue struct {
	Present      int
	ULForwarding *ULForwarding `vht5gc:"referenceFieldValue:163"`
}

type ProtocolExtensionContainerQosFlowLevelQosParametersExtIEs struct {
	List []QosFlowLevelQosParametersExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowLevelQosParametersExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowLevelQosParametersExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowLevelQosParametersExtIEsPresentNothing int = iota /* No components present */
	QosFlowLevelQosParametersExtIEsPresentQosMonitoringRequest
)

type QosFlowLevelQosParametersExtIEsExtensionValue struct {
	Present              int
	QosMonitoringRequest *QosMonitoringRequest `vht5gc:"referenceFieldValue:181"`
}

type ProtocolExtensionContainerQosFlowWithCauseItemExtIEs struct {
	List []QosFlowWithCauseItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowWithCauseItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowWithCauseItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowWithCauseItemExtIEsPresentNothing int = iota /* No components present */
)

type QosFlowWithCauseItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerQosFlowModifyConfirmItemExtIEs struct {
	List []QosFlowModifyConfirmItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowModifyConfirmItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowModifyConfirmItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowModifyConfirmItemExtIEsPresentNothing int = iota /* No components present */
)

type QosFlowModifyConfirmItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerQosFlowNotifyItemExtIEs struct {
	List []QosFlowNotifyItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowNotifyItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowNotifyItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowNotifyItemExtIEsPresentNothing int = iota /* No components present */
	QosFlowNotifyItemExtIEsPresentCurrentQoSParaSetIndex
)

type QosFlowNotifyItemExtIEsExtensionValue struct {
	Present                int
	CurrentQoSParaSetIndex *AlternativeQoSParaSetNotifyIndex `vht5gc:"referenceFieldValue:221"`
}

type ProtocolExtensionContainerQosFlowPerTNLInformationExtIEs struct {
	List []QosFlowPerTNLInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowPerTNLInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowPerTNLInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowPerTNLInformationExtIEsPresentNothing int = iota /* No components present */
)

type QosFlowPerTNLInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerQosFlowPerTNLInformationItemExtIEs struct {
	List []QosFlowPerTNLInformationItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowPerTNLInformationItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowPerTNLInformationItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowPerTNLInformationItemExtIEsPresentNothing int = iota /* No components present */
)

type QosFlowPerTNLInformationItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerQosFlowSetupRequestItemExtIEs struct {
	List []QosFlowSetupRequestItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowSetupRequestItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowSetupRequestItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowSetupRequestItemExtIEsPresentNothing int = iota /* No components present */
	QosFlowSetupRequestItemExtIEsPresentTSCTrafficCharacteristics
	QosFlowSetupRequestItemExtIEsPresentRedundantQosFlowIndicator
)

type QosFlowSetupRequestItemExtIEsExtensionValue struct {
	Present                   int
	TSCTrafficCharacteristics *TSCTrafficCharacteristics `vht5gc:"valueExt,referenceFieldValue:196"`
	RedundantQosFlowIndicator *RedundantQosFlowIndicator `vht5gc:"referenceFieldValue:194"`
}

type ProtocolExtensionContainerQosFlowItemWithDataForwardingExtIEs struct {
	List []QosFlowItemWithDataForwardingExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowItemWithDataForwardingExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowItemWithDataForwardingExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowItemWithDataForwardingExtIEsPresentNothing int = iota /* No components present */
	QosFlowItemWithDataForwardingExtIEsPresentCurrentQoSParaSetIndex
)

type QosFlowItemWithDataForwardingExtIEsExtensionValue struct {
	Present                int
	CurrentQoSParaSetIndex *AlternativeQoSParaSetIndex `vht5gc:"referenceFieldValue:221"`
}

type ProtocolExtensionContainerQosFlowToBeForwardedItemExtIEs struct {
	List []QosFlowToBeForwardedItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QosFlowToBeForwardedItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QosFlowToBeForwardedItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QosFlowToBeForwardedItemExtIEsPresentNothing int = iota /* No components present */
)

type QosFlowToBeForwardedItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerQoSFlowsUsageReportItemExtIEs struct {
	List []QoSFlowsUsageReportItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type QoSFlowsUsageReportItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue QoSFlowsUsageReportItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	QoSFlowsUsageReportItemExtIEsPresentNothing int = iota /* No components present */
)

type QoSFlowsUsageReportItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerRANStatusTransferTransparentContainerExtIEs struct {
	List []RANStatusTransferTransparentContainerExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type RANStatusTransferTransparentContainerExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue RANStatusTransferTransparentContainerExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RANStatusTransferTransparentContainerExtIEsPresentNothing int = iota /* No components present */
)

type RANStatusTransferTransparentContainerExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerRATRestrictionsItemExtIEs struct {
	List []RATRestrictionsItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type RATRestrictionsItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue RATRestrictionsItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RATRestrictionsItemExtIEsPresentNothing int = iota /* No components present */
	RATRestrictionsItemExtIEsPresentExtendedRATRestrictionInformation
)

type RATRestrictionsItemExtIEsExtensionValue struct {
	Present                           int
	ExtendedRATRestrictionInformation *ExtendedRATRestrictionInformation `vht5gc:"valueExt,referenceFieldValue:180"`
}

type ProtocolExtensionContainerRecommendedCellsForPagingExtIEs struct {
	List []RecommendedCellsForPagingExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type RecommendedCellsForPagingExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue RecommendedCellsForPagingExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RecommendedCellsForPagingExtIEsPresentNothing int = iota /* No components present */
)

type RecommendedCellsForPagingExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerRecommendedCellItemExtIEs struct {
	List []RecommendedCellItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type RecommendedCellItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue RecommendedCellItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RecommendedCellItemExtIEsPresentNothing int = iota /* No components present */
)

type RecommendedCellItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerRecommendedRANNodesForPagingExtIEs struct {
	List []RecommendedRANNodesForPagingExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type RecommendedRANNodesForPagingExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue RecommendedRANNodesForPagingExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RecommendedRANNodesForPagingExtIEsPresentNothing int = iota /* No components present */
)

type RecommendedRANNodesForPagingExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerRecommendedRANNodeItemExtIEs struct {
	List []RecommendedRANNodeItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type RecommendedRANNodeItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue RecommendedRANNodeItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RecommendedRANNodeItemExtIEsPresentNothing int = iota /* No components present */
)

type RecommendedRANNodeItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerRedundantPDUSessionInformationExtIEs struct {
	List []RedundantPDUSessionInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type RedundantPDUSessionInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue RedundantPDUSessionInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RedundantPDUSessionInformationExtIEsPresentNothing int = iota /* No components present */
)

type RedundantPDUSessionInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerRIMInformationTransferExtIEs struct {
	List []RIMInformationTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type RIMInformationTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue RIMInformationTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RIMInformationTransferExtIEsPresentNothing int = iota /* No components present */
)

type RIMInformationTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerRIMInformationExtIEs struct {
	List []RIMInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type RIMInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue RIMInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	RIMInformationExtIEsPresentNothing int = iota /* No components present */
)

type RIMInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerScheduledCommunicationTimeExtIEs struct {
	List []ScheduledCommunicationTimeExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ScheduledCommunicationTimeExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ScheduledCommunicationTimeExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ScheduledCommunicationTimeExtIEsPresentNothing int = iota /* No components present */
)

type ScheduledCommunicationTimeExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSecondaryRATUsageInformationExtIEs struct {
	List []SecondaryRATUsageInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SecondaryRATUsageInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SecondaryRATUsageInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SecondaryRATUsageInformationExtIEsPresentNothing int = iota /* No components present */
)

type SecondaryRATUsageInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSecondaryRATDataUsageReportTransferExtIEs struct {
	List []SecondaryRATDataUsageReportTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SecondaryRATDataUsageReportTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SecondaryRATDataUsageReportTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SecondaryRATDataUsageReportTransferExtIEsPresentNothing int = iota /* No components present */
)

type SecondaryRATDataUsageReportTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSecurityContextExtIEs struct {
	List []SecurityContextExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SecurityContextExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SecurityContextExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SecurityContextExtIEsPresentNothing int = iota /* No components present */
)

type SecurityContextExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSecurityIndicationExtIEs struct {
	List []SecurityIndicationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SecurityIndicationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SecurityIndicationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SecurityIndicationExtIEsPresentNothing int = iota /* No components present */
	SecurityIndicationExtIEsPresentMaximumIntegrityProtectedDataRateDL
)

type SecurityIndicationExtIEsExtensionValue struct {
	Present                             int
	MaximumIntegrityProtectedDataRateDL *MaximumIntegrityProtectedDataRate `vht5gc:"referenceFieldValue:151"`
}

type ProtocolExtensionContainerSecurityResultExtIEs struct {
	List []SecurityResultExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SecurityResultExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SecurityResultExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SecurityResultExtIEsPresentNothing int = iota /* No components present */
)

type SecurityResultExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSensorMeasurementConfigurationExtIEs struct {
	List []SensorMeasurementConfigurationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SensorMeasurementConfigurationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SensorMeasurementConfigurationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SensorMeasurementConfigurationExtIEsPresentNothing int = iota /* No components present */
)

type SensorMeasurementConfigurationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSensorMeasConfigNameItemExtIEs struct {
	List []SensorMeasConfigNameItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SensorMeasConfigNameItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SensorMeasConfigNameItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SensorMeasConfigNameItemExtIEsPresentNothing int = iota /* No components present */
)

type SensorMeasConfigNameItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerServedGUAMIItemExtIEs struct {
	List []ServedGUAMIItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ServedGUAMIItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ServedGUAMIItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ServedGUAMIItemExtIEsPresentNothing int = iota /* No components present */
	ServedGUAMIItemExtIEsPresentGUAMIType
)

type ServedGUAMIItemExtIEsExtensionValue struct {
	Present   int
	GUAMIType *GUAMIType `vht5gc:"referenceFieldValue:176"`
}

type ProtocolExtensionContainerServiceAreaInformationItemExtIEs struct {
	List []ServiceAreaInformationItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ServiceAreaInformationItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ServiceAreaInformationItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ServiceAreaInformationItemExtIEsPresentNothing int = iota /* No components present */
)

type ServiceAreaInformationItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSliceOverloadItemExtIEs struct {
	List []SliceOverloadItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SliceOverloadItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SliceOverloadItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SliceOverloadItemExtIEsPresentNothing int = iota /* No components present */
)

type SliceOverloadItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSliceSupportItemExtIEs struct {
	List []SliceSupportItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SliceSupportItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SliceSupportItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SliceSupportItemExtIEsPresentNothing int = iota /* No components present */
)

type SliceSupportItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSNPNMobilityInformationExtIEs struct {
	List []SNPNMobilityInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SNPNMobilityInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SNPNMobilityInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SNPNMobilityInformationExtIEsPresentNothing int = iota /* No components present */
)

type SNPNMobilityInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSNSSAIExtIEs struct {
	List []SNSSAIExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SNSSAIExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SNSSAIExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SNSSAIExtIEsPresentNothing int = iota /* No components present */
)

type SNSSAIExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSONConfigurationTransferExtIEs struct {
	List []SONConfigurationTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SONConfigurationTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SONConfigurationTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SONConfigurationTransferExtIEsPresentNothing int = iota /* No components present */
)

type SONConfigurationTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSONInformationReplyExtIEs struct {
	List []SONInformationReplyExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SONInformationReplyExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SONInformationReplyExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SONInformationReplyExtIEsPresentNothing int = iota /* No components present */
)

type SONInformationReplyExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSourceNGRANNodeToTargetNGRANNodeTransparentContainerExtIEs struct {
	List []SourceNGRANNodeToTargetNGRANNodeTransparentContainerExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SourceNGRANNodeToTargetNGRANNodeTransparentContainerExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SourceNGRANNodeToTargetNGRANNodeTransparentContainerExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SourceNGRANNodeToTargetNGRANNodeTransparentContainerExtIEsPresentNothing int = iota /* No components present */
	SourceNGRANNodeToTargetNGRANNodeTransparentContainerExtIEsPresentSgNBUEX2APID
	SourceNGRANNodeToTargetNGRANNodeTransparentContainerExtIEsPresentUEHistoryInformationFromTheUE
)

type SourceNGRANNodeToTargetNGRANNodeTransparentContainerExtIEsExtensionValue struct {
	Present                       int
	SgNBUEX2APID                  *SgNBUEX2APID                  `vht5gc:"referenceFieldValue:182"`
	UEHistoryInformationFromTheUE *UEHistoryInformationFromTheUE `vht5gc:"referenceFieldValue:253,valueLB:0,valueUB:1"`
}

type ProtocolExtensionContainerSourceRANNodeIDExtIEs struct {
	List []SourceRANNodeIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SourceRANNodeIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SourceRANNodeIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SourceRANNodeIDExtIEsPresentNothing int = iota /* No components present */
)

type SourceRANNodeIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSourceToTargetAMFInformationRerouteExtIEs struct {
	List []SourceToTargetAMFInformationRerouteExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SourceToTargetAMFInformationRerouteExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SourceToTargetAMFInformationRerouteExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SourceToTargetAMFInformationRerouteExtIEsPresentNothing int = iota /* No components present */
)

type SourceToTargetAMFInformationRerouteExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerSupportedTAItemExtIEs struct {
	List []SupportedTAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type SupportedTAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue SupportedTAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	SupportedTAItemExtIEsPresentNothing int = iota /* No components present */
	SupportedTAItemExtIEsPresentConfiguredTACIndication
	SupportedTAItemExtIEsPresentRATInformation
)

type SupportedTAItemExtIEsExtensionValue struct {
	Present                 int
	ConfiguredTACIndication *ConfiguredTACIndication `vht5gc:"referenceFieldValue:272"`
	RATInformation          *RATInformation          `vht5gc:"referenceFieldValue:179"`
}

type ProtocolExtensionContainerTAIExtIEs struct {
	List []TAIExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TAIExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TAIExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TAIExtIEsPresentNothing int = iota /* No components present */
)

type TAIExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTAIBroadcastEUTRAItemExtIEs struct {
	List []TAIBroadcastEUTRAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TAIBroadcastEUTRAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TAIBroadcastEUTRAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TAIBroadcastEUTRAItemExtIEsPresentNothing int = iota /* No components present */
)

type TAIBroadcastEUTRAItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTAIBroadcastNRItemExtIEs struct {
	List []TAIBroadcastNRItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TAIBroadcastNRItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TAIBroadcastNRItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TAIBroadcastNRItemExtIEsPresentNothing int = iota /* No components present */
)

type TAIBroadcastNRItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTAICancelledEUTRAItemExtIEs struct {
	List []TAICancelledEUTRAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TAICancelledEUTRAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TAICancelledEUTRAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TAICancelledEUTRAItemExtIEsPresentNothing int = iota /* No components present */
)

type TAICancelledEUTRAItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTAICancelledNRItemExtIEs struct {
	List []TAICancelledNRItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TAICancelledNRItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TAICancelledNRItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TAICancelledNRItemExtIEsPresentNothing int = iota /* No components present */
)

type TAICancelledNRItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTAIListForInactiveItemExtIEs struct {
	List []TAIListForInactiveItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TAIListForInactiveItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TAIListForInactiveItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TAIListForInactiveItemExtIEsPresentNothing int = iota /* No components present */
)

type TAIListForInactiveItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTAIListForPagingItemExtIEs struct {
	List []TAIListForPagingItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TAIListForPagingItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TAIListForPagingItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TAIListForPagingItemExtIEsPresentNothing int = iota /* No components present */
)

type TAIListForPagingItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTargeteNBIDExtIEs struct {
	List []TargeteNBIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TargeteNBIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TargeteNBIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TargeteNBIDExtIEsPresentNothing int = iota /* No components present */
)

type TargeteNBIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTargetNGRANNodeToSourceNGRANNodeTransparentContainerExtIEs struct {
	List []TargetNGRANNodeToSourceNGRANNodeTransparentContainerExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TargetNGRANNodeToSourceNGRANNodeTransparentContainerExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TargetNGRANNodeToSourceNGRANNodeTransparentContainerExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TargetNGRANNodeToSourceNGRANNodeTransparentContainerExtIEsPresentNothing int = iota /* No components present */
	TargetNGRANNodeToSourceNGRANNodeTransparentContainerExtIEsPresentDAPSResponseInfoList
)

type TargetNGRANNodeToSourceNGRANNodeTransparentContainerExtIEsExtensionValue struct {
	Present              int
	DAPSResponseInfoList *DAPSResponseInfoList `vht5gc:"referenceFieldValue:267"`
}

type ProtocolExtensionContainerTargetNGRANNodeToSourceNGRANNodeFailureTransparentContainerExtIEs struct {
	List []TargetNGRANNodeToSourceNGRANNodeFailureTransparentContainerExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TargetNGRANNodeToSourceNGRANNodeFailureTransparentContainerExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TargetNGRANNodeToSourceNGRANNodeFailureTransparentContainerExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TargetNGRANNodeToSourceNGRANNodeFailureTransparentContainerExtIEsPresentNothing int = iota /* No components present */
)

type TargetNGRANNodeToSourceNGRANNodeFailureTransparentContainerExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTargetRANNodeIDExtIEs struct {
	List []TargetRANNodeIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TargetRANNodeIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TargetRANNodeIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TargetRANNodeIDExtIEsPresentNothing int = iota /* No components present */
)

type TargetRANNodeIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTargetRNCIDExtIEs struct {
	List []TargetRNCIDExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TargetRNCIDExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TargetRNCIDExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TargetRNCIDExtIEsPresentNothing int = iota /* No components present */
)

type TargetRNCIDExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTNLAssociationItemExtIEs struct {
	List []TNLAssociationItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TNLAssociationItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TNLAssociationItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TNLAssociationItemExtIEsPresentNothing int = iota /* No components present */
)

type TNLAssociationItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTooearlyIntersystemHOExtIEs struct {
	List []TooearlyIntersystemHOExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TooearlyIntersystemHOExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TooearlyIntersystemHOExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TooearlyIntersystemHOExtIEsPresentNothing int = iota /* No components present */
)

type TooearlyIntersystemHOExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTraceActivationExtIEs struct {
	List []TraceActivationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TraceActivationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TraceActivationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TraceActivationExtIEsPresentNothing int = iota /* No components present */
	TraceActivationExtIEsPresentMDTConfiguration
	TraceActivationExtIEsPresentTraceCollectionEntityURI
)

type TraceActivationExtIEsExtensionValue struct {
	Present                  int
	MDTConfiguration         *MDTConfiguration `vht5gc:"valueExt,referenceFieldValue:255"`
	TraceCollectionEntityURI *URIAddress       `vht5gc:"referenceFieldValue:257"`
}

type ProtocolExtensionContainerTAIBasedMDTExtIEs struct {
	List []TAIBasedMDTExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TAIBasedMDTExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TAIBasedMDTExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TAIBasedMDTExtIEsPresentNothing int = iota /* No components present */
)

type TAIBasedMDTExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTABasedMDTExtIEs struct {
	List []TABasedMDTExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TABasedMDTExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TABasedMDTExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TABasedMDTExtIEsPresentNothing int = iota /* No components present */
)

type TABasedMDTExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTSCAssistanceInformationExtIEs struct {
	List []TSCAssistanceInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TSCAssistanceInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TSCAssistanceInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TSCAssistanceInformationExtIEsPresentNothing int = iota /* No components present */
)

type TSCAssistanceInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerTSCTrafficCharacteristicsExtIEs struct {
	List []TSCTrafficCharacteristicsExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type TSCTrafficCharacteristicsExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue TSCTrafficCharacteristicsExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	TSCTrafficCharacteristicsExtIEsPresentNothing int = iota /* No components present */
)

type TSCTrafficCharacteristicsExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUEAggregateMaximumBitRateExtIEs struct {
	List []UEAggregateMaximumBitRateExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UEAggregateMaximumBitRateExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UEAggregateMaximumBitRateExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEAggregateMaximumBitRateExtIEsPresentNothing int = iota /* No components present */
)

type UEAggregateMaximumBitRateExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUEAssociatedLogicalNGConnectionItemExtIEs struct {
	List []UEAssociatedLogicalNGConnectionItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UEAssociatedLogicalNGConnectionItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UEAssociatedLogicalNGConnectionItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEAssociatedLogicalNGConnectionItemExtIEsPresentNothing int = iota /* No components present */
)

type UEAssociatedLogicalNGConnectionItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUEContextResumeRequestTransferExtIEs struct {
	List []UEContextResumeRequestTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UEContextResumeRequestTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UEContextResumeRequestTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextResumeRequestTransferExtIEsPresentNothing int = iota /* No components present */
)

type UEContextResumeRequestTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUEContextResumeResponseTransferExtIEs struct {
	List []UEContextResumeResponseTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UEContextResumeResponseTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UEContextResumeResponseTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextResumeResponseTransferExtIEsPresentNothing int = iota /* No components present */
)

type UEContextResumeResponseTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUEContextSuspendRequestTransferExtIEs struct {
	List []UEContextSuspendRequestTransferExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UEContextSuspendRequestTransferExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UEContextSuspendRequestTransferExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEContextSuspendRequestTransferExtIEsPresentNothing int = iota /* No components present */
)

type UEContextSuspendRequestTransferExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUEDifferentiationInfoExtIEs struct {
	List []UEDifferentiationInfoExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UEDifferentiationInfoExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UEDifferentiationInfoExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEDifferentiationInfoExtIEsPresentNothing int = iota /* No components present */
)

type UEDifferentiationInfoExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUENGAPIDPairExtIEs struct {
	List []UENGAPIDPairExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UENGAPIDPairExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UENGAPIDPairExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UENGAPIDPairExtIEsPresentNothing int = iota /* No components present */
)

type UENGAPIDPairExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUEPresenceInAreaOfInterestItemExtIEs struct {
	List []UEPresenceInAreaOfInterestItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UEPresenceInAreaOfInterestItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UEPresenceInAreaOfInterestItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UEPresenceInAreaOfInterestItemExtIEsPresentNothing int = iota /* No components present */
)

type UEPresenceInAreaOfInterestItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUERadioCapabilityForPagingExtIEs struct {
	List []UERadioCapabilityForPagingExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UERadioCapabilityForPagingExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UERadioCapabilityForPagingExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UERadioCapabilityForPagingExtIEsPresentNothing int = iota /* No components present */
	UERadioCapabilityForPagingExtIEsPresentUERadioCapabilityForPagingOfNBIoT
)

type UERadioCapabilityForPagingExtIEsExtensionValue struct {
	Present                           int
	UERadioCapabilityForPagingOfNBIoT *UERadioCapabilityForPagingOfNBIoT `vht5gc:"referenceFieldValue:214"`
}

type ProtocolExtensionContainerUESecurityCapabilitiesExtIEs struct {
	List []UESecurityCapabilitiesExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UESecurityCapabilitiesExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UESecurityCapabilitiesExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UESecurityCapabilitiesExtIEsPresentNothing int = iota /* No components present */
)

type UESecurityCapabilitiesExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerULCPSecurityInformationExtIEs struct {
	List []ULCPSecurityInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ULCPSecurityInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ULCPSecurityInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ULCPSecurityInformationExtIEsPresentNothing int = iota /* No components present */
)

type ULCPSecurityInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerULNGUUPTNLModifyItemExtIEs struct {
	List []ULNGUUPTNLModifyItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type ULNGUUPTNLModifyItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue ULNGUUPTNLModifyItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	ULNGUUPTNLModifyItemExtIEsPresentNothing int = iota /* No components present */
	ULNGUUPTNLModifyItemExtIEsPresentRedundantULNGUUPTNLInformation
	ULNGUUPTNLModifyItemExtIEsPresentRedundantDLNGUUPTNLInformation
)

type ULNGUUPTNLModifyItemExtIEsExtensionValue struct {
	Present                        int
	RedundantULNGUUPTNLInformation *UPTransportLayerInformation `vht5gc:"referenceFieldValue:195,valueLB:0,valueUB:1"`
	RedundantDLNGUUPTNLInformation *UPTransportLayerInformation `vht5gc:"referenceFieldValue:192,valueLB:0,valueUB:1"`
}

type ProtocolExtensionContainerUnavailableGUAMIItemExtIEs struct {
	List []UnavailableGUAMIItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UnavailableGUAMIItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UnavailableGUAMIItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UnavailableGUAMIItemExtIEsPresentNothing int = iota /* No components present */
)

type UnavailableGUAMIItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUPTransportLayerInformationItemExtIEs struct {
	List []UPTransportLayerInformationItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UPTransportLayerInformationItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UPTransportLayerInformationItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UPTransportLayerInformationItemExtIEsPresentNothing int = iota /* No components present */
)

type UPTransportLayerInformationItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUPTransportLayerInformationPairItemExtIEs struct {
	List []UPTransportLayerInformationPairItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UPTransportLayerInformationPairItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UPTransportLayerInformationPairItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UPTransportLayerInformationPairItemExtIEsPresentNothing int = iota /* No components present */
)

type UPTransportLayerInformationPairItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUserLocationInformationEUTRAExtIEs struct {
	List []UserLocationInformationEUTRAExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UserLocationInformationEUTRAExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UserLocationInformationEUTRAExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UserLocationInformationEUTRAExtIEsPresentNothing int = iota /* No components present */
	UserLocationInformationEUTRAExtIEsPresentPSCellInformation
)

type UserLocationInformationEUTRAExtIEsExtensionValue struct {
	Present           int
	PSCellInformation *NGRANCGI `vht5gc:"referenceFieldValue:149,valueLB:0,valueUB:2"`
}

type ProtocolExtensionContainerUserLocationInformationN3IWFExtIEs struct {
	List []UserLocationInformationN3IWFExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UserLocationInformationN3IWFExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UserLocationInformationN3IWFExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UserLocationInformationN3IWFExtIEsPresentNothing int = iota /* No components present */
)

type UserLocationInformationN3IWFExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUserLocationInformationTNGFExtIEs struct {
	List []UserLocationInformationTNGFExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UserLocationInformationTNGFExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UserLocationInformationTNGFExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UserLocationInformationTNGFExtIEsPresentNothing int = iota /* No components present */
)

type UserLocationInformationTNGFExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUserLocationInformationTWIFExtIEs struct {
	List []UserLocationInformationTWIFExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UserLocationInformationTWIFExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UserLocationInformationTWIFExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UserLocationInformationTWIFExtIEsPresentNothing int = iota /* No components present */
)

type UserLocationInformationTWIFExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerUserLocationInformationNRExtIEs struct {
	List []UserLocationInformationNRExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UserLocationInformationNRExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UserLocationInformationNRExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UserLocationInformationNRExtIEsPresentNothing int = iota /* No components present */
	UserLocationInformationNRExtIEsPresentPSCellInformation
	UserLocationInformationNRExtIEsPresentNID
)

type UserLocationInformationNRExtIEsExtensionValue struct {
	Present           int
	PSCellInformation *NGRANCGI `vht5gc:"referenceFieldValue:149,valueLB:0,valueUB:2"`
	NID               *NID      `vht5gc:"referenceFieldValue:263"`
}

type ProtocolExtensionContainerUserPlaneSecurityInformationExtIEs struct {
	List []UserPlaneSecurityInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type UserPlaneSecurityInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue UserPlaneSecurityInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	UserPlaneSecurityInformationExtIEsPresentNothing int = iota /* No components present */
)

type UserPlaneSecurityInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerVolumeTimedReportItemExtIEs struct {
	List []VolumeTimedReportItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type VolumeTimedReportItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue VolumeTimedReportItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	VolumeTimedReportItemExtIEsPresentNothing int = iota /* No components present */
)

type VolumeTimedReportItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerWLANMeasurementConfigurationExtIEs struct {
	List []WLANMeasurementConfigurationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type WLANMeasurementConfigurationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue WLANMeasurementConfigurationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	WLANMeasurementConfigurationExtIEsPresentNothing int = iota /* No components present */
)

type WLANMeasurementConfigurationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerWLANMeasConfigNameItemExtIEs struct {
	List []WLANMeasConfigNameItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type WLANMeasConfigNameItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue WLANMeasConfigNameItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	WLANMeasConfigNameItemExtIEsPresentNothing int = iota /* No components present */
)

type WLANMeasConfigNameItemExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerWUSAssistanceInformationExtIEs struct {
	List []WUSAssistanceInformationExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type WUSAssistanceInformationExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue WUSAssistanceInformationExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	WUSAssistanceInformationExtIEsPresentNothing int = iota /* No components present */
)

type WUSAssistanceInformationExtIEsExtensionValue struct {
	Present int
}

type ProtocolExtensionContainerXnExtTLAItemExtIEs struct {
	List []XnExtTLAItemExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type XnExtTLAItemExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue XnExtTLAItemExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	XnExtTLAItemExtIEsPresentNothing int = iota /* No components present */
	XnExtTLAItemExtIEsPresentSCTPTLAs
)

type XnExtTLAItemExtIEsExtensionValue struct {
	Present  int
	SCTPTLAs *SCTPTLAs `vht5gc:"referenceFieldValue:173"`
}

type ProtocolExtensionContainerXnTNLConfigurationInfoExtIEs struct {
	List []XnTNLConfigurationInfoExtIEs `vht5gc:"sizeLB:1,sizeUB:65535"`
}

type XnTNLConfigurationInfoExtIEs struct {
	Id             ProtocolExtensionID
	Criticality    Criticality
	ExtensionValue XnTNLConfigurationInfoExtIEsExtensionValue `vht5gc:"openType,referenceFieldName:Id"`
}

const (
	XnTNLConfigurationInfoExtIEsPresentNothing int = iota /* No components present */
)

type XnTNLConfigurationInfoExtIEsExtensionValue struct {
	Present int
}

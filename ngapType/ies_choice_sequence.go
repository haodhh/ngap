package ngapType

type AdditionalDLUPTNLInformationForHOList struct {
	List []AdditionalDLUPTNLInformationForHOItem `vht5gc:"valueExt,sizeLB:1,sizeUB:3"`
}

type AdditionalDLUPTNLInformationForHOItem struct {
	AdditionalDLNGUUPTNLInformation        UPTransportLayerInformation `vht5gc:"valueLB:0,valueUB:1"`
	AdditionalQosFlowSetupResponseList     QosFlowListWithDataForwarding
	AdditionalDLForwardingUPTNLInformation *UPTransportLayerInformation                                           `vht5gc:"valueLB:0,valueUB:1,optional"`
	IEExtensions                           *ProtocolExtensionContainerAdditionalDLUPTNLInformationForHOItemExtIEs `vht5gc:"optional"`
}

type AllocationAndRetentionPriority struct {
	PriorityLevelARP        PriorityLevelARP
	PreEmptionCapability    PreEmptionCapability
	PreEmptionVulnerability PreEmptionVulnerability
	IEExtensions            *ProtocolExtensionContainerAllocationAndRetentionPriorityExtIEs `vht5gc:"optional"`
}

type AllowedCAGListPerPLMN struct {
	List []CAGID `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type AllowedNSSAI struct {
	List []AllowedNSSAIItem `vht5gc:"valueExt,sizeLB:1,sizeUB:8"`
}

type AllowedNSSAIItem struct {
	SNSSAI       SNSSAI                                            `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerAllowedNSSAIItemExtIEs `vht5gc:"optional"`
}

type AllowedPNINPNList struct {
	List []AllowedPNINPNItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type AllowedPNINPNItem struct {
	PLMNIdentity          PLMNIdentity
	PNINPNRestricted      PNINPNRestricted
	AllowedCAGListPerPLMN AllowedCAGListPerPLMN
	IEExtensions          *ProtocolExtensionContainerAllowedPNINPNItemExtIEs `vht5gc:"optional"`
}

type AllowedTACs struct {
	List []TAC `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type AlternativeQoSParaSetList struct {
	List []AlternativeQoSParaSetItem `vht5gc:"valueExt,sizeLB:1,sizeUB:8"`
}

type AlternativeQoSParaSetItem struct {
	AlternativeQoSParaSetIndex AlternativeQoSParaSetIndex
	GuaranteedFlowBitRateDL    *BitRate                                                   `vht5gc:"optional"`
	GuaranteedFlowBitRateUL    *BitRate                                                   `vht5gc:"optional"`
	PacketDelayBudget          *PacketDelayBudget                                         `vht5gc:"optional"`
	PacketErrorRate            *PacketErrorRate                                           `vht5gc:"valueExt,optional"`
	IEExtensions               *ProtocolExtensionContainerAlternativeQoSParaSetItemExtIEs `vht5gc:"optional"`
}

const (
	AMFPagingTargetPresentNothing int = iota /* No components present */
	AMFPagingTargetPresentGlobalRANNodeID
	AMFPagingTargetPresentTAI
	AMFPagingTargetPresentChoiceExtensions
)

type AMFPagingTarget struct {
	Present          int
	GlobalRANNodeID  *GlobalRANNodeID `vht5gc:"valueLB:0,valueUB:3"`
	TAI              *TAI             `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerAMFPagingTargetExtIEs
}

type AMFTNLAssociationSetupList struct {
	List []AMFTNLAssociationSetupItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type AMFTNLAssociationSetupItem struct {
	AMFTNLAssociationAddress CPTransportLayerInformation                                 `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions             *ProtocolExtensionContainerAMFTNLAssociationSetupItemExtIEs `vht5gc:"optional"`
}

type AMFTNLAssociationToAddList struct {
	List []AMFTNLAssociationToAddItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type AMFTNLAssociationToAddItem struct {
	AMFTNLAssociationAddress CPTransportLayerInformation `vht5gc:"valueLB:0,valueUB:1"`
	TNLAssociationUsage      *TNLAssociationUsage        `vht5gc:"optional"`
	TNLAddressWeightFactor   TNLAddressWeightFactor
	IEExtensions             *ProtocolExtensionContainerAMFTNLAssociationToAddItemExtIEs `vht5gc:"optional"`
}

type AMFTNLAssociationToRemoveList struct {
	List []AMFTNLAssociationToRemoveItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type AMFTNLAssociationToRemoveItem struct {
	AMFTNLAssociationAddress CPTransportLayerInformation                                    `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions             *ProtocolExtensionContainerAMFTNLAssociationToRemoveItemExtIEs `vht5gc:"optional"`
}

type AMFTNLAssociationToUpdateList struct {
	List []AMFTNLAssociationToUpdateItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type AMFTNLAssociationToUpdateItem struct {
	AMFTNLAssociationAddress CPTransportLayerInformation                                    `vht5gc:"valueLB:0,valueUB:1"`
	TNLAssociationUsage      *TNLAssociationUsage                                           `vht5gc:"optional"`
	TNLAddressWeightFactor   *TNLAddressWeightFactor                                        `vht5gc:"optional"`
	IEExtensions             *ProtocolExtensionContainerAMFTNLAssociationToUpdateItemExtIEs `vht5gc:"optional"`
}

type AreaOfInterest struct {
	AreaOfInterestTAIList     *AreaOfInterestTAIList                          `vht5gc:"optional"`
	AreaOfInterestCellList    *AreaOfInterestCellList                         `vht5gc:"optional"`
	AreaOfInterestRANNodeList *AreaOfInterestRANNodeList                      `vht5gc:"optional"`
	IEExtensions              *ProtocolExtensionContainerAreaOfInterestExtIEs `vht5gc:"optional"`
}

type AreaOfInterestCellList struct {
	List []AreaOfInterestCellItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type AreaOfInterestCellItem struct {
	NGRANCGI     NGRANCGI                                                `vht5gc:"valueLB:0,valueUB:2"`
	IEExtensions *ProtocolExtensionContainerAreaOfInterestCellItemExtIEs `vht5gc:"optional"`
}

type AreaOfInterestList struct {
	List []AreaOfInterestItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type AreaOfInterestItem struct {
	AreaOfInterest               AreaOfInterest `vht5gc:"valueExt"`
	LocationReportingReferenceID LocationReportingReferenceID
	IEExtensions                 *ProtocolExtensionContainerAreaOfInterestItemExtIEs `vht5gc:"optional"`
}

type AreaOfInterestRANNodeList struct {
	List []AreaOfInterestRANNodeItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type AreaOfInterestRANNodeItem struct {
	GlobalRANNodeID GlobalRANNodeID                                            `vht5gc:"valueLB:0,valueUB:3"`
	IEExtensions    *ProtocolExtensionContainerAreaOfInterestRANNodeItemExtIEs `vht5gc:"optional"`
}

type AreaOfInterestTAIList struct {
	List []AreaOfInterestTAIItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type AreaOfInterestTAIItem struct {
	TAI          TAI                                                    `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerAreaOfInterestTAIItemExtIEs `vht5gc:"optional"`
}

type AssistanceDataForPaging struct {
	AssistanceDataForRecommendedCells *AssistanceDataForRecommendedCells                       `vht5gc:"valueExt,optional"`
	PagingAttemptInformation          *PagingAttemptInformation                                `vht5gc:"valueExt,optional"`
	IEExtensions                      *ProtocolExtensionContainerAssistanceDataForPagingExtIEs `vht5gc:"optional"`
}

type AssistanceDataForRecommendedCells struct {
	RecommendedCellsForPaging RecommendedCellsForPaging                                          `vht5gc:"valueExt"`
	IEExtensions              *ProtocolExtensionContainerAssistanceDataForRecommendedCellsExtIEs `vht5gc:"optional"`
}

type AssociatedQosFlowList struct {
	List []AssociatedQosFlowItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type AssociatedQosFlowItem struct {
	QosFlowIdentifier        QosFlowIdentifier
	QosFlowMappingIndication *QosFlowMappingIndication                              `vht5gc:"optional"`
	IEExtensions             *ProtocolExtensionContainerAssociatedQosFlowItemExtIEs `vht5gc:"optional"`
}

const (
	AreaScopeOfMDTNRPresentNothing int = iota /* No components present */
	AreaScopeOfMDTNRPresentCellBased
	AreaScopeOfMDTNRPresentTABased
	AreaScopeOfMDTNRPresentPLMNWide
	AreaScopeOfMDTNRPresentTAIBased
	AreaScopeOfMDTNRPresentChoiceExtensions
)

type AreaScopeOfMDTNR struct {
	Present          int
	CellBased        *CellBasedMDTNR `vht5gc:"valueExt"`
	TABased          *TABasedMDT     `vht5gc:"valueExt"`
	PLMNWide         *Null
	TAIBased         *TAIBasedMDT `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerAreaScopeOfMDTNRExtIEs
}

const (
	AreaScopeOfMDTEUTRAPresentNothing int = iota /* No components present */
	AreaScopeOfMDTEUTRAPresentCellBased
	AreaScopeOfMDTEUTRAPresentTABased
	AreaScopeOfMDTEUTRAPresentPLMNWide
	AreaScopeOfMDTEUTRAPresentTAIBased
	AreaScopeOfMDTEUTRAPresentChoiceExtensions
)

type AreaScopeOfMDTEUTRA struct {
	Present          int
	CellBased        *CellBasedMDTEUTRA `vht5gc:"valueExt"`
	TABased          *TABasedMDT        `vht5gc:"valueExt"`
	PLMNWide         *Null
	TAIBased         *TAIBasedMDT `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerAreaScopeOfMDTEUTRAExtIEs
}

type AreaScopeOfNeighCellsList struct {
	List []AreaScopeOfNeighCellsItem `vht5gc:"valueExt,sizeLB:1,sizeUB:8"`
}

type AreaScopeOfNeighCellsItem struct {
	NrFrequencyInfo NRFrequencyInfo                                            `vht5gc:"valueExt"`
	PciListForMDT   *PCIListForMDT                                             `vht5gc:"optional"`
	IEExtensions    *ProtocolExtensionContainerAreaScopeOfNeighCellsItemExtIEs `vht5gc:"optional"`
}

const (
	BroadcastCancelledAreaListPresentNothing int = iota /* No components present */
	BroadcastCancelledAreaListPresentCellIDCancelledEUTRA
	BroadcastCancelledAreaListPresentTAICancelledEUTRA
	BroadcastCancelledAreaListPresentEmergencyAreaIDCancelledEUTRA
	BroadcastCancelledAreaListPresentCellIDCancelledNR
	BroadcastCancelledAreaListPresentTAICancelledNR
	BroadcastCancelledAreaListPresentEmergencyAreaIDCancelledNR
	BroadcastCancelledAreaListPresentChoiceExtensions
)

type BroadcastCancelledAreaList struct {
	Present                       int
	CellIDCancelledEUTRA          *CellIDCancelledEUTRA
	TAICancelledEUTRA             *TAICancelledEUTRA
	EmergencyAreaIDCancelledEUTRA *EmergencyAreaIDCancelledEUTRA
	CellIDCancelledNR             *CellIDCancelledNR
	TAICancelledNR                *TAICancelledNR
	EmergencyAreaIDCancelledNR    *EmergencyAreaIDCancelledNR
	ChoiceExtensions              *ProtocolIESingleContainerBroadcastCancelledAreaListExtIEs
}

const (
	BroadcastCompletedAreaListPresentNothing int = iota /* No components present */
	BroadcastCompletedAreaListPresentCellIDBroadcastEUTRA
	BroadcastCompletedAreaListPresentTAIBroadcastEUTRA
	BroadcastCompletedAreaListPresentEmergencyAreaIDBroadcastEUTRA
	BroadcastCompletedAreaListPresentCellIDBroadcastNR
	BroadcastCompletedAreaListPresentTAIBroadcastNR
	BroadcastCompletedAreaListPresentEmergencyAreaIDBroadcastNR
	BroadcastCompletedAreaListPresentChoiceExtensions
)

type BroadcastCompletedAreaList struct {
	Present                       int
	CellIDBroadcastEUTRA          *CellIDBroadcastEUTRA
	TAIBroadcastEUTRA             *TAIBroadcastEUTRA
	EmergencyAreaIDBroadcastEUTRA *EmergencyAreaIDBroadcastEUTRA
	CellIDBroadcastNR             *CellIDBroadcastNR
	TAIBroadcastNR                *TAIBroadcastNR
	EmergencyAreaIDBroadcastNR    *EmergencyAreaIDBroadcastNR
	ChoiceExtensions              *ProtocolIESingleContainerBroadcastCompletedAreaListExtIEs
}

type BroadcastPLMNList struct {
	List []BroadcastPLMNItem `vht5gc:"valueExt,sizeLB:1,sizeUB:12"`
}

type BroadcastPLMNItem struct {
	PLMNIdentity        PLMNIdentity
	TAISliceSupportList SliceSupportList
	IEExtensions        *ProtocolExtensionContainerBroadcastPLMNItemExtIEs `vht5gc:"optional"`
}

type BluetoothMeasurementConfiguration struct {
	BluetoothMeasConfig         BluetoothMeasConfig
	BluetoothMeasConfigNameList *BluetoothMeasConfigNameList                                       `vht5gc:"optional"`
	BtRssi                      *BtRssi                                                            `vht5gc:"optional"`
	IEExtensions                *ProtocolExtensionContainerBluetoothMeasurementConfigurationExtIEs `vht5gc:"optional"`
}

type BluetoothMeasConfigNameList struct {
	List []BluetoothMeasConfigNameItem `vht5gc:"valueExt,sizeLB:1,sizeUB:4"`
}

type BluetoothMeasConfigNameItem struct {
	BluetoothName BluetoothName
	IEExtensions  *ProtocolExtensionContainerBluetoothMeasConfigNameItemExtIEs `vht5gc:"optional"`
}

type CancelledCellsInEAIEUTRA struct {
	List []CancelledCellsInEAIEUTRAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CancelledCellsInEAIEUTRAItem struct {
	EUTRACGI           EUTRACGI `vht5gc:"valueExt"`
	NumberOfBroadcasts NumberOfBroadcasts
	IEExtensions       *ProtocolExtensionContainerCancelledCellsInEAIEUTRAItemExtIEs `vht5gc:"optional"`
}

type CancelledCellsInEAINR struct {
	List []CancelledCellsInEAINRItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CancelledCellsInEAINRItem struct {
	NRCGI              NRCGI `vht5gc:"valueExt"`
	NumberOfBroadcasts NumberOfBroadcasts
	IEExtensions       *ProtocolExtensionContainerCancelledCellsInEAINRItemExtIEs `vht5gc:"optional"`
}

type CancelledCellsInTAIEUTRA struct {
	List []CancelledCellsInTAIEUTRAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CancelledCellsInTAIEUTRAItem struct {
	EUTRACGI           EUTRACGI `vht5gc:"valueExt"`
	NumberOfBroadcasts NumberOfBroadcasts
	IEExtensions       *ProtocolExtensionContainerCancelledCellsInTAIEUTRAItemExtIEs `vht5gc:"optional"`
}

type CancelledCellsInTAINR struct {
	List []CancelledCellsInTAINRItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CancelledCellsInTAINRItem struct {
	NRCGI              NRCGI `vht5gc:"valueExt"`
	NumberOfBroadcasts NumberOfBroadcasts
	IEExtensions       *ProtocolExtensionContainerCancelledCellsInTAINRItemExtIEs `vht5gc:"optional"`
}

type CandidateCellList struct {
	List []CandidateCellItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type CandidateCellItem struct {
	CandidateCell CandidateCell                                      `vht5gc:"valueLB:0,valueUB:2"`
	IEExtensions  *ProtocolExtensionContainerCandidateCellItemExtIEs `vht5gc:"optional"`
}

const (
	CandidateCellPresentNothing int = iota /* No components present */
	CandidateCellPresentCandidateCGI
	CandidateCellPresentCandidatePCI
	CandidateCellPresentChoiceExtensions
)

type CandidateCell struct {
	Present          int
	CandidateCGI     *CandidateCellID `vht5gc:"valueExt"`
	CandidatePCI     *CandidatePCI    `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerCandidateCellExtIEs
}

type CandidateCellID struct {
	CandidateCellID NRCGI                                            `vht5gc:"valueExt"`
	IEExtensions    *ProtocolExtensionContainerCandidateCellIDExtIEs `vht5gc:"optional"`
}

type CandidatePCI struct {
	CandidatePCI     int64                                         `vht5gc:"valueExt,valueLB:0,valueUB:1007"`
	CandidateNRARFCN int64                                         `vht5gc:"valueLB:0,valueUB:3279165"`
	IEExtensions     *ProtocolExtensionContainerCandidatePCIExtIEs `vht5gc:"optional"`
}

const (
	CausePresentNothing int = iota /* No components present */
	CausePresentRadioNetwork
	CausePresentTransport
	CausePresentNas
	CausePresentProtocol
	CausePresentMisc
	CausePresentChoiceExtensions
)

type Cause struct {
	Present          int
	RadioNetwork     *CauseRadioNetwork
	Transport        *CauseTransport
	Nas              *CauseNas
	Protocol         *CauseProtocol
	Misc             *CauseMisc
	ChoiceExtensions *ProtocolIESingleContainerCauseExtIEs
}

type CellCAGInformation struct {
	NGRANCGI     NGRANCGI `vht5gc:"valueLB:0,valueUB:2"`
	CellCAGList  CellCAGList
	IEExtensions *ProtocolExtensionContainerCellCAGInformationExtIEs `vht5gc:"optional"`
}

type CellCAGList struct {
	List []CAGID `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type CellIDBroadcastEUTRA struct {
	List []CellIDBroadcastEUTRAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CellIDBroadcastEUTRAItem struct {
	EUTRACGI     EUTRACGI                                                  `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerCellIDBroadcastEUTRAItemExtIEs `vht5gc:"optional"`
}

type CellIDBroadcastNR struct {
	List []CellIDBroadcastNRItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CellIDBroadcastNRItem struct {
	NRCGI        NRCGI                                                  `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerCellIDBroadcastNRItemExtIEs `vht5gc:"optional"`
}

type CellIDCancelledEUTRA struct {
	List []CellIDCancelledEUTRAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CellIDCancelledEUTRAItem struct {
	EUTRACGI           EUTRACGI `vht5gc:"valueExt"`
	NumberOfBroadcasts NumberOfBroadcasts
	IEExtensions       *ProtocolExtensionContainerCellIDCancelledEUTRAItemExtIEs `vht5gc:"optional"`
}

type CellIDCancelledNR struct {
	List []CellIDCancelledNRItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CellIDCancelledNRItem struct {
	NRCGI              NRCGI `vht5gc:"valueExt"`
	NumberOfBroadcasts NumberOfBroadcasts
	IEExtensions       *ProtocolExtensionContainerCellIDCancelledNRItemExtIEs `vht5gc:"optional"`
}

const (
	CellIDListForRestartPresentNothing int = iota /* No components present */
	CellIDListForRestartPresentEUTRACGIListforRestart
	CellIDListForRestartPresentNRCGIListforRestart
	CellIDListForRestartPresentChoiceExtensions
)

type CellIDListForRestart struct {
	Present                int
	EUTRACGIListforRestart *EUTRACGIList
	NRCGIListforRestart    *NRCGIList
	ChoiceExtensions       *ProtocolIESingleContainerCellIDListForRestartExtIEs
}

type CellType struct {
	CellSize     CellSize
	IEExtensions *ProtocolExtensionContainerCellTypeExtIEs `vht5gc:"optional"`
}

type CNAssistedRANTuning struct {
	ExpectedUEBehaviour *ExpectedUEBehaviour                                 `vht5gc:"valueExt,optional"`
	IEExtensions        *ProtocolExtensionContainerCNAssistedRANTuningExtIEs `vht5gc:"optional"`
}

type CNTypeRestrictionsForEquivalent struct {
	List []CNTypeRestrictionsForEquivalentItem `vht5gc:"valueExt,sizeLB:1,sizeUB:15"`
}

type CNTypeRestrictionsForEquivalentItem struct {
	PlmnIdentity PLMNIdentity
	CnType       CnType
	IEExtensions *ProtocolExtensionContainerCNTypeRestrictionsForEquivalentItemExtIEs `vht5gc:"optional"`
}

type CompletedCellsInEAIEUTRA struct {
	List []CompletedCellsInEAIEUTRAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CompletedCellsInEAIEUTRAItem struct {
	EUTRACGI     EUTRACGI                                                      `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerCompletedCellsInEAIEUTRAItemExtIEs `vht5gc:"optional"`
}

type CompletedCellsInEAINR struct {
	List []CompletedCellsInEAINRItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CompletedCellsInEAINRItem struct {
	NRCGI        NRCGI                                                      `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerCompletedCellsInEAINRItemExtIEs `vht5gc:"optional"`
}

type CompletedCellsInTAIEUTRA struct {
	List []CompletedCellsInTAIEUTRAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CompletedCellsInTAIEUTRAItem struct {
	EUTRACGI     EUTRACGI                                                      `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerCompletedCellsInTAIEUTRAItemExtIEs `vht5gc:"optional"`
}

type CompletedCellsInTAINR struct {
	List []CompletedCellsInTAINRItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type CompletedCellsInTAINRItem struct {
	NRCGI        NRCGI                                                      `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerCompletedCellsInTAINRItemExtIEs `vht5gc:"optional"`
}

type CoreNetworkAssistanceInformationForInactive struct {
	UEIdentityIndexValue            UEIdentityIndexValue `vht5gc:"valueLB:0,valueUB:1"`
	UESpecificDRX                   *PagingDRX           `vht5gc:"optional"`
	PeriodicRegistrationUpdateTimer PeriodicRegistrationUpdateTimer
	MICOModeIndication              *MICOModeIndication `vht5gc:"optional"`
	TAIListForInactive              TAIListForInactive
	ExpectedUEBehaviour             *ExpectedUEBehaviour                                                         `vht5gc:"valueExt,optional"`
	IEExtensions                    *ProtocolExtensionContainerCoreNetworkAssistanceInformationForInactiveExtIEs `vht5gc:"optional"`
}

type COUNTValueForPDCPSN12 struct {
	PDCPSN12     int64                                                  `vht5gc:"valueLB:0,valueUB:4095"`
	HFNPDCPSN12  int64                                                  `vht5gc:"valueLB:0,valueUB:1048575"`
	IEExtensions *ProtocolExtensionContainerCOUNTValueForPDCPSN12ExtIEs `vht5gc:"optional"`
}

type COUNTValueForPDCPSN18 struct {
	PDCPSN18     int64                                                  `vht5gc:"valueLB:0,valueUB:262143"`
	HFNPDCPSN18  int64                                                  `vht5gc:"valueLB:0,valueUB:16383"`
	IEExtensions *ProtocolExtensionContainerCOUNTValueForPDCPSN18ExtIEs `vht5gc:"optional"`
}

const (
	CPTransportLayerInformationPresentNothing int = iota /* No components present */
	CPTransportLayerInformationPresentEndpointIPAddress
	CPTransportLayerInformationPresentChoiceExtensions
)

type CPTransportLayerInformation struct {
	Present           int
	EndpointIPAddress *TransportLayerAddress
	ChoiceExtensions  *ProtocolIESingleContainerCPTransportLayerInformationExtIEs
}

type CriticalityDiagnostics struct {
	ProcedureCode             *ProcedureCode                                          `vht5gc:"optional"`
	TriggeringMessage         *TriggeringMessage                                      `vht5gc:"optional"`
	ProcedureCriticality      *Criticality                                            `vht5gc:"optional"`
	IEsCriticalityDiagnostics *CriticalityDiagnosticsIEList                           `vht5gc:"optional"`
	IEExtensions              *ProtocolExtensionContainerCriticalityDiagnosticsExtIEs `vht5gc:"optional"`
}

type CriticalityDiagnosticsIEList struct {
	List []CriticalityDiagnosticsIEItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type CriticalityDiagnosticsIEItem struct {
	IECriticality Criticality
	IEID          ProtocolIEID
	TypeOfError   TypeOfError
	IEExtensions  *ProtocolExtensionContainerCriticalityDiagnosticsIEItemExtIEs `vht5gc:"optional"`
}

type CellBasedMDTNR struct {
	CellIdListforMDT CellIdListforMDTNR
	IEExtensions     *ProtocolExtensionContainerCellBasedMDTNRExtIEs `vht5gc:"optional"`
}

type CellIdListforMDTNR struct {
	List []NRCGI `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type CellBasedMDTEUTRA struct {
	CellIdListforMDT CellIdListforMDTEUTRA
	IEExtensions     *ProtocolExtensionContainerCellBasedMDTEUTRAExtIEs `vht5gc:"optional"`
}

type CellIdListforMDTEUTRA struct {
	List []EUTRACGI `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type DataForwardingResponseDRBList struct {
	List []DataForwardingResponseDRBItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type DataForwardingResponseDRBItem struct {
	DRBID                        DRBID
	DLForwardingUPTNLInformation *UPTransportLayerInformation                                   `vht5gc:"valueLB:0,valueUB:1,optional"`
	ULForwardingUPTNLInformation *UPTransportLayerInformation                                   `vht5gc:"valueLB:0,valueUB:1,optional"`
	IEExtensions                 *ProtocolExtensionContainerDataForwardingResponseDRBItemExtIEs `vht5gc:"optional"`
}

type DAPSRequestInfo struct {
	DAPSIndicator DAPSIndicator
	IEExtensions  *ProtocolExtensionContainerDAPSRequestInfoExtIEs `vht5gc:"optional"`
}

type DAPSResponseInfoList struct {
	List []DAPSResponseInfoItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type DAPSResponseInfoItem struct {
	DRBID            DRBID
	DAPSResponseInfo DAPSResponseInfo                                      `vht5gc:"valueExt"`
	IEExtensions     *ProtocolExtensionContainerDAPSResponseInfoItemExtIEs `vht5gc:"optional"`
}

type DAPSResponseInfo struct {
	Dapsresponseindicator Dapsresponseindicator
	IEExtensions          *ProtocolExtensionContainerDAPSResponseInfoExtIEs `vht5gc:"optional"`
}

type DataForwardingResponseERABList struct {
	List []DataForwardingResponseERABListItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type DataForwardingResponseERABListItem struct {
	ERABID                       ERABID
	DLForwardingUPTNLInformation UPTransportLayerInformation                                         `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions                 *ProtocolExtensionContainerDataForwardingResponseERABListItemExtIEs `vht5gc:"optional"`
}

type DLCPSecurityInformation struct {
	DlNASMAC     DLNASMAC
	IEExtensions *ProtocolExtensionContainerDLCPSecurityInformationExtIEs `vht5gc:"optional"`
}

type DRBsSubjectToStatusTransferList struct {
	List []DRBsSubjectToStatusTransferItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type DRBsSubjectToStatusTransferItem struct {
	DRBID        DRBID
	DRBStatusUL  DRBStatusUL                                                      `vht5gc:"valueLB:0,valueUB:2"`
	DRBStatusDL  DRBStatusDL                                                      `vht5gc:"valueLB:0,valueUB:2"`
	IEExtensions *ProtocolExtensionContainerDRBsSubjectToStatusTransferItemExtIEs `vht5gc:"optional"`
}

const (
	DRBStatusDLPresentNothing int = iota /* No components present */
	DRBStatusDLPresentDRBStatusDL12
	DRBStatusDLPresentDRBStatusDL18
	DRBStatusDLPresentChoiceExtensions
)

type DRBStatusDL struct {
	Present          int
	DRBStatusDL12    *DRBStatusDL12 `vht5gc:"valueExt"`
	DRBStatusDL18    *DRBStatusDL18 `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerDRBStatusDLExtIEs
}

type DRBStatusDL12 struct {
	DLCOUNTValue COUNTValueForPDCPSN12                          `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerDRBStatusDL12ExtIEs `vht5gc:"optional"`
}

type DRBStatusDL18 struct {
	DLCOUNTValue COUNTValueForPDCPSN18                          `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerDRBStatusDL18ExtIEs `vht5gc:"optional"`
}

const (
	DRBStatusULPresentNothing int = iota /* No components present */
	DRBStatusULPresentDRBStatusUL12
	DRBStatusULPresentDRBStatusUL18
	DRBStatusULPresentChoiceExtensions
)

type DRBStatusUL struct {
	Present          int
	DRBStatusUL12    *DRBStatusUL12 `vht5gc:"valueExt"`
	DRBStatusUL18    *DRBStatusUL18 `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerDRBStatusULExtIEs
}

type DRBStatusUL12 struct {
	ULCOUNTValue              COUNTValueForPDCPSN12                          `vht5gc:"valueExt"`
	ReceiveStatusOfULPDCPSDUs *BitString                                     `vht5gc:"sizeLB:1,sizeUB:2048,optional"`
	IEExtensions              *ProtocolExtensionContainerDRBStatusUL12ExtIEs `vht5gc:"optional"`
}

type DRBStatusUL18 struct {
	ULCOUNTValue              COUNTValueForPDCPSN18                          `vht5gc:"valueExt"`
	ReceiveStatusOfULPDCPSDUs *BitString                                     `vht5gc:"sizeLB:1,sizeUB:131072,optional"`
	IEExtensions              *ProtocolExtensionContainerDRBStatusUL18ExtIEs `vht5gc:"optional"`
}

type DRBsToQosFlowsMappingList struct {
	List []DRBsToQosFlowsMappingItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type DRBsToQosFlowsMappingItem struct {
	DRBID                 DRBID
	AssociatedQosFlowList AssociatedQosFlowList
	IEExtensions          *ProtocolExtensionContainerDRBsToQosFlowsMappingItemExtIEs `vht5gc:"optional"`
}

type Dynamic5QIDescriptor struct {
	PriorityLevelQos       PriorityLevelQos
	PacketDelayBudget      PacketDelayBudget
	PacketErrorRate        PacketErrorRate                                       `vht5gc:"valueExt"`
	FiveQI                 *FiveQI                                               `vht5gc:"optional"`
	DelayCritical          *DelayCritical                                        `vht5gc:"optional"`
	AveragingWindow        *AveragingWindow                                      `vht5gc:"optional"`
	MaximumDataBurstVolume *MaximumDataBurstVolume                               `vht5gc:"optional"`
	IEExtensions           *ProtocolExtensionContainerDynamic5QIDescriptorExtIEs `vht5gc:"optional"`
}

type EarlyStatusTransferTransparentContainer struct {
	ProcedureStage ProcedureStageChoice                                                     `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions   *ProtocolExtensionContainerEarlyStatusTransferTransparentContainerExtIEs `vht5gc:"optional"`
}

const (
	ProcedureStageChoicePresentNothing int = iota /* No components present */
	ProcedureStageChoicePresentFirstDlCount
	ProcedureStageChoicePresentChoiceExtensions
)

type ProcedureStageChoice struct {
	Present          int
	FirstDlCount     *FirstDLCount `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerProcedureStageChoiceExtIEs
}

type FirstDLCount struct {
	DRBsSubjectToEarlyStatusTransfer DRBsSubjectToEarlyStatusTransferList
	IEExtensions                     *ProtocolExtensionContainerFirstDLCountExtIEs `vht5gc:"optional"`
}

type DRBsSubjectToEarlyStatusTransferList struct {
	List []DRBsSubjectToEarlyStatusTransferItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type DRBsSubjectToEarlyStatusTransferItem struct {
	DRBID        DRBID
	FirstDLCOUNT DRBStatusDL                                                           `vht5gc:"valueLB:0,valueUB:2"`
	IEExtensions *ProtocolExtensionContainerDRBsSubjectToEarlyStatusTransferItemExtIEs `vht5gc:"optional"`
}

type EmergencyAreaIDBroadcastEUTRA struct {
	List []EmergencyAreaIDBroadcastEUTRAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type EmergencyAreaIDBroadcastEUTRAItem struct {
	EmergencyAreaID          EmergencyAreaID
	CompletedCellsInEAIEUTRA CompletedCellsInEAIEUTRA
	IEExtensions             *ProtocolExtensionContainerEmergencyAreaIDBroadcastEUTRAItemExtIEs `vht5gc:"optional"`
}

type EmergencyAreaIDBroadcastNR struct {
	List []EmergencyAreaIDBroadcastNRItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type EmergencyAreaIDBroadcastNRItem struct {
	EmergencyAreaID       EmergencyAreaID
	CompletedCellsInEAINR CompletedCellsInEAINR
	IEExtensions          *ProtocolExtensionContainerEmergencyAreaIDBroadcastNRItemExtIEs `vht5gc:"optional"`
}

type EmergencyAreaIDCancelledEUTRA struct {
	List []EmergencyAreaIDCancelledEUTRAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type EmergencyAreaIDCancelledEUTRAItem struct {
	EmergencyAreaID          EmergencyAreaID
	CancelledCellsInEAIEUTRA CancelledCellsInEAIEUTRA
	IEExtensions             *ProtocolExtensionContainerEmergencyAreaIDCancelledEUTRAItemExtIEs `vht5gc:"optional"`
}

type EmergencyAreaIDCancelledNR struct {
	List []EmergencyAreaIDCancelledNRItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type EmergencyAreaIDCancelledNRItem struct {
	EmergencyAreaID       EmergencyAreaID
	CancelledCellsInEAINR CancelledCellsInEAINR
	IEExtensions          *ProtocolExtensionContainerEmergencyAreaIDCancelledNRItemExtIEs `vht5gc:"optional"`
}

type EmergencyAreaIDList struct {
	List []EmergencyAreaID `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type EmergencyAreaIDListForRestart struct {
	List []EmergencyAreaID `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type EmergencyFallbackIndicator struct {
	EmergencyFallbackRequestIndicator EmergencyFallbackRequestIndicator
	EmergencyServiceTargetCN          *EmergencyServiceTargetCN                                   `vht5gc:"optional"`
	IEExtensions                      *ProtocolExtensionContainerEmergencyFallbackIndicatorExtIEs `vht5gc:"optional"`
}

const (
	ENBIDPresentNothing int = iota /* No components present */
	ENBIDPresentMacroENBID
	ENBIDPresentHomeENBID
	ENBIDPresentShortMacroENBID
	ENBIDPresentLongMacroENBID
	ENBIDPresentChoiceExtensions
)

type ENBID struct {
	Present          int
	MacroENBID       *BitString `vht5gc:"sizeLB:20,sizeUB:20"`
	HomeENBID        *BitString `vht5gc:"sizeLB:28,sizeUB:28"`
	ShortMacroENBID  *BitString `vht5gc:"sizeLB:18,sizeUB:18"`
	LongMacroENBID   *BitString `vht5gc:"sizeLB:21,sizeUB:21"`
	ChoiceExtensions *ProtocolIESingleContainerENBIDExtIEs
}

type EndpointIPAddressAndPort struct {
	EndpointIPAddress TransportLayerAddress
	PortNumber        PortNumber
	IEExtensions      *ProtocolExtensionContainerEndpointIPAddressAndPortExtIEs `vht5gc:"optional"`
}

type EquivalentPLMNs struct {
	List []PLMNIdentity `vht5gc:"valueExt,sizeLB:1,sizeUB:15"`
}

type EPSTAI struct {
	PLMNIdentity PLMNIdentity
	EPSTAC       EPSTAC
	IEExtensions *ProtocolExtensionContainerEPSTAIExtIEs `vht5gc:"optional"`
}

type ERABInformationList struct {
	List []ERABInformationItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type ERABInformationItem struct {
	ERABID       ERABID
	DLForwarding *DLForwarding                                        `vht5gc:"optional"`
	IEExtensions *ProtocolExtensionContainerERABInformationItemExtIEs `vht5gc:"optional"`
}

type EUTRACGI struct {
	PLMNIdentity      PLMNIdentity
	EUTRACellIdentity EUTRACellIdentity
	IEExtensions      *ProtocolExtensionContainerEUTRACGIExtIEs `vht5gc:"optional"`
}

type EUTRACGIList struct {
	List []EUTRACGI `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type EUTRACGIListForWarning struct {
	List []EUTRACGI `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type ExpectedUEActivityBehaviour struct {
	ExpectedActivityPeriod                 *ExpectedActivityPeriod                                      `vht5gc:"optional"`
	ExpectedIdlePeriod                     *ExpectedIdlePeriod                                          `vht5gc:"optional"`
	SourceOfUEActivityBehaviourInformation *SourceOfUEActivityBehaviourInformation                      `vht5gc:"optional"`
	IEExtensions                           *ProtocolExtensionContainerExpectedUEActivityBehaviourExtIEs `vht5gc:"optional"`
}

type ExpectedUEBehaviour struct {
	ExpectedUEActivityBehaviour *ExpectedUEActivityBehaviour                         `vht5gc:"valueExt,optional"`
	ExpectedHOInterval          *ExpectedHOInterval                                  `vht5gc:"optional"`
	ExpectedUEMobility          *ExpectedUEMobility                                  `vht5gc:"optional"`
	ExpectedUEMovingTrajectory  *ExpectedUEMovingTrajectory                          `vht5gc:"optional"`
	IEExtensions                *ProtocolExtensionContainerExpectedUEBehaviourExtIEs `vht5gc:"optional"`
}

type ExpectedUEMovingTrajectory struct {
	List []ExpectedUEMovingTrajectoryItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type ExpectedUEMovingTrajectoryItem struct {
	NGRANCGI         NGRANCGI                                                        `vht5gc:"valueLB:0,valueUB:2"`
	TimeStayedInCell *int64                                                          `vht5gc:"valueLB:0,valueUB:4095,optional"`
	IEExtensions     *ProtocolExtensionContainerExpectedUEMovingTrajectoryItemExtIEs `vht5gc:"optional"`
}

type ExtendedAMFName struct {
	AMFNameVisibleString *AMFNameVisibleString                            `vht5gc:"optional"`
	AMFNameUTF8String    *AMFNameUTF8String                               `vht5gc:"optional"`
	IEExtensions         *ProtocolExtensionContainerExtendedAMFNameExtIEs `vht5gc:"optional"`
}

type ExtendedRANNodeName struct {
	RANNodeNameVisibleString *RANNodeNameVisibleString                            `vht5gc:"optional"`
	RANNodeNameUTF8String    *RANNodeNameUTF8String                               `vht5gc:"optional"`
	IEExtensions             *ProtocolExtensionContainerExtendedRANNodeNameExtIEs `vht5gc:"optional"`
}

type ExtendedRATRestrictionInformation struct {
	PrimaryRATRestriction   BitString                                                          `vht5gc:"sizeExt,sizeLB:8,sizeUB:8"`
	SecondaryRATRestriction BitString                                                          `vht5gc:"sizeExt,sizeLB:8,sizeUB:8"`
	IEExtensions            *ProtocolExtensionContainerExtendedRATRestrictionInformationExtIEs `vht5gc:"optional"`
}

type ExtendedSliceSupportList struct {
	List []SliceSupportItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

const (
	EventTriggerPresentNothing int = iota /* No components present */
	EventTriggerPresentOutOfCoverage
	EventTriggerPresentEventL1LoggedMDTConfig
	EventTriggerPresentChoiceExtensions
)

type EventTrigger struct {
	Present                int
	OutOfCoverage          *OutOfCoverage
	EventL1LoggedMDTConfig *EventL1LoggedMDTConfig `vht5gc:"valueExt"`
	ChoiceExtensions       *ProtocolIESingleContainerEventTriggerExtIEs
}

type EventL1LoggedMDTConfig struct {
	L1Threshold   MeasurementThresholdL1LoggedMDT `vht5gc:"valueLB:0,valueUB:2"`
	Hysteresis    Hysteresis
	TimeToTrigger TimeToTrigger
	IEExtensions  *ProtocolExtensionContainerEventL1LoggedMDTConfigExtIEs `vht5gc:"optional"`
}

const (
	MeasurementThresholdL1LoggedMDTPresentNothing int = iota /* No components present */
	MeasurementThresholdL1LoggedMDTPresentThresholdRSRP
	MeasurementThresholdL1LoggedMDTPresentThresholdRSRQ
	MeasurementThresholdL1LoggedMDTPresentChoiceExtensions
)

type MeasurementThresholdL1LoggedMDT struct {
	Present          int
	ThresholdRSRP    *ThresholdRSRP
	ThresholdRSRQ    *ThresholdRSRQ
	ChoiceExtensions *ProtocolIESingleContainerMeasurementThresholdL1LoggedMDTExtIEs
}

type FailureIndication struct {
	UERLFReportContainer UERLFReportContainer                               `vht5gc:"valueLB:0,valueUB:2"`
	IEExtensions         *ProtocolExtensionContainerFailureIndicationExtIEs `vht5gc:"optional"`
}

type FiveGSTMSI struct {
	AMFSetID     AMFSetID
	AMFPointer   AMFPointer
	FiveGTMSI    FiveGTMSI
	IEExtensions *ProtocolExtensionContainerFiveGSTMSIExtIEs `vht5gc:"optional"`
}

type ForbiddenAreaInformation struct {
	List []ForbiddenAreaInformationItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type ForbiddenAreaInformationItem struct {
	PLMNIdentity  PLMNIdentity
	ForbiddenTACs ForbiddenTACs
	IEExtensions  *ProtocolExtensionContainerForbiddenAreaInformationItemExtIEs `vht5gc:"optional"`
}

type ForbiddenTACs struct {
	List []TAC `vht5gc:"valueExt,sizeLB:1,sizeUB:4096"`
}

type FromEUTRANtoNGRAN struct {
	SourceeNBID       IntersystemSONeNBID                                `vht5gc:"valueExt"`
	TargetNGRANnodeID IntersystemSONNGRANnodeID                          `vht5gc:"valueExt"`
	IEExtensions      *ProtocolExtensionContainerFromEUTRANtoNGRANExtIEs `vht5gc:"optional"`
}

type FromNGRANtoEUTRAN struct {
	SourceNGRANnodeID IntersystemSONNGRANnodeID                          `vht5gc:"valueExt"`
	TargeteNBID       IntersystemSONeNBID                                `vht5gc:"valueExt"`
	IEExtensions      *ProtocolExtensionContainerFromNGRANtoEUTRANExtIEs `vht5gc:"optional"`
}

type GBRQosInformation struct {
	MaximumFlowBitRateDL    BitRate
	MaximumFlowBitRateUL    BitRate
	GuaranteedFlowBitRateDL BitRate
	GuaranteedFlowBitRateUL BitRate
	NotificationControl     *NotificationControl                               `vht5gc:"optional"`
	MaximumPacketLossRateDL *PacketLossRate                                    `vht5gc:"optional"`
	MaximumPacketLossRateUL *PacketLossRate                                    `vht5gc:"optional"`
	IEExtensions            *ProtocolExtensionContainerGBRQosInformationExtIEs `vht5gc:"optional"`
}

type GlobalENBID struct {
	PLMNidentity PLMNIdentity
	ENBID        ENBID                                        `vht5gc:"valueLB:0,valueUB:4"`
	IEExtensions *ProtocolExtensionContainerGlobalENBIDExtIEs `vht5gc:"optional"`
}

type GlobalGNBID struct {
	PLMNIdentity PLMNIdentity
	GNBID        GNBID                                        `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions *ProtocolExtensionContainerGlobalGNBIDExtIEs `vht5gc:"optional"`
}

type GlobalN3IWFID struct {
	PLMNIdentity PLMNIdentity
	N3IWFID      N3IWFID                                        `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions *ProtocolExtensionContainerGlobalN3IWFIDExtIEs `vht5gc:"optional"`
}

type GlobalLineID struct {
	GlobalLineIdentity GlobalLineIdentity
	LineType           *LineType                                     `vht5gc:"optional"`
	IEExtensions       *ProtocolExtensionContainerGlobalLineIDExtIEs `vht5gc:"optional"`
}

type GlobalNgENBID struct {
	PLMNIdentity PLMNIdentity
	NgENBID      NgENBID                                        `vht5gc:"valueLB:0,valueUB:3"`
	IEExtensions *ProtocolExtensionContainerGlobalNgENBIDExtIEs `vht5gc:"optional"`
}

const (
	GlobalRANNodeIDPresentNothing int = iota /* No components present */
	GlobalRANNodeIDPresentGlobalGNBID
	GlobalRANNodeIDPresentGlobalNgENBID
	GlobalRANNodeIDPresentGlobalN3IWFID
	GlobalRANNodeIDPresentChoiceExtensions
)

type GlobalRANNodeID struct {
	Present          int
	GlobalGNBID      *GlobalGNBID   `vht5gc:"valueExt"`
	GlobalNgENBID    *GlobalNgENBID `vht5gc:"valueExt"`
	GlobalN3IWFID    *GlobalN3IWFID `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerGlobalRANNodeIDExtIEs
}

type GlobalTNGFID struct {
	PLMNIdentity PLMNIdentity
	TNGFID       TNGFID                                        `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions *ProtocolExtensionContainerGlobalTNGFIDExtIEs `vht5gc:"optional"`
}

type GlobalTWIFID struct {
	PLMNIdentity PLMNIdentity
	TWIFID       TWIFID                                        `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions *ProtocolExtensionContainerGlobalTWIFIDExtIEs `vht5gc:"optional"`
}

type GlobalWAGFID struct {
	PLMNIdentity PLMNIdentity
	WAGFID       WAGFID                                        `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions *ProtocolExtensionContainerGlobalWAGFIDExtIEs `vht5gc:"optional"`
}

const (
	GNBIDPresentNothing int = iota /* No components present */
	GNBIDPresentGNBID
	GNBIDPresentChoiceExtensions
)

type GNBID struct {
	Present          int
	GNBID            *BitString `vht5gc:"sizeLB:22,sizeUB:32"`
	ChoiceExtensions *ProtocolIESingleContainerGNBIDExtIEs
}

type GTPTunnel struct {
	TransportLayerAddress TransportLayerAddress
	GTPTEID               GTPTEID
	IEExtensions          *ProtocolExtensionContainerGTPTunnelExtIEs `vht5gc:"optional"`
}

type GUAMI struct {
	PLMNIdentity PLMNIdentity
	AMFRegionID  AMFRegionID
	AMFSetID     AMFSetID
	AMFPointer   AMFPointer
	IEExtensions *ProtocolExtensionContainerGUAMIExtIEs `vht5gc:"optional"`
}

type HandoverCommandTransfer struct {
	DLForwardingUPTNLInformation  *UPTransportLayerInformation                             `vht5gc:"valueLB:0,valueUB:1,optional"`
	QosFlowToBeForwardedList      *QosFlowToBeForwardedList                                `vht5gc:"optional"`
	DataForwardingResponseDRBList *DataForwardingResponseDRBList                           `vht5gc:"optional"`
	IEExtensions                  *ProtocolExtensionContainerHandoverCommandTransferExtIEs `vht5gc:"optional"`
}

type HandoverPreparationUnsuccessfulTransfer struct {
	Cause        Cause                                                                    `vht5gc:"valueLB:0,valueUB:5"`
	IEExtensions *ProtocolExtensionContainerHandoverPreparationUnsuccessfulTransferExtIEs `vht5gc:"optional"`
}

type HandoverRequestAcknowledgeTransfer struct {
	DLNGUUPTNLInformation         UPTransportLayerInformation  `vht5gc:"valueLB:0,valueUB:1"`
	DLForwardingUPTNLInformation  *UPTransportLayerInformation `vht5gc:"valueLB:0,valueUB:1,optional"`
	SecurityResult                *SecurityResult              `vht5gc:"valueExt,optional"`
	QosFlowSetupResponseList      QosFlowListWithDataForwarding
	QosFlowFailedToSetupList      *QosFlowListWithCause                                               `vht5gc:"optional"`
	DataForwardingResponseDRBList *DataForwardingResponseDRBList                                      `vht5gc:"optional"`
	IEExtensions                  *ProtocolExtensionContainerHandoverRequestAcknowledgeTransferExtIEs `vht5gc:"optional"`
}

type HandoverRequiredTransfer struct {
	DirectForwardingPathAvailability *DirectForwardingPathAvailability                         `vht5gc:"optional"`
	IEExtensions                     *ProtocolExtensionContainerHandoverRequiredTransferExtIEs `vht5gc:"optional"`
}

type HandoverResourceAllocationUnsuccessfulTransfer struct {
	Cause                  Cause                                                                           `vht5gc:"valueLB:0,valueUB:5"`
	CriticalityDiagnostics *CriticalityDiagnostics                                                         `vht5gc:"valueExt,optional"`
	IEExtensions           *ProtocolExtensionContainerHandoverResourceAllocationUnsuccessfulTransferExtIEs `vht5gc:"optional"`
}

type HOReport struct {
	HandoverReportType     HandoverReportType
	HandoverCause          Cause                                     `vht5gc:"valueLB:0,valueUB:5"`
	SourcecellCGI          NGRANCGI                                  `vht5gc:"valueLB:0,valueUB:2"`
	TargetcellCGI          NGRANCGI                                  `vht5gc:"valueLB:0,valueUB:2"`
	ReestablishmentcellCGI *NGRANCGI                                 `vht5gc:"valueLB:0,valueUB:2,optional"`
	SourcecellCRNTI        *BitString                                `vht5gc:"sizeLB:16,sizeUB:16,optional"`
	TargetcellinEUTRAN     *EUTRACGI                                 `vht5gc:"valueExt,optional"`
	MobilityInformation    *MobilityInformation                      `vht5gc:"optional"`
	UERLFReportContainer   *UERLFReportContainer                     `vht5gc:"valueLB:0,valueUB:2,optional"`
	IEExtensions           *ProtocolExtensionContainerHOReportExtIEs `vht5gc:"optional"`
}

type InfoOnRecommendedCellsAndRANNodesForPaging struct {
	RecommendedCellsForPaging  RecommendedCellsForPaging                                                   `vht5gc:"valueExt"`
	RecommendRANNodesForPaging RecommendedRANNodesForPaging                                                `vht5gc:"valueExt"`
	IEExtensions               *ProtocolExtensionContainerInfoOnRecommendedCellsAndRANNodesForPagingExtIEs `vht5gc:"optional"`
}

type ImmediateMDTNr struct {
	MeasurementsToActivate            MeasurementsToActivate
	M1Configuration                   *M1Configuration                                `vht5gc:"valueExt,optional"`
	M4Configuration                   *M4Configuration                                `vht5gc:"valueExt,optional"`
	M5Configuration                   *M5Configuration                                `vht5gc:"valueExt,optional"`
	M6Configuration                   *M6Configuration                                `vht5gc:"valueExt,optional"`
	M7Configuration                   *M7Configuration                                `vht5gc:"valueExt,optional"`
	BluetoothMeasurementConfiguration *BluetoothMeasurementConfiguration              `vht5gc:"valueExt,optional"`
	WLANMeasurementConfiguration      *WLANMeasurementConfiguration                   `vht5gc:"valueExt,optional"`
	MDTLocationInfo                   *MDTLocationInfo                                `vht5gc:"valueExt,optional"`
	SensorMeasurementConfiguration    *SensorMeasurementConfiguration                 `vht5gc:"valueExt,optional"`
	IEExtensions                      *ProtocolExtensionContainerImmediateMDTNrExtIEs `vht5gc:"optional"`
}

type InterSystemFailureIndication struct {
	UERLFReportContainer *UERLFReportContainer                                         `vht5gc:"valueLB:0,valueUB:2,optional"`
	IEExtensions         *ProtocolExtensionContainerInterSystemFailureIndicationExtIEs `vht5gc:"optional"`
}

type IntersystemSONConfigurationTransfer struct {
	TransferType              IntersystemSONTransferType                                           `vht5gc:"valueLB:0,valueUB:2"`
	IntersystemSONInformation IntersystemSONInformation                                            `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions              *ProtocolExtensionContainerIntersystemSONConfigurationTransferExtIEs `vht5gc:"optional"`
}

const (
	IntersystemSONTransferTypePresentNothing int = iota /* No components present */
	IntersystemSONTransferTypePresentFromEUTRANtoNGRAN
	IntersystemSONTransferTypePresentFromNGRANtoEUTRAN
	IntersystemSONTransferTypePresentChoiceExtensions
)

type IntersystemSONTransferType struct {
	Present           int
	FromEUTRANtoNGRAN *FromEUTRANtoNGRAN `vht5gc:"valueExt"`
	FromNGRANtoEUTRAN *FromNGRANtoEUTRAN `vht5gc:"valueExt"`
	ChoiceExtensions  *ProtocolIESingleContainerIntersystemSONTransferTypeExtIEs
}

type IntersystemSONeNBID struct {
	GlobaleNBID    GlobalENBID                                          `vht5gc:"valueExt"`
	SelectedEPSTAI EPSTAI                                               `vht5gc:"valueExt"`
	IEExtensions   *ProtocolExtensionContainerIntersystemSONeNBIDExtIEs `vht5gc:"optional"`
}

type IntersystemSONNGRANnodeID struct {
	GlobalRANNodeID GlobalRANNodeID                                            `vht5gc:"valueLB:0,valueUB:3"`
	SelectedTAI     TAI                                                        `vht5gc:"valueExt"`
	IEExtensions    *ProtocolExtensionContainerIntersystemSONNGRANnodeIDExtIEs `vht5gc:"optional"`
}

const (
	IntersystemSONInformationPresentNothing int = iota /* No components present */
	IntersystemSONInformationPresentIntersystemSONInformationReport
	IntersystemSONInformationPresentChoiceExtensions
)

type IntersystemSONInformation struct {
	Present                         int
	IntersystemSONInformationReport *IntersystemSONInformationReport `vht5gc:"valueLB:0,valueUB:2"`
	ChoiceExtensions                *ProtocolIESingleContainerIntersystemSONInformationExtIEs
}

const (
	IntersystemSONInformationReportPresentNothing int = iota /* No components present */
	IntersystemSONInformationReportPresentHOReportInformation
	IntersystemSONInformationReportPresentFailureIndicationInformation
	IntersystemSONInformationReportPresentChoiceExtensions
)

type IntersystemSONInformationReport struct {
	Present                      int
	HOReportInformation          *InterSystemHOReport          `vht5gc:"valueExt"`
	FailureIndicationInformation *InterSystemFailureIndication `vht5gc:"valueExt"`
	ChoiceExtensions             *ProtocolIESingleContainerIntersystemSONInformationReportExtIEs
}

type InterSystemHOReport struct {
	HandoverReportType InterSystemHandoverReportType                        `vht5gc:"valueLB:0,valueUB:2"`
	IEExtensions       *ProtocolExtensionContainerInterSystemHOReportExtIEs `vht5gc:"optional"`
}

const (
	InterSystemHandoverReportTypePresentNothing int = iota /* No components present */
	InterSystemHandoverReportTypePresentTooearlyIntersystemHO
	InterSystemHandoverReportTypePresentIntersystemUnnecessaryHO
	InterSystemHandoverReportTypePresentChoiceExtensions
)

type InterSystemHandoverReportType struct {
	Present                  int
	TooearlyIntersystemHO    *TooearlyIntersystemHO    `vht5gc:"valueExt"`
	IntersystemUnnecessaryHO *IntersystemUnnecessaryHO `vht5gc:"valueExt"`
	ChoiceExtensions         *ProtocolIESingleContainerInterSystemHandoverReportTypeExtIEs
}

type IntersystemUnnecessaryHO struct {
	SourcecellID      NGRANCGI `vht5gc:"valueLB:0,valueUB:2"`
	TargetcellID      EUTRACGI `vht5gc:"valueExt"`
	EarlyIRATHO       EarlyIRATHO
	CandidateCellList CandidateCellList
	IEExtensions      *ProtocolExtensionContainerIntersystemUnnecessaryHOExtIEs `vht5gc:"optional"`
}

type LAI struct {
	PLMNidentity PLMNIdentity
	LAC          LAC
	IEExtensions *ProtocolExtensionContainerLAIExtIEs `vht5gc:"optional"`
}

const (
	LastVisitedCellInformationPresentNothing int = iota /* No components present */
	LastVisitedCellInformationPresentNGRANCell
	LastVisitedCellInformationPresentEUTRANCell
	LastVisitedCellInformationPresentUTRANCell
	LastVisitedCellInformationPresentGERANCell
	LastVisitedCellInformationPresentChoiceExtensions
)

type LastVisitedCellInformation struct {
	Present          int
	NGRANCell        *LastVisitedNGRANCellInformation `vht5gc:"valueExt"`
	EUTRANCell       *LastVisitedEUTRANCellInformation
	UTRANCell        *LastVisitedUTRANCellInformation
	GERANCell        *LastVisitedGERANCellInformation
	ChoiceExtensions *ProtocolIESingleContainerLastVisitedCellInformationExtIEs
}

type LastVisitedCellItem struct {
	LastVisitedCellInformation LastVisitedCellInformation                           `vht5gc:"valueLB:0,valueUB:4"`
	IEExtensions               *ProtocolExtensionContainerLastVisitedCellItemExtIEs `vht5gc:"optional"`
}

type LastVisitedNGRANCellInformation struct {
	GlobalCellID                          NGRANCGI `vht5gc:"valueLB:0,valueUB:2"`
	CellType                              CellType `vht5gc:"valueExt"`
	TimeUEStayedInCell                    TimeUEStayedInCell
	TimeUEStayedInCellEnhancedGranularity *TimeUEStayedInCellEnhancedGranularity                           `vht5gc:"optional"`
	HOCauseValue                          *Cause                                                           `vht5gc:"valueLB:0,valueUB:5,optional"`
	IEExtensions                          *ProtocolExtensionContainerLastVisitedNGRANCellInformationExtIEs `vht5gc:"optional"`
}

type LocationReportingRequestType struct {
	EventType                                 EventType
	ReportArea                                ReportArea
	AreaOfInterestList                        *AreaOfInterestList                                           `vht5gc:"optional"`
	LocationReportingReferenceIDToBeCancelled *LocationReportingReferenceID                                 `vht5gc:"optional"`
	IEExtensions                              *ProtocolExtensionContainerLocationReportingRequestTypeExtIEs `vht5gc:"optional"`
}

type LoggedMDTNr struct {
	LoggingInterval                   LoggingInterval
	LoggingDuration                   LoggingDuration
	LoggedMDTTrigger                  LoggedMDTTrigger                             `vht5gc:"valueLB:0,valueUB:2"`
	BluetoothMeasurementConfiguration *BluetoothMeasurementConfiguration           `vht5gc:"valueExt,optional"`
	WLANMeasurementConfiguration      *WLANMeasurementConfiguration                `vht5gc:"valueExt,optional"`
	SensorMeasurementConfiguration    *SensorMeasurementConfiguration              `vht5gc:"valueExt,optional"`
	AreaScopeOfNeighCellsList         *AreaScopeOfNeighCellsList                   `vht5gc:"optional"`
	IEExtensions                      *ProtocolExtensionContainerLoggedMDTNrExtIEs `vht5gc:"optional"`
}

const (
	LoggedMDTTriggerPresentNothing int = iota /* No components present */
	LoggedMDTTriggerPresentPeriodical
	LoggedMDTTriggerPresentEventTrigger
	LoggedMDTTriggerPresentChoiceExtensions
)

type LoggedMDTTrigger struct {
	Present          int
	Periodical       *Null
	EventTrigger     *EventTrigger `vht5gc:"valueLB:0,valueUB:2"`
	ChoiceExtensions *ProtocolIESingleContainerLoggedMDTTriggerExtIEs
}

type LTEV2XServicesAuthorized struct {
	VehicleUE    *VehicleUE                                                `vht5gc:"optional"`
	PedestrianUE *PedestrianUE                                             `vht5gc:"optional"`
	IEExtensions *ProtocolExtensionContainerLTEV2XServicesAuthorizedExtIEs `vht5gc:"optional"`
}

type LTEUESidelinkAggregateMaximumBitrate struct {
	UESidelinkAggregateMaximumBitRate BitRate
	IEExtensions                      *ProtocolExtensionContainerLTEUESidelinkAggregateMaximumBitratesExtIEs `vht5gc:"optional"`
}

type MobilityRestrictionList struct {
	ServingPLMN              PLMNIdentity
	EquivalentPLMNs          *EquivalentPLMNs                                         `vht5gc:"optional"`
	RATRestrictions          *RATRestrictions                                         `vht5gc:"optional"`
	ForbiddenAreaInformation *ForbiddenAreaInformation                                `vht5gc:"optional"`
	ServiceAreaInformation   *ServiceAreaInformation                                  `vht5gc:"optional"`
	IEExtensions             *ProtocolExtensionContainerMobilityRestrictionListExtIEs `vht5gc:"optional"`
}

type MDTPLMNList struct {
	List []PLMNIdentity `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type MDTConfiguration struct {
	MdtConfigNR    *MDTConfigurationNR                               `vht5gc:"valueExt,optional"`
	MdtConfigEUTRA *MDTConfigurationEUTRA                            `vht5gc:"valueExt,optional"`
	IEExtensions   *ProtocolExtensionContainerMDTConfigurationExtIEs `vht5gc:"optional"`
}

type MDTConfigurationNR struct {
	MdtActivation              MDTActivation
	AreaScopeOfMDT             AreaScopeOfMDTNR                                    `vht5gc:"valueLB:0,valueUB:4"`
	MDTModeNr                  MDTModeNr                                           `vht5gc:"valueLB:0,valueUB:2"`
	SignallingBasedMDTPLMNList *MDTPLMNList                                        `vht5gc:"optional"`
	IEExtensions               *ProtocolExtensionContainerMDTConfigurationNRExtIEs `vht5gc:"optional"`
}

type MDTConfigurationEUTRA struct {
	MdtActivation              MDTActivation
	AreaScopeOfMDT             AreaScopeOfMDTEUTRA `vht5gc:"valueLB:0,valueUB:4"`
	MDTMode                    MDTModeEutra
	SignallingBasedMDTPLMNList *MDTPLMNList                                           `vht5gc:"optional"`
	IEExtensions               *ProtocolExtensionContainerMDTConfigurationEUTRAExtIEs `vht5gc:"optional"`
}

const (
	MDTModeNrPresentNothing int = iota /* No components present */
	MDTModeNrPresentImmediateMDTNr
	MDTModeNrPresentLoggedMDTNr
	MDTModeNrPresentChoiceExtensions
)

type MDTModeNr struct {
	Present          int
	ImmediateMDTNr   *ImmediateMDTNr `vht5gc:"valueExt"`
	LoggedMDTNr      *LoggedMDTNr    `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerMDTModeNrExtIEs
}

type M1Configuration struct {
	M1reportingTrigger  M1ReportingTrigger
	M1thresholdEventA2  *M1ThresholdEventA2                              `vht5gc:"valueExt,optional"`
	M1periodicReporting *M1PeriodicReporting                             `vht5gc:"valueExt,optional"`
	IEExtensions        *ProtocolExtensionContainerM1ConfigurationExtIEs `vht5gc:"optional"`
}

type M1ThresholdEventA2 struct {
	M1ThresholdType M1ThresholdType                                     `vht5gc:"valueLB:0,valueUB:3"`
	IEExtensions    *ProtocolExtensionContainerM1ThresholdEventA2ExtIEs `vht5gc:"optional"`
}

const (
	M1ThresholdTypePresentNothing int = iota /* No components present */
	M1ThresholdTypePresentThresholdRSRP
	M1ThresholdTypePresentThresholdRSRQ
	M1ThresholdTypePresentThresholdSINR
	M1ThresholdTypePresentChoiceExtensions
)

type M1ThresholdType struct {
	Present          int
	ThresholdRSRP    *ThresholdRSRP
	ThresholdRSRQ    *ThresholdRSRQ
	ThresholdSINR    *ThresholdSINR
	ChoiceExtensions *ProtocolIESingleContainerM1ThresholdTypeExtIEs
}

type M1PeriodicReporting struct {
	ReportInterval ReportIntervalMDT
	ReportAmount   ReportAmountMDT
	IEExtensions   *ProtocolExtensionContainerM1PeriodicReportingExtIEs `vht5gc:"optional"`
}

type M4Configuration struct {
	M4period     M4period
	M4LinksToLog LinksToLog
	IEExtensions *ProtocolExtensionContainerM4ConfigurationExtIEs `vht5gc:"optional"`
}

type M5Configuration struct {
	M5period     M5period
	M5LinksToLog LinksToLog
	IEExtensions *ProtocolExtensionContainerM5ConfigurationExtIEs `vht5gc:"optional"`
}

type M6Configuration struct {
	M6reportInterval M6reportInterval
	M6LinksToLog     LinksToLog
	IEExtensions     *ProtocolExtensionContainerM6ConfigurationExtIEs `vht5gc:"optional"`
}

type M7Configuration struct {
	M7period     M7period
	M7LinksToLog LinksToLog
	IEExtensions *ProtocolExtensionContainerM7ConfigurationExtIEs `vht5gc:"optional"`
}

type MDTLocationInfo struct {
	MDTLocationInformation MDTLocationInformation
	IEExtensions           *ProtocolExtensionContainerMDTLocationInfoExtIEs `vht5gc:"optional"`
}

const (
	N3IWFIDPresentNothing int = iota /* No components present */
	N3IWFIDPresentN3IWFID
	N3IWFIDPresentChoiceExtensions
)

type N3IWFID struct {
	Present          int
	N3IWFID          *BitString `vht5gc:"sizeLB:16,sizeUB:16"`
	ChoiceExtensions *ProtocolIESingleContainerN3IWFIDExtIEs
}

type NBIoTPagingEDRXInfo struct {
	NBIoTPagingEDRXCycle  NBIoTPagingEDRXCycle
	NBIoTPagingTimeWindow *NBIoTPagingTimeWindow                               `vht5gc:"optional"`
	IEExtensions          *ProtocolExtensionContainerNBIoTPagingEDRXInfoExtIEs `vht5gc:"optional"`
}

const (
	NgENBIDPresentNothing int = iota /* No components present */
	NgENBIDPresentMacroNgENBID
	NgENBIDPresentShortMacroNgENBID
	NgENBIDPresentLongMacroNgENBID
	NgENBIDPresentChoiceExtensions
)

type NgENBID struct {
	Present           int
	MacroNgENBID      *BitString `vht5gc:"sizeLB:20,sizeUB:20"`
	ShortMacroNgENBID *BitString `vht5gc:"sizeLB:18,sizeUB:18"`
	LongMacroNgENBID  *BitString `vht5gc:"sizeLB:21,sizeUB:21"`
	ChoiceExtensions  *ProtocolIESingleContainerNgENBIDExtIEs
}

const (
	NGRANCGIPresentNothing int = iota /* No components present */
	NGRANCGIPresentNRCGI
	NGRANCGIPresentEUTRACGI
	NGRANCGIPresentChoiceExtensions
)

type NGRANCGI struct {
	Present          int
	NRCGI            *NRCGI    `vht5gc:"valueExt"`
	EUTRACGI         *EUTRACGI `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerNGRANCGIExtIEs
}

type NGRANTNLAssociationToRemoveList struct {
	List []NGRANTNLAssociationToRemoveItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type NGRANTNLAssociationToRemoveItem struct {
	TNLAssociationTransportLayerAddress    CPTransportLayerInformation                                      `vht5gc:"valueLB:0,valueUB:1"`
	TNLAssociationTransportLayerAddressAMF *CPTransportLayerInformation                                     `vht5gc:"valueLB:0,valueUB:1,optional"`
	IEExtensions                           *ProtocolExtensionContainerNGRANTNLAssociationToRemoveItemExtIEs `vht5gc:"optional"`
}

type NonDynamic5QIDescriptor struct {
	FiveQI                 FiveQI
	PriorityLevelQos       *PriorityLevelQos                                        `vht5gc:"optional"`
	AveragingWindow        *AveragingWindow                                         `vht5gc:"optional"`
	MaximumDataBurstVolume *MaximumDataBurstVolume                                  `vht5gc:"optional"`
	IEExtensions           *ProtocolExtensionContainerNonDynamic5QIDescriptorExtIEs `vht5gc:"optional"`
}

type NotAllowedTACs struct {
	List []TAC `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

const (
	NPNAccessInformationPresentNothing int = iota /* No components present */
	NPNAccessInformationPresentPNINPNAccessInformation
	NPNAccessInformationPresentChoiceExtensions
)

type NPNAccessInformation struct {
	Present                 int
	PNINPNAccessInformation *CellCAGList
	ChoiceExtensions        *ProtocolIESingleContainerNPNAccessInformationExtIEs
}

const (
	NPNMobilityInformationPresentNothing int = iota /* No components present */
	NPNMobilityInformationPresentSNPNMobilityInformation
	NPNMobilityInformationPresentPNINPNMobilityInformation
	NPNMobilityInformationPresentChoiceExtensions
)

type NPNMobilityInformation struct {
	Present                   int
	SNPNMobilityInformation   *SNPNMobilityInformation   `vht5gc:"valueExt"`
	PNINPNMobilityInformation *PNINPNMobilityInformation `vht5gc:"valueExt"`
	ChoiceExtensions          *ProtocolIESingleContainerNPNMobilityInformationExtIEs
}

const (
	NPNPagingAssistanceInformationPresentNothing int = iota /* No components present */
	NPNPagingAssistanceInformationPresentPNINPNPagingAssistance
	NPNPagingAssistanceInformationPresentChoiceExtensions
)

type NPNPagingAssistanceInformation struct {
	Present                int
	PNINPNPagingAssistance *AllowedPNINPNList
	ChoiceExtensions       *ProtocolIESingleContainerNPNPagingAssistanceInformationExtIEs
}

const (
	NPNSupportPresentNothing int = iota /* No components present */
	NPNSupportPresentSNPN
	NPNSupportPresentChoiceExtensions
)

type NPNSupport struct {
	Present          int
	SNPN             *NID
	ChoiceExtensions *ProtocolIESingleContainerNPNSupportExtIEs
}

type NRCGI struct {
	PLMNIdentity   PLMNIdentity
	NRCellIdentity NRCellIdentity
	IEExtensions   *ProtocolExtensionContainerNRCGIExtIEs `vht5gc:"optional"`
}

type NRCGIList struct {
	List []NRCGI `vht5gc:"valueExt,sizeLB:1,sizeUB:16384"`
}

type NRCGIListForWarning struct {
	List []NRCGI `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type NRFrequencyBandList struct {
	List []NRFrequencyBandItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type NRFrequencyBandItem struct {
	NrFrequencyBand NRFrequencyBand
	IEExtensions    *ProtocolExtensionContainerNRFrequencyBandItemExtIEs `vht5gc:"optional"`
}

type NRFrequencyInfo struct {
	NrARFCN           NRARFCN
	FrequencyBandList NRFrequencyBandList
	IEExtensions      *ProtocolExtensionContainerNRFrequencyInfoExtIEs `vht5gc:"optional"`
}

type NRV2XServicesAuthorized struct {
	VehicleUE    *VehicleUE                                               `vht5gc:"optional"`
	PedestrianUE *PedestrianUE                                            `vht5gc:"optional"`
	IEExtensions *ProtocolExtensionContainerNRV2XServicesAuthorizedExtIEs `vht5gc:"optional"`
}

type NRUESidelinkAggregateMaximumBitrate struct {
	UESidelinkAggregateMaximumBitRate BitRate
	IEExtensions                      *ProtocolExtensionContainerNRUESidelinkAggregateMaximumBitrateExtIEs `vht5gc:"optional"`
}

const (
	OverloadResponsePresentNothing int = iota /* No components present */
	OverloadResponsePresentOverloadAction
	OverloadResponsePresentChoiceExtensions
)

type OverloadResponse struct {
	Present          int
	OverloadAction   *OverloadAction
	ChoiceExtensions *ProtocolIESingleContainerOverloadResponseExtIEs
}

type OverloadStartNSSAIList struct {
	List []OverloadStartNSSAIItem `vht5gc:"valueExt,sizeLB:1,sizeUB:1024"`
}

type OverloadStartNSSAIItem struct {
	SliceOverloadList                   SliceOverloadList
	SliceOverloadResponse               *OverloadResponse                                       `vht5gc:"valueLB:0,valueUB:1,optional"`
	SliceTrafficLoadReductionIndication *TrafficLoadReductionIndication                         `vht5gc:"optional"`
	IEExtensions                        *ProtocolExtensionContainerOverloadStartNSSAIItemExtIEs `vht5gc:"optional"`
}

type PacketErrorRate struct {
	PERScalar    int64                                            `vht5gc:"valueExt,valueLB:0,valueUB:9"`
	PERExponent  int64                                            `vht5gc:"valueExt,valueLB:0,valueUB:9"`
	IEExtensions *ProtocolExtensionContainerPacketErrorRateExtIEs `vht5gc:"optional"`
}

type PagingAssisDataforCEcapabUE struct {
	EUTRACGI                 EUTRACGI `vht5gc:"valueExt"`
	CoverageEnhancementLevel CoverageEnhancementLevel
	IEExtensions             *ProtocolExtensionContainerPagingAssisDataforCEcapabUEExtIEs `vht5gc:"optional"`
}

type PagingAttemptInformation struct {
	PagingAttemptCount             PagingAttemptCount
	IntendedNumberOfPagingAttempts IntendedNumberOfPagingAttempts
	NextPagingAreaScope            *NextPagingAreaScope                                      `vht5gc:"optional"`
	IEExtensions                   *ProtocolExtensionContainerPagingAttemptInformationExtIEs `vht5gc:"optional"`
}

type PagingeDRXInformation struct {
	PagingEDRXCycle  PagingEDRXCycle
	PagingTimeWindow *PagingTimeWindow                                      `vht5gc:"optional"`
	IEExtensions     *ProtocolExtensionContainerPagingeDRXInformationExtIEs `vht5gc:"optional"`
}

type PathSwitchRequestAcknowledgeTransfer struct {
	ULNGUUPTNLInformation *UPTransportLayerInformation                                          `vht5gc:"valueLB:0,valueUB:1,optional"`
	SecurityIndication    *SecurityIndication                                                   `vht5gc:"valueExt,optional"`
	IEExtensions          *ProtocolExtensionContainerPathSwitchRequestAcknowledgeTransferExtIEs `vht5gc:"optional"`
}

type PathSwitchRequestSetupFailedTransfer struct {
	Cause        Cause                                                                 `vht5gc:"valueLB:0,valueUB:5"`
	IEExtensions *ProtocolExtensionContainerPathSwitchRequestSetupFailedTransferExtIEs `vht5gc:"optional"`
}

type PathSwitchRequestTransfer struct {
	DLNGUUPTNLInformation        UPTransportLayerInformation   `vht5gc:"valueLB:0,valueUB:1"`
	DLNGUTNLInformationReused    *DLNGUTNLInformationReused    `vht5gc:"optional"`
	UserPlaneSecurityInformation *UserPlaneSecurityInformation `vht5gc:"valueExt,optional"`
	QosFlowAcceptedList          QosFlowAcceptedList
	IEExtensions                 *ProtocolExtensionContainerPathSwitchRequestTransferExtIEs `vht5gc:"optional"`
}

type PathSwitchRequestUnsuccessfulTransfer struct {
	Cause        Cause                                                                  `vht5gc:"valueLB:0,valueUB:5"`
	IEExtensions *ProtocolExtensionContainerPathSwitchRequestUnsuccessfulTransferExtIEs `vht5gc:"optional"`
}

type PC5QoSParameters struct {
	Pc5QoSFlowList           PC5QoSFlowList
	Pc5LinkAggregateBitRates *BitRate                                          `vht5gc:"optional"`
	IEExtensions             *ProtocolExtensionContainerPC5QoSParametersExtIEs `vht5gc:"optional"`
}

type PC5QoSFlowList struct {
	List []PC5QoSFlowItem `vht5gc:"valueExt,sizeLB:1,sizeUB:2048"`
}

type PC5QoSFlowItem struct {
	PQI             FiveQI
	Pc5FlowBitRates *PC5FlowBitRates                                `vht5gc:"valueExt,optional"`
	Range           *Range                                          `vht5gc:"optional"`
	IEExtensions    *ProtocolExtensionContainerPC5QoSFlowItemExtIEs `vht5gc:"optional"`
}

type PC5FlowBitRates struct {
	GuaranteedFlowBitRate BitRate
	MaximumFlowBitRate    BitRate
	IEExtensions          *ProtocolExtensionContainerPC5FlowBitRatesExtIEs `vht5gc:"optional"`
}

type PCIListForMDT struct {
	List []NRPCI `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type PDUSessionAggregateMaximumBitRate struct {
	PDUSessionAggregateMaximumBitRateDL BitRate
	PDUSessionAggregateMaximumBitRateUL BitRate
	IEExtensions                        *ProtocolExtensionContainerPDUSessionAggregateMaximumBitRateExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceAdmittedList struct {
	List []PDUSessionResourceAdmittedItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceAdmittedItem struct {
	PDUSessionID                       PDUSessionID
	HandoverRequestAcknowledgeTransfer OctetString
	IEExtensions                       *ProtocolExtensionContainerPDUSessionResourceAdmittedItemExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceFailedToModifyListModCfm struct {
	List []PDUSessionResourceFailedToModifyItemModCfm `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceFailedToModifyItemModCfm struct {
	PDUSessionID                                           PDUSessionID
	PDUSessionResourceModifyIndicationUnsuccessfulTransfer OctetString
	IEExtensions                                           *ProtocolExtensionContainerPDUSessionResourceFailedToModifyItemModCfmExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceFailedToModifyListModRes struct {
	List []PDUSessionResourceFailedToModifyItemModRes `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceFailedToModifyItemModRes struct {
	PDUSessionID                                 PDUSessionID
	PDUSessionResourceModifyUnsuccessfulTransfer OctetString
	IEExtensions                                 *ProtocolExtensionContainerPDUSessionResourceFailedToModifyItemModResExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceFailedToResumeListRESReq struct {
	List []PDUSessionResourceFailedToResumeItemRESReq `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceFailedToResumeItemRESReq struct {
	PDUSessionID PDUSessionID
	Cause        Cause                                                                       `vht5gc:"valueLB:0,valueUB:5"`
	IEExtensions *ProtocolExtensionContainerPDUSessionResourceFailedToResumeItemRESReqExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceFailedToResumeListRESRes struct {
	List []PDUSessionResourceFailedToResumeItemRESRes `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceFailedToResumeItemRESRes struct {
	PDUSessionID PDUSessionID
	Cause        Cause                                                                       `vht5gc:"valueLB:0,valueUB:5"`
	IEExtensions *ProtocolExtensionContainerPDUSessionResourceFailedToResumeItemRESResExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceFailedToSetupListCxtFail struct {
	List []PDUSessionResourceFailedToSetupItemCxtFail `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceFailedToSetupItemCxtFail struct {
	PDUSessionID                                PDUSessionID
	PDUSessionResourceSetupUnsuccessfulTransfer OctetString
	IEExtensions                                *ProtocolExtensionContainerPDUSessionResourceFailedToSetupItemCxtFailExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceFailedToSetupListCxtRes struct {
	List []PDUSessionResourceFailedToSetupItemCxtRes `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceFailedToSetupItemCxtRes struct {
	PDUSessionID                                PDUSessionID
	PDUSessionResourceSetupUnsuccessfulTransfer OctetString
	IEExtensions                                *ProtocolExtensionContainerPDUSessionResourceFailedToSetupItemCxtResExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceFailedToSetupListHOAck struct {
	List []PDUSessionResourceFailedToSetupItemHOAck `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceFailedToSetupItemHOAck struct {
	PDUSessionID                                   PDUSessionID
	HandoverResourceAllocationUnsuccessfulTransfer OctetString
	IEExtensions                                   *ProtocolExtensionContainerPDUSessionResourceFailedToSetupItemHOAckExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceFailedToSetupListPSReq struct {
	List []PDUSessionResourceFailedToSetupItemPSReq `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceFailedToSetupItemPSReq struct {
	PDUSessionID                         PDUSessionID
	PathSwitchRequestSetupFailedTransfer OctetString
	IEExtensions                         *ProtocolExtensionContainerPDUSessionResourceFailedToSetupItemPSReqExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceFailedToSetupListSURes struct {
	List []PDUSessionResourceFailedToSetupItemSURes `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceFailedToSetupItemSURes struct {
	PDUSessionID                                PDUSessionID
	PDUSessionResourceSetupUnsuccessfulTransfer OctetString
	IEExtensions                                *ProtocolExtensionContainerPDUSessionResourceFailedToSetupItemSUResExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceHandoverList struct {
	List []PDUSessionResourceHandoverItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceHandoverItem struct {
	PDUSessionID            PDUSessionID
	HandoverCommandTransfer OctetString
	IEExtensions            *ProtocolExtensionContainerPDUSessionResourceHandoverItemExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceInformationList struct {
	List []PDUSessionResourceInformationItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceInformationItem struct {
	PDUSessionID              PDUSessionID
	QosFlowInformationList    QosFlowInformationList
	DRBsToQosFlowsMappingList *DRBsToQosFlowsMappingList                                         `vht5gc:"optional"`
	IEExtensions              *ProtocolExtensionContainerPDUSessionResourceInformationItemExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceListCxtRelCpl struct {
	List []PDUSessionResourceItemCxtRelCpl `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceItemCxtRelCpl struct {
	PDUSessionID PDUSessionID
	IEExtensions *ProtocolExtensionContainerPDUSessionResourceItemCxtRelCplExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceListCxtRelReq struct {
	List []PDUSessionResourceItemCxtRelReq `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceItemCxtRelReq struct {
	PDUSessionID PDUSessionID
	IEExtensions *ProtocolExtensionContainerPDUSessionResourceItemCxtRelReqExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceListHORqd struct {
	List []PDUSessionResourceItemHORqd `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceItemHORqd struct {
	PDUSessionID             PDUSessionID
	HandoverRequiredTransfer OctetString
	IEExtensions             *ProtocolExtensionContainerPDUSessionResourceItemHORqdExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceModifyConfirmTransfer struct {
	QosFlowModifyConfirmList      QosFlowModifyConfirmList
	ULNGUUPTNLInformation         UPTransportLayerInformation                                              `vht5gc:"valueLB:0,valueUB:1"`
	AdditionalNGUUPTNLInformation *UPTransportLayerInformationPairList                                     `vht5gc:"optional"`
	QosFlowFailedToModifyList     *QosFlowListWithCause                                                    `vht5gc:"optional"`
	IEExtensions                  *ProtocolExtensionContainerPDUSessionResourceModifyConfirmTransferExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceModifyIndicationUnsuccessfulTransfer struct {
	Cause        Cause                                                                                   `vht5gc:"valueLB:0,valueUB:5"`
	IEExtensions *ProtocolExtensionContainerPDUSessionResourceModifyIndicationUnsuccessfulTransferExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceModifyRequestTransfer struct {
	ProtocolIEs ProtocolIEContainerPDUSessionResourceModifyRequestTransferIEs
}

type PDUSessionResourceModifyResponseTransfer struct {
	DLNGUUPTNLInformation                *UPTransportLayerInformation                                              `vht5gc:"valueLB:0,valueUB:1,optional"`
	ULNGUUPTNLInformation                *UPTransportLayerInformation                                              `vht5gc:"valueLB:0,valueUB:1,optional"`
	QosFlowAddOrModifyResponseList       *QosFlowAddOrModifyResponseList                                           `vht5gc:"optional"`
	AdditionalDLQosFlowPerTNLInformation *QosFlowPerTNLInformationList                                             `vht5gc:"optional"`
	QosFlowFailedToAddOrModifyList       *QosFlowListWithCause                                                     `vht5gc:"optional"`
	IEExtensions                         *ProtocolExtensionContainerPDUSessionResourceModifyResponseTransferExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceModifyIndicationTransfer struct {
	DLQosFlowPerTNLInformation           QosFlowPerTNLInformation                                                    `vht5gc:"valueExt"`
	AdditionalDLQosFlowPerTNLInformation *QosFlowPerTNLInformationList                                               `vht5gc:"optional"`
	IEExtensions                         *ProtocolExtensionContainerPDUSessionResourceModifyIndicationTransferExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceModifyListModCfm struct {
	List []PDUSessionResourceModifyItemModCfm `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceModifyItemModCfm struct {
	PDUSessionID                            PDUSessionID
	PDUSessionResourceModifyConfirmTransfer OctetString
	IEExtensions                            *ProtocolExtensionContainerPDUSessionResourceModifyItemModCfmExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceModifyListModInd struct {
	List []PDUSessionResourceModifyItemModInd `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceModifyItemModInd struct {
	PDUSessionID                               PDUSessionID
	PDUSessionResourceModifyIndicationTransfer OctetString
	IEExtensions                               *ProtocolExtensionContainerPDUSessionResourceModifyItemModIndExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceModifyListModReq struct {
	List []PDUSessionResourceModifyItemModReq `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceModifyItemModReq struct {
	PDUSessionID                            PDUSessionID
	NASPDU                                  *NASPDU `vht5gc:"optional"`
	PDUSessionResourceModifyRequestTransfer OctetString
	IEExtensions                            *ProtocolExtensionContainerPDUSessionResourceModifyItemModReqExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceModifyListModRes struct {
	List []PDUSessionResourceModifyItemModRes `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceModifyItemModRes struct {
	PDUSessionID                             PDUSessionID
	PDUSessionResourceModifyResponseTransfer OctetString
	IEExtensions                             *ProtocolExtensionContainerPDUSessionResourceModifyItemModResExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceModifyUnsuccessfulTransfer struct {
	Cause                  Cause                                                                         `vht5gc:"valueLB:0,valueUB:5"`
	CriticalityDiagnostics *CriticalityDiagnostics                                                       `vht5gc:"valueExt,optional"`
	IEExtensions           *ProtocolExtensionContainerPDUSessionResourceModifyUnsuccessfulTransferExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceNotifyList struct {
	List []PDUSessionResourceNotifyItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceNotifyItem struct {
	PDUSessionID                     PDUSessionID
	PDUSessionResourceNotifyTransfer OctetString
	IEExtensions                     *ProtocolExtensionContainerPDUSessionResourceNotifyItemExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceNotifyReleasedTransfer struct {
	Cause        Cause                                                                     `vht5gc:"valueLB:0,valueUB:5"`
	IEExtensions *ProtocolExtensionContainerPDUSessionResourceNotifyReleasedTransferExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceNotifyTransfer struct {
	QosFlowNotifyList   *QosFlowNotifyList                                                `vht5gc:"optional"`
	QosFlowReleasedList *QosFlowListWithCause                                             `vht5gc:"optional"`
	IEExtensions        *ProtocolExtensionContainerPDUSessionResourceNotifyTransferExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceReleaseCommandTransfer struct {
	Cause        Cause                                                                     `vht5gc:"valueLB:0,valueUB:5"`
	IEExtensions *ProtocolExtensionContainerPDUSessionResourceReleaseCommandTransferExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceReleasedListNot struct {
	List []PDUSessionResourceReleasedItemNot `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceReleasedItemNot struct {
	PDUSessionID                             PDUSessionID
	PDUSessionResourceNotifyReleasedTransfer OctetString
	IEExtensions                             *ProtocolExtensionContainerPDUSessionResourceReleasedItemNotExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceReleasedListPSAck struct {
	List []PDUSessionResourceReleasedItemPSAck `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceReleasedItemPSAck struct {
	PDUSessionID                          PDUSessionID
	PathSwitchRequestUnsuccessfulTransfer OctetString
	IEExtensions                          *ProtocolExtensionContainerPDUSessionResourceReleasedItemPSAckExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceReleasedListPSFail struct {
	List []PDUSessionResourceReleasedItemPSFail `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceReleasedItemPSFail struct {
	PDUSessionID                          PDUSessionID
	PathSwitchRequestUnsuccessfulTransfer OctetString
	IEExtensions                          *ProtocolExtensionContainerPDUSessionResourceReleasedItemPSFailExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceReleasedListRelRes struct {
	List []PDUSessionResourceReleasedItemRelRes `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceReleasedItemRelRes struct {
	PDUSessionID                              PDUSessionID
	PDUSessionResourceReleaseResponseTransfer OctetString
	IEExtensions                              *ProtocolExtensionContainerPDUSessionResourceReleasedItemRelResExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceReleaseResponseTransfer struct {
	IEExtensions *ProtocolExtensionContainerPDUSessionResourceReleaseResponseTransferExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceResumeListRESReq struct {
	List []PDUSessionResourceResumeItemRESReq `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceResumeItemRESReq struct {
	PDUSessionID                   PDUSessionID
	UEContextResumeRequestTransfer OctetString
	IEExtensions                   *ProtocolExtensionContainerPDUSessionResourceResumeItemRESReqExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceResumeListRESRes struct {
	List []PDUSessionResourceResumeItemRESRes `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceResumeItemRESRes struct {
	PDUSessionID                    PDUSessionID
	UEContextResumeResponseTransfer OctetString
	IEExtensions                    *ProtocolExtensionContainerPDUSessionResourceResumeItemRESResExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceSecondaryRATUsageList struct {
	List []PDUSessionResourceSecondaryRATUsageItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceSecondaryRATUsageItem struct {
	PDUSessionID                        PDUSessionID
	SecondaryRATDataUsageReportTransfer OctetString
	IEExtensions                        *ProtocolExtensionContainerPDUSessionResourceSecondaryRATUsageItemExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceSetupListCxtReq struct {
	List []PDUSessionResourceSetupItemCxtReq `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceSetupItemCxtReq struct {
	PDUSessionID                           PDUSessionID
	NASPDU                                 *NASPDU `vht5gc:"optional"`
	SNSSAI                                 SNSSAI  `vht5gc:"valueExt"`
	PDUSessionResourceSetupRequestTransfer OctetString
	IEExtensions                           *ProtocolExtensionContainerPDUSessionResourceSetupItemCxtReqExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceSetupListCxtRes struct {
	List []PDUSessionResourceSetupItemCxtRes `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceSetupItemCxtRes struct {
	PDUSessionID                            PDUSessionID
	PDUSessionResourceSetupResponseTransfer OctetString
	IEExtensions                            *ProtocolExtensionContainerPDUSessionResourceSetupItemCxtResExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceSetupListHOReq struct {
	List []PDUSessionResourceSetupItemHOReq `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceSetupItemHOReq struct {
	PDUSessionID            PDUSessionID
	SNSSAI                  SNSSAI `vht5gc:"valueExt"`
	HandoverRequestTransfer OctetString
	IEExtensions            *ProtocolExtensionContainerPDUSessionResourceSetupItemHOReqExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceSetupListSUReq struct {
	List []PDUSessionResourceSetupItemSUReq `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceSetupItemSUReq struct {
	PDUSessionID                           PDUSessionID
	PDUSessionNASPDU                       *NASPDU `vht5gc:"optional"`
	SNSSAI                                 SNSSAI  `vht5gc:"valueExt"`
	PDUSessionResourceSetupRequestTransfer OctetString
	IEExtensions                           *ProtocolExtensionContainerPDUSessionResourceSetupItemSUReqExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceSetupListSURes struct {
	List []PDUSessionResourceSetupItemSURes `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceSetupItemSURes struct {
	PDUSessionID                            PDUSessionID
	PDUSessionResourceSetupResponseTransfer OctetString
	IEExtensions                            *ProtocolExtensionContainerPDUSessionResourceSetupItemSUResExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceSetupRequestTransfer struct {
	ProtocolIEs ProtocolIEContainerPDUSessionResourceSetupRequestTransferIEs
}

type PDUSessionResourceSetupResponseTransfer struct {
	DLQosFlowPerTNLInformation           QosFlowPerTNLInformation                                                 `vht5gc:"valueExt"`
	AdditionalDLQosFlowPerTNLInformation *QosFlowPerTNLInformationList                                            `vht5gc:"optional"`
	SecurityResult                       *SecurityResult                                                          `vht5gc:"valueExt,optional"`
	QosFlowFailedToSetupList             *QosFlowListWithCause                                                    `vht5gc:"optional"`
	IEExtensions                         *ProtocolExtensionContainerPDUSessionResourceSetupResponseTransferExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceSetupUnsuccessfulTransfer struct {
	Cause                  Cause                                                                        `vht5gc:"valueLB:0,valueUB:5"`
	CriticalityDiagnostics *CriticalityDiagnostics                                                      `vht5gc:"valueExt,optional"`
	IEExtensions           *ProtocolExtensionContainerPDUSessionResourceSetupUnsuccessfulTransferExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceSuspendListSUSReq struct {
	List []PDUSessionResourceSuspendItemSUSReq `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceSuspendItemSUSReq struct {
	PDUSessionID                    PDUSessionID
	UEContextSuspendRequestTransfer OctetString
	IEExtensions                    *ProtocolExtensionContainerPDUSessionResourceSuspendItemSUSReqExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceSwitchedList struct {
	List []PDUSessionResourceSwitchedItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceSwitchedItem struct {
	PDUSessionID                         PDUSessionID
	PathSwitchRequestAcknowledgeTransfer OctetString
	IEExtensions                         *ProtocolExtensionContainerPDUSessionResourceSwitchedItemExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceToBeSwitchedDLList struct {
	List []PDUSessionResourceToBeSwitchedDLItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceToBeSwitchedDLItem struct {
	PDUSessionID              PDUSessionID
	PathSwitchRequestTransfer OctetString
	IEExtensions              *ProtocolExtensionContainerPDUSessionResourceToBeSwitchedDLItemExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceToReleaseListHOCmd struct {
	List []PDUSessionResourceToReleaseItemHOCmd `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceToReleaseItemHOCmd struct {
	PDUSessionID                            PDUSessionID
	HandoverPreparationUnsuccessfulTransfer OctetString
	IEExtensions                            *ProtocolExtensionContainerPDUSessionResourceToReleaseItemHOCmdExtIEs `vht5gc:"optional"`
}

type PDUSessionResourceToReleaseListRelCmd struct {
	List []PDUSessionResourceToReleaseItemRelCmd `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type PDUSessionResourceToReleaseItemRelCmd struct {
	PDUSessionID                             PDUSessionID
	PDUSessionResourceReleaseCommandTransfer OctetString
	IEExtensions                             *ProtocolExtensionContainerPDUSessionResourceToReleaseItemRelCmdExtIEs `vht5gc:"optional"`
}

type PDUSessionUsageReport struct {
	RATType                   RATType
	PDUSessionTimedReportList VolumeTimedReportList
	IEExtensions              *ProtocolExtensionContainerPDUSessionUsageReportExtIEs `vht5gc:"optional"`
}

type PLMNSupportList struct {
	List []PLMNSupportItem `vht5gc:"valueExt,sizeLB:1,sizeUB:12"`
}

type PLMNSupportItem struct {
	PLMNIdentity     PLMNIdentity
	SliceSupportList SliceSupportList
	IEExtensions     *ProtocolExtensionContainerPLMNSupportItemExtIEs `vht5gc:"optional"`
}

type PNINPNMobilityInformation struct {
	AllowedPNINPIList AllowedPNINPNList
	IEExtensions      *ProtocolExtensionContainerPNINPNMobilityInformationExtIEs `vht5gc:"optional"`
}

const (
	PWSFailedCellIDListPresentNothing int = iota /* No components present */
	PWSFailedCellIDListPresentEUTRACGIPWSFailedList
	PWSFailedCellIDListPresentNRCGIPWSFailedList
	PWSFailedCellIDListPresentChoiceExtensions
)

type PWSFailedCellIDList struct {
	Present               int
	EUTRACGIPWSFailedList *EUTRACGIList
	NRCGIPWSFailedList    *NRCGIList
	ChoiceExtensions      *ProtocolIESingleContainerPWSFailedCellIDListExtIEs
}

const (
	QosCharacteristicsPresentNothing int = iota /* No components present */
	QosCharacteristicsPresentNonDynamic5QI
	QosCharacteristicsPresentDynamic5QI
	QosCharacteristicsPresentChoiceExtensions
)

type QosCharacteristics struct {
	Present          int
	NonDynamic5QI    *NonDynamic5QIDescriptor `vht5gc:"valueExt"`
	Dynamic5QI       *Dynamic5QIDescriptor    `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerQosCharacteristicsExtIEs
}

type QosFlowAcceptedList struct {
	List []QosFlowAcceptedItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type QosFlowAcceptedItem struct {
	QosFlowIdentifier QosFlowIdentifier
	IEExtensions      *ProtocolExtensionContainerQosFlowAcceptedItemExtIEs `vht5gc:"optional"`
}

type QosFlowAddOrModifyRequestList struct {
	List []QosFlowAddOrModifyRequestItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type QosFlowAddOrModifyRequestItem struct {
	QosFlowIdentifier         QosFlowIdentifier
	QosFlowLevelQosParameters *QosFlowLevelQosParameters                                     `vht5gc:"valueExt,optional"`
	ERABID                    *ERABID                                                        `vht5gc:"optional"`
	IEExtensions              *ProtocolExtensionContainerQosFlowAddOrModifyRequestItemExtIEs `vht5gc:"optional"`
}

type QosFlowAddOrModifyResponseList struct {
	List []QosFlowAddOrModifyResponseItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type QosFlowAddOrModifyResponseItem struct {
	QosFlowIdentifier QosFlowIdentifier
	IEExtensions      *ProtocolExtensionContainerQosFlowAddOrModifyResponseItemExtIEs `vht5gc:"optional"`
}

type QosFlowInformationList struct {
	List []QosFlowInformationItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type QosFlowInformationItem struct {
	QosFlowIdentifier QosFlowIdentifier
	DLForwarding      *DLForwarding                                           `vht5gc:"optional"`
	IEExtensions      *ProtocolExtensionContainerQosFlowInformationItemExtIEs `vht5gc:"optional"`
}

type QosFlowLevelQosParameters struct {
	QosCharacteristics             QosCharacteristics                                         `vht5gc:"valueLB:0,valueUB:2"`
	AllocationAndRetentionPriority AllocationAndRetentionPriority                             `vht5gc:"valueExt"`
	GBRQosInformation              *GBRQosInformation                                         `vht5gc:"valueExt,optional"`
	ReflectiveQosAttribute         *ReflectiveQosAttribute                                    `vht5gc:"optional"`
	AdditionalQosFlowInformation   *AdditionalQosFlowInformation                              `vht5gc:"optional"`
	IEExtensions                   *ProtocolExtensionContainerQosFlowLevelQosParametersExtIEs `vht5gc:"optional"`
}

type QosFlowListWithCause struct {
	List []QosFlowWithCauseItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type QosFlowWithCauseItem struct {
	QosFlowIdentifier QosFlowIdentifier
	Cause             Cause                                                 `vht5gc:"valueLB:0,valueUB:5"`
	IEExtensions      *ProtocolExtensionContainerQosFlowWithCauseItemExtIEs `vht5gc:"optional"`
}

type QosFlowModifyConfirmList struct {
	List []QosFlowModifyConfirmItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type QosFlowModifyConfirmItem struct {
	QosFlowIdentifier QosFlowIdentifier
	IEExtensions      *ProtocolExtensionContainerQosFlowModifyConfirmItemExtIEs `vht5gc:"optional"`
}

type QosFlowNotifyList struct {
	List []QosFlowNotifyItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type QosFlowNotifyItem struct {
	QosFlowIdentifier QosFlowIdentifier
	NotificationCause NotificationCause
	IEExtensions      *ProtocolExtensionContainerQosFlowNotifyItemExtIEs `vht5gc:"optional"`
}

type QosFlowPerTNLInformation struct {
	UPTransportLayerInformation UPTransportLayerInformation `vht5gc:"valueLB:0,valueUB:1"`
	AssociatedQosFlowList       AssociatedQosFlowList
	IEExtensions                *ProtocolExtensionContainerQosFlowPerTNLInformationExtIEs `vht5gc:"optional"`
}

type QosFlowPerTNLInformationList struct {
	List []QosFlowPerTNLInformationItem `vht5gc:"valueExt,sizeLB:1,sizeUB:3"`
}

type QosFlowPerTNLInformationItem struct {
	QosFlowPerTNLInformation QosFlowPerTNLInformation                                      `vht5gc:"valueExt"`
	IEExtensions             *ProtocolExtensionContainerQosFlowPerTNLInformationItemExtIEs `vht5gc:"optional"`
}

type QosFlowSetupRequestList struct {
	List []QosFlowSetupRequestItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type QosFlowSetupRequestItem struct {
	QosFlowIdentifier         QosFlowIdentifier
	QosFlowLevelQosParameters QosFlowLevelQosParameters                                `vht5gc:"valueExt"`
	ERABID                    *ERABID                                                  `vht5gc:"optional"`
	IEExtensions              *ProtocolExtensionContainerQosFlowSetupRequestItemExtIEs `vht5gc:"optional"`
}

type QosFlowListWithDataForwarding struct {
	List []QosFlowItemWithDataForwarding `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type QosFlowItemWithDataForwarding struct {
	QosFlowIdentifier      QosFlowIdentifier
	DataForwardingAccepted *DataForwardingAccepted                                        `vht5gc:"optional"`
	IEExtensions           *ProtocolExtensionContainerQosFlowItemWithDataForwardingExtIEs `vht5gc:"optional"`
}

type QosFlowToBeForwardedList struct {
	List []QosFlowToBeForwardedItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type QosFlowToBeForwardedItem struct {
	QosFlowIdentifier QosFlowIdentifier
	IEExtensions      *ProtocolExtensionContainerQosFlowToBeForwardedItemExtIEs `vht5gc:"optional"`
}

type QoSFlowsUsageReportList struct {
	List []QoSFlowsUsageReportItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type QoSFlowsUsageReportItem struct {
	QosFlowIdentifier       QosFlowIdentifier
	RATType                 RATType
	QoSFlowsTimedReportList VolumeTimedReportList
	IEExtensions            *ProtocolExtensionContainerQoSFlowsUsageReportItemExtIEs `vht5gc:"optional"`
}

type RANStatusTransferTransparentContainer struct {
	DRBsSubjectToStatusTransferList DRBsSubjectToStatusTransferList
	IEExtensions                    *ProtocolExtensionContainerRANStatusTransferTransparentContainerExtIEs `vht5gc:"optional"`
}

type RATRestrictions struct {
	List []RATRestrictionsItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type RATRestrictionsItem struct {
	PLMNIdentity              PLMNIdentity
	RATRestrictionInformation RATRestrictionInformation
	IEExtensions              *ProtocolExtensionContainerRATRestrictionsItemExtIEs `vht5gc:"optional"`
}

type RecommendedCellsForPaging struct {
	RecommendedCellList RecommendedCellList
	IEExtensions        *ProtocolExtensionContainerRecommendedCellsForPagingExtIEs `vht5gc:"optional"`
}

type RecommendedCellList struct {
	List []RecommendedCellItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type RecommendedCellItem struct {
	NGRANCGI         NGRANCGI                                             `vht5gc:"valueLB:0,valueUB:2"`
	TimeStayedInCell *int64                                               `vht5gc:"valueLB:0,valueUB:4095,optional"`
	IEExtensions     *ProtocolExtensionContainerRecommendedCellItemExtIEs `vht5gc:"optional"`
}

type RecommendedRANNodesForPaging struct {
	RecommendedRANNodeList RecommendedRANNodeList
	IEExtensions           *ProtocolExtensionContainerRecommendedRANNodesForPagingExtIEs `vht5gc:"optional"`
}

type RecommendedRANNodeList struct {
	List []RecommendedRANNodeItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type RecommendedRANNodeItem struct {
	AMFPagingTarget AMFPagingTarget                                         `vht5gc:"valueLB:0,valueUB:2"`
	IEExtensions    *ProtocolExtensionContainerRecommendedRANNodeItemExtIEs `vht5gc:"optional"`
}

type RedundantPDUSessionInformation struct {
	RSN          RSN
	IEExtensions *ProtocolExtensionContainerRedundantPDUSessionInformationExtIEs `vht5gc:"optional"`
}

const (
	ResetTypePresentNothing int = iota /* No components present */
	ResetTypePresentNGInterface
	ResetTypePresentPartOfNGInterface
	ResetTypePresentChoiceExtensions
)

type ResetType struct {
	Present           int
	NGInterface       *ResetAll
	PartOfNGInterface *UEAssociatedLogicalNGConnectionList
	ChoiceExtensions  *ProtocolIESingleContainerResetTypeExtIEs
}

type RIMInformationTransfer struct {
	TargetRANNodeID TargetRANNodeID                                         `vht5gc:"valueExt"`
	SourceRANNodeID SourceRANNodeID                                         `vht5gc:"valueExt"`
	RIMInformation  RIMInformation                                          `vht5gc:"valueExt"`
	IEExtensions    *ProtocolExtensionContainerRIMInformationTransferExtIEs `vht5gc:"optional"`
}

type RIMInformation struct {
	TargetgNBSetID GNBSetID
	RIMRSDetection RIMRSDetection
	IEExtensions   *ProtocolExtensionContainerRIMInformationExtIEs `vht5gc:"optional"`
}

type ScheduledCommunicationTime struct {
	DayofWeek      *BitString                                                  `vht5gc:"sizeLB:7,sizeUB:7,optional"`
	TimeofDayStart *int64                                                      `vht5gc:"valueExt,valueLB:0,valueUB:86399,optional"`
	TimeofDayEnd   *int64                                                      `vht5gc:"valueExt,valueLB:0,valueUB:86399,optional"`
	IEExtensions   *ProtocolExtensionContainerScheduledCommunicationTimeExtIEs `vht5gc:"optional"`
}

type SCTPTLAs struct {
	List []TransportLayerAddress `vht5gc:"valueExt,sizeLB:1,sizeUB:2"`
}

type SecondaryRATUsageInformation struct {
	PDUSessionUsageReport   *PDUSessionUsageReport                                        `vht5gc:"valueExt,optional"`
	QosFlowsUsageReportList *QoSFlowsUsageReportList                                      `vht5gc:"optional"`
	IEExtensions            *ProtocolExtensionContainerSecondaryRATUsageInformationExtIEs `vht5gc:"optional"`
}

type SecondaryRATDataUsageReportTransfer struct {
	SecondaryRATUsageInformation *SecondaryRATUsageInformation                                        `vht5gc:"valueExt,optional"`
	IEExtensions                 *ProtocolExtensionContainerSecondaryRATDataUsageReportTransferExtIEs `vht5gc:"optional"`
}

type SecurityContext struct {
	NextHopChainingCount NextHopChainingCount
	NextHopNH            SecurityKey
	IEExtensions         *ProtocolExtensionContainerSecurityContextExtIEs `vht5gc:"optional"`
}

type SecurityIndication struct {
	IntegrityProtectionIndication       IntegrityProtectionIndication
	ConfidentialityProtectionIndication ConfidentialityProtectionIndication
	MaximumIntegrityProtectedDataRateUL *MaximumIntegrityProtectedDataRate                  `vht5gc:"optional"`
	IEExtensions                        *ProtocolExtensionContainerSecurityIndicationExtIEs `vht5gc:"optional"`
}

type SecurityResult struct {
	IntegrityProtectionResult       IntegrityProtectionResult
	ConfidentialityProtectionResult ConfidentialityProtectionResult
	IEExtensions                    *ProtocolExtensionContainerSecurityResultExtIEs `vht5gc:"optional"`
}

type SensorMeasurementConfiguration struct {
	SensorMeasConfig         SensorMeasConfig
	SensorMeasConfigNameList *SensorMeasConfigNameList                                       `vht5gc:"optional"`
	IEExtensions             *ProtocolExtensionContainerSensorMeasurementConfigurationExtIEs `vht5gc:"optional"`
}

type SensorMeasConfigNameList struct {
	List []SensorMeasConfigNameItem `vht5gc:"valueExt,sizeLB:1,sizeUB:3"`
}

type SensorMeasConfigNameItem struct {
	SensorNameConfig SensorNameConfig                                          `vht5gc:"valueLB:0,valueUB:3"`
	IEExtensions     *ProtocolExtensionContainerSensorMeasConfigNameItemExtIEs `vht5gc:"optional"`
}

const (
	SensorNameConfigPresentNothing int = iota /* No components present */
	SensorNameConfigPresentUncompensatedBarometricConfig
	SensorNameConfigPresentUeSpeedConfig
	SensorNameConfigPresentUeOrientationConfig
	SensorNameConfigPresentChoiceExtensions
)

type SensorNameConfig struct {
	Present                       int
	UncompensatedBarometricConfig *UncompensatedBarometricConfig
	UeSpeedConfig                 *UeSpeedConfig
	UeOrientationConfig           *UeOrientationConfig
	ChoiceExtensions              *ProtocolIESingleContainerSensorNameConfigExtIEs
}

type ServedGUAMIList struct {
	List []ServedGUAMIItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type ServedGUAMIItem struct {
	GUAMI         GUAMI                                            `vht5gc:"valueExt"`
	BackupAMFName *AMFName                                         `vht5gc:"optional"`
	IEExtensions  *ProtocolExtensionContainerServedGUAMIItemExtIEs `vht5gc:"optional"`
}

type ServiceAreaInformation struct {
	List []ServiceAreaInformationItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type ServiceAreaInformationItem struct {
	PLMNIdentity   PLMNIdentity
	AllowedTACs    *AllowedTACs                                                `vht5gc:"optional"`
	NotAllowedTACs *NotAllowedTACs                                             `vht5gc:"optional"`
	IEExtensions   *ProtocolExtensionContainerServiceAreaInformationItemExtIEs `vht5gc:"optional"`
}

type SliceOverloadList struct {
	List []SliceOverloadItem `vht5gc:"valueExt,sizeLB:1,sizeUB:1024"`
}

type SliceOverloadItem struct {
	SNSSAI       SNSSAI                                             `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerSliceOverloadItemExtIEs `vht5gc:"optional"`
}

type SliceSupportList struct {
	List []SliceSupportItem `vht5gc:"valueExt,sizeLB:1,sizeUB:1024"`
}

type SliceSupportItem struct {
	SNSSAI       SNSSAI                                            `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerSliceSupportItemExtIEs `vht5gc:"optional"`
}

type SNPNMobilityInformation struct {
	ServingNID   NID
	IEExtensions *ProtocolExtensionContainerSNPNMobilityInformationExtIEs `vht5gc:"optional"`
}

type SNSSAI struct {
	SST          SST
	SD           *SD                                     `vht5gc:"optional"`
	IEExtensions *ProtocolExtensionContainerSNSSAIExtIEs `vht5gc:"optional"`
}

type SONConfigurationTransfer struct {
	TargetRANNodeID        TargetRANNodeID                                           `vht5gc:"valueExt"`
	SourceRANNodeID        SourceRANNodeID                                           `vht5gc:"valueExt"`
	SONInformation         SONInformation                                            `vht5gc:"valueLB:0,valueUB:2"`
	XnTNLConfigurationInfo *XnTNLConfigurationInfo                                   `vht5gc:"valueExt,optional"`
	IEExtensions           *ProtocolExtensionContainerSONConfigurationTransferExtIEs `vht5gc:"optional"`
}

const (
	SONInformationPresentNothing int = iota /* No components present */
	SONInformationPresentSONInformationRequest
	SONInformationPresentSONInformationReply
	SONInformationPresentChoiceExtensions
)

type SONInformation struct {
	Present               int
	SONInformationRequest *SONInformationRequest
	SONInformationReply   *SONInformationReply `vht5gc:"valueExt"`
	ChoiceExtensions      *ProtocolIESingleContainerSONInformationExtIEs
}

type SONInformationReply struct {
	XnTNLConfigurationInfo *XnTNLConfigurationInfo                              `vht5gc:"valueExt,optional"`
	IEExtensions           *ProtocolExtensionContainerSONInformationReplyExtIEs `vht5gc:"optional"`
}

const (
	SONInformationReportPresentNothing int = iota /* No components present */
	SONInformationReportPresentFailureIndicationInformation
	SONInformationReportPresentHOReportInformation
	SONInformationReportPresentChoiceExtensions
)

type SONInformationReport struct {
	Present                      int
	FailureIndicationInformation *FailureIndication `vht5gc:"valueExt"`
	HOReportInformation          *HOReport          `vht5gc:"valueExt"`
	ChoiceExtensions             *ProtocolIESingleContainerSONInformationReportExtIEs
}

type SourceNGRANNodeToTargetNGRANNodeTransparentContainer struct {
	RRCContainer                      RRCContainer
	PDUSessionResourceInformationList *PDUSessionResourceInformationList `vht5gc:"optional"`
	ERABInformationList               *ERABInformationList               `vht5gc:"optional"`
	TargetCellID                      NGRANCGI                           `vht5gc:"valueLB:0,valueUB:2"`
	IndexToRFSP                       *IndexToRFSP                       `vht5gc:"optional"`
	UEHistoryInformation              UEHistoryInformation
	IEExtensions                      *ProtocolExtensionContainerSourceNGRANNodeToTargetNGRANNodeTransparentContainerExtIEs `vht5gc:"optional"`
}

type SourceRANNodeID struct {
	GlobalRANNodeID GlobalRANNodeID                                  `vht5gc:"valueLB:0,valueUB:3"`
	SelectedTAI     TAI                                              `vht5gc:"valueExt"`
	IEExtensions    *ProtocolExtensionContainerSourceRANNodeIDExtIEs `vht5gc:"optional"`
}

type SourceToTargetAMFInformationReroute struct {
	ConfiguredNSSAI     *ConfiguredNSSAI                                                     `vht5gc:"optional"`
	RejectedNSSAIinPLMN *RejectedNSSAIinPLMN                                                 `vht5gc:"optional"`
	RejectedNSSAIinTA   *RejectedNSSAIinTA                                                   `vht5gc:"optional"`
	IEExtensions        *ProtocolExtensionContainerSourceToTargetAMFInformationRerouteExtIEs `vht5gc:"optional"`
}

type SupportedTAList struct {
	List []SupportedTAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type SupportedTAItem struct {
	TAC               TAC
	BroadcastPLMNList BroadcastPLMNList
	IEExtensions      *ProtocolExtensionContainerSupportedTAItemExtIEs `vht5gc:"optional"`
}

type TAI struct {
	PLMNIdentity PLMNIdentity
	TAC          TAC
	IEExtensions *ProtocolExtensionContainerTAIExtIEs `vht5gc:"optional"`
}

type TAIBroadcastEUTRA struct {
	List []TAIBroadcastEUTRAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type TAIBroadcastEUTRAItem struct {
	TAI                      TAI `vht5gc:"valueExt"`
	CompletedCellsInTAIEUTRA CompletedCellsInTAIEUTRA
	IEExtensions             *ProtocolExtensionContainerTAIBroadcastEUTRAItemExtIEs `vht5gc:"optional"`
}

type TAIBroadcastNR struct {
	List []TAIBroadcastNRItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type TAIBroadcastNRItem struct {
	TAI                   TAI `vht5gc:"valueExt"`
	CompletedCellsInTAINR CompletedCellsInTAINR
	IEExtensions          *ProtocolExtensionContainerTAIBroadcastNRItemExtIEs `vht5gc:"optional"`
}

type TAICancelledEUTRA struct {
	List []TAICancelledEUTRAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type TAICancelledEUTRAItem struct {
	TAI                      TAI `vht5gc:"valueExt"`
	CancelledCellsInTAIEUTRA CancelledCellsInTAIEUTRA
	IEExtensions             *ProtocolExtensionContainerTAICancelledEUTRAItemExtIEs `vht5gc:"optional"`
}

type TAICancelledNR struct {
	List []TAICancelledNRItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type TAICancelledNRItem struct {
	TAI                   TAI `vht5gc:"valueExt"`
	CancelledCellsInTAINR CancelledCellsInTAINR
	IEExtensions          *ProtocolExtensionContainerTAICancelledNRItemExtIEs `vht5gc:"optional"`
}

type TAIListForInactive struct {
	List []TAIListForInactiveItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type TAIListForInactiveItem struct {
	TAI          TAI                                                     `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerTAIListForInactiveItemExtIEs `vht5gc:"optional"`
}

type TAIListForPaging struct {
	List []TAIListForPagingItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type TAIListForPagingItem struct {
	TAI          TAI                                                   `vht5gc:"valueExt"`
	IEExtensions *ProtocolExtensionContainerTAIListForPagingItemExtIEs `vht5gc:"optional"`
}

type TAIListForRestart struct {
	List []TAI `vht5gc:"valueExt,sizeLB:1,sizeUB:2048"`
}

type TAIListForWarning struct {
	List []TAI `vht5gc:"valueExt,sizeLB:1,sizeUB:65535"`
}

type TargeteNBID struct {
	GlobalENBID    GlobalNgENBID                                `vht5gc:"valueExt"`
	SelectedEPSTAI EPSTAI                                       `vht5gc:"valueExt"`
	IEExtensions   *ProtocolExtensionContainerTargeteNBIDExtIEs `vht5gc:"optional"`
}

const (
	TargetIDPresentNothing int = iota /* No components present */
	TargetIDPresentTargetRANNodeID
	TargetIDPresentTargeteNBID
	TargetIDPresentChoiceExtensions
)

type TargetID struct {
	Present          int
	TargetRANNodeID  *TargetRANNodeID `vht5gc:"valueExt"`
	TargeteNBID      *TargeteNBID     `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerTargetIDExtIEs
}

type TargetNGRANNodeToSourceNGRANNodeTransparentContainer struct {
	RRCContainer RRCContainer
	IEExtensions *ProtocolExtensionContainerTargetNGRANNodeToSourceNGRANNodeTransparentContainerExtIEs `vht5gc:"optional"`
}

type TargetNGRANNodeToSourceNGRANNodeFailureTransparentContainer struct {
	CellCAGInformation CellCAGInformation                                                                           `vht5gc:"valueExt"`
	IEExtensions       *ProtocolExtensionContainerTargetNGRANNodeToSourceNGRANNodeFailureTransparentContainerExtIEs `vht5gc:"optional"`
}

type TargetRANNodeID struct {
	GlobalRANNodeID GlobalRANNodeID                                  `vht5gc:"valueLB:0,valueUB:3"`
	SelectedTAI     TAI                                              `vht5gc:"valueExt"`
	IEExtensions    *ProtocolExtensionContainerTargetRANNodeIDExtIEs `vht5gc:"optional"`
}

type TargetRNCID struct {
	LAI           LAI `vht5gc:"valueExt"`
	RNCID         RNCID
	ExtendedRNCID *ExtendedRNCID                               `vht5gc:"optional"`
	IEExtensions  *ProtocolExtensionContainerTargetRNCIDExtIEs `vht5gc:"optional"`
}

const (
	TNGFIDPresentNothing int = iota /* No components present */
	TNGFIDPresentTNGFID
	TNGFIDPresentChoiceExtensions
)

type TNGFID struct {
	Present          int
	TNGFID           *BitString
	ChoiceExtensions *ProtocolIESingleContainerTNGFIDExtIEs
}

type TNLAssociationList struct {
	List []TNLAssociationItem `vht5gc:"valueExt,sizeLB:1,sizeUB:32"`
}

type TNLAssociationItem struct {
	TNLAssociationAddress CPTransportLayerInformation                         `vht5gc:"valueLB:0,valueUB:1"`
	Cause                 Cause                                               `vht5gc:"valueLB:0,valueUB:5"`
	IEExtensions          *ProtocolExtensionContainerTNLAssociationItemExtIEs `vht5gc:"optional"`
}

type TooearlyIntersystemHO struct {
	SourcecellID         EUTRACGI                                               `vht5gc:"valueExt"`
	FailurecellID        NGRANCGI                                               `vht5gc:"valueLB:0,valueUB:2"`
	UERLFReportContainer *UERLFReportContainer                                  `vht5gc:"valueLB:0,valueUB:2,optional"`
	IEExtensions         *ProtocolExtensionContainerTooearlyIntersystemHOExtIEs `vht5gc:"optional"`
}

type TraceActivation struct {
	NGRANTraceID                   NGRANTraceID
	InterfacesToTrace              InterfacesToTrace
	TraceDepth                     TraceDepth
	TraceCollectionEntityIPAddress TransportLayerAddress
	IEExtensions                   *ProtocolExtensionContainerTraceActivationExtIEs `vht5gc:"optional"`
}

type TAIBasedMDT struct {
	TAIListforMDT TAIListforMDT
	IEExtensions  *ProtocolExtensionContainerTAIBasedMDTExtIEs `vht5gc:"optional"`
}

type TAIListforMDT struct {
	List []TAI `vht5gc:"valueExt,sizeLB:1,sizeUB:8"`
}

type TABasedMDT struct {
	TAListforMDT TAListforMDT
	IEExtensions *ProtocolExtensionContainerTABasedMDTExtIEs `vht5gc:"optional"`
}

type TAListforMDT struct {
	List []TAC `vht5gc:"valueExt,sizeLB:1,sizeUB:8"`
}

const (
	TWIFIDPresentNothing int = iota /* No components present */
	TWIFIDPresentTWIFID
	TWIFIDPresentChoiceExtensions
)

type TWIFID struct {
	Present          int
	TWIFID           *BitString
	ChoiceExtensions *ProtocolIESingleContainerTWIFIDExtIEs
}

type TSCAssistanceInformation struct {
	Periodicity      Periodicity
	BurstArrivalTime *BurstArrivalTime                                         `vht5gc:"optional"`
	IEExtensions     *ProtocolExtensionContainerTSCAssistanceInformationExtIEs `vht5gc:"optional"`
}

type TSCTrafficCharacteristics struct {
	TSCAssistanceInformationDL *TSCAssistanceInformation                                  `vht5gc:"valueExt,optional"`
	TSCAssistanceInformationUL *TSCAssistanceInformation                                  `vht5gc:"valueExt,optional"`
	IEExtensions               *ProtocolExtensionContainerTSCTrafficCharacteristicsExtIEs `vht5gc:"optional"`
}

type UEAggregateMaximumBitRate struct {
	UEAggregateMaximumBitRateDL BitRate
	UEAggregateMaximumBitRateUL BitRate
	IEExtensions                *ProtocolExtensionContainerUEAggregateMaximumBitRateExtIEs `vht5gc:"optional"`
}

type UEAssociatedLogicalNGConnectionList struct {
	List []UEAssociatedLogicalNGConnectionItem `vht5gc:"valueExt,sizeLB:1,sizeUB:65536"`
}

type UEAssociatedLogicalNGConnectionItem struct {
	AMFUENGAPID  *AMFUENGAPID                                                         `vht5gc:"optional"`
	RANUENGAPID  *RANUENGAPID                                                         `vht5gc:"optional"`
	IEExtensions *ProtocolExtensionContainerUEAssociatedLogicalNGConnectionItemExtIEs `vht5gc:"optional"`
}

type UEContextResumeRequestTransfer struct {
	QosFlowFailedToResumeList *QosFlowListWithCause                                           `vht5gc:"optional"`
	IEExtensions              *ProtocolExtensionContainerUEContextResumeRequestTransferExtIEs `vht5gc:"optional"`
}

type UEContextResumeResponseTransfer struct {
	QosFlowFailedToResumeList *QosFlowListWithCause                                            `vht5gc:"optional"`
	IEExtensions              *ProtocolExtensionContainerUEContextResumeResponseTransferExtIEs `vht5gc:"optional"`
}

type UEContextSuspendRequestTransfer struct {
	SuspendIndicator *SuspendIndicator                                                `vht5gc:"optional"`
	IEExtensions     *ProtocolExtensionContainerUEContextSuspendRequestTransferExtIEs `vht5gc:"optional"`
}

type UEDifferentiationInfo struct {
	PeriodicCommunicationIndicator *PeriodicCommunicationIndicator                        `vht5gc:"optional"`
	PeriodicTime                   *int64                                                 `vht5gc:"valueExt,valueLB:1,valueUB:3600,optional"`
	ScheduledCommunicationTime     *ScheduledCommunicationTime                            `vht5gc:"valueExt,optional"`
	StationaryIndication           *StationaryIndication                                  `vht5gc:"optional"`
	TrafficProfile                 *TrafficProfile                                        `vht5gc:"optional"`
	BatteryIndication              *BatteryIndication                                     `vht5gc:"optional"`
	IEExtensions                   *ProtocolExtensionContainerUEDifferentiationInfoExtIEs `vht5gc:"optional"`
}

type UEHistoryInformation struct {
	List []LastVisitedCellItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

const (
	UEHistoryInformationFromTheUEPresentNothing int = iota /* No components present */
	UEHistoryInformationFromTheUEPresentNR
	UEHistoryInformationFromTheUEPresentChoiceExtensions
)

type UEHistoryInformationFromTheUE struct {
	Present          int
	NR               *NRMobilityHistoryReport
	ChoiceExtensions *ProtocolIESingleContainerUEHistoryInformationFromTheUEExtIEs
}

const (
	UEIdentityIndexValuePresentNothing int = iota /* No components present */
	UEIdentityIndexValuePresentIndexLength10
	UEIdentityIndexValuePresentChoiceExtensions
)

type UEIdentityIndexValue struct {
	Present          int
	IndexLength10    *BitString `vht5gc:"sizeLB:10,sizeUB:10"`
	ChoiceExtensions *ProtocolIESingleContainerUEIdentityIndexValueExtIEs
}

const (
	UENGAPIDsPresentNothing int = iota /* No components present */
	UENGAPIDsPresentUENGAPIDPair
	UENGAPIDsPresentAMFUENGAPID
	UENGAPIDsPresentChoiceExtensions
)

type UENGAPIDs struct {
	Present          int
	UENGAPIDPair     *UENGAPIDPair `vht5gc:"valueExt"`
	AMFUENGAPID      *AMFUENGAPID
	ChoiceExtensions *ProtocolIESingleContainerUENGAPIDsExtIEs
}

type UENGAPIDPair struct {
	AMFUENGAPID  AMFUENGAPID
	RANUENGAPID  RANUENGAPID
	IEExtensions *ProtocolExtensionContainerUENGAPIDPairExtIEs `vht5gc:"optional"`
}

const (
	UEPagingIdentityPresentNothing int = iota /* No components present */
	UEPagingIdentityPresentFiveGSTMSI
	UEPagingIdentityPresentChoiceExtensions
)

type UEPagingIdentity struct {
	Present          int
	FiveGSTMSI       *FiveGSTMSI `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerUEPagingIdentityExtIEs
}

type UEPresenceInAreaOfInterestList struct {
	List []UEPresenceInAreaOfInterestItem `vht5gc:"valueExt,sizeLB:1,sizeUB:64"`
}

type UEPresenceInAreaOfInterestItem struct {
	LocationReportingReferenceID LocationReportingReferenceID
	UEPresence                   UEPresence
	IEExtensions                 *ProtocolExtensionContainerUEPresenceInAreaOfInterestItemExtIEs `vht5gc:"optional"`
}

type UERadioCapabilityForPaging struct {
	UERadioCapabilityForPagingOfNR    *UERadioCapabilityForPagingOfNR                             `vht5gc:"optional"`
	UERadioCapabilityForPagingOfEUTRA *UERadioCapabilityForPagingOfEUTRA                          `vht5gc:"optional"`
	IEExtensions                      *ProtocolExtensionContainerUERadioCapabilityForPagingExtIEs `vht5gc:"optional"`
}

const (
	UERLFReportContainerPresentNothing int = iota /* No components present */
	UERLFReportContainerPresentNR
	UERLFReportContainerPresentLTE
	UERLFReportContainerPresentChoiceExtensions
)

type UERLFReportContainer struct {
	Present          int
	NR               *NRUERLFReportContainer
	LTE              *LTEUERLFReportContainer
	ChoiceExtensions *ProtocolIESingleContainerUERLFReportContainerExtIEs
}

type UESecurityCapabilities struct {
	NRencryptionAlgorithms             NRencryptionAlgorithms
	NRintegrityProtectionAlgorithms    NRintegrityProtectionAlgorithms
	EUTRAencryptionAlgorithms          EUTRAencryptionAlgorithms
	EUTRAintegrityProtectionAlgorithms EUTRAintegrityProtectionAlgorithms
	IEExtensions                       *ProtocolExtensionContainerUESecurityCapabilitiesExtIEs `vht5gc:"optional"`
}

type ULCPSecurityInformation struct {
	UlNASMAC     ULNASMAC
	UlNASCount   ULNASCount
	IEExtensions *ProtocolExtensionContainerULCPSecurityInformationExtIEs `vht5gc:"optional"`
}

type ULNGUUPTNLModifyList struct {
	List []ULNGUUPTNLModifyItem `vht5gc:"valueExt,sizeLB:1,sizeUB:4"`
}

type ULNGUUPTNLModifyItem struct {
	ULNGUUPTNLInformation UPTransportLayerInformation                           `vht5gc:"valueLB:0,valueUB:1"`
	DLNGUUPTNLInformation UPTransportLayerInformation                           `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions          *ProtocolExtensionContainerULNGUUPTNLModifyItemExtIEs `vht5gc:"optional"`
}

type UnavailableGUAMIList struct {
	List []UnavailableGUAMIItem `vht5gc:"valueExt,sizeLB:1,sizeUB:256"`
}

type UnavailableGUAMIItem struct {
	GUAMI                        GUAMI                                                 `vht5gc:"valueExt"`
	TimerApproachForGUAMIRemoval *TimerApproachForGUAMIRemoval                         `vht5gc:"optional"`
	BackupAMFName                *AMFName                                              `vht5gc:"optional"`
	IEExtensions                 *ProtocolExtensionContainerUnavailableGUAMIItemExtIEs `vht5gc:"optional"`
}

const (
	UPTransportLayerInformationPresentNothing int = iota /* No components present */
	UPTransportLayerInformationPresentGTPTunnel
	UPTransportLayerInformationPresentChoiceExtensions
)

type UPTransportLayerInformation struct {
	Present          int
	GTPTunnel        *GTPTunnel `vht5gc:"valueExt"`
	ChoiceExtensions *ProtocolIESingleContainerUPTransportLayerInformationExtIEs
}

type UPTransportLayerInformationList struct {
	List []UPTransportLayerInformationItem `vht5gc:"valueExt,sizeLB:1,sizeUB:3"`
}

type UPTransportLayerInformationItem struct {
	NGUUPTNLInformation UPTransportLayerInformation                                      `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions        *ProtocolExtensionContainerUPTransportLayerInformationItemExtIEs `vht5gc:"optional"`
}

type UPTransportLayerInformationPairList struct {
	List []UPTransportLayerInformationPairItem `vht5gc:"valueExt,sizeLB:1,sizeUB:3"`
}

type UPTransportLayerInformationPairItem struct {
	ULNGUUPTNLInformation UPTransportLayerInformation                                          `vht5gc:"valueLB:0,valueUB:1"`
	DLNGUUPTNLInformation UPTransportLayerInformation                                          `vht5gc:"valueLB:0,valueUB:1"`
	IEExtensions          *ProtocolExtensionContainerUPTransportLayerInformationPairItemExtIEs `vht5gc:"optional"`
}

const (
	UserLocationInformationPresentNothing int = iota /* No components present */
	UserLocationInformationPresentUserLocationInformationEUTRA
	UserLocationInformationPresentUserLocationInformationNR
	UserLocationInformationPresentUserLocationInformationN3IWF
	UserLocationInformationPresentChoiceExtensions
)

type UserLocationInformation struct {
	Present                      int
	UserLocationInformationEUTRA *UserLocationInformationEUTRA `vht5gc:"valueExt"`
	UserLocationInformationNR    *UserLocationInformationNR    `vht5gc:"valueExt"`
	UserLocationInformationN3IWF *UserLocationInformationN3IWF `vht5gc:"valueExt"`
	ChoiceExtensions             *ProtocolIESingleContainerUserLocationInformationExtIEs
}

type UserLocationInformationEUTRA struct {
	EUTRACGI     EUTRACGI                                                      `vht5gc:"valueExt"`
	TAI          TAI                                                           `vht5gc:"valueExt"`
	TimeStamp    *TimeStamp                                                    `vht5gc:"optional"`
	IEExtensions *ProtocolExtensionContainerUserLocationInformationEUTRAExtIEs `vht5gc:"optional"`
}

type UserLocationInformationN3IWF struct {
	IPAddress    TransportLayerAddress
	PortNumber   PortNumber
	IEExtensions *ProtocolExtensionContainerUserLocationInformationN3IWFExtIEs `vht5gc:"optional"`
}

type UserLocationInformationTNGF struct {
	TNAPID       TNAPID
	IPAddress    TransportLayerAddress
	PortNumber   *PortNumber                                                  `vht5gc:"optional"`
	IEExtensions *ProtocolExtensionContainerUserLocationInformationTNGFExtIEs `vht5gc:"optional"`
}

type UserLocationInformationTWIF struct {
	TWAPID       TWAPID
	IPAddress    TransportLayerAddress
	PortNumber   *PortNumber                                                  `vht5gc:"optional"`
	IEExtensions *ProtocolExtensionContainerUserLocationInformationTWIFExtIEs `vht5gc:"optional"`
}

const (
	UserLocationInformationWAGFPresentNothing int = iota /* No components present */
	UserLocationInformationWAGFPresentGlobalLineID
	UserLocationInformationWAGFPresentHFCNodeID
	UserLocationInformationWAGFPresentChoiceExtensions
)

type UserLocationInformationWAGF struct {
	Present          int
	GlobalLineID     *GlobalLineID `vht5gc:"valueExt"`
	HFCNodeID        *HFCNodeID
	ChoiceExtensions *ProtocolIESingleContainerUserLocationInformationWAGFExtIEs
}

type UserLocationInformationNR struct {
	NRCGI        NRCGI                                                      `vht5gc:"valueExt"`
	TAI          TAI                                                        `vht5gc:"valueExt"`
	TimeStamp    *TimeStamp                                                 `vht5gc:"optional"`
	IEExtensions *ProtocolExtensionContainerUserLocationInformationNRExtIEs `vht5gc:"optional"`
}

type UserPlaneSecurityInformation struct {
	SecurityResult     SecurityResult                                                `vht5gc:"valueExt"`
	SecurityIndication SecurityIndication                                            `vht5gc:"valueExt"`
	IEExtensions       *ProtocolExtensionContainerUserPlaneSecurityInformationExtIEs `vht5gc:"optional"`
}

type VolumeTimedReportList struct {
	List []VolumeTimedReportItem `vht5gc:"valueExt,sizeLB:1,sizeUB:2"`
}

type VolumeTimedReportItem struct {
	StartTimeStamp OctetString                                            `vht5gc:"sizeLB:4,sizeUB:4"`
	EndTimeStamp   OctetString                                            `vht5gc:"sizeLB:4,sizeUB:4"`
	UsageCountUL   int64                                                  `vht5gc:"valueLB:0,valueUB:18446744073709551615"`
	UsageCountDL   int64                                                  `vht5gc:"valueLB:0,valueUB:18446744073709551615"`
	IEExtensions   *ProtocolExtensionContainerVolumeTimedReportItemExtIEs `vht5gc:"optional"`
}

const (
	WAGFIDPresentNothing int = iota /* No components present */
	WAGFIDPresentWAGFID
	WAGFIDPresentChoiceExtensions
)

type WAGFID struct {
	Present          int
	WAGFID           *BitString
	ChoiceExtensions *ProtocolIESingleContainerWAGFIDExtIEs
}

const (
	WarningAreaListPresentNothing int = iota /* No components present */
	WarningAreaListPresentEUTRACGIListForWarning
	WarningAreaListPresentNRCGIListForWarning
	WarningAreaListPresentTAIListForWarning
	WarningAreaListPresentEmergencyAreaIDList
	WarningAreaListPresentChoiceExtensions
)

type WarningAreaList struct {
	Present                int
	EUTRACGIListForWarning *EUTRACGIListForWarning
	NRCGIListForWarning    *NRCGIListForWarning
	TAIListForWarning      *TAIListForWarning
	EmergencyAreaIDList    *EmergencyAreaIDList
	ChoiceExtensions       *ProtocolIESingleContainerWarningAreaListExtIEs
}

type WLANMeasurementConfiguration struct {
	WlanMeasConfig         WLANMeasConfig
	WlanMeasConfigNameList *WLANMeasConfigNameList                                       `vht5gc:"optional"`
	WlanRssi               *WlanRssi                                                     `vht5gc:"optional"`
	WlanRtt                *WlanRtt                                                      `vht5gc:"optional"`
	IEExtensions           *ProtocolExtensionContainerWLANMeasurementConfigurationExtIEs `vht5gc:"optional"`
}

type WLANMeasConfigNameList struct {
	List []WLANMeasConfigNameItem `vht5gc:"valueExt,sizeLB:1,sizeUB:4"`
}

type WLANMeasConfigNameItem struct {
	WLANName     WLANName
	IEExtensions *ProtocolExtensionContainerWLANMeasConfigNameItemExtIEs `vht5gc:"optional"`
}

type WUSAssistanceInformation struct {
	PagingProbabilityInformation PagingProbabilityInformation
	IEExtensions                 *ProtocolExtensionContainerWUSAssistanceInformationExtIEs `vht5gc:"optional"`
}

type XnExtTLAs struct {
	List []XnExtTLAItem `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type XnExtTLAItem struct {
	IPsecTLA     *TransportLayerAddress                        `vht5gc:"optional"`
	GTPTLAs      *XnGTPTLAs                                    `vht5gc:"optional"`
	IEExtensions *ProtocolExtensionContainerXnExtTLAItemExtIEs `vht5gc:"optional"`
}

type XnGTPTLAs struct {
	List []TransportLayerAddress `vht5gc:"valueExt,sizeLB:1,sizeUB:16"`
}

type XnTLAs struct {
	List []TransportLayerAddress `vht5gc:"valueExt,sizeLB:1,sizeUB:2"`
}

type XnTNLConfigurationInfo struct {
	XnTransportLayerAddresses         XnTLAs
	XnExtendedTransportLayerAddresses *XnExtTLAs                                              `vht5gc:"optional"`
	IEExtensions                      *ProtocolExtensionContainerXnTNLConfigurationInfoExtIEs `vht5gc:"optional"`
}

#NG Application Protocol (NGAP)

Reference: 38.413

Title: NG-RAN; NG Application Protocol (NGAP)

Status: Under change control 

Type: Technical specification (TS)

Meetings: RAN#89-e

Version: 16.3.0

Upload date: 2020-10-02

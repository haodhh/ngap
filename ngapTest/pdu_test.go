package ngapTest

import (
	"testing"

	"ngap"
	"ngap/ngapBuild"
)

func TestHandoverRequestAcknowledgeTransfer(t *testing.T) {
	p1 := ngapBuild.BuildHandoverRequestAcknowledgeTransfer()
	b1, err := ngap.EncodeHandoverRequestAcknowledgeTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodeHandoverRequestAcknowledgeTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceModifyIndicationUnsuccessfulTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceModifyIndicationUnsuccessfulTransfer()
	b1, err := ngap.EncodePDUSessionResourceModifyIndicationUnsuccessfulTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceModifyIndicationUnsuccessfulTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceModifyUnsuccessfulTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceModifyUnsuccessfulTransfer()
	b1, err := ngap.EncodePDUSessionResourceModifyUnsuccessfulTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceModifyUnsuccessfulTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestHandoverResourceAllocationUnsuccessfulTransfer(t *testing.T) {
	p1 := ngapBuild.BuildHandoverResourceAllocationUnsuccessfulTransfer()
	b1, err := ngap.EncodeHandoverResourceAllocationUnsuccessfulTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodeHandoverResourceAllocationUnsuccessfulTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPathSwitchRequestSetupFailedTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPathSwitchRequestSetupFailedTransfer()
	b1, err := ngap.EncodePathSwitchRequestSetupFailedTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePathSwitchRequestSetupFailedTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceSetupUnsuccessfulTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceSetupUnsuccessfulTransfer()
	b1, err := ngap.EncodePDUSessionResourceSetupUnsuccessfulTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceSetupUnsuccessfulTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestHandoverCommandTransfer(t *testing.T) {
	p1 := ngapBuild.BuildHandoverCommandTransfer()
	b1, err := ngap.EncodeHandoverCommandTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodeHandoverCommandTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestHandoverRequiredTransfer(t *testing.T) {
	p1 := ngapBuild.BuildHandoverRequiredTransfer()
	b1, err := ngap.EncodeHandoverRequiredTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodeHandoverRequiredTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceModifyConfirmTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceModifyConfirmTransfer()
	b1, err := ngap.EncodePDUSessionResourceModifyConfirmTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceModifyConfirmTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceModifyIndicationTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceModifyIndicationTransfer()
	b1, err := ngap.EncodePDUSessionResourceModifyIndicationTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceModifyIndicationTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceModifyRequestTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceModifyRequestTransfer()
	b1, err := ngap.EncodePDUSessionResourceModifyRequestTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceModifyRequestTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceModifyResponseTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceModifyResponseTransfer()
	b1, err := ngap.EncodePDUSessionResourceModifyResponseTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceModifyResponseTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceNotifyTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceNotifyTransfer()
	b1, err := ngap.EncodePDUSessionResourceNotifyTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceNotifyTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceNotifyReleasedTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceNotifyReleasedTransfer()
	b1, err := ngap.EncodePDUSessionResourceNotifyReleasedTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceNotifyReleasedTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPathSwitchRequestUnsuccessfulTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPathSwitchRequestUnsuccessfulTransfer()
	b1, err := ngap.EncodePathSwitchRequestUnsuccessfulTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePathSwitchRequestUnsuccessfulTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceReleaseResponseTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceReleaseResponseTransfer()
	b1, err := ngap.EncodePDUSessionResourceReleaseResponseTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceReleaseResponseTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEContextResumeRequestTransfer(t *testing.T) {
	p1 := ngapBuild.BuildUEContextResumeRequestTransfer()
	b1, err := ngap.EncodeUEContextResumeRequestTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodeUEContextResumeRequestTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEContextResumeResponseTransfer(t *testing.T) {
	p1 := ngapBuild.BuildUEContextResumeResponseTransfer()
	b1, err := ngap.EncodeUEContextResumeResponseTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodeUEContextResumeResponseTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestSecondaryRATDataUsageReportTransfer(t *testing.T) {
	p1 := ngapBuild.BuildSecondaryRATDataUsageReportTransfer()
	b1, err := ngap.EncodeSecondaryRATDataUsageReportTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodeSecondaryRATDataUsageReportTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceSetupRequestTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceSetupRequestTransfer()
	b1, err := ngap.EncodePDUSessionResourceSetupRequestTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceSetupRequestTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceSetupResponseTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceSetupResponseTransfer()
	b1, err := ngap.EncodePDUSessionResourceSetupResponseTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceSetupResponseTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEContextSuspendRequestTransfer(t *testing.T) {
	p1 := ngapBuild.BuildUEContextSuspendRequestTransfer()
	b1, err := ngap.EncodeUEContextSuspendRequestTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodeUEContextSuspendRequestTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPathSwitchRequestAcknowledgeTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPathSwitchRequestAcknowledgeTransfer()
	b1, err := ngap.EncodePathSwitchRequestAcknowledgeTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePathSwitchRequestAcknowledgeTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPathSwitchRequestTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPathSwitchRequestTransfer()
	b1, err := ngap.EncodePathSwitchRequestTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePathSwitchRequestTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestHandoverPreparationUnsuccessfulTransfer(t *testing.T) {
	p1 := ngapBuild.BuildHandoverPreparationUnsuccessfulTransfer()
	b1, err := ngap.EncodeHandoverPreparationUnsuccessfulTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodeHandoverPreparationUnsuccessfulTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceReleaseCommandTransfer(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceReleaseCommandTransfer()
	b1, err := ngap.EncodePDUSessionResourceReleaseCommandTransfer(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.DecodePDUSessionResourceReleaseCommandTransfer(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

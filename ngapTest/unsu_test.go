package ngapTest

import (
	"testing"

	"ngap"
	"ngap/ngapBuild"
)

func TestInitialContextSetupFailure(t *testing.T) {
	p1 := ngapBuild.BuildInitialContextSetupFailure()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEContextResumeFailure(t *testing.T) {
	p1 := ngapBuild.BuildUEContextResumeFailure()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEContextSuspendFailure(t *testing.T) {
	p1 := ngapBuild.BuildUEContextSuspendFailure()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEContextModificationFailure(t *testing.T) {
	p1 := ngapBuild.BuildUEContextModificationFailure()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestHandoverPreparationFailure(t *testing.T) {
	p1 := ngapBuild.BuildHandoverPreparationFailure()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestHandoverFailure(t *testing.T) {
	p1 := ngapBuild.BuildHandoverFailure()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPathSwitchRequestFailure(t *testing.T) {
	p1 := ngapBuild.BuildPathSwitchRequestFailure()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestNGSetupFailure(t *testing.T) {
	p1 := ngapBuild.BuildNGSetupFailure()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestRANConfigurationUpdateFailure(t *testing.T) {
	p1 := ngapBuild.BuildRANConfigurationUpdateFailure()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestAMFConfigurationUpdateFailure(t *testing.T) {
	p1 := ngapBuild.BuildAMFConfigurationUpdateFailure()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

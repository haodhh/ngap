package ngapTest

import (
	"testing"

	"ngap"
	"ngap/ngapBuild"
)

func TestPDUSessionResourceSetupRequest(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceSetupRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceReleaseCommand(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceReleaseCommand()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceModifyRequest(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceModifyRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceNotify(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceNotify()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPDUSessionResourceModifyIndication(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceModifyIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestInitialContextSetupRequest(t *testing.T) {
	p1 := ngapBuild.BuildInitialContextSetupRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEContextReleaseRequest(t *testing.T) {
	p1 := ngapBuild.BuildUEContextReleaseRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEContextReleaseCommand(t *testing.T) {
	p1 := ngapBuild.BuildUEContextReleaseCommand()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEContextResumeRequest(t *testing.T) {
	p1 := ngapBuild.BuildUEContextResumeRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEContextSuspendRequest(t *testing.T) {
	p1 := ngapBuild.BuildUEContextSuspendRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEContextModificationRequest(t *testing.T) {
	p1 := ngapBuild.BuildUEContextModificationRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestRRCInactiveTransitionReport(t *testing.T) {
	p1 := ngapBuild.BuildRRCInactiveTransitionReport()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestRetrieveUEInformation(t *testing.T) {
	p1 := ngapBuild.BuildRetrieveUEInformation()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUEInformationTransfer(t *testing.T) {
	p1 := ngapBuild.BuildUEInformationTransfer()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestRANCPRelocationIndication(t *testing.T) {
	p1 := ngapBuild.BuildRANCPRelocationIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestHandoverRequired(t *testing.T) {
	p1 := ngapBuild.BuildHandoverRequired()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestHandoverRequest(t *testing.T) {
	p1 := ngapBuild.BuildHandoverRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestHandoverNotify(t *testing.T) {
	p1 := ngapBuild.BuildHandoverNotify()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPathSwitchRequest(t *testing.T) {
	p1 := ngapBuild.BuildPathSwitchRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestHandoverCancel(t *testing.T) {
	p1 := ngapBuild.BuildHandoverCancel()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestHandoverSuccess(t *testing.T) {
	p1 := ngapBuild.BuildHandoverSuccess()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUplinkRANEarlyStatusTransfer(t *testing.T) {
	p1 := ngapBuild.BuildUplinkRANEarlyStatusTransfer()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestDownlinkRANEarlyStatusTransfer(t *testing.T) {
	p1 := ngapBuild.BuildDownlinkRANEarlyStatusTransfer()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUplinkRANStatusTransfer(t *testing.T) {
	p1 := ngapBuild.BuildUplinkRANStatusTransfer()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestDownlinkRANStatusTransfer(t *testing.T) {
	p1 := ngapBuild.BuildDownlinkRANStatusTransfer()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPaging(t *testing.T) {
	p1 := ngapBuild.BuildPaging()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestInitialUEMessage(t *testing.T) {
	p1 := ngapBuild.BuildInitialUEMessage()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestDownlinkNASTransport(t *testing.T) {
	p1 := ngapBuild.BuildDownlinkNASTransport()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUplinkNASTransport(t *testing.T) {
	p1 := ngapBuild.BuildUplinkNASTransport()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestNASNonDeliveryIndication(t *testing.T) {
	p1 := ngapBuild.BuildNASNonDeliveryIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestRerouteNASRequest(t *testing.T) {
	p1 := ngapBuild.BuildRerouteNASRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestNGSetupRequest(t *testing.T) {
	p1 := ngapBuild.BuildNGSetupRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestRANConfigurationUpdate(t *testing.T) {
	p1 := ngapBuild.BuildRANConfigurationUpdate()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestAMFConfigurationUpdate(t *testing.T) {
	p1 := ngapBuild.BuildAMFConfigurationUpdate()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestAMFStatusIndication(t *testing.T) {
	p1 := ngapBuild.BuildAMFStatusIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestNGReset(t *testing.T) {
	p1 := ngapBuild.BuildNGReset()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestErrorIndication(t *testing.T) {
	p1 := ngapBuild.BuildErrorIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestOverloadStart(t *testing.T) {
	p1 := ngapBuild.BuildOverloadStart()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestOverloadStop(t *testing.T) {
	p1 := ngapBuild.BuildOverloadStop()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUplinkRANConfigurationTransfer(t *testing.T) {
	p1 := ngapBuild.BuildUplinkRANConfigurationTransfer()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestDownlinkRANConfigurationTransfer(t *testing.T) {
	p1 := ngapBuild.BuildDownlinkRANConfigurationTransfer()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestWriteReplaceWarningRequest(t *testing.T) {
	p1 := ngapBuild.BuildWriteReplaceWarningRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPWSCancelRequest(t *testing.T) {
	p1 := ngapBuild.BuildPWSCancelRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPWSRestartIndication(t *testing.T) {
	p1 := ngapBuild.BuildPWSRestartIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPWSFailureIndication(t *testing.T) {
	p1 := ngapBuild.BuildPWSFailureIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestDownlinkUEAssociatedNRPPaTransport(t *testing.T) {
	p1 := ngapBuild.BuildDownlinkUEAssociatedNRPPaTransport()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUplinkUEAssociatedNRPPaTransport(t *testing.T) {
	p1 := ngapBuild.BuildUplinkUEAssociatedNRPPaTransport()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestDownlinkNonUEAssociatedNRPPaTransport(t *testing.T) {
	p1 := ngapBuild.BuildDownlinkNonUEAssociatedNRPPaTransport()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUplinkNonUEAssociatedNRPPaTransport(t *testing.T) {
	p1 := ngapBuild.BuildUplinkNonUEAssociatedNRPPaTransport()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestTraceStart(t *testing.T) {
	p1 := ngapBuild.BuildTraceStart()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestTraceFailureIndication(t *testing.T) {
	p1 := ngapBuild.BuildTraceFailureIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestDeactivateTrace(t *testing.T) {
	p1 := ngapBuild.BuildDeactivateTrace()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestCellTrafficTrace(t *testing.T) {
	p1 := ngapBuild.BuildCellTrafficTrace()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestLocationReportingControl(t *testing.T) {
	p1 := ngapBuild.BuildLocationReportingControl()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestLocationReportingFailureIndication(t *testing.T) {
	p1 := ngapBuild.BuildLocationReportingFailureIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestLocationReport(t *testing.T) {
	p1 := ngapBuild.BuildLocationReport()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUETNLABindingReleaseRequest(t *testing.T) {
	p1 := ngapBuild.BuildUETNLABindingReleaseRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUERadioCapabilityInfoIndication(t *testing.T) {
	p1 := ngapBuild.BuildUERadioCapabilityInfoIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUERadioCapabilityCheckRequest(t *testing.T) {
	p1 := ngapBuild.BuildUERadioCapabilityCheckRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestPrivateMessage(t *testing.T) {
	p1 := ngapBuild.BuildPrivateMessage()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestSecondaryRATDataUsageReport(t *testing.T) {
	p1 := ngapBuild.BuildSecondaryRATDataUsageReport()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUplinkRIMInformationTransfer(t *testing.T) {
	p1 := ngapBuild.BuildUplinkRIMInformationTransfer()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestDownlinkRIMInformationTransfer(t *testing.T) {
	p1 := ngapBuild.BuildDownlinkRIMInformationTransfer()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestConnectionEstablishmentIndication(t *testing.T) {
	p1 := ngapBuild.BuildConnectionEstablishmentIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestUERadioCapabilityIDMappingRequest(t *testing.T) {
	p1 := ngapBuild.BuildUERadioCapabilityIDMappingRequest()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

func TestAMFCPRelocationIndication(t *testing.T) {
	p1 := ngapBuild.BuildAMFCPRelocationIndication()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
}

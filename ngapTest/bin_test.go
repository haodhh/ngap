package ngapTest

import (
	"encoding/hex"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"testing"

	"ngap"
)

func TestDecodeBinary(t *testing.T){
	hexStr := "20150031000004000100050100414d4600600008000002f839cafe0000564001ff005000100002f839000110080102031008112233"
	bin, _ := hex.DecodeString(hexStr)
	pdu, err := ngap.Decode(bin)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(ngap.PrettyPrint(pdu))
}

func TestGenTrace(t *testing.T) {
	data, _ := ioutil.ReadFile("msg.txt")
	sFile := strings.Split(fmt.Sprintf("%s", data), "\n")
	for i, s := range sFile {
		si := strings.Fields(s)
		fmt.Print("\n\tpdu"+strconv.Itoa(i)+" := ngapBuild.Build"+si[0]+"()" +
			"\n\tbin"+strconv.Itoa(i)+", _ := ngap.Encode(pdu"+strconv.Itoa(i)+")" +
			"\n\tallBin = append(allBin, bin"+strconv.Itoa(i)+")")
	}
}
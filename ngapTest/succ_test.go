package ngapTest

import (
	"testing"

	"ngap"
	"ngap/ngapBuild"
)

func TestPDUSessionResourceSetupResponse(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceSetupResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestPDUSessionResourceReleaseResponse(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceReleaseResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestPDUSessionResourceModifyResponse(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceModifyResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestPDUSessionResourceModifyConfirm(t *testing.T) {
	p1 := ngapBuild.BuildPDUSessionResourceModifyConfirm()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestInitialContextSetupResponse(t *testing.T) {
	p1 := ngapBuild.BuildInitialContextSetupResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestUEContextReleaseComplete(t *testing.T) {
	p1 := ngapBuild.BuildUEContextReleaseComplete()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestUEContextResumeResponse(t *testing.T) {
	p1 := ngapBuild.BuildUEContextResumeResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestUEContextSuspendResponse(t *testing.T) {
	p1 := ngapBuild.BuildUEContextSuspendResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestUEContextModificationResponse(t *testing.T) {
	p1 := ngapBuild.BuildUEContextModificationResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestHandoverCommand(t *testing.T) {
	p1 := ngapBuild.BuildHandoverCommand()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestHandoverRequestAcknowledge(t *testing.T) {
	p1 := ngapBuild.BuildHandoverRequestAcknowledge()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestPathSwitchRequestAcknowledge(t *testing.T) {
	p1 := ngapBuild.BuildPathSwitchRequestAcknowledge()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestHandoverCancelAcknowledge(t *testing.T) {
	p1 := ngapBuild.BuildHandoverCancelAcknowledge()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestNGSetupResponse(t *testing.T) {
	p1 := ngapBuild.BuildNGSetupResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestRANConfigurationUpdateAcknowledge(t *testing.T) {
	p1 := ngapBuild.BuildRANConfigurationUpdateAcknowledge()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestAMFConfigurationUpdateAcknowledge(t *testing.T) {
	p1 := ngapBuild.BuildAMFConfigurationUpdateAcknowledge()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestNGResetAcknowledge(t *testing.T) {
	p1 := ngapBuild.BuildNGResetAcknowledge()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestWriteReplaceWarningResponse(t *testing.T) {
	p1 := ngapBuild.BuildWriteReplaceWarningResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestPWSCancelResponse(t *testing.T) {
	p1 := ngapBuild.BuildPWSCancelResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestUERadioCapabilityCheckResponse(t *testing.T) {
	p1 := ngapBuild.BuildUERadioCapabilityCheckResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

func TestUERadioCapabilityIDMappingResponse(t *testing.T) {
	p1 := ngapBuild.BuildUERadioCapabilityIDMappingResponse()
	b1, err := ngap.Encode(p1)
	if err != nil {
		t.Fatal(err)
	}
	p2, err := ngap.Decode(b1)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(p2)
	//t.Log(ngap.Print(*p2))
}

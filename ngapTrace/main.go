package main

import (
	"flag"
	"log"
	"net"
	"strings"

	"github.com/ishidawataru/sctp"
	"ngap"
	"ngap/ngapBuild"
)

func serveClient(conn net.Conn, bufsize int) error {
	for {
		buf := make([]byte, bufsize+128) // add overhead of SCTPSndRcvInfoWrappedConn
		n, err := conn.Read(buf)
		if err != nil {
			log.Printf("read failed: %v", err)
			return err
		}
		log.Printf("read: %d", n)
		n, err = conn.Write(buf[:n])
		if err != nil {
			log.Printf("write failed: %v", err)
			return err
		}
		log.Printf("write: %d", n)
	}
}

func main() {
	var server = flag.Bool("server", false, "")
	var ip = flag.String("ip", "0.0.0.0", "")
	var port = flag.Int("port", 0, "")
	var lport = flag.Int("lport", 0, "")
	var bufsize = flag.Int("bufsize", 256, "")
	var sndbuf = flag.Int("sndbuf", 0, "")
	var rcvbuf = flag.Int("rcvbuf", 0, "")

	flag.Parse()

	ips := []net.IPAddr{}

	for _, i := range strings.Split(*ip, ",") {
		if a, err := net.ResolveIPAddr("ip", i); err == nil {
			log.Printf("Resolved address '%s' to %s", i, a)
			ips = append(ips, *a)
		} else {
			log.Printf("Error resolving address '%s': %v", i, err)
		}
	}

	addr := &sctp.SCTPAddr{
		IPAddrs: ips,
		Port:    *port,
	}
	log.Printf("raw addr: %+v\n", addr.ToRawSockAddrBuf())

	if *server {
		ln, err := sctp.ListenSCTP("sctp", addr)
		if err != nil {
			log.Fatalf("failed to listen: %v", err)
		}
		log.Printf("Listen on %s", ln.Addr())

		for {
			conn, err := ln.Accept()
			if err != nil {
				log.Fatalf("failed to accept: %v", err)
			}
			log.Printf("Accepted Connection from RemoteAddr: %s", conn.RemoteAddr())
			wconn := sctp.NewSCTPSndRcvInfoWrappedConn(conn.(*sctp.SCTPConn))
			if *sndbuf != 0 {
				err = wconn.SetWriteBuffer(*sndbuf)
				if err != nil {
					log.Fatalf("failed to set write buf: %v", err)
				}
			}
			if *rcvbuf != 0 {
				err = wconn.SetReadBuffer(*rcvbuf)
				if err != nil {
					log.Fatalf("failed to set read buf: %v", err)
				}
			}
			*sndbuf, err = wconn.GetWriteBuffer()
			if err != nil {
				log.Fatalf("failed to get write buf: %v", err)
			}
			*rcvbuf, err = wconn.GetWriteBuffer()
			if err != nil {
				log.Fatalf("failed to get read buf: %v", err)
			}
			log.Printf("SndBufSize: %d, RcvBufSize: %d", *sndbuf, *rcvbuf)

			go serveClient(wconn, *bufsize)
		}

	} else {
		var laddr *sctp.SCTPAddr
		if *lport != 0 {
			laddr = &sctp.SCTPAddr{
				Port: *lport,
			}
		}
		conn, err := sctp.DialSCTP("sctp", laddr, addr)
		if err != nil {
			log.Fatalf("failed to dial: %v", err)
		}

		log.Printf("Dail LocalAddr: %s; RemoteAddr: %s", conn.LocalAddr(), conn.RemoteAddr())

		if *sndbuf != 0 {
			err = conn.SetWriteBuffer(*sndbuf)
			if err != nil {
				log.Fatalf("failed to set write buf: %v", err)
			}
		}
		if *rcvbuf != 0 {
			err = conn.SetReadBuffer(*rcvbuf)
			if err != nil {
				log.Fatalf("failed to set read buf: %v", err)
			}
		}

		*sndbuf, err = conn.GetWriteBuffer()
		if err != nil {
			log.Fatalf("failed to get write buf: %v", err)
		}
		*rcvbuf, err = conn.GetReadBuffer()
		if err != nil {
			log.Fatalf("failed to get read buf: %v", err)
		}
		log.Printf("SndBufSize: %d, RcvBufSize: %d", *sndbuf, *rcvbuf)

		ppid := 0
		for _, buf := range getBuf() {
			info := &sctp.SndRcvInfo{
				Stream: uint16(ppid),
				PPID:   uint32(ppid),
			}
			ppid += 1
			conn.SubscribeEvents(sctp.SCTP_EVENT_DATA_IO)
			_, err = conn.SCTPWrite(buf, info)
			if err != nil {
				log.Printf("failed to write: %v", err)
			}
			_, info, err = conn.SCTPRead(buf)
			if err != nil {
				log.Printf("failed to read: %v", err)
			}
			// time.Sleep(time.Second)
		}
	}
}

func getBuf() [][]byte {
	var allBin [][]byte
	pdu0 := ngapBuild.BuildAMFConfigurationUpdate()
	bin0, _ := ngap.Encode(pdu0)
	allBin = append(allBin, bin0)
	pdu1 := ngapBuild.BuildHandoverCancel()
	bin1, _ := ngap.Encode(pdu1)
	allBin = append(allBin, bin1)
	pdu2 := ngapBuild.BuildHandoverRequired()
	bin2, _ := ngap.Encode(pdu2)
	allBin = append(allBin, bin2)
	pdu3 := ngapBuild.BuildHandoverRequest()
	bin3, _ := ngap.Encode(pdu3)
	allBin = append(allBin, bin3)
	pdu4 := ngapBuild.BuildInitialContextSetupRequest()
	bin4, _ := ngap.Encode(pdu4)
	allBin = append(allBin, bin4)
	pdu5 := ngapBuild.BuildNGReset()
	bin5, _ := ngap.Encode(pdu5)
	allBin = append(allBin, bin5)
	pdu6 := ngapBuild.BuildNGSetupRequest()
	bin6, _ := ngap.Encode(pdu6)
	allBin = append(allBin, bin6)
	pdu7 := ngapBuild.BuildPathSwitchRequest()
	bin7, _ := ngap.Encode(pdu7)
	allBin = append(allBin, bin7)
	pdu8 := ngapBuild.BuildPDUSessionResourceModifyRequest()
	bin8, _ := ngap.Encode(pdu8)
	allBin = append(allBin, bin8)
	pdu9 := ngapBuild.BuildPDUSessionResourceModifyIndication()
	bin9, _ := ngap.Encode(pdu9)
	allBin = append(allBin, bin9)
	pdu10 := ngapBuild.BuildPDUSessionResourceReleaseCommand()
	bin10, _ := ngap.Encode(pdu10)
	allBin = append(allBin, bin10)
	pdu11 := ngapBuild.BuildPDUSessionResourceSetupRequest()
	bin11, _ := ngap.Encode(pdu11)
	allBin = append(allBin, bin11)
	pdu12 := ngapBuild.BuildPWSCancelRequest()
	bin12, _ := ngap.Encode(pdu12)
	allBin = append(allBin, bin12)
	pdu13 := ngapBuild.BuildRANConfigurationUpdate()
	bin13, _ := ngap.Encode(pdu13)
	allBin = append(allBin, bin13)
	pdu14 := ngapBuild.BuildUEContextModificationRequest()
	bin14, _ := ngap.Encode(pdu14)
	allBin = append(allBin, bin14)
	pdu15 := ngapBuild.BuildUEContextReleaseCommand()
	bin15, _ := ngap.Encode(pdu15)
	allBin = append(allBin, bin15)
	pdu16 := ngapBuild.BuildUEContextResumeRequest()
	bin16, _ := ngap.Encode(pdu16)
	allBin = append(allBin, bin16)
	pdu17 := ngapBuild.BuildUEContextSuspendRequest()
	bin17, _ := ngap.Encode(pdu17)
	allBin = append(allBin, bin17)
	pdu18 := ngapBuild.BuildUERadioCapabilityCheckRequest()
	bin18, _ := ngap.Encode(pdu18)
	allBin = append(allBin, bin18)
	pdu19 := ngapBuild.BuildUERadioCapabilityIDMappingRequest()
	bin19, _ := ngap.Encode(pdu19)
	allBin = append(allBin, bin19)
	pdu20 := ngapBuild.BuildWriteReplaceWarningRequest()
	bin20, _ := ngap.Encode(pdu20)
	allBin = append(allBin, bin20)
	pdu21 := ngapBuild.BuildAMFCPRelocationIndication()
	bin21, _ := ngap.Encode(pdu21)
	allBin = append(allBin, bin21)
	pdu22 := ngapBuild.BuildAMFStatusIndication()
	bin22, _ := ngap.Encode(pdu22)
	allBin = append(allBin, bin22)
	pdu23 := ngapBuild.BuildCellTrafficTrace()
	bin23, _ := ngap.Encode(pdu23)
	allBin = append(allBin, bin23)
	pdu24 := ngapBuild.BuildConnectionEstablishmentIndication()
	bin24, _ := ngap.Encode(pdu24)
	allBin = append(allBin, bin24)
	pdu25 := ngapBuild.BuildDeactivateTrace()
	bin25, _ := ngap.Encode(pdu25)
	allBin = append(allBin, bin25)
	pdu26 := ngapBuild.BuildDownlinkNASTransport()
	bin26, _ := ngap.Encode(pdu26)
	allBin = append(allBin, bin26)
	pdu27 := ngapBuild.BuildDownlinkNonUEAssociatedNRPPaTransport()
	bin27, _ := ngap.Encode(pdu27)
	allBin = append(allBin, bin27)
	pdu28 := ngapBuild.BuildDownlinkRANConfigurationTransfer()
	bin28, _ := ngap.Encode(pdu28)
	allBin = append(allBin, bin28)
	pdu29 := ngapBuild.BuildDownlinkRANEarlyStatusTransfer()
	bin29, _ := ngap.Encode(pdu29)
	allBin = append(allBin, bin29)
	pdu30 := ngapBuild.BuildDownlinkRANStatusTransfer()
	bin30, _ := ngap.Encode(pdu30)
	allBin = append(allBin, bin30)
	pdu31 := ngapBuild.BuildDownlinkRIMInformationTransfer()
	bin31, _ := ngap.Encode(pdu31)
	allBin = append(allBin, bin31)
	pdu32 := ngapBuild.BuildDownlinkUEAssociatedNRPPaTransport()
	bin32, _ := ngap.Encode(pdu32)
	allBin = append(allBin, bin32)
	pdu33 := ngapBuild.BuildErrorIndication()
	bin33, _ := ngap.Encode(pdu33)
	allBin = append(allBin, bin33)
	pdu34 := ngapBuild.BuildHandoverNotify()
	bin34, _ := ngap.Encode(pdu34)
	allBin = append(allBin, bin34)
	pdu35 := ngapBuild.BuildHandoverSuccess()
	bin35, _ := ngap.Encode(pdu35)
	allBin = append(allBin, bin35)
	pdu36 := ngapBuild.BuildInitialUEMessage()
	bin36, _ := ngap.Encode(pdu36)
	allBin = append(allBin, bin36)
	pdu37 := ngapBuild.BuildLocationReport()
	bin37, _ := ngap.Encode(pdu37)
	allBin = append(allBin, bin37)
	pdu38 := ngapBuild.BuildLocationReportingControl()
	bin38, _ := ngap.Encode(pdu38)
	allBin = append(allBin, bin38)
	pdu39 := ngapBuild.BuildLocationReportingFailureIndication()
	bin39, _ := ngap.Encode(pdu39)
	allBin = append(allBin, bin39)
	pdu40 := ngapBuild.BuildNASNonDeliveryIndication()
	bin40, _ := ngap.Encode(pdu40)
	allBin = append(allBin, bin40)
	pdu41 := ngapBuild.BuildOverloadStart()
	bin41, _ := ngap.Encode(pdu41)
	allBin = append(allBin, bin41)
	pdu42 := ngapBuild.BuildOverloadStop()
	bin42, _ := ngap.Encode(pdu42)
	allBin = append(allBin, bin42)
	pdu43 := ngapBuild.BuildPaging()
	bin43, _ := ngap.Encode(pdu43)
	allBin = append(allBin, bin43)
	pdu44 := ngapBuild.BuildPDUSessionResourceNotify()
	bin44, _ := ngap.Encode(pdu44)
	allBin = append(allBin, bin44)
	pdu45 := ngapBuild.BuildPrivateMessage()
	bin45, _ := ngap.Encode(pdu45)
	allBin = append(allBin, bin45)
	pdu46 := ngapBuild.BuildPWSFailureIndication()
	bin46, _ := ngap.Encode(pdu46)
	allBin = append(allBin, bin46)
	pdu47 := ngapBuild.BuildPWSRestartIndication()
	bin47, _ := ngap.Encode(pdu47)
	allBin = append(allBin, bin47)
	pdu48 := ngapBuild.BuildRANCPRelocationIndication()
	bin48, _ := ngap.Encode(pdu48)
	allBin = append(allBin, bin48)
	pdu49 := ngapBuild.BuildRerouteNASRequest()
	bin49, _ := ngap.Encode(pdu49)
	allBin = append(allBin, bin49)
	pdu50 := ngapBuild.BuildRetrieveUEInformation()
	bin50, _ := ngap.Encode(pdu50)
	allBin = append(allBin, bin50)
	pdu51 := ngapBuild.BuildRRCInactiveTransitionReport()
	bin51, _ := ngap.Encode(pdu51)
	allBin = append(allBin, bin51)
	pdu52 := ngapBuild.BuildSecondaryRATDataUsageReport()
	bin52, _ := ngap.Encode(pdu52)
	allBin = append(allBin, bin52)
	pdu53 := ngapBuild.BuildTraceFailureIndication()
	bin53, _ := ngap.Encode(pdu53)
	allBin = append(allBin, bin53)
	pdu54 := ngapBuild.BuildTraceStart()
	bin54, _ := ngap.Encode(pdu54)
	allBin = append(allBin, bin54)
	pdu55 := ngapBuild.BuildUEContextReleaseRequest()
	bin55, _ := ngap.Encode(pdu55)
	allBin = append(allBin, bin55)
	pdu56 := ngapBuild.BuildUEInformationTransfer()
	bin56, _ := ngap.Encode(pdu56)
	allBin = append(allBin, bin56)
	pdu57 := ngapBuild.BuildUERadioCapabilityInfoIndication()
	bin57, _ := ngap.Encode(pdu57)
	allBin = append(allBin, bin57)
	pdu58 := ngapBuild.BuildUETNLABindingReleaseRequest()
	bin58, _ := ngap.Encode(pdu58)
	allBin = append(allBin, bin58)
	pdu59 := ngapBuild.BuildUplinkNASTransport()
	bin59, _ := ngap.Encode(pdu59)
	allBin = append(allBin, bin59)
	pdu60 := ngapBuild.BuildUplinkNonUEAssociatedNRPPaTransport()
	bin60, _ := ngap.Encode(pdu60)
	allBin = append(allBin, bin60)
	pdu61 := ngapBuild.BuildUplinkRANConfigurationTransfer()
	bin61, _ := ngap.Encode(pdu61)
	allBin = append(allBin, bin61)
	pdu62 := ngapBuild.BuildUplinkRANEarlyStatusTransfer()
	bin62, _ := ngap.Encode(pdu62)
	allBin = append(allBin, bin62)
	pdu63 := ngapBuild.BuildUplinkRANStatusTransfer()
	bin63, _ := ngap.Encode(pdu63)
	allBin = append(allBin, bin63)
	pdu64 := ngapBuild.BuildUplinkRIMInformationTransfer()
	bin64, _ := ngap.Encode(pdu64)
	allBin = append(allBin, bin64)
	pdu65 := ngapBuild.BuildUplinkUEAssociatedNRPPaTransport()
	bin65, _ := ngap.Encode(pdu65)
	allBin = append(allBin, bin65)
	pdu66 := ngapBuild.BuildAMFConfigurationUpdateAcknowledge()
	bin66, _ := ngap.Encode(pdu66)
	allBin = append(allBin, bin66)
	pdu67 := ngapBuild.BuildHandoverCancelAcknowledge()
	bin67, _ := ngap.Encode(pdu67)
	allBin = append(allBin, bin67)
	pdu68 := ngapBuild.BuildHandoverCommand()
	bin68, _ := ngap.Encode(pdu68)
	allBin = append(allBin, bin68)
	pdu69 := ngapBuild.BuildHandoverRequestAcknowledge()
	bin69, _ := ngap.Encode(pdu69)
	allBin = append(allBin, bin69)
	pdu70 := ngapBuild.BuildInitialContextSetupResponse()
	bin70, _ := ngap.Encode(pdu70)
	allBin = append(allBin, bin70)
	pdu71 := ngapBuild.BuildNGResetAcknowledge()
	bin71, _ := ngap.Encode(pdu71)
	allBin = append(allBin, bin71)
	pdu72 := ngapBuild.BuildNGSetupResponse()
	bin72, _ := ngap.Encode(pdu72)
	allBin = append(allBin, bin72)
	pdu73 := ngapBuild.BuildPathSwitchRequestAcknowledge()
	bin73, _ := ngap.Encode(pdu73)
	allBin = append(allBin, bin73)
	pdu74 := ngapBuild.BuildPDUSessionResourceModifyResponse()
	bin74, _ := ngap.Encode(pdu74)
	allBin = append(allBin, bin74)
	pdu75 := ngapBuild.BuildPDUSessionResourceModifyConfirm()
	bin75, _ := ngap.Encode(pdu75)
	allBin = append(allBin, bin75)
	pdu76 := ngapBuild.BuildPDUSessionResourceReleaseResponse()
	bin76, _ := ngap.Encode(pdu76)
	allBin = append(allBin, bin76)
	pdu77 := ngapBuild.BuildPDUSessionResourceSetupResponse()
	bin77, _ := ngap.Encode(pdu77)
	allBin = append(allBin, bin77)
	pdu78 := ngapBuild.BuildPWSCancelResponse()
	bin78, _ := ngap.Encode(pdu78)
	allBin = append(allBin, bin78)
	pdu79 := ngapBuild.BuildRANConfigurationUpdateAcknowledge()
	bin79, _ := ngap.Encode(pdu79)
	allBin = append(allBin, bin79)
	pdu80 := ngapBuild.BuildUEContextModificationResponse()
	bin80, _ := ngap.Encode(pdu80)
	allBin = append(allBin, bin80)
	pdu81 := ngapBuild.BuildUEContextReleaseComplete()
	bin81, _ := ngap.Encode(pdu81)
	allBin = append(allBin, bin81)
	pdu82 := ngapBuild.BuildUEContextResumeResponse()
	bin82, _ := ngap.Encode(pdu82)
	allBin = append(allBin, bin82)
	pdu83 := ngapBuild.BuildUEContextSuspendResponse()
	bin83, _ := ngap.Encode(pdu83)
	allBin = append(allBin, bin83)
	pdu84 := ngapBuild.BuildUERadioCapabilityCheckResponse()
	bin84, _ := ngap.Encode(pdu84)
	allBin = append(allBin, bin84)
	pdu85 := ngapBuild.BuildUERadioCapabilityIDMappingResponse()
	bin85, _ := ngap.Encode(pdu85)
	allBin = append(allBin, bin85)
	pdu86 := ngapBuild.BuildWriteReplaceWarningResponse()
	bin86, _ := ngap.Encode(pdu86)
	allBin = append(allBin, bin86)
	pdu87 := ngapBuild.BuildAMFConfigurationUpdateFailure()
	bin87, _ := ngap.Encode(pdu87)
	allBin = append(allBin, bin87)
	pdu88 := ngapBuild.BuildHandoverPreparationFailure()
	bin88, _ := ngap.Encode(pdu88)
	allBin = append(allBin, bin88)
	pdu89 := ngapBuild.BuildHandoverFailure()
	bin89, _ := ngap.Encode(pdu89)
	allBin = append(allBin, bin89)
	pdu90 := ngapBuild.BuildInitialContextSetupFailure()
	bin90, _ := ngap.Encode(pdu90)
	allBin = append(allBin, bin90)
	pdu91 := ngapBuild.BuildNGSetupFailure()
	bin91, _ := ngap.Encode(pdu91)
	allBin = append(allBin, bin91)
	pdu92 := ngapBuild.BuildPathSwitchRequestFailure()
	bin92, _ := ngap.Encode(pdu92)
	allBin = append(allBin, bin92)
	pdu93 := ngapBuild.BuildRANConfigurationUpdateFailure()
	bin93, _ := ngap.Encode(pdu93)
	allBin = append(allBin, bin93)
	pdu94 := ngapBuild.BuildUEContextModificationFailure()
	bin94, _ := ngap.Encode(pdu94)
	allBin = append(allBin, bin94)
	pdu95 := ngapBuild.BuildUEContextResumeFailure()
	bin95, _ := ngap.Encode(pdu95)
	allBin = append(allBin, bin95)
	pdu96 := ngapBuild.BuildUEContextSuspendFailure()
	bin96, _ := ngap.Encode(pdu96)
	allBin = append(allBin, bin96)
	return allBin
}

package ngap

import (
	"ngap/ngapCodec"
	"ngap/ngapType"
)

func DecodeHandoverRequestAcknowledgeTransfer(b []byte) (pdu *ngapType.HandoverRequestAcknowledgeTransfer, err error) {
	pdu = &ngapType.HandoverRequestAcknowledgeTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodeHandoverRequestAcknowledgeTransfer(pdu ngapType.HandoverRequestAcknowledgeTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceModifyIndicationUnsuccessfulTransfer(b []byte) (pdu *ngapType.PDUSessionResourceModifyIndicationUnsuccessfulTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceModifyIndicationUnsuccessfulTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceModifyIndicationUnsuccessfulTransfer(pdu ngapType.PDUSessionResourceModifyIndicationUnsuccessfulTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceModifyUnsuccessfulTransfer(b []byte) (pdu *ngapType.PDUSessionResourceModifyUnsuccessfulTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceModifyUnsuccessfulTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceModifyUnsuccessfulTransfer(pdu ngapType.PDUSessionResourceModifyUnsuccessfulTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodeHandoverResourceAllocationUnsuccessfulTransfer(b []byte) (pdu *ngapType.HandoverResourceAllocationUnsuccessfulTransfer, err error) {
	pdu = &ngapType.HandoverResourceAllocationUnsuccessfulTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodeHandoverResourceAllocationUnsuccessfulTransfer(pdu ngapType.HandoverResourceAllocationUnsuccessfulTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePathSwitchRequestSetupFailedTransfer(b []byte) (pdu *ngapType.PathSwitchRequestSetupFailedTransfer, err error) {
	pdu = &ngapType.PathSwitchRequestSetupFailedTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePathSwitchRequestSetupFailedTransfer(pdu ngapType.PathSwitchRequestSetupFailedTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceSetupUnsuccessfulTransfer(b []byte) (pdu *ngapType.PDUSessionResourceSetupUnsuccessfulTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceSetupUnsuccessfulTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceSetupUnsuccessfulTransfer(pdu ngapType.PDUSessionResourceSetupUnsuccessfulTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodeHandoverCommandTransfer(b []byte) (pdu *ngapType.HandoverCommandTransfer, err error) {
	pdu = &ngapType.HandoverCommandTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodeHandoverCommandTransfer(pdu ngapType.HandoverCommandTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodeHandoverRequiredTransfer(b []byte) (pdu *ngapType.HandoverRequiredTransfer, err error) {
	pdu = &ngapType.HandoverRequiredTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodeHandoverRequiredTransfer(pdu ngapType.HandoverRequiredTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceModifyConfirmTransfer(b []byte) (pdu *ngapType.PDUSessionResourceModifyConfirmTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceModifyConfirmTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceModifyConfirmTransfer(pdu ngapType.PDUSessionResourceModifyConfirmTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceModifyIndicationTransfer(b []byte) (pdu *ngapType.PDUSessionResourceModifyIndicationTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceModifyIndicationTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceModifyIndicationTransfer(pdu ngapType.PDUSessionResourceModifyIndicationTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceModifyRequestTransfer(b []byte) (pdu *ngapType.PDUSessionResourceModifyRequestTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceModifyRequestTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceModifyRequestTransfer(pdu ngapType.PDUSessionResourceModifyRequestTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceModifyResponseTransfer(b []byte) (pdu *ngapType.PDUSessionResourceModifyResponseTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceModifyResponseTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceModifyResponseTransfer(pdu ngapType.PDUSessionResourceModifyResponseTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceNotifyTransfer(b []byte) (pdu *ngapType.PDUSessionResourceNotifyTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceNotifyTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceNotifyTransfer(pdu ngapType.PDUSessionResourceNotifyTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceNotifyReleasedTransfer(b []byte) (pdu *ngapType.PDUSessionResourceNotifyReleasedTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceNotifyReleasedTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceNotifyReleasedTransfer(pdu ngapType.PDUSessionResourceNotifyReleasedTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePathSwitchRequestUnsuccessfulTransfer(b []byte) (pdu *ngapType.PathSwitchRequestUnsuccessfulTransfer, err error) {
	pdu = &ngapType.PathSwitchRequestUnsuccessfulTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePathSwitchRequestUnsuccessfulTransfer(pdu ngapType.PathSwitchRequestUnsuccessfulTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceReleaseResponseTransfer(b []byte) (pdu *ngapType.PDUSessionResourceReleaseResponseTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceReleaseResponseTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceReleaseResponseTransfer(pdu ngapType.PDUSessionResourceReleaseResponseTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodeUEContextResumeRequestTransfer(b []byte) (pdu *ngapType.UEContextResumeRequestTransfer, err error) {
	pdu = &ngapType.UEContextResumeRequestTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodeUEContextResumeRequestTransfer(pdu ngapType.UEContextResumeRequestTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodeUEContextResumeResponseTransfer(b []byte) (pdu *ngapType.UEContextResumeResponseTransfer, err error) {
	pdu = &ngapType.UEContextResumeResponseTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodeUEContextResumeResponseTransfer(pdu ngapType.UEContextResumeResponseTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodeSecondaryRATDataUsageReportTransfer(b []byte) (pdu *ngapType.SecondaryRATDataUsageReportTransfer, err error) {
	pdu = &ngapType.SecondaryRATDataUsageReportTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodeSecondaryRATDataUsageReportTransfer(pdu ngapType.SecondaryRATDataUsageReportTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceSetupRequestTransfer(b []byte) (pdu *ngapType.PDUSessionResourceSetupRequestTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceSetupRequestTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceSetupRequestTransfer(pdu ngapType.PDUSessionResourceSetupRequestTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceSetupResponseTransfer(b []byte) (pdu *ngapType.PDUSessionResourceSetupResponseTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceSetupResponseTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceSetupResponseTransfer(pdu ngapType.PDUSessionResourceSetupResponseTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodeUEContextSuspendRequestTransfer(b []byte) (pdu *ngapType.UEContextSuspendRequestTransfer, err error) {
	pdu = &ngapType.UEContextSuspendRequestTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodeUEContextSuspendRequestTransfer(pdu ngapType.UEContextSuspendRequestTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePathSwitchRequestAcknowledgeTransfer(b []byte) (pdu *ngapType.PathSwitchRequestAcknowledgeTransfer, err error) {
	pdu = &ngapType.PathSwitchRequestAcknowledgeTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePathSwitchRequestAcknowledgeTransfer(pdu ngapType.PathSwitchRequestAcknowledgeTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePathSwitchRequestTransfer(b []byte) (pdu *ngapType.PathSwitchRequestTransfer, err error) {
	pdu = &ngapType.PathSwitchRequestTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePathSwitchRequestTransfer(pdu ngapType.PathSwitchRequestTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodeHandoverPreparationUnsuccessfulTransfer(b []byte) (pdu *ngapType.HandoverPreparationUnsuccessfulTransfer, err error) {
	pdu = &ngapType.HandoverPreparationUnsuccessfulTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodeHandoverPreparationUnsuccessfulTransfer(pdu ngapType.HandoverPreparationUnsuccessfulTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

func DecodePDUSessionResourceReleaseCommandTransfer(b []byte) (pdu *ngapType.PDUSessionResourceReleaseCommandTransfer, err error) {
	pdu = &ngapType.PDUSessionResourceReleaseCommandTransfer{}
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt")
	return
}

func EncodePDUSessionResourceReleaseCommandTransfer(pdu ngapType.PDUSessionResourceReleaseCommandTransfer) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt")
}

package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"

	"ngap/ngapType"
)

func main() {
	dataC, _ := ioutil.ReadFile("../ngapAsn1/NGAP-PDU-Contents")
	datasC := strings.Split(string(dataC), "\n")
	dataD, _ := ioutil.ReadFile("../ngapAsn1/NGAP-PDU-Descriptions")
	datasD := strings.Split(string(dataD), "\n")

	str := "package ngapBuild\n\nimport (\n\t\"ngap/ngapType\"\n)\n"

	for i := 430; i < 2725; i++ {
		dataiC := strings.Fields(datasC[i])
		if len(dataiC) == 4 && dataiC[2] == "SEQUENCE" {
			s1, s2, s3 := IEsGetPDUPresent(dataiC[0], datasD)
			// InitiatingMessage SuccessfulOutcome UnsuccessfulOutcome
			if s1 == "SuccessfulOutcome" || s1 == "UnsuccessfulOutcome" {
				continue
			}
			s4 := PDUsGetABC0(dataiC[0])
			str += "\nfunc Build" + s4 + "() (pdu ngapType.NGAPPDU) {\n"
			str += "\tpdu.Present = ngapType.NGAPPDUPresent" + s1 + "\n" +
				"\tpdu." + s1 + " = new(ngapType." + s1 + ")\n" +
				"\tpduMsg := pdu." + s1 + "\n" +
				"\tpduMsg.ProcedureCode.Value = ngapType.ProcedureCode" + s2 + "\n" +
				"\tpduMsg.Criticality.Value = ngapType.CriticalityPresent" + s3 + "\n" +
				"\tpduMsg.Value.Present = ngapType." + s1 + "Present" + s4 + "\n" +
				"\tpduMsg.Value." + s4 + " = new(ngapType." + s4 + ")\n" +
				"\tpduContent := pduMsg.Value." + s4 + "\n" +
				"\tpduContentIEs := &pduContent.ProtocolIEs\n"

			for j := i + 6; j < i+100; j++ {
				datajC := strings.Fields(datasC[j])
				if len(datajC) != 10 {
					if datajC[0] == "..." {
						continue
					} else if datajC[0] == "}" {
						break
					} else {
						fmt.Println(j)
					}
				}

				str += "\n\tie" + strconv.Itoa(j-i-5) + " := ngapType." + s4 + "IEs{}\n" +
					"\tie" + strconv.Itoa(j-i-5) + ".Id.Value = ngapType.ProtocolIEID" + PDUsGetABC1(datajC[2]) + "\n" +
					"\tie" + strconv.Itoa(j-i-5) + ".Criticality.Value = ngapType.CriticalityPresent" + PDUsGetABC0(datajC[4]) + "\n" +
					"\tie" + strconv.Itoa(j-i-5) + ".Value.Present = ngapType." + s4 + "IEsPresent" + PDUsGetABC1(datajC[2]) + "\n" +
					"\tie" + strconv.Itoa(j-i-5) + ".Value." + PDUsGetABC1(datajC[2]) + " = new(ngapType." + PDUsGetABC0(datajC[6]) + ")\n" +
					"\t*ie" + strconv.Itoa(j-i-5) + ".Value." + PDUsGetABC1(datajC[2]) + " = Build" + PDUsGetABC0(datajC[6]) + "()\n" +
					"\n\tpduContentIEs.List = append(pduContentIEs.List, ie" + strconv.Itoa(j-i-5) + ")\n"
			}

			str += "\n\treturn\n}\n"
		}
	}

	filePointer, _ := os.Create("../ngapBuild/PDUDescriptions.go")
	_, _ = filePointer.WriteString(str)
}

func PDUsGetABC0(in string) (out string) {
	ins := strings.Split(in, "-")
	for i := 0; i < len(ins); i++ {
		out += strings.Title(ins[i])
	}
	return
}

func PDUsGetABC1(in string) (out string) {
	ins := strings.Split(in, "-")
	for i := 1; i < len(ins); i++ {
		out += strings.Title(ins[i])
	}
	return
}

func IEsGetPDUPresent(in string, datasD []string) (str1, str2, str3 string) {
	for i, di := range datasD {
		data := strings.Fields(di)
		if len(data) == 3 {
			if in == data[2] {
				if data[0] == "INITIATING" {
					str1 = "InitiatingMessage"
				} else if data[0] == "SUCCESSFUL" {
					str1 = "SuccessfulOutcome"
				} else if data[0] == "UNSUCCESSFUL" {
					str1 = "UnsuccessfulOutcome"
				}
				if strings.Fields(datasD[i+1])[0] == "PROCEDURE" {
					str2 = PDUsGetABC1(strings.Fields(datasD[i+1])[2])
					str3 = PDUsGetABC0(strings.Fields(datasD[i+2])[1])
				} else if strings.Fields(datasD[i+2])[0] == "PROCEDURE" {
					str2 = PDUsGetABC1(strings.Fields(datasD[i+2])[2])
					str3 = PDUsGetABC0(strings.Fields(datasD[i+3])[1])
				} else if strings.Fields(datasD[i+3])[0] == "PROCEDURE" {
					str2 = PDUsGetABC1(strings.Fields(datasD[i+3])[2])
					str3 = PDUsGetABC0(strings.Fields(datasD[i+4])[1])
				}
			}
		}
	}
	return
}

func BuildAMFConfigurationUpdate() (pdu ngapType.NGAPPDU) {
	pdu.Present = ngapType.NGAPPDUPresentInitiatingMessage
	pdu.InitiatingMessage = new(ngapType.InitiatingMessage)
	pduMsg := pdu.InitiatingMessage
	pduMsg.ProcedureCode.Value = ngapType.ProcedureCodeAMFConfigurationUpdate
	pduMsg.Criticality.Value = ngapType.CriticalityPresentReject
	pduMsg.Value.Present = ngapType.SuccessfulOutcomePresentAMFConfigurationUpdateAcknowledge
	pduMsg.Value.AMFConfigurationUpdate = new(ngapType.AMFConfigurationUpdate)
	pduContent := pduMsg.Value.AMFConfigurationUpdate
	pduContentIEs := &pduContent.ProtocolIEs

	// AMFUENGAPID
	ie := ngapType.AMFConfigurationUpdateIEs{}
	ie.Id.Value = ngapType.ProtocolIEIDAMFName
	ie.Criticality.Value = ngapType.CriticalityPresentReject
	ie.Value.Present = ngapType.AMFConfigurationUpdateIEsPresentAMFName
	ie.Value.AMFName = new(ngapType.AMFName)
	//*ie.Value.AMFName = BuildAMFName()

	pduContentIEs.List = append(pduContentIEs.List, ie)
	return
}

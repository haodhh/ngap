package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("..\\ngapAsn1\\NGAP-PDU-Descriptions")
	datas := strings.Split(string(data), "\n")
	str := "package ngapType\n" +
		"\nconst (\n" +
		"\tNGAPPDUPresentNothing int = iota /* No components present */\n\tNGAPPDUPresentInitiatingMessage\n\tNGAPPDU" +
		"PresentSuccessfulOutcome\n\tNGAPPDUPresentUnsuccessfulOutcome\n\t/* Extensions may appear below */\n)\n\ntyp" +
		"e NGAPPDU struct {\n\tPresent int\n\tInitiatingMessage *InitiatingMessage\n\tSuccessfulOutcome *SuccessfulOu" +
		"tcome\n\tUnsuccessfulOutcome *UnsuccessfulOutcome\n}\n\ntype InitiatingMessage struct {\n\tProcedureCode Pro" +
		"cedureCode\n\tCriticality Criticality\n\tValue InitiatingMessageValue `aper:\"openType,referenceFieldName:Pr" +
		"ocedureCode\"`\n}\n\ntype SuccessfulOutcome struct {\n\tProcedureCode ProcedureCode\n\tCriticality Criticali" +
		"ty\n\tValue SuccessfulOutcomeValue `aper:\"openType,referenceFieldName:ProcedureCode\"`\n}\n\ntype Unsuccess" +
		"fulOutcome struct {\n\tProcedureCode ProcedureCode\n\tCriticality Criticality\n\tValue UnsuccessfulOutcomeVa" +
		"lue `aper:\"openType,referenceFieldName:ProcedureCode\"`\n}\n"

	var class1, class2 []string
	var pdus [][]string
	for i := 261; i < 283; i++ {
		datai := strings.Fields(datas[i])
		class1 = append(class1, datai[0])
	}
	for i := 287; i < 332; i++ {
		datai := strings.Fields(datas[i])
		class2 = append(class2, datai[0])
	}
	for i := 336; i < 763; i++ {
		datai := strings.Fields(datas[i])
		pdus = append(pdus, datai)
	}

	proCode := DescriptionsGetProCode()

	stric := "\nconst (\n\tInitiatingMessagePresentNothing int = iota /* No components present */\n"
	stris := "\ntype InitiatingMessageValue struct {\n\tPresent int\n"
	strsc := "\nconst (\n\tSuccessfulOutcomePresentNothing int = iota /* No components present */\n"
	strss := "\ntype SuccessfulOutcomeValue struct {\n\tPresent int\n"
	struc := "\nconst (\n\tUnsuccessfulOutcomePresentNothing int = iota /* No components present */\n"
	strus := "\ntype UnsuccessfulOutcomeValue struct {\n\tPresent int\n"

	for _, c := range class1 {
		for i, _ := range pdus {
			if len(pdus[i]) > 0 && pdus[i][0] == c {
				if pdus[i+2][0] == "SUCCESSFUL" {
					if pdus[i+3][0] == "UNSUCCESSFUL" {
						stric += "\tInitiatingMessagePresent" + pdus[i+1][2] + "\n"
						strsc += "\tSuccessfulOutcomePresent" + pdus[i+2][2] + "\n"
						struc += "\tUnsuccessfulOutcomePresent" + pdus[i+3][2] + "\n"
						stris += "\t" + pdus[i+1][2] + " *" + pdus[i+1][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+4][2], proCode) + "\"`\n"
						strss += "\t" + pdus[i+2][2] + " *" + pdus[i+2][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+4][2], proCode) + "\"`\n"
						strus += "\t" + pdus[i+3][2] + " *" + pdus[i+3][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+4][2], proCode) + "\"`\n"
					} else {
						stric += "\tInitiatingMessagePresent" + pdus[i+1][2] + "\n"
						strsc += "\tSuccessfulOutcomePresent" + pdus[i+2][2] + "\n"
						stris += "\t" + pdus[i+1][2] + " *" + pdus[i+1][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+3][2], proCode) + "\"`\n"
						strss += "\t" + pdus[i+2][2] + " *" + pdus[i+2][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+3][2], proCode) + "\"`\n"
					}
				} else {
					stric += "\tInitiatingMessagePresent" + pdus[i+1][2] + "\n"
					stris += "\t" + pdus[i+1][2] + " *" + pdus[i+1][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+2][2], proCode) + "\"`\n"
				}
			}
		}
	}

	for _, c := range class2 {
		for i, _ := range pdus {
			if len(pdus[i]) > 0 && pdus[i][0] == c {
				stric += "\tInitiatingMessagePresent" + pdus[i+1][2] + "\n"
				stris += "\t" + pdus[i+1][2] + " *" + pdus[i+1][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+2][2], proCode) + "\"`\n"
			}
		}
	}

	stric += ")\n"
	stris += "}\n"
	strsc += ")\n"
	strss += "}\n"
	struc += ")\n"
	strus += "}\n"

	str += stric + stris + strsc + strss + struc + strus
	filePointer, _ := os.Create("../ngapType/PDUDescriptions.go")
	_, _ = filePointer.WriteString(str)
}

func GetDescriptionsABC0(in string) (out string) {
	ins := strings.Split(in, "-")
	for i := 0; i < len(ins); i++ {
		out += strings.Title(ins[i])
	}
	return
}

func GetDescriptionsABC1(in string) (out string) {
	ins := strings.Split(in, "-")
	for i := 1; i < len(ins); i++ {
		out += strings.Title(ins[i])
	}
	return
}

func DescriptionsGetProCode() (out []string) {
	data, _ := ioutil.ReadFile("../ngapAsn1/NGAP-Constants")
	datas := strings.Split(string(data), "\n")
	for i := 33; i < 99; i++ {
		datai := strings.Fields(datas[i])
		out = append(out, datai[0])
	}
	return
}

func DescriptionsGetProCodeID(in string, proCode []string) (out string) {
	for i, p := range proCode {
		if in == p {
			return fmt.Sprintf("%d", i)
		}
	}
	return "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}

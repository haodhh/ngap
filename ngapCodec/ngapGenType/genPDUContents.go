package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	listProIE := ContentsGetListProIE()

	data, _ := ioutil.ReadFile("../ngapAsn1/NGAP-PDU-Contents")
	//fmt.Print(data)
	datas := strings.Split(string(data), "\n")
	str := "package ngapType\n"
	for i := 430; i < 2725; i++ {
		datai := strings.Fields(datas[i])
		if len(datai) == 4 && datai[2] == "SEQUENCE" {
			datai1 := strings.Fields(datas[i+1])
			if datai1[0] == "privateIEs" {
				str += "\ntype " + datai[0] + " struct {\n" +
					"\tPrivateIEs PrivateIEContainer" + datai[0] + "IEs\n}\n"
				str += "\ntype PrivateIEContainer" + datai[0] + "IEs struct {\n" +
					"\tList []" + datai[0] + "IEs `aper:\"sizeLB:0,sizeUB:65535\"`\n}\n"
				str += "\ntype " + datai[0] + "IEs struct {\n" +
					"\tId PrivateIEID\n" +
					"\tCriticality Criticality\n" +
					"\tValue " + datai[0] + "IEsValue `aper:\"openType,referenceFieldName:Id\"`\n" +
					"}\n"
				str += "\nconst (\n" +
					"\t" + datai[0] + "IEsPresentNothing int = iota /* No components present */\n" +
					")\n"
				str += "\ntype " + datai[0] + "IEsValue struct {\n" +
					"\tPresent int\n" +
					"}\n"
			} else {
				str += "\ntype " + datai[0] + " struct {\n" +
					"\tProtocolIEs ProtocolIEContainer" + datai[0] + "IEs\n}\n"
				str += "\ntype ProtocolIEContainer" + datai[0] + "IEs struct {\n" +
					"\tList []" + datai[0] + "IEs `aper:\"sizeLB:0,sizeUB:65535\"`\n}\n"
				str += "\ntype " + datai[0] + "IEs struct {\n" +
					"\tId ProtocolIEID\n" +
					"\tCriticality Criticality\n" +
					"\tValue " + datai[0] + "IEsValue `aper:\"openType,referenceFieldName:Id\"`\n" +
					"}\n"
				strConst := ""
				x := 6
				for {
					datax := strings.Fields(datas[i+x])
					if datax[0] == "..." {
						break
					} else {
						fmt.Println(datax[2])
						strConst += "\t" + datai[0] + "IEsPresent" + ContentsGetABC1(datax[2]) + "\n"
						x++
					}
				}
				strStruct := ""
				x = 6
				for {
					datax := strings.Fields(datas[i+x])
					if datax[0] == "..." {
						break
					} else {
						if datax[6] == "OCTET" {
							datax[6] = "OctetString"
						}
						strStruct += "\t" + ContentsGetABC1(datax[2]) + " *" + ContentsGetABC0(datax[6]) + " " +
							"`aper:\"referenceFieldValue:" + ContentGetProIEID(datax[2], listProIE) + "\"`\n"
						x++
					}
				}
				str += "\nconst (\n" +
					"\t" + datai[0] + "IEsPresentNothing int = iota /* No components present */\n" +
					strConst +
					")\n"
				str += "\ntype " + datai[0] + "IEsValue struct {\n" +
					"\tPresent int\n" +
					strStruct +
					"}\n"
			}
		}
	}

	filePointer, _ := os.Create("C:\\Users\\HD\\GitLab\\haodhh\\ngap\\ngapType\\PDUContents.go")
	_, _ = filePointer.WriteString(str)
}

func ContentsGetABC0(in string) (out string) {
	ins := strings.Split(in, "-")
	for i := 0; i < len(ins); i++ {
		out += strings.Title(ins[i])
	}
	return
}

func ContentsGetABC1(in string) (out string) {
	ins := strings.Split(in, "-")
	for i := 1; i < len(ins); i++ {
		out += strings.Title(ins[i])
	}
	return
}

func ContentsGetListProIE() (out []string) {
	data, _ := ioutil.ReadFile("../ngapAsn1/NGAP-Constants")
	datas := strings.Split(string(data), "\n")
	for i := 181; i < 455; i++ {
		datai := strings.Fields(datas[i])
		out = append(out, datai[0])
	}
	return
}

func ContentGetProIEID(in string, list []string) (out string) {
	for i, l := range list {
		if in == l {
			return strconv.Itoa(i)
		}
	}
	return "abcdefghijklmnopq"
}

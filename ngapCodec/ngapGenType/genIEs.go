package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func main() {
	listMaxNoOf := IEsGetListMaxNoOf()
	data, _ := ioutil.ReadFile("../ngapAsn1/NGAP-IEs")
	datas := strings.Split(string(data), "\n")
	str := "package ngapType\n"
	strIEsPIESC1 := "package ngapType\n"
	strIEsPIESC2 := "package ngapType\n"
	strIEsCS := "package ngapType\n"
	for i := 175; i < len(datas); i++ {
		datai := strings.Fields(datas[i])
		if len(datai) >= 3 && datai[1] == "::=" && datai[2] == "INTEGER" {
			ext, min, max := IEsGetMinMaxInteger(datai)
			str += "\ntype " + IEsGetABC0(datai[0]) + " struct {\n" +
				"\tValue int64 `aper:\"" + ext + "valueLB:" + min + ",valueUB:" + max + "\"`" +
				"\n}\n"
		} else if len(datai) >= 3 && datai[1] == "::=" && datai[2] == "ENUMERATED" {
			str += IEsGetEnumerated(i, datai, datas)
		} else if len(datai) >= 3 && datai[1] == "::=" && datai[2] == "VisibleString" {
			str += "\ntype " + IEsGetABC0(datai[0]) + " struct {\n" +
				"\tValue string" +
				"\n}\n"
		} else if len(datai) >= 3 && datai[1] == "::=" && datai[2] == "PrintableString" {
			str += "\ntype " + IEsGetABC0(datai[0]) + " struct {\n" +
				"\tValue string `aper:\"sizeExt,sizeLB:1,sizeUB:150\"`" +
				"\n}\n"
		} else if len(datai) >= 3 && datai[1] == "::=" && datai[2] == "UTF8String" {
			str += "\ntype " + IEsGetABC0(datai[0]) + " struct {\n" +
				"\tValue string `aper:\"sizeExt,sizeLB:1,sizeUB:150\"`" +
				"\n}\n"
		} else if len(datai) >= 3 && datai[1] == "::=" && datai[2] == "OCTET" && datai[3] == "STRING" {
			tag := IEsGetTagOctetString(datai)
			str += "\ntype " + IEsGetABC0(datai[0]) + " struct {\n" +
				"\tValue OctetString" + tag +
				"\n}\n"
		} else if len(datai) >= 3 && datai[1] == "::=" && datai[2] == "BIT" && datai[3] == "STRING" {
			tag := IEsGetTagBitString(datai)
			str += "\ntype " + IEsGetABC0(datai[0]) + " struct {\n" +
				"\tValue BitString" + tag +
				"\n}\n"
		} else if len(datai) >= 3 && datai[2] == "::=" && datai[1] == "NGAP-PROTOCOL-IES" {
			strIEsPIESC1 += IEsGetProtocolIESingleContainer(i, datai, datas)
		} else if len(datai) >= 3 && datai[2] == "::=" && datai[1] == "NGAP-PROTOCOL-EXTENSION" {
			strIEsPIESC2 += IEsGetProtocolExtensionContainer(i, datai, datas)
		} else if len(datai) >= 3 && datai[1] == "::=" && datai[2] == "CHOICE" {
			a, b := IEsGetChoice(i, datai, datas)
			str += a
			strIEsCS += b
		} else if len(datai) >= 3 && datai[1] == "::=" && datai[2] == "SEQUENCE" {
			a, b := IEsGetSequence(i, datai, datas, listMaxNoOf)
			str += a
			strIEsCS += b
		} else if len(datai) >= 3 && datai[1] == "::=" {
			str += "\ntype " + IEsGetABC0(datai[0]) + " struct {\n" +
				"\n}\n"
			fmt.Println(datai)
		}
	}

	filePointer, _ := os.Create("../ngapType/IEs.go")
	_, _ = filePointer.WriteString(str)
	filePointer, _ = os.Create("../ngapType/IEsPIESC1.go")
	_, _ = filePointer.WriteString(strIEsPIESC1)
	filePointer, _ = os.Create("../ngapType/IEsPIESC2.go")
	_, _ = filePointer.WriteString(strIEsPIESC2)
	filePointer, _ = os.Create("../ngapType/IEsCS.go")
	_, _ = filePointer.WriteString(strIEsCS)
}

func IEsGetABC0(in string) (out string) {
	ins := strings.Split(in, "-")
	for i := 0; i < len(ins); i++ {
		out += strings.Title(ins[i])
	}
	return
}

func IEsGetABC1(in string) (out string) {
	ins := strings.Split(in, "-")
	for i := 1; i < len(ins); i++ {
		out += strings.Title(ins[i])
	}
	return
}

func IEsGetListProIE() (out []string) {
	data, _ := ioutil.ReadFile("../ngapAsn1/NGAP-Constants")
	datas := strings.Split(string(data), "\n")
	for i := 181; i < 455; i++ {
		datai := strings.Fields(datas[i])
		out = append(out, datai[0])
	}
	return
}

func IEsGetProIEID(in string, list []string) (out string) {
	for i, l := range list {
		if in == l {
			return strconv.Itoa(i)
		}
	}
	return "abcdefghijklmnopqabcdefghijklmnopqabcdefghijklmnopqabcdefghijklmnopqabcdefghijklmnopqabcdefghijklmnopq"
}

func IEsGetMinMaxInteger(in []string) (ext string, out1 string, out2 string) {
	if len(in) == 4 {
		ext = ""
		out1 = strings.Split(strings.Split(in[3], "..")[0], "(")[1]
		out2 = strings.Split(strings.Split(in[3], "..")[1], ")")[0]
		if out2 == "maxNRARFCN" {
			out2 = "3279165"
		}
	} else if len(in) == 5 {
		ext = "valueExt,"
		temp := strings.Split(in[3], "|")
		if len(temp) > 1 {
			out1 = strings.Split(strings.Split(in[3], "..")[0], "(")[1]
			out2 = strings.Split(temp[len(temp)-1], ",")[0]
		} else {
			out1 = strings.Split(strings.Split(in[3], "..")[0], "(")[1]
			out2 = strings.Split(strings.Split(in[3], "..")[1], ",")[0]
		}
	} else if len(in) == 6 {
		ext = "valueExt,"
		out1 = strings.Split(strings.Split(in[3], "..")[0], "(")[1]
		out2 = strings.Split(strings.Split(in[5], "..")[1], ")")[0]
	} else {
		fmt.Println(in)
	}
	return
}

func IEsGetEnumerated(i int, datai []string, datas []string) (enum string) {
	if len(datai) == 4 {
		enum = "\nconst (\n"
		ext := ""
		x := 0
		for j := i + 1; j < (i + 100); j++ {
			if strings.Fields(datas[j])[0] == "..." {
				ext = "valueExt,"
				break
			} else if strings.Fields(datas[j])[0] == "}" {
				break
			} else {
				x = j - i - 1
				enum += "\t" + IEsGetABC0(datai[0]) + "Present" + IEsGetABC0(strings.Split(strings.Fields(datas[j])[0], ",")[0]) + " Enumerated = " + strconv.Itoa(x) + "\n"
			}
		}
		enum += ")\n\ntype " + IEsGetABC0(datai[0]) + " struct {\n" +
			"\tValue Enumerated `aper:\"" + ext + "valueLB:0,valueUB:" + strconv.Itoa(x) + "\"`\n" +
			"}\n"
	} else {
		fmt.Println(datai)
	}
	return
}

func IEsGetTagOctetString(in []string) (aper string) {
	if len(in) == 4 {
		return
	} else if len(in) == 5 {
		abc := strings.Split(in[4], "..")
		if len(abc) > 1 {
			aper = " `aper:\"sizeLB:" + strings.Split(abc[0], "(")[2] + ",sizeUB:" + strings.Split(abc[1], ")")[0] + "\"`"
		} else {
			aper = " `aper:\"sizeLB:" + strings.Split(strings.Split(in[4], "(")[2], ")")[0] + ",sizeUB:" + strings.Split(strings.Split(in[4], "(")[2], ")")[0] + "\"`"
		}
	} else {
		fmt.Println(in)
	}
	return
}

func IEsGetTagBitString(in []string) (aper string) {
	if len(in) == 4 {
		return
	} else if len(in) == 5 {
		abc := strings.Split(in[4], "..")
		if len(abc) > 1 {
			aper = " `aper:\"sizeLB:" + strings.Split(abc[0], "(")[2] + ",sizeUB:" + strings.Split(abc[1], ")")[0] + "\"`"
		} else {
			aper = " `aper:\"sizeLB:" + strings.Split(strings.Split(in[4], "(")[2], ")")[0] + ",sizeUB:" + strings.Split(strings.Split(in[4], "(")[2], ")")[0] + "\"`"
		}
	} else if len(in) == 6 {
		abc := strings.Split(in[4], "..")
		if len(abc) > 1 {
			aper = " `aper:\"sizeExt,sizeLB:" + strings.Split(abc[0], "(")[2] + ",sizeUB:" + strings.Split(abc[1], ",")[0] + "\"`"
		} else {
			aper = " `aper:\"sizeExt,sizeLB:" + strings.Split(strings.Split(in[4], "(")[2], ",")[0] + ",sizeUB:" + strings.Split(strings.Split(in[4], "(")[2], ",")[0] + "\"`"
		}
	} else {
		fmt.Println(in)
	}
	return
}

func IEsGetChoice(i int, datai []string, datas []string) (enum, choice string) {
	choice = "\nconst (\n\t" + IEsGetABC0(datai[0]) + "PresentNothing int = iota /* No components present */\n"
	for j := i + 1; j < (i + 100); j++ {
		if strings.Fields(datas[j])[0] == "}" {
			break
		} else if strings.Fields(datas[j])[0] == "..." {
			continue
		} else {
			choice += "\t" + IEsGetABC0(datai[0]) + "Present" + IEsGetABC0(strings.Fields(datas[j])[0]) + "\n"
		}
	}
	choice += ")\n\ntype " + IEsGetABC0(datai[0]) + " struct {\n" +
		"\tPresent int\n"
	for j := i + 1; j < (i + 100); j++ {
		if strings.Fields(datas[j])[0] == "}" {
			break
		} else if strings.Fields(datas[j])[0] == "..." {
			continue
		} else if strings.Fields(datas[j])[0] == "choice-Extensions" {
			choice += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *ProtocolIESingleContainer" + IEsGetABC0(strings.Split(strings.Split(strings.Fields(datas[j])[3], "{")[1], "}")[0]) + "\n"
		} else if strings.Fields(datas[j])[1] == "NULL," {
			choice += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *Null\n"
		} else if strings.Fields(datas[j])[1] == "ENUMERATED" {
			choice += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *" + IEsGetABC0(strings.Fields(datas[j])[0]) + "\n"
			enum += "\nconst (\n\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + "PresentTrue Enumerated = 0\n)\n" +
				"\ntype " + IEsGetABC0(strings.Fields(datas[j])[0]) + " struct {\n\tValue Enumerated `aper:\"valueLB:0,valueUB:0\"`\n}\n"
		} else if strings.Fields(datas[j])[1] == "BIT" && strings.Fields(datas[j])[2] == "STRING" {
			if len(strings.Fields(datas[j])) == 4 {
				choice += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *BitString `aper:\"sizeLB:" + strings.Split(strings.Split(strings.Fields(datas[j])[3], "(")[2], ")")[0] + ",sizeUB:" + strings.Split(strings.Split(strings.Fields(datas[j])[3], "(")[2], ")")[0] + "\"`\n"
			} else if len(strings.Fields(datas[j])) == 5 {
				choice += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *BitString \n"
			} else {
				fmt.Println(datas[j])
			}
		} else {
			choice += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *" + IEsGetABC0(strings.Split(strings.Fields(datas[j])[1], ",")[0]) + "\n"
		}
	}
	choice += "}\n"
	return
}

func IEsGetSequence(i int, datai []string, datas []string, listMaxNoOf [][]string) (enum, sequence string) {
	if len(datai) == 6 {
		sequence = "\ntype " + IEsGetABC0(datai[0]) + " struct {\n" +
			"\tList []" + IEsGetABC0(datai[5]) + " `aper:\"valueExt,sizeLB:1,sizeUB:" + IEsGetMaxNoOf(strings.Split(strings.Split(datai[3], "..")[1], ")")[0], listMaxNoOf) + "\"`\n" +
			"}\n"
	} else if len(datai) == 4 {
		sequence += "\ntype " + IEsGetABC0(datai[0]) + " struct {\n"
		for j := i + 1; j < (i + 100); j++ {
			if strings.Fields(datas[j])[0] == "}" {
				break
			} else if strings.Fields(datas[j])[0] == "..." || strings.Fields(datas[j])[0] == "--" {
				continue
			} else if strings.Fields(datas[j])[0] == "iE-Extensions" {
				sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *" + IEsGetABC0(strings.Fields(datas[j])[1]) + IEsGetABC0(strings.Split(strings.Split(strings.Fields(datas[j])[3], "{")[1], "}")[0]) + "\n"
			} else if strings.Fields(datas[j])[0] == "protocolIEs" {
				sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *" + IEsGetABC0(strings.Fields(datas[j])[1]) + IEsGetABC0(strings.Split(strings.Split(strings.Fields(datas[j])[3], "{")[1], "}")[0]) + "\n"
			} else if len(strings.Fields(datas[j])) == 2 {
				sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " " + IEsGetABC0(strings.Split(strings.Fields(datas[j])[1], ",")[0]) + "\n"
			} else if len(strings.Fields(datas[j])) == 3 && strings.Fields(datas[j])[2] == "OPTIONAL," {
				sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *" + IEsGetABC0(strings.Fields(datas[j])[1]) + "\n"
			} else if strings.Fields(datas[j])[1] == "OCTET" && len(strings.Fields(datas[j])) == 5 {
				sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " OctetString\n"
			} else if strings.Fields(datas[j])[1] == "OCTET" && len(strings.Fields(datas[j])) == 4 {
				sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " OctetString `aper:\"sizeLB:4,sizeUB:4\"`\n"
			} else if strings.Fields(datas[j])[1] == "ENUMERATED" {
				if strings.Fields(datas[j])[len(strings.Fields(datas[j]))-1] != "OPTIONAL," {
					sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " " + IEsGetABC0(strings.Fields(datas[j])[0]) + "\n"
				} else {
					sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *" + IEsGetABC0(strings.Fields(datas[j])[0]) + "\n"
				}
				dataenum := strings.Fields(datas[j])
				enum += "\nconst (\n"
				x := 3
				ext := ""
				max := -1
				for {
					if dataenum[x] == "}," {
						break
					} else if dataenum[x] == "..." || dataenum[x] == "...}," {
						ext = "valueExt,"
						break
					} else {
						max++
						enum += "\t" + IEsGetABC0(dataenum[0]) + "Present" + IEsGetABC0(strings.Split(dataenum[x], ",")[0]) + " Enumerated = " + strconv.Itoa(max) + "\n"
					}
					x++
				}
				enum += ")\n\ntype " + IEsGetABC0(dataenum[0]) + " struct {\n\tValue Enumerated `aper:\"" + ext + ",valueLB:0,valueUB:" + strconv.Itoa(max) + "\"`\n}\n"
			} else if strings.Fields(datas[j])[1] == "INTEGER" {
				if len(strings.Fields(datas[j])) == 3 {
					min, max := strings.Split(strings.Split(strings.Fields(datas[j])[2], "..")[0], "(")[1], strings.Split(strings.Split(strings.Fields(datas[j])[2], "..")[1], ")")[0]
					sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " int64 `aper:\"valueLB:" + min + ",valueUB:" + max + "\"`\n"
				} else if len(strings.Fields(datas[j])) == 4 && strings.Fields(datas[j])[3] == "OPTIONAL," {
					min, max := strings.Split(strings.Split(strings.Fields(datas[j])[2], "..")[0], "(")[1], strings.Split(strings.Split(strings.Fields(datas[j])[2], "..")[1], ")")[0]
					sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *int64 `aper:\"valueLB:" + min + ",valueUB:" + max + "\"`\n"
				} else if len(strings.Fields(datas[j])) == 4 && strings.Fields(datas[j])[3] != "OPTIONAL," {
					min, max := strings.Split(strings.Split(strings.Fields(datas[j])[2], "..")[0], "(")[1], strings.Split(strings.Split(strings.Fields(datas[j])[2], "..")[1], ",")[0]
					sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " int64 `aper:\"valueExt,valueLB:" + min + ",valueUB:" + max + "\"`\n"
				} else if len(strings.Fields(datas[j])) == 5 && strings.Fields(datas[j])[4] == "OPTIONAL," {
					min, max := strings.Split(strings.Split(strings.Fields(datas[j])[2], "..")[0], "(")[1], strings.Split(strings.Split(strings.Fields(datas[j])[2], "..")[1], ",")[0]
					sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *int64 `aper:\"valueExt,valueLB:" + min + ",valueUB:" + max + "\"`\n"
				} else {
					fmt.Println(datas[j])
				}
			} else if strings.Fields(datas[j])[1] == "BIT" || strings.Fields(datas[j])[2] == "STRING" {
				if len(strings.Fields(datas[j])) == 4 {
					if len(strings.Split(strings.Fields(datas[j])[3], "..")) > 1 {
						min, max := strings.Split(strings.Split(strings.Fields(datas[j])[3], "..")[0], "(")[2], strings.Split(strings.Split(strings.Fields(datas[j])[3], "..")[1], ")")[0]
						sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " BitString `aper:\"sizeLB:" + min + ",sizeUB:" + max + "\"`\n"
					} else {
						min, max := strings.Split(strings.Split(strings.Fields(datas[j])[3], ")")[0], "(")[2], strings.Split(strings.Split(strings.Fields(datas[j])[3], ")")[0], "(")[2]
						sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " BitString `aper:\"sizeLB:" + min + ",sizeUB:" + max + "\"`\n"
					}
				} else if len(strings.Fields(datas[j])) == 5 && strings.Fields(datas[j])[4] == "OPTIONAL," {
					if len(strings.Split(strings.Fields(datas[j])[3], "..")) > 1 {
						min, max := strings.Split(strings.Split(strings.Fields(datas[j])[3], "..")[0], "(")[2], strings.Split(strings.Split(strings.Fields(datas[j])[3], "..")[1], ")")[0]
						sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *BitString `aper:\"sizeLB:" + min + ",sizeUB:" + max + "\"`\n"
					} else {
						min, max := strings.Split(strings.Split(strings.Fields(datas[j])[3], ")")[0], "(")[2], strings.Split(strings.Split(strings.Fields(datas[j])[3], ")")[0], "(")[2]
						sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *BitString `aper:\"sizeLB:" + min + ",sizeUB:" + max + "\"`\n"
					}
				} else if len(strings.Fields(datas[j])) == 5 && strings.Fields(datas[j])[4] != "OPTIONAL," {
					if len(strings.Split(strings.Fields(datas[j])[3], "..")) > 1 {
						min, max := strings.Split(strings.Split(strings.Fields(datas[j])[3], "..")[0], "(")[1], strings.Split(strings.Split(strings.Fields(datas[j])[3], "..")[1], ",")[0]
						sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " BitString `aper:\"sizeExt,sizeLB:" + min + ",sizeUB:" + max + "\"`\n"
					} else {
						min, max := strings.Split(strings.Split(strings.Fields(datas[j])[3], ",")[0], "(")[2], strings.Split(strings.Split(strings.Fields(datas[j])[3], ",")[0], "(")[2]
						sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " BitString `aper:\"sizeExt,sizeLB:" + min + ",sizeUB:" + max + "\"`\n"
					}
				} else if len(strings.Fields(datas[j])) == 6 && strings.Fields(datas[j])[5] == "OPTIONAL," {
					min, max := strings.Split(strings.Split(strings.Fields(datas[j])[3], "..")[0], "(")[1], strings.Split(strings.Split(strings.Fields(datas[j])[3], "..")[1], ",")[0]
					sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " *BitString `aper:\"sizeExt,sizeLB:" + min + ",sizeUB:" + max + "\"`\n"
				} else {
					fmt.Println(datas[j])
				}
			} else {
				sequence += "\t" + IEsGetABC0(strings.Fields(datas[j])[0]) + " " + IEsGetABC0(strings.Fields(datas[j])[1]) + "\n"
			}
		}
		sequence += "}\n"
	} else {
		fmt.Println(datai)
	}
	return
}

func IEsGetMaxNoOf(in string, list [][]string) (out string) {
	for _, l := range list {
		if in == l[0] {
			return l[3]
		}
	}
	fmt.Println(in, " ==> asddaisidajdjasadafasfasfafafasfasfdjiasji")
	return "asddaisidajdjasadafasfasfafafasfasfdjiasji"
}

func IEsGetListMaxNoOf() (out [][]string) {
	data, _ := ioutil.ReadFile("C:\\Users\\HD\\GitLab\\haodhh\\ngap\\ngapAsn1\\NGAP-Constants")
	datas := strings.Split(string(data), "\n")
	for i := 116; i < 174; i++ {
		datai := strings.Fields(datas[i])
		out = append(out, datai)
	}
	return
}

func IEsGetProtocolExtensionContainer(i0 int, datai0 []string, datas []string) (ex string) {
	ex += "\ntype ProtocolExtensionContainer" + IEsGetABC0(datai0[0]) + " struct {\n" +
		"\tList []" + IEsGetABC0(datai0[0]) + " `aper:\"sizeLB:1,sizeUB:65535\"`\n" +
		"}\n"

	ex += "\ntype " + IEsGetABC0(datai0[0]) + " struct {\n" +
		"\tId             ProtocolExtensionID\n" +
		"\tCriticality    Criticality\n" +
		"\tExtensionValue " + IEsGetABC0(datai0[0]) + "ExtensionValue `aper:\"openType,referenceFieldName:Id\"`\n" +
		"}\n"

	ex1, ex2 := "", ""
	for i := i0 + 1; i < i0+100; i++ {
		datai := strings.Fields(datas[i])
		if datai[0] == "..." {
			continue
		} else if datai[0] == "}" {
			break
		} else {
			ex1 += "\t" + IEsGetABC0(datai0[0]) + "Present" + IEsGetABC1(datai[2]) + "\n"
			if datai[6] == "OCTET" {
				ex2 += "\t" + IEsGetABC1(datai[2]) + " *OctetString `aper:\"referenceFieldValue:" + IEsGetABC1(datai[2]) + "\"`\n"
			} else {
				ex2 += "\t" + IEsGetABC1(datai[2]) + " *" + IEsGetABC0(datai[6]) + " `aper:\"referenceFieldValue:" + IEsGetABC1(datai[2]) + "\"`\n"
			}
		}
	}

	ex += "\nconst (\n" +
		"\t" + IEsGetABC0(datai0[0]) + "PresentNothing int = iota /* No components present */\n" +
		ex1 +
		")\n" +
		"\ntype " + IEsGetABC0(datai0[0]) + "ExtensionValue struct {\n" +
		"\tPresent int\n" +
		ex2 +
		"}\n"

	return ex
}

func IEsGetProtocolIESingleContainer(i0 int, datai0 []string, datas []string) (ex string) {
	listProIE := IEsGetListProIE()

	ex += "\ntype ProtocolIESingleContainer" + IEsGetABC0(datai0[0]) + " struct {\n" +
		"\tList []" + IEsGetABC0(datai0[0]) + " `aper:\"sizeLB:1,sizeUB:65535\"`\n" +
		"}\n"

	ex += "\ntype " + IEsGetABC0(datai0[0]) + " struct {\n" +
		"\tId             ProtocolIEID\n" +
		"\tCriticality    Criticality\n" +
		"\tIESingleValue " + IEsGetABC0(datai0[0]) + "IESingleValue `aper:\"openType,referenceFieldName:Id\"`\n" +
		"}\n"

	ex1, ex2 := "", ""
	for i := i0 + 1; i < i0+100; i++ {
		datai := strings.Fields(datas[i])
		if datai[0] == "..." {
			continue
		} else if datai[0] == "}" {
			break
		} else {
			ex1 += "\t" + IEsGetABC0(datai0[0]) + "Present" + IEsGetABC1(datai[2]) + "\n"
			if datai[6] == "OCTET" {
				ex2 += "\t" + IEsGetABC1(datai[2]) + " *OctetString `aper:\"referenceFieldValue:" + IEsGetProIEID(datai[2], listProIE) + "\"`\n"
			} else {
				ex2 += "\t" + IEsGetABC1(datai[2]) + " *" + IEsGetABC0(datai[6]) + " `aper:\"referenceFieldValue:" + IEsGetProIEID(datai[2], listProIE) + "\"`\n"
			}
		}
	}

	ex += "\nconst (\n" +
		"\t" + IEsGetABC0(datai0[0]) + "PresentNothing int = iota /* No components present */\n" +
		ex1 +
		")\n" +
		"\ntype " + IEsGetABC0(datai0[0]) + "IESingleValue struct {\n" +
		"\tPresent int\n" +
		ex2 +
		"}\n"

	return ex
}

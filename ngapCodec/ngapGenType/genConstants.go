package main

import (
	"io/ioutil"
	"os"
	"strings"
)

func main() {
	data, _ := ioutil.ReadFile("../ngapAsn1/NGAP-Constants")
	datas := strings.Split(string(data), "\n")
	str := "package ngapType\n"
	str += "\nconst (\n"
	for i := 33; i < 99; i++ {
		datai := strings.Fields(datas[i])
		if len(datai) == 4 {
			str += "\tProcedureCode" + ConstantsGetABC1(datai[0]) + " int64 = " + datai[3] + "\n"
		}
	}
	str += ")\n"
	str += "\nconst (\n"
	for i := 106; i < 109; i++ {
		datai := strings.Fields(datas[i])
		if len(datai) == 4 {
			str += "\t" + ConstantsGetABC0(datai[0]) + " int64 = " + datai[3] + "\n"
		}
	}
	str += ")\n"
	str += "\nconst (\n"
	for i := 116; i < 174; i++ {
		datai := strings.Fields(datas[i])
		if len(datai) == 4 {
			str += "\t" + ConstantsGetABC0(datai[0]) + " int64 = " + datai[3] + "\n"
		}
	}
	str += ")\n"
	str += "\nconst (\n"
	for i := 181; i < 455; i++ {
		datai := strings.Fields(datas[i])
		if len(datai) == 4 {
			str += "\tProtocolIEID" + ConstantsGetABC1(datai[0]) + " int64 = " + datai[3] + "\n"
		}
	}
	str += ")\n"

	filePointer, _ := os.Create("../ngapType/Constants.go")
	_, _ = filePointer.WriteString(str)
}

func ConstantsGetABC0(in string) (out string) {
	ins := strings.Split(in, "-")
	for i:=0; i<len(ins); i++ {
		out += strings.Title(ins[i])
	}
	return
}

func ConstantsGetABC1(in string) (out string) {
	ins := strings.Split(in, "-")
	for i:=1; i<len(ins); i++ {
		out += strings.Title(ins[i])
	}
	return
}

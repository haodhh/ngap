package ngapType

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"
)

func NGAPPDUDescriptions() {
	str := "package ngapType\n\nconst (\n\tNGAPPDUPresentNothing int = iota /* No components present */\n\tNGAPPDUPresentInitiatingMessage\n\tNGAPPDUPresentSuccessfulOutcome\n\tNGAPPDUPresentUnsuccessfulOutcome\n\t/* Extensions may appear below */\n)\n\ntype NGAPPDU struct {\n\tPresent int\n\tInitiatingMessage *InitiatingMessage\n\tSuccessfulOutcome *SuccessfulOutcome\n\tUnsuccessfulOutcome *UnsuccessfulOutcome\n}\n\ntype InitiatingMessage struct {\n\tProcedureCode ProcedureCode\n\tCriticality Criticality\n\tValue InitiatingMessageValue `vht5gc:\"openType,referenceFieldName:ProcedureCode\"`\n}\n\ntype SuccessfulOutcome struct {\n\tProcedureCode ProcedureCode\n\tCriticality Criticality\n\tValue SuccessfulOutcomeValue `vht5gc:\"openType,referenceFieldName:ProcedureCode\"`\n}\n\ntype UnsuccessfulOutcome struct {\n\tProcedureCode ProcedureCode\n\tCriticality Criticality\n\tValue UnsuccessfulOutcomeValue `vht5gc:\"openType,referenceFieldName:ProcedureCode\"`\n}\n"

	data, _ := ioutil.ReadFile("C:\\Users\\HD\\GoPath\\src\\ngap\\ngapAsn1\\NGAP-PDU-Descriptions")
	datas := strings.Split(string(data), "\n")

	var class1, class2 []string
	var pdus [][]string

	for n, ds := range datas {
		d := strings.Fields(ds)
		if len(d) == 4 && d[0] == "NGAP-ELEMENTARY-PROCEDURES-CLASS-1" {
			for i:=n+1; i<len(datas); i++ {
				d := strings.Fields(datas[i])
				if len(d) == 1 && d[0] == "..." {
					break
				} else {
					class1 = append(class1, d[0])
				}
			}
		}
		if len(d) == 4 && d[0] == "NGAP-ELEMENTARY-PROCEDURES-CLASS-2" {
			for i:=n+1; i<len(datas); i++ {
				d := strings.Fields(datas[i])
				if len(d) == 1 && d[0] == "..." {
					break
				} else {
					class2 = append(class2, d[0])
				}
			}
		}
		pdus = append(pdus, d)
	}

	stric := "\nconst (\n\tInitiatingMessagePresentNothing int = iota /* No components present */\n"
	stris := "\ntype InitiatingMessageValue struct {\n\tPresent int\n"
	strsc := "\nconst (\n\tSuccessfulOutcomePresentNothing int = iota /* No components present */\n"
	strss := "\ntype SuccessfulOutcomeValue struct {\n\tPresent int\n"
	struc := "\nconst (\n\tUnsuccessfulOutcomePresentNothing int = iota /* No components present */\n"
	strus := "\ntype UnsuccessfulOutcomeValue struct {\n\tPresent int\n"

	proCode := DescriptionsGetProCode()

	for _, c := range class1 {
		for i, _ := range pdus {
			if len(pdus[i]) > 0 && pdus[i][0] == c && pdus[i][1] == "NGAP-ELEMENTARY-PROCEDURE" && pdus[i][2] == "::=" {
				if pdus[i+2][0] == "SUCCESSFUL" {
					if pdus[i+3][0] == "UNSUCCESSFUL" {
						stric += "\tInitiatingMessagePresent" + pdus[i+1][2] + "\n"
						strsc += "\tSuccessfulOutcomePresent" + pdus[i+2][2] + "\n"
						struc += "\tUnsuccessfulOutcomePresent" + pdus[i+3][2] + "\n"
						stris += "\t" + pdus[i+1][2] + " *" + pdus[i+1][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+4][2], proCode) + "\"`\n"
						strss += "\t" + pdus[i+2][2] + " *" + pdus[i+2][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+4][2], proCode) + "\"`\n"
						strus += "\t" + pdus[i+3][2] + " *" + pdus[i+3][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+4][2], proCode) + "\"`\n"
					} else {
						stric += "\tInitiatingMessagePresent" + pdus[i+1][2] + "\n"
						strsc += "\tSuccessfulOutcomePresent" + pdus[i+2][2] + "\n"
						stris += "\t" + pdus[i+1][2] + " *" + pdus[i+1][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+3][2], proCode) + "\"`\n"
						strss += "\t" + pdus[i+2][2] + " *" + pdus[i+2][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+3][2], proCode) + "\"`\n"
					}
				} else {
					stric += "\tInitiatingMessagePresent" + pdus[i+1][2] + "\n"
					stris += "\t" + pdus[i+1][2] + " *" + pdus[i+1][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+2][2], proCode) + "\"`\n"
				}
			}
		}
	}

	for _, c := range class2 {
		for i, _ := range pdus {
			if len(pdus[i]) > 0 && pdus[i][0] == c && pdus[i][1] == "NGAP-ELEMENTARY-PROCEDURE" && pdus[i][2] == "::=" {
				stric += "\tInitiatingMessagePresent" + pdus[i+1][2] + "\n"
				stris += "\t" + pdus[i+1][2] + " *" + pdus[i+1][2] + " `aper:\"valueExt,referenceFieldValue:" + DescriptionsGetProCodeID(pdus[i+2][2], proCode) + "\"`\n"
			}
		}
	}

	stric += ")\n"
	stris += "}\n"
	strsc += ")\n"
	strss += "}\n"
	struc += ")\n"
	strus += "}\n"

	str += stric + stris + strsc + strss + struc + strus
	filePointer, _ := os.Create("C:\\Users\\HD\\GoPath\\src\\ngap\\ngapAsn1\\pdu_descriptions.go")
	_, _ = filePointer.WriteString(str)
}

func DescriptionsGetProCode() (out []string) {
	data, _ := ioutil.ReadFile("C:\\Users\\HD\\GoPath\\src\\ngap\\ngapAsn1\\NGAP-Constants")
	datas := strings.Split(string(data), "\n")
	for i := 33; i < 99; i++ {
		datai := strings.Fields(datas[i])
		out = append(out, datai[0])
	}
	return
}

func DescriptionsGetProCodeID(in string, proCode []string) (out string) {
	for i, p := range proCode {
		if in == p {
			return fmt.Sprintf("%d", i)
		}
	}
	return "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"
}

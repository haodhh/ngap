package main

import (
	"ngap/ngapCodec/ngapGenerate/ngapType"
)

func main() {
	ngapType.NGAPPDUDescriptions()
	ngapType.NGAPPDUContents()
	ngapType.NGAPIEs()
}

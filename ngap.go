package ngap

import (
	"encoding/json"

	"ngap/ngapCodec"
	"ngap/ngapType"
)

func Decode(b []byte) (pdu *ngapType.NGAPPDU, err error) {
	pdu = new(ngapType.NGAPPDU)
	err = ngapCodec.UnmarshalWithParams(b, pdu, "valueExt,valueLB:0,valueUB:2")
	return
}

func Encode(pdu ngapType.NGAPPDU) ([]byte, error) {
	return ngapCodec.MarshalWithParams(pdu, "valueExt,valueLB:0,valueUB:2")
}

func PrettyPrint(i interface{}) (string, error) {
	if s, e := json.MarshalIndent(i, "", "\t"); e != nil {
		return "", e
	} else {
		return string(s), nil
	}
}
